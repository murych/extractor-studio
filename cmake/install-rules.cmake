install(
    TARGETS extractor-studio_exe
    RUNTIME COMPONENT extractor-studio_Runtime
)

if(PROJECT_IS_TOP_LEVEL)
  include(CPack)
endif()
