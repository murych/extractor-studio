include(FetchContent)
set(CMAKE_TLS_VERIFY true)

FetchContent_Declare(
  TabToolbar
  GIT_REPOSITORY https://gitlab.com/syntgroup/libs/TabToolbar.git
  GIT_TAG 5c0c0ec28ddfa8f5c6b870be88000ae703583501)

FetchContent_Declare(
  extractor-core
  GIT_REPOSITORY https://gitlab.com/syntgroup/libs/extractor-core.git
  GIT_TAG 6799c008c93235ad6304f613916e16e1214eebad)

FetchContent_MakeAvailable(TabToolbar extractor-core)
