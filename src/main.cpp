#include <QApplication>

#include "logger.h"
#include "mainwindow.h"
#include "version.h"
#include <QDateTime>
#include <QLoggingCategory>

Q_LOGGING_CATEGORY(LOG_APP, "MAIN")

int
main(int argc, char* argv[])
{
  QApplication::setOrganizationName(COMPANY_NAME);
  QApplication::setOrganizationDomain(COMPANY_DOMAIN);
  QApplication::setApplicationName(APPLICATION_NAME);
  QApplication::setApplicationVersion(PROJECT_VER);
  QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
  QApplication::setApplicationDisplayName(QApplication::applicationName());

  QApplication application{ argc, argv };

  qInstallMessageHandler(Logger::messageOutput);

  qCInfo(LOG_APP).noquote()
    << APPLICATION_NAME << "version" << PROJECT_VER << "commit"
    << GIT_COMMIT_HASH
    << QDateTime::fromSecsSinceEpoch(QString(GIT_COMMIT_TIMESTAMP).toInt())
         .toString(Qt::ISODate);

  qCInfo(LOG_APP).noquote()
    << "OS info:" << QSysInfo::prettyProductName() << QSysInfo::productVersion()
    << QSysInfo::kernelVersion();

  auto window{ std::make_unique<MainWindow>() };
  window->show();

  const auto ret_code{ QApplication::exec() };

  qCInfo(LOG_APP) << "app exited with code" << ret_code;

  return ret_code;
}
