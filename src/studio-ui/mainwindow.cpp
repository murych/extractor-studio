#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "config/config_app.h"
#include "config/config_lab.h"
#include "dialogs/about_dialog.h"
#include "gcode/gcode_builder.h"
#include "models/project/extractor_project.h"
#include "models/project/laboratory.h"
#include "models/project/layout.h"
#include "models/project/project_info.h"
#include "models/project/protocol.h"
#include "models/steps/step.h"
#include "settings/settings_dialog.h"
#include "wizard/page_device_info.h"
#include "wizard/page_project_info.h"
#include "wizard/project_wizard.h"
#include "json/datetime_decorator.h"
#include "json/int_decorator.h"
#include "json/string_decorator.h"
#include <QApplication>
#include <QCborMap>
#include <QCborValue>
#include <QCloseEvent>
#include <QFileDialog>
#include <QJsonDocument>
#include <QLoggingCategory>
#include <QMessageBox>
#include <QSaveFile>
#include <QSettings>
#include <tab-toolbar/Builder.h>
#include <tab-toolbar/Group.h>
#include <tab-toolbar/Page.h>
#include <tab-toolbar/StyleTools.h>
#include <tab-toolbar/SubGroup.h>
#include <tab-toolbar/TabToolbar.h>

Q_LOGGING_CATEGORY(MAIN, "WINDOW")

namespace {
const auto FILE_BIN{ QStringLiteral(".extProj") };
const auto FILE_JSON{ QStringLiteral(".json") };
const auto FILE_FILTER_BIN{
  QObject::tr("Extractor Project File (*%1)").arg(FILE_BIN)
};
const auto FILE_FILTER_JSON{ QObject::tr("JSON File (*%1)").arg(FILE_JSON) };
const QStringList PROJECT_FILTERS{ FILE_FILTER_JSON, FILE_FILTER_BIN };

enum ProjectFormat
{
  Binary = 0,
  Json
};

enum Pages
{
  Home = 0,
  Project,
  RunInfo,
  Report,
};

enum Tabs
{
  TFile,
  TLayout,
  TProtocol,
  TReport,
  THelp
};

//! @todo вынести в конфигурацию какую-нибудь супер внутреннюю
constexpr auto SPEC_MAJ_VER{ 0 };
constexpr auto SPEC_MIN_VER{ 1 };

const auto TAB_NAME_HOME{ QObject::tr("Home") };
const auto TAB_NAME_FILE{ QObject::tr("File") };
const auto TAB_NAME_LAYOUT{ QObject::tr("  Layout  ") };
const auto TAB_NAME_PROTOCOL{ QObject::tr("  Protocol  ") };
const auto TAB_NAME_REPORT{ QObject::tr("  Report  ") };
const auto TAB_NAME_HELP{ QObject::tr("  Help  ") };

const auto GROUP_NAME_PROJECT{ QObject::tr("Project") };
const auto GROUP_NAME_SETTINGS{ QObject::tr("Setings") };
const auto GROUP_NAME_PLATES{ QObject::tr("Add Plate") };
const auto GROUP_NAME_STEPS{ QObject::tr("Add Step") };
const auto GROUP_NAME_PROTOCOL_EXPORT{ QObject::tr("Export") };
}

MainWindow::MainWindow(QWidget* parent)
  : QMainWindow{ parent }
  , m_ui{ std::make_unique<Ui::MainWindow>() }
  , m_settingsDialog{ std::make_unique<settings::SettingsDialog>() }
  , m_currentWorkDir{ QDir::homePath() }
  , m_toolbar{ std::make_unique<tt::TabToolbar>(this, 75, 2) }
{
  setupResources();
  m_ui->setupUi(this);

  readSettings();

  m_ui->stackedWidget->setCurrentIndex(Pages::Home);

  onProjectClosed();

  m_ui->actionAboutQt->setIcon(
    style()->standardIcon(QStyle::SP_TitleBarMenuButton));

  m_ui->actionSaveProject->setEnabled(false);
  m_ui->actionSaveProjectAs->setEnabled(false);

  connectFileActions();
  connectEditActions();
  connectAboutActions();

  addToolBar(Qt::TopToolBarArea, m_toolbar.get());
  m_toolbar->SetStyle("Vienna");
  setupToolbar();
}

MainWindow::~MainWindow()
{
  qCDebug(MAIN) << "~MainWindow";
  writeSettings();
}

void
MainWindow::setupResources()
{
  Q_INIT_RESOURCE(color);
  Q_INIT_RESOURCE(StyleTemplate);
}

void
MainWindow::projectCreate()
{
  qCDebug(MAIN) << "MainWindow::newFile";
  if (!maybeSave()) {
    return;
  }

  emit clearEditors();

  wizard::ProjectWizard wizard{ this };
  if (wizard.exec() == QDialog::Rejected) {
    return;
  }

  QApplication::setOverrideCursor(Qt::WaitCursor);

  models::project::ExtractorProject project;

  project.setSpecVersion(SPEC_MAJ_VER, SPEC_MIN_VER);

  project.info->title->setValue(
    wizard.field(wizard::fields::PROJECT_TITLE).toString());
  project.info->author->setValue(
    wizard.field(wizard::fields::PROJECT_AUTHOR).toString());
  project.info->revision->setValue(
    wizard.field(wizard::fields::PROJECT_REVISION).toInt());

  // осторожно: грязный ХАК
  // пока не придумано, как корректно передавать индексы из комбобокса с типом
  // устройства (а там элементов на один меньше, чем в enum'ах, так как
  // фильтруем unknown), добавляем единичьку
  project.info->deviceType->setValue(
    wizard.field(wizard::fields::DEVICE_TYPE).toInt() + 1);
  project.info->deviceFamily->setValue(
    wizard.field(wizard::fields::DEVICE_MODEL).toInt() + 1);

  const auto cdt{ QDateTime::currentDateTime().toLocalTime() };
  project.info->dateCreated->setValue(cdt);
  project.info->dateModified->setValue(cdt);

  qCDebug(MAIN) << "created new project" << &project;

  m_ui->project->setProject(project.toJson());

  onProjectOpened();

  QApplication::restoreOverrideCursor();
}

void
MainWindow::open()
{
  qCDebug(MAIN) << "open";

  if (!maybeSave()) {
    return;
  }

  QFileDialog dialog{ this };
  dialog.setNameFilters(PROJECT_FILTERS);
  dialog.setDirectory(m_currentWorkDir);
  dialog.setWindowModality(Qt::WindowModal);
  dialog.setAcceptMode(QFileDialog::AcceptOpen);
  dialog.setFileMode(QFileDialog::ExistingFile);
  if (dialog.exec() != QDialog::Accepted) {
    return;
  }

  const auto file_name{ dialog.selectedFiles().constFirst() };
  const QFileInfo file_info{ file_name };

  QFile file{ file_name };
  if (!file.open(QFile::ReadOnly)) {
    QMessageBox::warning(
      this,
      QApplication::applicationName(),
      tr("Cannot open project %1:\n%2")
        .arg(QDir::toNativeSeparators(file_name), file.errorString()));
    return;
  }

  const auto data{ file.readAll() };
  const auto is_text{ (
    dialog.selectedNameFilter() == FILE_FILTER_JSON ? Json : Binary) };
  const auto json_document{
    is_text == Json
      ? QJsonDocument::fromJson(data)
      : QJsonDocument(QCborValue::fromCbor(data).toMap().toJsonObject())
  };
  const auto json{ json_document.object() };
  m_ui->project->setProject(json);

  m_currentWorkDir = file_info.absolutePath();
  writeSettings();

  onProjectOpened();
  statusBar()->showMessage(tr("Project opened"), 2000);
}

void
MainWindow::close()
{
  if (!maybeSave()) {
    return;
  }

  onProjectClosed();

  QApplication::exit(0);
}

void
MainWindow::setCurrentFile(const QString& file_name)
{
  m_curFile = file_name;
  setDirty(false);

  setWindowFilePath(m_curFile.isEmpty() ? QStringLiteral("untitled project")
                                        : m_curFile);
}

void
MainWindow::readSettings()
{
  config::ApplicationConfig config;
  config.readSettings();
  this->move(config.windowPos());
  this->resize(config.windowSize());
  this->m_currentWorkDir = config.workDir();
}

void
MainWindow::writeSettings()
{
  config::ApplicationConfig config;
  config.setWindowPos(this->pos());
  config.setWindowSize(this->size());
  config.setWorkDir(this->m_currentWorkDir);
  config.writeSettings();
}

bool
MainWindow::maybeSave()
{
  if (!m_projectModified) {
    return true;
  }

  const QMessageBox::StandardButton ret = QMessageBox::warning(
    this,
    QApplication::applicationName(),
    tr("The document has been modified.\n"
       "Do you want to save your changes?"),
    QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);

  switch (ret) {
    case QMessageBox::Save:
      return save();
    case QMessageBox::Cancel:
      return false;
    default:
      break;
  }

  return true;
}

bool
MainWindow::save()
{
  if (m_curFile.isEmpty()) {
    return saveAs();
  }

  return projectSave(m_curFile);
}

bool
MainWindow::saveAs()
{
  QFileDialog dialog{ this };
  dialog.setNameFilters(PROJECT_FILTERS);
  dialog.setDirectory(m_currentWorkDir);
  dialog.setWindowModality(Qt::WindowModal);
  dialog.setAcceptMode(QFileDialog::AcceptSave);
  dialog.setFileMode(QFileDialog::AnyFile);
  if (dialog.exec() != QDialog::Accepted) {
    return false;
  }

  const QFileInfo info{ dialog.selectedFiles().constFirst() };
  const auto has_extension{ !info.suffix().isEmpty() };
  const auto binary{ dialog.selectedNameFilter() == FILE_FILTER_BIN };

  m_currentWorkDir = info.absolutePath();
  writeSettings();

  const auto file_path{ info.absoluteFilePath() +
                        (has_extension ? QLatin1String()
                                       : (binary ? FILE_BIN : FILE_JSON)) };
  return projectSave(file_path);
}

bool
MainWindow::projectSave(const QString& file_path)
{
  qCDebug(MAIN) << "saving project to" << file_path;
  QString error_message;
  QApplication::setOverrideCursor(Qt::WaitCursor);

  config::LaboratoryConfig config;
  config.readSettings();

  models::project::ExtractorProject project;
  project.laboratory->setName(config.labName());
  project.laboratory->setAddress(config.labAddress());
  project.laboratory->setEmail(config.labEmail());
  project.laboratory->setPhone(config.labPhone());

  const auto json{ m_ui->project->getProject() };
  //  qCDebug(MAIN) << json;

  project.info->update(json.value(project.info->key()).toObject());
  project.layout->update(json.value(project.layout->key()).toObject());
  project.protocol->update(json.value(project.protocol->key()).toObject());

  const QFileInfo info{ file_path };
  QSaveFile file{ file_path };

  if (file.open(QFile::WriteOnly)) {
    file.write(info.suffix() == FILE_BIN
                 ? QCborValue::fromJsonValue(project.toJson()).toCbor()
                 : QJsonDocument(project.toJson()).toJson());

    if (!file.commit()) {
      error_message =
        tr("Cannot write file %1:\n%2.")
          .arg(QDir::toNativeSeparators(file_path), file.errorString());
    }
  } else {
    error_message =
      tr("Cannot open file %1 for writing:\n%2.")
        .arg(QDir::toNativeSeparators(file_path), file.errorString());
  }
  QApplication::restoreOverrideCursor();

  if (!error_message.isEmpty()) {
    QMessageBox::warning(this, QApplication::applicationName(), error_message);
    return false;
  }

  m_projectModified = false;

  setCurrentFile(file_path);

  statusBar()->showMessage(tr("Project saved"), 2000);

  return true;
}

void
MainWindow::enableEditActions(const bool status)
{
  m_ui->actionSaveProject->setEnabled(status);
  m_ui->actionSaveProjectAs->setEnabled(status);

  m_ui->actionAddStandard96plate->setEnabled(status);

  m_ui->actionAddCollectStep->setEnabled(status);
  m_ui->actionAddDryStep->setEnabled(status);
  m_ui->actionAddHeaterControlStep->setEnabled(status);
  m_ui->actionAddLeaveStep->setEnabled(status);
  m_ui->actionAddManualMoveStep->setEnabled(status);
  m_ui->actionAddMixStep->setEnabled(status);
  m_ui->actionAddPickUpStep->setEnabled(status);
  m_ui->actionAddReleaseStep->setEnabled(status);
  m_ui->actionAddSleepStep->setEnabled(status);
  m_ui->actionAddWaitStep->setEnabled(status);
  m_ui->actionExportProtocol->setEnabled(status);

  m_ui->actionReportExport->setEnabled(status);
  m_ui->actionReportPrint->setEnabled(status);
  m_ui->actionReportSaveAsCSV->setEnabled(status);
  m_ui->actionReportSaveAsExcel->setEnabled(status);
  m_ui->actionReportSaveAsPDF->setEnabled(status);
  m_ui->actionReportSaveAsText->setEnabled(status);
}

void
MainWindow::connectEditActions()
{
  m_addStepActions.insert(
    { { models::steps::Step::EStepType::Pickup, m_ui->actionAddPickUpStep },
      { models::steps::Step::EStepType::Leave, m_ui->actionAddLeaveStep },
      { models::steps::Step::EStepType::Mix, m_ui->actionAddMixStep },
      { models::steps::Step::EStepType::Dry, m_ui->actionAddDryStep },
      { models::steps::Step::EStepType::Sleep, m_ui->actionAddSleepStep },
      { models::steps::Step::EStepType::Move, m_ui->actionAddManualMoveStep },
      { models::steps::Step::EStepType::Collect, m_ui->actionAddCollectStep },
      { models::steps::Step::EStepType::Release, m_ui->actionAddReleaseStep },
      { models::steps::Step::EStepType::Heater,
        m_ui->actionAddHeaterControlStep },
      { models::steps::Step::EStepType::Pause, m_ui->actionAddWaitStep } });
  m_addPlateActions.insert({ { 0, m_ui->actionAddStandard96plate } });

  for (int type = step_type::Pickup; type < step_type::User; ++type) {
    connect(m_addStepActions.at(type), &QAction::triggered, this, [this, type] {
      qCInfo(MAIN)
        << "creating step"
        << QVariant::fromValue(static_cast<step_type>(type)).toString();
      m_ui->project->addStep(type);
    });
  }

  for (int plate_type = 0; plate_type < 1; ++plate_type) {
    connect(m_addPlateActions.at(plate_type),
            &QAction::triggered,
            this,
            [this, plate_type] { m_ui->project->addPlate(plate_type); });
  }

  connect(m_ui->actionExportProtocol,
          &QAction::triggered,
          this,
          &MainWindow ::exportProtocol);
}

void
MainWindow::connectFileActions()
{
  connect(m_ui->actionNewProject,
          &QAction::triggered,
          this,
          &MainWindow::projectCreate);
  connect(
    m_ui->actionSaveProject, &QAction::triggered, this, &MainWindow::save);
  connect(
    m_ui->actionSaveProjectAs, &QAction::triggered, this, &MainWindow::saveAs);
  connect(
    m_ui->actionOpenProject, &QAction::triggered, this, &MainWindow::open);
  connect(m_ui->actionSettings,
          &QAction::triggered,
          m_settingsDialog.get(),
          &settings::SettingsDialog::exec);
}

void
MainWindow::connectAboutActions()
{
  connect(m_ui->actionHelp, &QAction::triggered, this, [this] {
    QMessageBox::information(this,
                             QApplication::applicationName(),
                             tr("Application help will be here"));
  });

  connect(m_ui->actionAbout, &QAction::triggered, this, [this] {
    dialogs::AboutDialog dialog{ this };
    dialog.exec();
  });

  connect(m_ui->actionAboutQt, &QAction::triggered, this, [this] {
    QApplication::aboutQt();
  });
}

void
MainWindow::setupToolbar()
{
  auto* file_page{ m_toolbar->AddPage(TAB_NAME_FILE) };
  auto* file_group_project{ file_page->AddGroup(GROUP_NAME_PROJECT) };
  file_group_project->AddAction(QToolButton::DelayedPopup,
                                m_ui->actionNewProject);
  file_group_project->AddAction(QToolButton::DelayedPopup,
                                m_ui->actionOpenProject);
  file_group_project->AddAction(QToolButton::DelayedPopup,
                                m_ui->actionSaveProject);
  file_group_project->AddAction(QToolButton::DelayedPopup,
                                m_ui->actionSaveProjectAs);

  auto* file_group_settings{ file_page->AddGroup(
    m_ui->actionSettings->text()) };
  file_group_settings->AddAction(QToolButton::DelayedPopup,
                                 m_ui->actionSettings);

#ifdef device_ready
  tt::Group* fileGroupDevice{ file_page->AddGroup(tr("Device")) };
  fileGroupDevice->AddAction(QToolButton::DelayedPopup, m_ui->actionConnect);
  fileGroupDevice->AddAction(QToolButton::DelayedPopup, m_ui->actionDisconnect);
#endif // device_redy

  tt::Page* layout_page{ m_toolbar->AddPage(TAB_NAME_LAYOUT) };
  tt::Group* layout_group_add{ layout_page->AddGroup(GROUP_NAME_PLATES) };
  tt::SubGroup* g1s{ layout_group_add->AddSubGroup(tt::SubGroup::Align::Yes) };
  g1s->AddAction(QToolButton::DelayedPopup, m_ui->actionAddStandard96plate);

  tt::Page* protocol_page{ m_toolbar->AddPage(TAB_NAME_PROTOCOL) };
  tt::Group* protocol_group_add{ protocol_page->AddGroup(GROUP_NAME_STEPS) };
  tt::SubGroup* g2s{ protocol_group_add->AddSubGroup(
    tt::SubGroup::Align::Yes) };
  g2s->AddAction(QToolButton::DelayedPopup, m_ui->actionAddPickUpStep);
  g2s->AddAction(QToolButton::DelayedPopup, m_ui->actionAddLeaveStep);
  tt::SubGroup* g3s{ protocol_group_add->AddSubGroup(
    tt::SubGroup::Align::Yes) };
  g3s->AddAction(QToolButton::DelayedPopup, m_ui->actionAddMixStep);
  g3s->AddAction(QToolButton::DelayedPopup, m_ui->actionAddDryStep);
  tt::SubGroup* g4s{ protocol_group_add->AddSubGroup(
    tt::SubGroup::Align::Yes) };
  g4s->AddAction(QToolButton::DelayedPopup, m_ui->actionAddCollectStep);
  g4s->AddAction(QToolButton::DelayedPopup, m_ui->actionAddReleaseStep);
  tt::SubGroup* g5s{ protocol_group_add->AddSubGroup(
    tt::SubGroup::Align::Yes) };
  g5s->AddAction(QToolButton::DelayedPopup, m_ui->actionAddHeaterControlStep);
  g5s->AddAction(QToolButton::DelayedPopup, m_ui->actionAddManualMoveStep);
  auto* g6s{ protocol_group_add->AddSubGroup(tt::SubGroup::Align::Yes) };
  g6s->AddAction(QToolButton::DelayedPopup, m_ui->actionAddWaitStep);
  g6s->AddAction(QToolButton::DelayedPopup, m_ui->actionAddSleepStep);
  tt::Group* protocol_group_export{ protocol_page->AddGroup(
    GROUP_NAME_PROTOCOL_EXPORT) };
  protocol_group_export->AddAction(QToolButton::DelayedPopup,
                                   m_ui->actionCheckErrors);
  protocol_group_export->AddAction(QToolButton::DelayedPopup,
                                   m_ui->actionExportProtocol);

#ifdef reports_ready
  tt::Page* reportPage{ m_toolbar->AddPage(tr("Report")) };
  m_menuReportExport.reset(new QMenu{ this });
  m_menuReportExport->addActions({ m_ui->actionReportSaveAsPDF,
                                   m_ui->actionReportSaveAsText,
                                   m_ui->actionReportSaveAsCSV,
                                   m_ui->actionReportSaveAsExcel });
  tt::Group* reportGroupFuncitons{ reportPage->AddGroup(tr("Function")) };
  reportGroupFuncitons->AddAction(QToolButton::DelayedPopup,
                                  m_ui->actionReportPrint);
  reportGroupFuncitons->AddAction(QToolButton::InstantPopup,
                                  m_ui->actionReportExport,
                                  m_menuReportExport.get());
#endif // reports_ready

  tt::Page* help_page{ m_toolbar->AddPage(TAB_NAME_HELP) };
  tt::Group* help_category_help{ help_page->AddGroup(TAB_NAME_HELP) };
  help_category_help->AddAction(QToolButton::DelayedPopup, m_ui->actionHelp);
  help_category_help->AddSeparator();
  help_category_help->AddAction(QToolButton::DelayedPopup, m_ui->actionAbout);
  help_category_help->AddAction(QToolButton::DelayedPopup, m_ui->actionAboutQt);

  //  toolbar->SetSpecialTabEnabled(true);
  m_toolbar->AddCornerAction(m_ui->actionHelp);

  //  connect(toolbar.get(), &tt::TabToolbar::SpecialTabClicked, this, [this] {
  //    QMessageBox::information(this, QApplication::applicationName(),
  //    "heelk");
  //  });
}

void
MainWindow::onProjectOpened()
{
  enableEditActions();

  m_projectModified = true;
  setDirty();

  m_ui->stackedWidget->setCurrentIndex(Project);
  m_toolbar->SetCurrentTab(TLayout);

  emit projectOpened();
}

void
MainWindow::onProjectClosed()
{
  enableEditActions(false);

  m_projectModified = false;
  setDirty(m_projectModified);

  m_ui->stackedWidget->setCurrentIndex(Home);

  emit projectClosed();
}

void
MainWindow::exportProtocol()
{
  qCDebug(MAIN) << "Export protocol";

  QFileDialog dialog{ this };
  dialog.setNameFilter("*.gcode");
  dialog.setDirectory(m_currentWorkDir);
  dialog.setWindowModality(Qt::WindowModal);
  dialog.setAcceptMode(QFileDialog::AcceptSave);
  dialog.setFileMode(QFileDialog::AnyFile);
  if (dialog.exec() != QDialog::Accepted) {
    QMessageBox::warning(this,
                         QApplication::applicationName(),
                         tr("file saving cancelled by user"));
    return;
  }

  QApplication::setOverrideCursor(Qt::WaitCursor);
  const QFileInfo info{ dialog.selectedFiles().constFirst() };
  const auto has_extension{ !info.suffix().isEmpty() };

  m_currentWorkDir = info.absolutePath();
  writeSettings();

  models::project::ExtractorProject project;
  const auto json{ m_ui->project->getProject() };

  project.info->update(json.value(project.info->key()).toObject());
  project.layout->update(json.value(project.layout->key()).toObject());
  project.protocol->update(json.value(project.protocol->key()).toObject());

  gcode::GCodeBuilder builder{ project };
  const auto result{ builder.build() };

  if (!result.has_value()) {
    QMessageBox::critical(
      this,
      QApplication::applicationName(),
      tr("failed to create gcode from described project, check for errors."),
      QMessageBox::Ok);
    return;
  }

  const auto file_path{ info.absoluteFilePath() +
                        (has_extension ? QLatin1String()
                                       : QStringLiteral(".gcode")) };
  QSaveFile file{ file_path };
  QString error_message;

  if (file.open(QFile::WriteOnly)) {
    file.write(result.value().toUtf8());
    if (!file.commit()) {
      error_message =
        tr("Cannot write file %1:\n%2.")
          .arg(QDir::toNativeSeparators(file_path), file.errorString());
    }
  } else {
    error_message =
      tr("Cannot open file %1 for writing:\n%2.")
        .arg(QDir::toNativeSeparators(file_path), file.errorString());
  }

  if (!error_message.isEmpty()) {
    QMessageBox::warning(this, QApplication::applicationName(), error_message);
    return;
  }
  QApplication::restoreOverrideCursor();
  statusBar()->showMessage(tr("Protocol exported"));
}

void
MainWindow::closeEvent(QCloseEvent* event)
{
  qCDebug(MAIN) << "MainWindow::closeEvent";
  if (maybeSave()) {
    writeSettings();
    event->accept();
  } else {
    event->ignore();
  }
}
