#include "page_project_info.h"
#include "ui_page_project_info.h"

namespace wizard {

PageProjectInfo::PageProjectInfo(QWidget* parent)
  : QWizardPage{ parent }
  , m_ui{ std::make_unique<Ui::PageProjectInfo>() }
{
  m_ui->setupUi(this);

  registerField(fields::PROJECT_TITLE + "*", m_ui->projectTitleLineEdit);
  registerField(fields::PROJECT_AUTHOR, m_ui->projectAuthorLineEdit);
  registerField(fields::PROJECT_REVISION, m_ui->projectRevisionSpinBox);
}

PageProjectInfo::~PageProjectInfo() = default;

} // namespace wizard
