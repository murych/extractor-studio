#ifndef WIZARD_PAGE_DEVICE_INFO_H
#define WIZARD_PAGE_DEVICE_INFO_H

#include <QSortFilterProxyModel>
#include <QWizardPage>

namespace Ui {
class PageDeviceInfo;
}

namespace models {
class DeviceTypeModel;
class DeviceFamilyModel;
} // namespace models

namespace wizard {

namespace fields {
const QString DEVICE_TYPE{ QStringLiteral("deviceType") };
const QString DEVICE_MODEL{ QStringLiteral("deviceModel") };
} // namespace fields

class PageDeviceInfo : public QWizardPage
{
  Q_OBJECT

public:
  explicit PageDeviceInfo(QWidget* parent = nullptr);
  ~PageDeviceInfo() override;

private:
  std::unique_ptr<Ui::PageDeviceInfo> m_ui{ nullptr };
  std::unique_ptr<models::DeviceTypeModel> m_deviceTypeModel{ nullptr };
  std::unique_ptr<models::DeviceFamilyModel> m_deviceFamilyModel{ nullptr };
  std::unique_ptr<QSortFilterProxyModel> m_typeProxyModel{ nullptr };
  std::unique_ptr<QSortFilterProxyModel> m_modelProxyModel{ nullptr };

  void deviceTypeSelected(const QString& value);
  void deviceModelSelected();
};

} // namespace wizard
#endif // WIZARD_PAGE_DEVICE_INFO_H
