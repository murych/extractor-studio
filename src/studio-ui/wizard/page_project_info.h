#ifndef WIZARD_PAGE_PROJECT_INFO_H
#define WIZARD_PAGE_PROJECT_INFO_H

#include <QWizardPage>

namespace Ui {
class PageProjectInfo;
}

namespace wizard {

namespace fields {
const QString PROJECT_TITLE{ QStringLiteral("projectTitle") };
const QString PROJECT_AUTHOR{ QStringLiteral("projectAuthor") };
const QString PROJECT_REVISION{ QStringLiteral("projectRevision") };
const QString PROJECT_DESCRIPTION{ QStringLiteral("projectDescription") };
}

class PageProjectInfo : public QWizardPage
{
  Q_OBJECT

public:
  explicit PageProjectInfo(QWidget* parent = nullptr);
  ~PageProjectInfo() override;

private:
  std::unique_ptr<Ui::PageProjectInfo> m_ui{ nullptr };
};

} // namespace wizard
#endif // WIZARD_PAGE_PROJECT_INFO_H
