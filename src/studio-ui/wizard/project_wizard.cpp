#include "project_wizard.h"
#include "ui_project_wizard.h"

#include <QLoggingCategory>
#include <QPlainTextEdit>

Q_LOGGING_CATEGORY(WIZARD, "wizard")

namespace wizard {

ProjectWizard::ProjectWizard(QWidget* parent)
  : QWizard{ parent }
  , m_ui{ std::make_unique<Ui::ProjectWizard>() }
{
  m_ui->setupUi(this);
  setDefaultProperty("QPlainTextEdit", "plainText", "textChanged()");
}

ProjectWizard::~ProjectWizard() = default;

} // namespace wizard
