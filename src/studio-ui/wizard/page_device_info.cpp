#include "page_device_info.h"
#include "ui_page_device_info.h"

#include "models/device/device_extractor.h"
#include "models/device_type_model.h"
#include "json/string_decorator.h"
#include <QDebug>
#include <QStringListModel>

namespace {
const QRegularExpression NOT_EMPTY{ QStringLiteral("^(?!\\s*$).+") };
}

namespace wizard {

PageDeviceInfo::PageDeviceInfo(QWidget* parent)
  : QWizardPage{ parent }
  , m_ui{ std::make_unique<Ui::PageDeviceInfo>() }
  , m_deviceTypeModel{ std::make_unique<models::DeviceTypeModel>() }
  , m_typeProxyModel{ std::make_unique<QSortFilterProxyModel>() }
{
  m_ui->setupUi(this);

  m_typeProxyModel->setSourceModel(m_deviceTypeModel.get());
  m_typeProxyModel->setFilterRegularExpression(NOT_EMPTY);

  m_ui->deviceTypeComboBox->setModel(m_typeProxyModel.get());

  registerField(fields::DEVICE_TYPE, m_ui->deviceTypeComboBox);
  registerField(fields::DEVICE_MODEL, m_ui->deviceModelComboBox);

  connect(m_ui->deviceTypeComboBox,
          QOverload<const QString&>::of(&QComboBox::textActivated),
          this,
          &PageDeviceInfo::deviceTypeSelected);
  deviceTypeSelected(m_ui->deviceTypeComboBox->currentText());
}

PageDeviceInfo::~PageDeviceInfo() = default;

void
PageDeviceInfo::deviceTypeSelected(const QString& value)
{
  if (value != models::device::GenericDevice::device_type_mapper.at(
                 device_type::Extractor)) {
    m_ui->deviceModelComboBox->setModel(new QStringListModel);
    m_ui->deviceModelComboBox->setEnabled(false);
    m_ui->textBrowser->clear();
    disconnect(m_ui->deviceModelComboBox, nullptr, nullptr, nullptr);
    return;
  }

  m_ui->deviceModelComboBox->setEnabled(true);

  m_deviceFamilyModel =
    std::make_unique<models::DeviceFamilyModel>(device_type::Extractor);
  m_modelProxyModel = std::make_unique<QSortFilterProxyModel>();

  m_modelProxyModel->setSourceModel(m_deviceFamilyModel.get());
  m_modelProxyModel->setFilterRegularExpression(NOT_EMPTY);

  m_ui->deviceModelComboBox->setModel(m_modelProxyModel.get());

  connect(m_ui->deviceModelComboBox,
          &QComboBox::textActivated,
          this,
          &PageDeviceInfo::deviceModelSelected);
  deviceModelSelected();
}

void PageDeviceInfo::deviceModelSelected(/*const QString& newType*/)
{
  const auto idx{ m_deviceFamilyModel->index(
    m_ui->deviceModelComboBox->currentIndex()) };

  const auto json{ m_deviceFamilyModel
                     ->data(idx, models::DeviceFamilyModel::JsonRole)
                     .toJsonObject() };

  const models::device::GenericDevice a_device{ json };
  m_ui->textBrowser->setText(a_device.description->value());
}

} // namespace wizard
