#ifndef PAGE_GENERAL_SETTINGS_H
#define PAGE_GENERAL_SETTINGS_H

#include <QWidget>

namespace Ui {
class PageGeneralSettings;
}

namespace settings {

class PageGeneral : public QWidget
{
  Q_OBJECT

public:
  explicit PageGeneral(QWidget* parent = nullptr);
  ~PageGeneral() override;

private:
  std::unique_ptr<Ui::PageGeneralSettings> m_ui{ nullptr };
};

} // namespace settings

#endif // PAGE_GENERAL_SETTINGS_H
