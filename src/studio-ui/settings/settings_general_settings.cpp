#include "settings_general_settings.h"
#include "ui_settings_general_settings.h"

namespace settings {

PageGeneral::PageGeneral(QWidget* parent)
  : QWidget{ parent }
  , m_ui{ std::make_unique<Ui::PageGeneralSettings>() }
{
  m_ui->setupUi(this);
}

PageGeneral::~PageGeneral() = default;

} // namespace settings
