#include "settings_dialog.h"
#include "ui_settings_dialog.h"

#include <QSettings>

namespace settings {

SettingsDialog::SettingsDialog(QWidget* parent)
  : QDialog{ parent }
  , m_ui{ std::make_unique<Ui::SettingsDialog>() }
{
  m_ui->setupUi(this);

  connect(this, &QDialog::accepted, m_ui->pageLab, &PageLabInfo::saveSettings);
}

SettingsDialog::~SettingsDialog() = default;

} // namespace settings
