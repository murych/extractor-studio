#ifndef PAGE_DEVICE_INFO_H
#define PAGE_DEVICE_INFO_H

#include <QWidget>

namespace Ui {
class SettingsPageDeviceInfo;
}

namespace settings {

class PageDevice : public QWidget
{
  Q_OBJECT

public:
  explicit PageDevice(QWidget* parent = nullptr);
  ~PageDevice() override;

private:
  std::unique_ptr<Ui::SettingsPageDeviceInfo> m_ui{ nullptr };
};

} // namespace settings

#endif // PAGE_DEVICE_INFO_H
