#include "settings_lab_info.h"
#include "ui_settings_lab_info.h"

#include "config/config_lab.h"

namespace settings {

PageLabInfo::PageLabInfo(QWidget* parent)
  : QWidget{ parent }
  , m_ui{ std::make_unique<Ui::PageLabInfo>() }
  , m_settings{ std::make_unique<config::LaboratoryConfig>() }
{
  m_ui->setupUi(this);
  m_settings->readSettings();

  m_ui->laboratoryNameLineEdit->setText(m_settings->labName());
  m_ui->laboratoryAddressLineEdit->setText(m_settings->labAddress());
  m_ui->laboratoryEmailLineEdit->setText(m_settings->labEmail());
  m_ui->laboratoryPhoneLineEdit->setText(m_settings->labPhone());
}

PageLabInfo::~PageLabInfo() = default;

void
PageLabInfo::saveSettings()
{
  m_settings->setLabName(m_ui->laboratoryNameLineEdit->text());
  m_settings->setLabAddress(m_ui->laboratoryAddressLineEdit->text());
  m_settings->setLabEmail(m_ui->laboratoryEmailLineEdit->text());
  m_settings->setLabPhone(m_ui->laboratoryPhoneLineEdit->text());

  m_settings->writeSettings();
}

} // namespace settings
