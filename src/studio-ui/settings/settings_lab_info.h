#ifndef PAGE_LAB_INFO_H
#define PAGE_LAB_INFO_H

#include <QWidget>

namespace Ui {
class PageLabInfo;
}

namespace config {
class LaboratoryConfig;
}

namespace settings {

class PageLabInfo : public QWidget
{
  Q_OBJECT

public:
  explicit PageLabInfo(QWidget* parent = nullptr);
  ~PageLabInfo() override;

  void saveSettings();

private:
  std::unique_ptr<Ui::PageLabInfo> m_ui{ nullptr };
  std::unique_ptr<config::LaboratoryConfig> m_settings{ nullptr };
};

} // namespace settings

#endif // PAGE_LAB_INFO_H
