#include "settings_device_info.h"
#include "ui_settings_device_info.h"

namespace settings {

PageDevice::PageDevice(QWidget* parent)
  : QWidget{ parent }
  , m_ui{ std::make_unique<Ui::SettingsPageDeviceInfo>() }
{
  m_ui->setupUi(this);
}

PageDevice::~PageDevice() = default;

} // namespace settings
