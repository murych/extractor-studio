#ifndef WIDGETS_DELEGATECOMBOBOXREAGENTTYPE_H
#define WIDGETS_DELEGATECOMBOBOXREAGENTTYPE_H

#include <QStyledItemDelegate>

namespace widgets {

class ReagentDelegate : public QStyledItemDelegate
{
  Q_OBJECT
public:
  using QStyledItemDelegate::QStyledItemDelegate;
  ~ReagentDelegate() override = default;

  // QAbstractItemDelegate interface

  void paint(QPainter* painter,
             const QStyleOptionViewItem& option,
             const QModelIndex& index) const override;
  QWidget* createEditor(QWidget* parent,
                        const QStyleOptionViewItem& option,
                        const QModelIndex& index) const override;
  void setEditorData(QWidget* editor, const QModelIndex& index) const override;
  void setModelData(QWidget* editor,
                    QAbstractItemModel* model,
                    const QModelIndex& index) const override;
  void updateEditorGeometry(QWidget* editor,
                            const QStyleOptionViewItem& option,
                            const QModelIndex& index) const override;
};

} // namespace widgets

#endif // WIDGETS_DELEGATECOMBOBOXREAGENTTYPE_H
