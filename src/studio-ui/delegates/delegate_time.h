#ifndef MODELS_TIMEDELEGATE_H
#define MODELS_TIMEDELEGATE_H

#include <QItemDelegate>

namespace widgets {

class TimeDelegate : public QItemDelegate
{
  Q_OBJECT
public:
  explicit TimeDelegate(QObject* parent = nullptr);
  ~TimeDelegate();

  // QAbstractItemDelegate interface
public:
  QWidget* createEditor(QWidget* parent,
                        const QStyleOptionViewItem& option,
                        const QModelIndex& index) const override;
  void setEditorData(QWidget* editor, const QModelIndex& index) const override;
  void setModelData(QWidget* editor,
                    QAbstractItemModel* model,
                    const QModelIndex& index) const override;
  void updateEditorGeometry(QWidget* editor,
                            const QStyleOptionViewItem& option,
                            const QModelIndex& index) const override;
};

} // namespace models

#endif // MODELS_TIMEDELEGATE_H
