#include "delegate_time.h"

#include <QLoggingCategory>
#include <QTimeEdit>
#include <studio-lib/logger.h>

Q_LOGGING_CATEGORY(TD, "TIME D")

namespace widgets {

TimeDelegate::TimeDelegate(QObject* parent)
  : QItemDelegate{ parent }
{}

TimeDelegate::~TimeDelegate() {}

QWidget*
TimeDelegate::createEditor(QWidget* parent,
                           const QStyleOptionViewItem& option,
                           const QModelIndex& index) const
{
  QTimeEdit* editor{ new QTimeEdit{ parent } };
  editor->setCurrentSection(QTimeEdit::SecondSection);
  editor->setDisplayFormat("HH:mm:ss");
  return editor;
}

void
TimeDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
  qCDebug(TD) << "TimeDelegate::setEditorData" << index
              << index.model()->data(index, Qt::EditRole);
  QTimeEdit* time_edit{ static_cast<QTimeEdit*>(editor) };
  //  QTime value{ 0, 0, 0 };
  //  value = value.addSecs(index.model()->data(index, Qt::EditRole).toUInt());
  QTime value{ index.model()->data(index, Qt::EditRole).toTime() };
  time_edit->setTime(value);
}

void
TimeDelegate::setModelData(QWidget* editor,
                           QAbstractItemModel* model,
                           const QModelIndex& index) const
{
  QTimeEdit* time_edit{ static_cast<QTimeEdit*>(editor) };
  const QString value = time_edit->time().toString("HH:mm:ss");
  qCDebug(TD) << "TimeDelegate::setModelData" << value;
  model->setData(index, value, Qt::EditRole);
}

void
TimeDelegate::updateEditorGeometry(QWidget* editor,
                                   const QStyleOptionViewItem& option,
                                   const QModelIndex& index) const
{
  editor->setGeometry(option.rect);
}

} // namespace models
