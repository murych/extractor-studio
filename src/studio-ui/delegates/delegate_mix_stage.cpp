#include "delegate_mix_stage.h"

#include "models/mix_pattern_tablemodel.h"
#include "models/moving_pos_model.h"
#include "models/step_speed_model.h"
#include "models/steps/moving_position.h"
#include <QComboBox>
#include <QPainter>
#include <QTimeEdit>

#include "logger.h"
#include <QLoggingCategory>
Q_LOGGING_CATEGORY(MD, "MIX DELEGATE")

using namespace models;

namespace widgets {

void
MixStageDelegate::paint(QPainter* painter,
                        const QStyleOptionViewItem& option,
                        const QModelIndex& index) const
{
  if (!index.isValid()) {
    return;
  }

  painter->save();

  QStyleOptionViewItem new_options{ option };
  new_options.rect.setLeft(option.rect.left() + 5);

  QString text;

  switch (static_cast<MixPatternTableModel::Cols>(index.column())) {
    case MixPatternTableModel::Speed:
    case MixPatternTableModel::PausePos: {
      text = index.model()->data(index).toString();
      break;
    }
    case MixPatternTableModel::Time:
    case MixPatternTableModel::PauseDur:
      text = index.model()->data(index).toTime().toString(
        QStringLiteral("HH:mm:ss"));
      break;
  }

  painter->drawText(new_options.rect, text, Qt::AlignVCenter | Qt::AlignLeft);
  painter->restore();
}

QWidget*
MixStageDelegate::createEditor(QWidget* parent,
                               const QStyleOptionViewItem& option,
                               const QModelIndex& index) const
{
  switch (index.column()) {
    case MixPatternTableModel::Speed: {
      auto* editor{ new QComboBox{ parent } };
      editor->setModel(new models::StepSpeedModel{ editor });
      Q_ASSERT(editor);
      const auto idx{ index.model()->data(index, Qt::EditRole).toInt() };
      editor->setCurrentIndex(idx);
      return editor;
    }
    case MixPatternTableModel::PausePos: {
      auto* editor{ new QComboBox{ parent } };
      Q_ASSERT(editor);
      const int idx{ index.model()->data(index, Qt::EditRole).toInt() };
      editor->setCurrentIndex(idx);
      return editor;
    }
    case MixPatternTableModel::Time:
    case MixPatternTableModel::PauseDur: {
      auto* editor{ new QTimeEdit{ parent } };
      Q_ASSERT(editor);
      editor->setDisplayFormat(QStringLiteral("HH:mm:ss"));
      editor->setCurrentSection(QDateTimeEdit::SecondSection);
      return editor;
    }
    default:
      break;
  }

  return QStyledItemDelegate::createEditor(parent, option, index);
}

void
MixStageDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
  switch (static_cast<MixPatternTableModel::Cols>(index.column())) {
    case MixPatternTableModel::PauseDur:
    case MixPatternTableModel::Time: {
      auto* time_edit{ dynamic_cast<QTimeEdit*>(editor) };
      Q_ASSERT(time_edit);
      time_edit->setTime(index.model()->data(index, Qt::EditRole).toTime());
      break;
    }
    case MixPatternTableModel::Speed: {
      auto* speed_edit{ dynamic_cast<QComboBox*>(editor) };
      Q_ASSERT(speed_edit);
      speed_edit->setCurrentIndex(
        index.model()->data(index, Qt::EditRole).toInt());
      break;
    }
    case MixPatternTableModel::PausePos: {
      auto* pos_edit{ dynamic_cast<QComboBox*>(editor) };
      Q_ASSERT(pos_edit);
      const auto pos{ models::steps::TRAVEL_POSITION_MAPPER.at(
        index.model()->data(index, Qt::EditRole).toInt()) };
      pos_edit->setCurrentIndex(pos_edit->findText(pos));
      break;
    }
    default:
      QStyledItemDelegate::setEditorData(editor, index);
      break;
  }
}

void
MixStageDelegate::setModelData(QWidget* editor,
                               QAbstractItemModel* model,
                               const QModelIndex& index) const
{
  switch (static_cast<MixPatternTableModel::Cols>(index.column())) {
    case MixPatternTableModel::Time:
    case MixPatternTableModel::PauseDur: {
      const auto* time_edit{ dynamic_cast<QTimeEdit*>(editor) };
      const auto time{ time_edit->time().toString("HH:mm:ss") };
      model->setData(index, time, Qt::EditRole);
      break;
    }
    case MixPatternTableModel::Speed: {
      const auto value{ dynamic_cast<QComboBox*>(editor)->currentIndex() };
      model->setData(index, value, Qt::EditRole);
      break;
    }
    case MixPatternTableModel::PausePos: {
      const auto* pos_edit{ dynamic_cast<QComboBox*>(editor) };
      Q_ASSERT(pos_edit);
      const auto row{ pos_edit->currentIndex() };
      const auto cur_idx{ pos_edit->model()->index(row, 0, {}) };
      const auto cur_data{ pos_edit->model()->data(cur_idx, Qt::EditRole) };
      model->setData(index, cur_data, Qt::EditRole);
      break;
    }
    default: {
      QStyledItemDelegate::setModelData(editor, model, index);
      break;
    }
  }
}

void
MixStageDelegate::updateEditorGeometry(QWidget* editor,
                                       const QStyleOptionViewItem& option,
                                       const QModelIndex& index) const
{
  Q_UNUSED(index);
  editor->setGeometry(option.rect);
}

} // namespace models
