#include "delegate_reagent.h"

#include "models/reagent_table_model.h"
#include "widgets/reagent_type_combobox.h"
#include "widgets/reagent_volume_spinbox.h"
#include <QPainter>
#include <widgets/kcolorbutton.h>

#include <QLoggingCategory>
#include <logger.h>
Q_LOGGING_CATEGORY(RD, "REAGENT DELEG")

using namespace models;
using namespace widgets;

namespace widgets {

void
ReagentDelegate::paint(QPainter* painter,
                       const QStyleOptionViewItem& option,
                       const QModelIndex& index) const
{
  if (!index.isValid()) {
    return;
  }

  if (index.column() == ReagentTableModel::Color) {
    QStyledItemDelegate::paint(painter, option, index);
    return;
  }

  painter->save();

  /*const*/ QString text /*{ index.model()->data(index).toString() }*/;
  QStyleOptionViewItem new_options{ option };

  switch (static_cast<ReagentTableModel::Cols>(index.column())) {
    case ReagentTableModel::Name: {
      new_options.rect.setLeft(option.rect.left() + 5);
      text = index.model()->data(index).toString();
      painter->drawText(
        new_options.rect, text, Qt::AlignVCenter | Qt::AlignLeft);
      break;
    }
    case ReagentTableModel::Volume: {
      new_options.rect.setRight(option.rect.right() - 5);
      text = tr("%1 uL").arg(index.model()->data(index).toString());
      painter->drawText(
        new_options.rect, text, Qt::AlignVCenter | Qt::AlignRight);
      break;
    }
    case ReagentTableModel::Type: {
      text = index.model()->data(index).toString();
      new_options.rect.setLeft(new_options.rect.left() + 20);
      const QRect rect{ option.rect.left(), option.rect.top(), 16, 16 };
      const QIcon icon{
        index.model()->data(index, Qt::DecorationRole).toString()
      };
      icon.paint(painter, rect);
      painter->drawText(
        new_options.rect, text, Qt::AlignVCenter | Qt::AlignLeft);
      break;
    }
    default: {
      break;
    }
  }

  painter->restore();
}

QWidget*
ReagentDelegate::createEditor(QWidget* parent,
                              const QStyleOptionViewItem& option,
                              const QModelIndex& index) const
{
  qCDebug(RD) << "createEditor" << option << index;
  switch (static_cast<ReagentTableModel::Cols>(index.column())) {
    case ReagentTableModel::Color: {
      auto* editor{ new KColorButton{ parent } };
      Q_ASSERT(editor);
      return editor;
    }
    case ReagentTableModel::Type: {
      auto* editor{ new ReagentTypeComboBox{ parent } };
      Q_ASSERT(editor);
      return editor;
    }
    case ReagentTableModel::Volume: {
      auto* editor{ new ReagentVolumeSpinBox{ parent } };
      Q_ASSERT(editor);
      return editor;
    }
    default:
      break;
  }

  return QStyledItemDelegate::createEditor(parent, option, index);
}

void
ReagentDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
  qCDebug(RD) << "setEditorData" << editor << index;
  switch (static_cast<ReagentTableModel::Cols>(index.column())) {
    case ReagentTableModel::Volume: {
      const auto value{ index.model()->data(index).toInt() };
      auto* spin_box{ dynamic_cast<ReagentVolumeSpinBox*>(editor) };
      Q_ASSERT(spin_box);
      spin_box->setValue(value);
      break;
    }

    case ReagentTableModel::Type: {
      auto type{ index.model()->data(index, Qt::EditRole).toInt() };
      auto* combo_box{ qobject_cast<ReagentTypeComboBox*>(editor) };
      combo_box->setCurrentIndex(type);
      break;
    }

    case ReagentTableModel::Color: {
      const auto color{ index.model()->data(index).toString() };
      auto* button{ dynamic_cast<KColorButton*>(editor) };
      Q_ASSERT(button);
      button->setColor(QColor{ color });
      break;
    }

    default:
      QStyledItemDelegate::setEditorData(editor, index);
      break;
  }
}

void
ReagentDelegate::setModelData(QWidget* editor,
                              QAbstractItemModel* model,
                              const QModelIndex& index) const
{
  qCDebug(RD) << "setModelData" << editor << model << index;
  switch (static_cast<ReagentTableModel::Cols>(index.column())) {
    case ReagentTableModel::Volume: {
      auto* spin_box{ qobject_cast<ReagentVolumeSpinBox*>(editor) };
      Q_ASSERT(spin_box);
      spin_box->interpretText();
      model->setData(index, spin_box->value(), Qt::EditRole);
      break;
    }

    case ReagentTableModel::Type: {
      auto* combo_box{ dynamic_cast<ReagentTypeComboBox*>(editor) };
      Q_ASSERT(combo_box);
      model->setData(index, combo_box->currentIndex(), Qt::EditRole);
      break;
    }

    case ReagentTableModel::Color: {
      KColorButton* button{ dynamic_cast<KColorButton*>(editor) };
      Q_ASSERT(button);
      model->setData(index, button->color().name(), Qt::EditRole);
      break;
    }

    default: {
      QStyledItemDelegate::setModelData(editor, model, index);
      break;
    }
  }
}

void
ReagentDelegate::updateEditorGeometry(QWidget* editor,
                                      const QStyleOptionViewItem& option,
                                      const QModelIndex& index) const
{
  Q_UNUSED(index);
  editor->setGeometry(option.rect);
}

} // namespace widgets
