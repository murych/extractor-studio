#ifndef UTILS_ICONMAPPER_H
#define UTILS_ICONMAPPER_H

#include <QIcon>

namespace utils {

struct IconMapper
{
    [[nodiscard]] static auto stepIcon(int type) -> QIcon;
    [[nodiscard]] static auto reagentIcon(int type) -> QIcon;
};

} // namespace utils

#endif // UTILS_ICONMAPPER_H
