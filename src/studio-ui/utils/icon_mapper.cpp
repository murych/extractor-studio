#include "icon_mapper.h"

#include "models/plates/reagent.h"
#include "models/steps/step.h"

namespace utils {

QIcon
IconMapper::stepIcon(int type)
{
  using StepType = models::steps::Step::EStepType;

  switch (static_cast<StepType>(type)) {
    case StepType::Heater:
      return QIcon{ ":/48px/color/heating_48px.png" };
      break;
    case StepType::Pause:
      return QIcon{ ":/48px/color/sleep_mode_48px.png" };
      break;
    case StepType::Pickup:
      return QIcon{ ":/48px/color/arrow_up_48px.png" };
      break;
    case StepType::Leave:
      return QIcon{ ":/48px/color/turn_48px.png" };
      break;
    case StepType::Mix:
      return QIcon{ ":/48px/color/curly_arrow_48px.png" };
      break;
    case StepType::Dry:
      return QIcon{ ":/48px/color/dry_48px.png" };
      break;
    case StepType::Sleep:
      return QIcon{ ":/48px/color/sleep_48px.png" };
      break;
    case StepType::Move:
      return QIcon{ ":/48px/color/stepper_motor_48px.png" };
      break;
    case StepType::Collect:
      return QIcon{ ":/48px/color/magnetic_collect_48px.png" };
      break;
    case StepType::Release:
      return QIcon{ ":/48px/color/magnetic_release_48px.png" };
      break;
    default:
      return QIcon{":/48/color/error_48px.png"};
    }
}

QIcon IconMapper::reagentIcon(int type)
{
    using ReagentType = models::plates::Reagent::EReagentType;
    switch (static_cast<ReagentType>(type)) {
    case ReagentType::Reagent:
      return QIcon{":/48/color/test_tube_48px.png"};
    case ReagentType::Sample:
      return QIcon{":/48/color/experiment_48px.png"};
    default:
      return QIcon{":/48/color/error_48px.png"};
    }
}

} // namespace utils
