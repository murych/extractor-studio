#include "screen_home.h"
#include "ui_screen_home.h"

#include "models/project/extractor_project.h"
#include "models/project/layout.h"
#include "models/project/project_info.h"
#include "models/project/protocol.h"
#include "json/datetime_decorator.h"
#include "json/entity_collection.h"
#include "json/string_decorator.h"
#include <QLoggingCategory>
#include <QTextStream>
#include <QXmlStreamWriter>

Q_LOGGING_CATEGORY(S_HOME, "SCREEN HOME")

namespace screens {

ScreenHome::ScreenHome(QWidget* parent)
  : m_ui{ std::make_unique<Ui::ScreenHome>() }
{
  m_ui->setupUi(this);
  m_ui->gb_projectSummary->setEnabled(false);
}

ScreenHome::~ScreenHome() = default;

void
ScreenHome::loadData(const QJsonObject& json)
{
  const models::project::ExtractorProject project{ json };

  m_ui->l_projectInfo->setText(fillProjectInfo(project.info->toJson()));
  m_ui->l_projectLayout->setText(fillLayoutInfo(project.layout->toJson()));
  m_ui->l_projectProtocol->setText(
    fillProtocolInfo(project.protocol->toJson()));
}

QString
ScreenHome::fillProjectInfo(const QJsonObject& json)
{
  const models::project::Project project{ json };
  QString q_html_file;

  QTextStream start_doc_stream(&q_html_file);
  start_doc_stream << QStringLiteral("<!DOCTYPE html>");

  QXmlStreamWriter html_writer(&q_html_file);
  html_writer.writeStartElement(QStringLiteral("html"));
  html_writer.writeAttribute(QStringLiteral("xmlns"),
                             QStringLiteral("http://www.w3.org/1999/xhtml"));
  html_writer.writeAttribute(QStringLiteral("lang"), QStringLiteral("en"));
  html_writer.writeAttribute(
    QStringLiteral("xml"), QStringLiteral("lang"), QStringLiteral("en"));
  html_writer.writeStartElement(QStringLiteral("head"));
  html_writer.writeStartElement(QStringLiteral("meta"));
  html_writer.writeAttribute(QStringLiteral("http-equiv"),
                             QStringLiteral("Content-Type"));
  html_writer.writeAttribute(QStringLiteral("content"),
                             QStringLiteral("text/html; charset=utf-8"));
  html_writer.writeEndElement(); // meta
  html_writer.writeStartElement(QStringLiteral("title"));
  html_writer.writeCharacters(project.title->value());
  html_writer.writeEndElement(); // title
  html_writer.writeStartElement(QStringLiteral("style"));
  html_writer.writeCharacters(QStringLiteral(
    "h1, h2, h3, h4 { color: rgb(83,129,53) } h1 { text-align: left; }"));
  html_writer.writeEndElement(); // style
  html_writer.writeEndElement(); // head
  html_writer.writeStartElement(QStringLiteral("body"));
  html_writer.writeStartElement(QStringLiteral("h2"));
  html_writer.writeCharacters(project.title->value());
  html_writer.writeEndElement(); // h1
  html_writer.writeStartElement(QStringLiteral("p"));
  html_writer.writeCharacters(project.author->label());
  html_writer.writeCharacters(QLatin1String(": "));
  html_writer.writeCharacters(project.author->value());
  html_writer.writeEmptyElement(QStringLiteral("br"));
  html_writer.writeCharacters(project.dateCreated->label());
  html_writer.writeCharacters(QLatin1String(": "));
  html_writer.writeCharacters(project.dateCreated->toPrettyString());
  html_writer.writeEmptyElement(QStringLiteral("br"));
  html_writer.writeCharacters(project.description->label());
  html_writer.writeCharacters(QLatin1String(": "));
  html_writer.writeCharacters(project.description->value());
  html_writer.writeEndElement(); // p
  html_writer.writeEndElement(); // body
  html_writer.writeEndElement(); // html

  return q_html_file;
}

QString
ScreenHome::fillLayoutInfo(const QJsonObject& json)
{
  const models::project::Layout layout{ json };
  const int count = layout.plates->derivedEntities().isEmpty()
                      ? 0
                      : layout.plates->derivedEntities().count();

  QString q_html_file;

  QTextStream start_doc_stream(&q_html_file);
  start_doc_stream << QStringLiteral("<!DOCTYPE html>");

  QXmlStreamWriter html_writer(&q_html_file);
  html_writer.writeStartElement(QStringLiteral("html"));
  html_writer.writeAttribute(QStringLiteral("xmlns"),
                             QStringLiteral("http://www.w3.org/1999/xhtml"));
  html_writer.writeAttribute(QStringLiteral("lang"), QStringLiteral("en"));
  html_writer.writeAttribute(
    QStringLiteral("xml"), QStringLiteral("lang"), QStringLiteral("en"));
  html_writer.writeStartElement(QStringLiteral("head"));
  html_writer.writeStartElement(QStringLiteral("meta"));
  html_writer.writeAttribute(QStringLiteral("http-equiv"),
                             QStringLiteral("Content-Type"));
  html_writer.writeAttribute(QStringLiteral("content"),
                             QStringLiteral("text/html; charset=utf-8"));
  html_writer.writeEndElement(); // meta
  html_writer.writeStartElement(QStringLiteral("title"));
  html_writer.writeCharacters(layout.key());
  html_writer.writeEndElement(); // title
  html_writer.writeStartElement(QStringLiteral("style"));
  html_writer.writeCharacters(QStringLiteral(
    "h1, h2, h3, h4 { color: rgb(83,129,53) } h1 { text-align: left; }"));
  html_writer.writeEndElement(); // style
  html_writer.writeEndElement(); // head
  html_writer.writeStartElement(QStringLiteral("body"));
  html_writer.writeStartElement(QStringLiteral("p"));
  html_writer.writeCharacters(tr("This protocol has: %1 plates").arg(count));
  html_writer.writeEndElement(); // p
  html_writer.writeEndElement(); // body
  html_writer.writeEndElement(); // html

  return q_html_file;
}

QString
ScreenHome::fillProtocolInfo(const QJsonObject& json)
{
  const models::project::Protocol protocol{ json };
  const int count = protocol.steps->derivedEntities().isEmpty()
                      ? 0
                      : protocol.steps->derivedEntities().count();

  QString q_html_file;

  QTextStream start_doc_stream(&q_html_file);
  start_doc_stream << QStringLiteral("<!DOCTYPE html>");

  QXmlStreamWriter html_writer(&q_html_file);
  html_writer.writeStartElement(QStringLiteral("html"));
  html_writer.writeAttribute(QStringLiteral("xmlns"),
                             QStringLiteral("http://www.w3.org/1999/xhtml"));
  html_writer.writeAttribute(QStringLiteral("lang"), QStringLiteral("en"));
  html_writer.writeAttribute(
    QStringLiteral("xml"), QStringLiteral("lang"), QStringLiteral("en"));
  html_writer.writeStartElement(QStringLiteral("head"));
  html_writer.writeStartElement(QStringLiteral("meta"));
  html_writer.writeAttribute(QStringLiteral("http-equiv"),
                             QStringLiteral("Content-Type"));
  html_writer.writeAttribute(QStringLiteral("content"),
                             QStringLiteral("text/html; charset=utf-8"));
  html_writer.writeEndElement(); // meta
  html_writer.writeStartElement(QStringLiteral("title"));
  html_writer.writeCharacters(protocol.key());
  html_writer.writeEndElement(); // title
  html_writer.writeStartElement(QStringLiteral("style"));
  html_writer.writeCharacters(QStringLiteral(
    "h1, h2, h3, h4 { color: rgb(83,129,53) } h1 { text-align: left; }"));
  html_writer.writeEndElement(); // style
  html_writer.writeEndElement(); // head
  html_writer.writeStartElement(QStringLiteral("body"));
  html_writer.writeStartElement(QStringLiteral("p"));
  html_writer.writeCharacters(tr("This protocol has: %1 steps").arg(count));
  html_writer.writeEndElement(); // p
  html_writer.writeEndElement(); // body
  html_writer.writeEndElement(); // html

  return q_html_file;
}

} // namespace screens
