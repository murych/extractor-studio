#include "screen_project.h"
#include "ui_screen_project.h"

#include "models/device/device_extractor.h"
#include "models/plates/plate.h"
#include "models/project/extractor_project.h"
#include "models/project/layout.h"
#include "models/project/project_info.h"
#include "models/project/protocol.h"
#include "models/project_tree_item.h"
#include "models/project_tree_model.h"
#include "utils/device_manager.h"
#include "widgets/category_layout.h"
#include "widgets/category_project.h"
#include "widgets/category_protocol.h"
#include "widgets/device_info.h"
#include "widgets/plate_info.h"
#include "widgets/project_info.h"
#include "widgets/step_info.h"
#include "json/entity_collection.h"
#include "json/enumerator_decorator.h"
#include "json/int_decorator.h"
#include <QJsonDocument>
#include <QLabel>
#include <QLoggingCategory>
#include <QMenu>
#include <QMessageBox>
#include <logger.h>
#include <memory>
#include <optional>

Q_LOGGING_CATEGORY(S_PROJECT, "SCREEN PROJECT")

namespace {

template<class T>
[[nodiscard]] inline auto
setupWidget(const QJsonObject& json)
{
  auto* widget{ new T };
  widget->setJson(json);
  return widget;
}

[[nodiscard]] auto
createWidget(models::tree_section section,
             models::tree_depth depth,
             const QJsonObject& json) -> widgets::AbstractModelWidget*
{
  switch (depth) {
    case models::tree_depth::Category: {
      switch (section) {
        case models::tree_section::Project:
          return setupWidget<widgets::CategoryProject>(json);

        case models::tree_section::Layout:
          return setupWidget<widgets::CategoryLayout>(json);

        case models::tree_section::Protocol:
          return setupWidget<widgets::CategoryProtocol>(json);

        default:
          break;
      }
      break;
    }
    case models::tree_depth::Subcategory: {
      switch (section) {
        case models::tree_section::InfoProject:
          return setupWidget<widgets::ProjectInfo>(json);

        case models::tree_section::InfoDevice:
          return setupWidget<widgets::DeviceInfo>(json);

        default:
          break;
      }
      break;
    }
    case models::tree_depth::Item: {
      switch (section) {
        case models::tree_section::Layout:
          return setupWidget<widgets::PlateInfo>(json);

        case models::tree_section::Protocol:
          return setupWidget<widgets::StepInfo>(json);

        default:
          break;
      }
      break;
    }
    default:
      break;
  }
  return nullptr;
}

}

namespace screens {

ScreenProject::ScreenProject(QWidget* parent)
  : m_ui{ std::make_unique<Ui::ScreenProject>() }
  , m_model{ std::make_unique<models::ProjectTreeModel>() }
  , m_curWidget{ std::make_unique<widgets::ProjectInfo>() }
{
  m_ui->setupUi(this);
  m_ui->b_copy->setVisible(false);
  m_ui->b_rename->setVisible(false);

  m_ui->treeView->setModel(m_model.get());
  m_ui->treeView->expandAll();

  connect(m_ui->treeView->selectionModel(),
          &QItemSelectionModel::currentRowChanged,
          this,
          &ScreenProject::updateSelected);

  connect(m_ui->b_moveDown,
          &QToolButton::clicked,
          this,
          &ScreenProject::moveItemDown);
  connect(
    m_ui->b_moveUp, &QToolButton::clicked, this, &ScreenProject::moveItemUp);
  connect(m_ui->b_cut, &QToolButton::clicked, this, &ScreenProject::cutItem);
  connect(
    m_ui->b_paste, &QToolButton::clicked, this, &ScreenProject::pasteItem);
  connect(
    m_ui->b_delete, &QToolButton::clicked, this, &ScreenProject::deleteItem);

  m_ui->viewport->addWidget(m_curWidget.get());
  m_ui->treeView->setCurrentIndex(m_model->index(0, 0, {}));

  m_ui->treeView->setContextMenuPolicy(Qt::CustomContextMenu);
  connect(m_ui->treeView,
          &QTreeView::customContextMenuRequested,
          this,
          &ScreenProject::onCustomContextMenu);
}

ScreenProject::~ScreenProject() = default;

void
ScreenProject::setCurrentIndex(const QModelIndex& index)
{
  if (!index.isValid()) {
    return;
  }

  m_ui->treeView->scrollTo(index);
  m_ui->treeView->setCurrentIndex(index);
}

void
ScreenProject::deduceCurrentDevice()
{
  const utils::DeviceManager manager;

  const models::project::Project project{ m_model->getProject() };
  const auto requested_type{ static_cast<device_type>(
    project.deviceType->value()) };
  const auto requested_family{ static_cast<extractor_model>(
    project.deviceFamily->value()) };

  const auto available_extractors{ manager.availableTypesInFamily(
    requested_type) };

  models::device::DeviceExtractor extractor;
  const auto* it{ std::find_if(
    available_extractors.cbegin(),
    available_extractors.cend(),
    [&](const auto& json) {
      return static_cast<extractor_model>(
               json.value(extractor.model->key()).toInt()) == requested_family;
    }) };
  if (it != available_extractors.end()) {
    extractor.update(*it);
  }

  m_maxPlatesAllowed = extractor.seats->derivedEntities().count();
}

void
ScreenProject::updateSelected(const QModelIndex& current,
                              const QModelIndex& previous)
{
  using namespace models;

  if (previous.row() == current.row() &&
      previous.parent() == current.parent()) {
    return;
  }

  if (previous.isValid()) {
    m_model->setData(previous, m_curWidget->getJson(), tree_role::ContentRole);
  }

  disconnect(m_curWidget.get(), nullptr, nullptr, nullptr);

  const auto new_section{ static_cast<tree_section>(
    m_model->data(current, tree_role::SectionRole).toUInt()) };
  const auto new_depth{ static_cast<tree_depth>(
    m_model->data(current, tree_role::DepthRole).toUInt()) };

  allowEditItem(m_model->isItemItem(current));

  const auto to_step{ new_depth == tree_depth::Item &&
                      new_section == tree_section::Protocol };
  const auto to_plate{ new_depth == tree_depth::Item &&
                       new_section == tree_section::Layout };

  const auto new_json{
    m_model->data(current, tree_role::ContentRole).toJsonObject()
  };

  auto* widget{ createWidget(new_section, new_depth, new_json) };
  if (widget == nullptr) {
    return;
  }

  m_ui->viewport->removeWidget(m_curWidget.get());
  m_curWidget.reset(widget);

  m_curWidget->setParent(m_ui->viewport);

  connect(m_curWidget.get(),
          &widgets::AbstractModelWidget::onModelTitleChanged,
          this,
          [this, current](const auto& title) {
            m_model->setData(current, title, Qt::EditRole);
          });

  qCDebug(S_PROJECT) << "\tcreated new widget" << m_curWidget.get();

  if (to_step) {
    // догружаем данные о добавленных в проект плашках
    qCDebug(S_PROJECT) << "\tloading avaliable plates data to new step widget";
    m_curWidget->loadAdditionalData(m_model->getLayout());
  }

  if (to_plate) {
    // догружаем данные о доступных посадочных местах
    qCDebug(S_PROJECT) << "\tloading avaliable seats data to new plate widget";
    m_curWidget->loadAdditionalData(m_model->getProject());
  }

  m_ui->viewport->addWidget(m_curWidget.get());
  qCDebug(S_PROJECT) << "\twidget" << m_curWidget.get() << "added to viewport";
}

void
ScreenProject::deleteItem()
{
  const auto index{ m_ui->treeView->currentIndex() };
  if (!index.isValid()) {
    return;
  }
  if (!m_model->isItemItem(index)) {
    return;
  }

  const auto answer{ QMessageBox::question(
    this,
    tr("Delete"),
    tr("Delete '%1'?").arg(m_model->data(index).toString())) };
  if (answer == QMessageBox::Discard) {
    return;
  }

  m_model->removeRow(index.row(), index.parent());
}

void
ScreenProject::moveItemUp()
{
  const auto previous{ m_ui->treeView->selectionModel()->currentIndex() };
  const auto index{ m_model->moveUp(previous) };
  setCurrentIndex(index);
}

void
ScreenProject::moveItemDown()
{
  const auto previous{ m_ui->treeView->selectionModel()->currentIndex() };
  const auto index{ m_model->moveDown(previous) };
  setCurrentIndex(index);
}

void
ScreenProject::cutItem()
{
  const auto index{ m_ui->treeView->selectionModel()->currentIndex() };
  if (!m_model->isItemItem(index)) {
    return;
  }

  setCurrentIndex(m_model->cut(index));
  m_ui->b_cut->setEnabled(m_model->hasCutItem());
}

void
ScreenProject::pasteItem()
{
  const auto previous{ m_ui->treeView->selectionModel()->currentIndex() };
  setCurrentIndex(m_model->paste(previous));
}

void
ScreenProject::allowEditItem(const bool allow)
{
  const auto index{ m_ui->treeView->selectionModel()->currentIndex() };
  if (!index.isValid()) {
    return;
  }

  const auto current_section{ static_cast<models::tree_section>(
    m_model->data(index, models::tree_role::SectionRole).toUInt()) };
  const auto current_row{ index.row() };
  const auto max_row{ m_model->rowCount(
    (current_section == models::tree_section::Layout)
      ? m_model->sectionLayout
      : m_model->sectionProtocol) };
  // не нравится вычитание единицы, проверить, почему rowCount возвращает на
  // 1 штуку больше, чем ожидаем
  const auto is_last{ current_row == max_row - 1 };
  const auto is_first{ current_row == 0 };

  m_ui->b_copy->setEnabled(allow);
  m_ui->b_cut->setEnabled(allow);
  m_ui->b_delete->setEnabled(allow);
  m_ui->b_moveDown->setEnabled(allow && !is_last);
  m_ui->b_moveUp->setEnabled(allow && !is_first);
  m_ui->b_paste->setEnabled(allow && m_model->hasCutItem());
  m_ui->b_rename->setEnabled(allow);
}

void
ScreenProject::addStep(int type)
{
  using namespace models;

  Q_ASSERT(m_model->sectionProtocol.isValid());

  const auto index{ m_ui->treeView->selectionModel()->currentIndex() };
  const auto is_item{ static_cast<tree_depth>(
                        m_model->data(index, tree_role::DepthRole).toUInt()) ==
                      tree_depth::Item };
  const auto is_protocol{
    static_cast<tree_section>(
      m_model->data(index, tree_role::SectionRole).toUInt()) ==
    tree_section::Protocol
  };

  const auto child_pos{ [&]() -> int {
    if (is_protocol && is_item) {
      auto* current_item{ m_model->itemFromIndex(index) };
      return current_item->childIdx() + 1;
    }
    if (m_model->rowCount(m_model->sectionProtocol) > 0) {
      return m_model->rowCount(m_model->sectionProtocol);
    }
    return 0;
  }() };

  if (!m_model->insertRow(child_pos, m_model->sectionProtocol)) {
    return;
  }

  const auto child_idx{ m_model->index(
    child_pos, 0, m_model->sectionProtocol) };
  if (!child_idx.isValid()) {
    return;
  }

  m_model->addStep(child_idx, type);

  setCurrentIndex(child_idx);
}

void
ScreenProject::addPlate(int type)
{
  Q_UNUSED(type);
  Q_ASSERT(m_model->sectionLayout.isValid());

  if (m_model->rowCount(m_model->sectionLayout) > (m_maxPlatesAllowed - 1)) {
    return;
  }

  const auto current_idx{ m_ui->treeView->selectionModel()->currentIndex() };
  const auto is_item{
    static_cast<models::tree_depth>(
      m_model->data(current_idx, models::tree_role::DepthRole).toUInt()) ==
    models::tree_depth::Item
  };
  const auto is_layout{
    static_cast<models::tree_section>(
      m_model->data(current_idx, models::tree_role::SectionRole).toUInt()) ==
    models::tree_section::Layout
  };

  const auto child_pos{ [&]() -> int {
    if (is_layout && is_item) {
      auto* current_item{ m_model->itemFromIndex(current_idx) };
      return current_item->childIdx() + 1;
    }
    if (m_model->rowCount(m_model->sectionLayout) > 0) {
      return m_model->rowCount(m_model->sectionLayout);
    }
    return 0;
  }() };

  if (!m_model->insertRow(child_pos, m_model->sectionLayout)) {
    return;
  }

  const auto child_idx{ m_model->index(child_pos, 0, m_model->sectionLayout) };
  if (!child_idx.isValid()) {
    return;
  }

  m_model->addPlate(child_idx, type);
  setCurrentIndex(child_idx);
}

void
ScreenProject::setProject(const QJsonObject& json)
{
  m_model->clear();
  m_model->setupModelData(json);

  deduceCurrentDevice();

  m_ui->treeView->expandAll();
  m_ui->treeView->setCurrentIndex(m_model->index(0, 0, {}));
}

QJsonObject
ScreenProject::getProject() const
{
  const models::project::ExtractorProject project;

  const auto info{ m_model->getProject() };
  const auto layout{ m_model->getLayout() };
  const auto protocol{ m_model->getProtocol() };

  const QJsonObject result{ { project.info->key(), info },
                            { project.layout->key(), layout },
                            { project.protocol->key(), protocol } };

  qCDebug(S_PROJECT) << result;
  return result;
}

void
ScreenProject::onCustomContextMenu(const QPoint& point)
{
  const auto index = m_ui->treeView->indexAt(point);
  if (!index.isValid()) {
    return;
  }
  if (static_cast<models::tree_depth>(
        m_model->data(index, models::tree_role::DepthRole).toUInt()) !=
      models::tree_depth::Item) {
    return;
  }
  auto menu = getContextMenu();
  menu->exec(m_ui->treeView->viewport()->mapToGlobal(point));
}

std::unique_ptr<QMenu>
ScreenProject::getContextMenu() const
{
  auto menu = std::make_unique<QMenu>();

  if (m_ui->b_copy->isEnabled()) {
    const auto* action = menu->addAction(tr("Copy"));
    connect(action, &QAction::triggered, this, &ScreenProject::pasteItem);
  }

  if (m_ui->b_paste->isEnabled()) {
    const auto* action = menu->addAction(tr("Paste"));
    connect(action, &QAction::triggered, this, &ScreenProject::pasteItem);
  }

  if (m_ui->b_cut->isEnabled()) {
    const auto* action = menu->addAction(tr("Cut"));
    connect(action, &QAction::triggered, this, &ScreenProject::cutItem);
  }

  if (m_ui->b_delete->isEnabled()) {
    const auto* action = menu->addAction(tr("Delete"));
    connect(action, &QAction::triggered, this, &ScreenProject::deleteItem);
  }

  if (m_ui->b_rename->isEnabled()) {
    const auto* action = menu->addAction(tr("Rename"));
    connect(action, &QAction::triggered, m_ui->b_rename, &QToolButton::clicked);
  }

  return menu;
}

} // namespace screens
