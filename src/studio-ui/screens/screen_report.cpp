#include "screen_report.h"
#include "ui_screen_report.h"

namespace screens {

ScreenReport::ScreenReport(QWidget* parent)
  : m_ui{ std::make_unique<Ui::ScreenReport>() }
{
  m_ui->setupUi(this);
}

ScreenReport::~ScreenReport() = default;

} // namespace screens
