#ifndef SCREEN_REPORT_H
#define SCREEN_REPORT_H
#include <QWidget>

namespace Ui {
class ScreenReport;
}

namespace screens {

class ScreenReport : public QWidget
{
  Q_OBJECT

public:
  explicit ScreenReport(QWidget* parent = nullptr);
  ~ScreenReport() override;

private:
  std::unique_ptr<Ui::ScreenReport> m_ui{ nullptr };
};

} // namespace screens

#endif // SCREEN_REPORT_H
