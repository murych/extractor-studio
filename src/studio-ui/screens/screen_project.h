#ifndef SCREENS_SCREEN_PROJECT_H
#define SCREENS_SCREEN_PROJECT_H

#include <QJsonObject>
#include <QMenu>
#include <QWidget>
#include <memory>

namespace Ui {
class ScreenProject;
}

namespace models {
class ProjectTreeModel;
} // namespace models

namespace widgets {
class AbstractModelWidget;
} // namespace widgets

namespace screens {

class ScreenProject : public QWidget
{
  Q_OBJECT

public:
  explicit ScreenProject(QWidget* parent = nullptr);
  ~ScreenProject() override;

  [[nodiscard]] QJsonObject getProject() const;

private:
  std::unique_ptr<Ui::ScreenProject> m_ui{ nullptr };
  std::unique_ptr<models::ProjectTreeModel> m_model{ nullptr };
  std::unique_ptr<widgets::AbstractModelWidget> m_curWidget{ nullptr };
  int m_maxPlatesAllowed{ 0 };

  void setCurrentIndex(const QModelIndex& index);
  void deduceCurrentDevice();

  void updateSelected(const QModelIndex& current, const QModelIndex& previous);
  void deleteItem();
  void moveItemUp();
  void moveItemDown();
  void cutItem();
  void pasteItem();
  void allowEditItem(bool allow);
  [[nodiscard]] std::unique_ptr<QMenu> getContextMenu() const;

  // AbstractScreen interface
public slots:
  void addStep(int type);
  void addPlate(int type);
  void setProject(const QJsonObject& json);
private slots:
  void onCustomContextMenu(const QPoint& point);
};

} // namespace screens
#endif // SCREENS_SCREEN_PROJECT_H
