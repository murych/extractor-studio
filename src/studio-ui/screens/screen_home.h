#ifndef SCREENHOME_H
#define SCREENHOME_H

#include <QWidget>

QT_BEGIN_NAMESPACE

namespace Ui {
class ScreenHome;
}

QT_END_NAMESPACE

namespace screens {

class ScreenHome : public QWidget
{
  Q_OBJECT
public:
  explicit ScreenHome(QWidget* parent = nullptr);
  ~ScreenHome() override;

  void loadData(const QJsonObject& json);

private:
  std::unique_ptr<Ui::ScreenHome> m_ui{ nullptr };

  QString fillProjectInfo(const QJsonObject& json);
  QString fillLayoutInfo(const QJsonObject& json);
  QString fillProtocolInfo(const QJsonObject& json);
};

} // namespace screens

#endif // SCREENHOME_H
