#ifndef DIALOGS_ABOUT_DIALOG_H
#define DIALOGS_ABOUT_DIALOG_H

#include <QDialog>

namespace Ui {
class AboutDialog;
}

namespace dialogs {

class AboutDialog : public QDialog
{
  Q_OBJECT

public:
  explicit AboutDialog(QWidget* parent = nullptr);
  ~AboutDialog() override;

private:
  std::unique_ptr<Ui::AboutDialog> m_ui{ nullptr };
};

} // namespace dialogs
#endif // DIALOGS_ABOUT_DIALOG_H
