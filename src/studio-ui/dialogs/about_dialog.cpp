#include "about_dialog.h"
#include "ui_about_dialog.h"

#include "version.h"
#include <QApplication>
#include <QDateTime>

namespace dialogs {

AboutDialog::AboutDialog(QWidget* parent)
  : QDialog{ parent }
  , m_ui{ std::make_unique<Ui::AboutDialog>() }
{
  m_ui->setupUi(this);
  m_ui->l_version->setText(
    tr("Version: %1").arg(QApplication::applicationVersion()));
  const auto commit_time =
    QDateTime::fromSecsSinceEpoch(QString(GIT_COMMIT_TIMESTAMP).toInt())
      .toLocalTime().toString();
  m_ui->l_git->setText(tr("Build info:\n%1 on %2 @ %3")
                         .arg(GIT_COMMIT_HASH, commit_time, GIT_BRANCH));

  setWindowTitle(QApplication::applicationName());
}

AboutDialog::~AboutDialog() = default;

} // namespace dialogs
