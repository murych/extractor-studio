#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMap>

namespace Ui {
class MainWindow;
}

namespace settings {
class SettingsDialog;
}

namespace tt {
class TabToolbar;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT
public:
  explicit MainWindow(QWidget* parent = nullptr);
  ~MainWindow() override;

private:
  std::unique_ptr<Ui::MainWindow> m_ui{ nullptr };
  std::unique_ptr<settings::SettingsDialog> m_settingsDialog{ nullptr };
  std::unique_ptr<tt::TabToolbar> m_toolbar{ nullptr };
  std::unique_ptr<QMenu> m_menuReportExport{ nullptr };

  bool m_projectModified{ false };
  //! директория с последним сохранением
  QString m_currentWorkDir;
  //! название текущего проекта
  QString m_curFile;

  static void setupResources();

signals:
  void projectOpened();
  void projectClosed();

  void clearEditors();

  void proejctToSave();

private slots:
  void open();
  bool save();
  bool saveAs();
  void close();

  bool maybeSave();
  void setCurrentFile(const QString& title);

  void readSettings();
  void writeSettings();

  void setupToolbar();

  bool projectSave(const QString& file_path);

  void setDirty(bool dirty = true) { setWindowModified(dirty); }

  void enableEditActions(bool status = true);
  void connectEditActions();
  void connectFileActions();
  void connectAboutActions();

  void projectCreate();

  void onProjectOpened();
  void onProjectClosed();

  void exportProtocol();

protected:
  void closeEvent(QCloseEvent* event) override;

private:
  std::unordered_map<int, QAction*> m_addStepActions;
  std::unordered_map<int, QAction*> m_addPlateActions;
};

#endif // MAINWINDOW_H
