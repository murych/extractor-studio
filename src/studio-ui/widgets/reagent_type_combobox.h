#ifndef REAGENT_TYPE_COMBOBOX_H
#define REAGENT_TYPE_COMBOBOX_H

#include <QComboBox>

namespace models {
class ReagentTypesModel;
}

namespace widgets {
class ReagentTypeComboBox : public QComboBox
{
  Q_OBJECT

public:
  explicit ReagentTypeComboBox(QWidget* parent);
  ~ReagentTypeComboBox() override;

private:
  std::unique_ptr<models::ReagentTypesModel> m_model{ nullptr };
};
} // namespace widgets

#endif // REAGENT_TYPE_COMBOBOX_H
