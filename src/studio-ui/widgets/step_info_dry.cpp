#include "step_info_dry.h"
#include "ui_step_info_dry.h"

#include "models/moving_pos_model.h"
#include "models/moving_pos_proxy.h"
#include "models/steps/moving_position.h"
#include "models/steps/step_dry.h"
#include "json/enumerator_decorator.h"
#include "json/int_decorator.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(W_SD, "W DRY")

using namespace models;

namespace widgets {

StepInfoDry::StepInfoDry(QWidget* parent)
  : AbstractStepInfo{ parent }
  , m_ui{ std::make_unique<Ui::StepInfoDry>() }
{
  m_ui->setupUi(this);

  auto* proxy{ new models::MovingPosProxy{ this } };
  proxy->setSourceModel(new models::MovingPosModel{ this });

  std::vector white_list{ steps::ETravelPos::AtWellSurface,
                          steps::ETravelPos::AtWorkPos };
  proxy->setWhiteList(std::move(white_list));
  m_ui->dryPositionComboBox->setModel(proxy);
}

StepInfoDry::~StepInfoDry() = default;

void
StepInfoDry::setData(models::steps::ParamsSpecific* new_data)
{
  m_step = dynamic_cast<steps::StepDry*>(new_data);
  loadDataToUi();
  setupConnections();
}

//! @todo починить загрузку значения
void
StepInfoDry::loadDataToUi()
{
  QTime time{ 0, 0, 0 };
  time = time.addSecs(m_step->duration->value());
  m_ui->dryDurationTimeEdit->setTime(time);

  m_ui->dryPositionComboBox->setCurrentIndex(
    m_ui->dryPositionComboBox->findText(
      steps::TRAVEL_POSITION_MAPPER.at(m_step->position->value())));
}

void
StepInfoDry::setupConnections()
{
  connect(m_ui->dryDurationTimeEdit,
          &QTimeEdit::timeChanged,
          m_step,
          [this](const auto& time) {
            const QTime start{ 0, 0, 0 };
            m_step->duration->setValue(start.secsTo(time));
          });

  connect(m_ui->dryPositionComboBox,
          QOverload<int>::of(&QComboBox::currentIndexChanged),
          m_step,
          &steps::StepDry::setPosition);
}

} // namespace widgets
