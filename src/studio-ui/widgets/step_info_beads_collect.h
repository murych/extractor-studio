#ifndef STEP_INFO_BEADS_COLLECT_H
#define STEP_INFO_BEADS_COLLECT_H

#include "abstract_step_widget.h"

namespace Ui {
class StepInfoBeadsCollect;
}

namespace models::steps {
class StepBeadsCollect;
}

namespace widgets {

class StepInfoBeadsCollect : public AbstractStepInfo
{
  Q_OBJECT

public:
  explicit StepInfoBeadsCollect(QWidget* parent = nullptr);
  ~StepInfoBeadsCollect() override;

private:
  std::unique_ptr<Ui::StepInfoBeadsCollect> m_ui{ nullptr };

  // AbstractStepInfo interface
protected:
  void setData(models::steps::ParamsSpecific* new_data) override;
  void loadDataToUi() override;
  void setupConnections() override;

private:
  //  std::unique_ptr<models::steps::StepBeadsCollect> m_step{ nullptr };
  models::steps::StepBeadsCollect* m_step{ nullptr };
};

} // namespace widgets
#endif // STEP_INFO_BEADS_COLLECT_H
