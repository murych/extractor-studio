#include "step_info_sleep.h"
#include "ui_step_info_sleep.h"

#include "models/steps/step_sleep.h"

#include "logger.h"
#include <QLoggingCategory>
Q_LOGGING_CATEGORY(W_SS, "W STEP SLEEP")

using namespace models;

namespace widgets {

StepInfoSleep::StepInfoSleep(QWidget* parent)
  : AbstractStepInfo{ parent }
  , m_ui{ std::make_unique<Ui::StepInfoSleep>() }
{
  m_ui->setupUi(this);
}

StepInfoSleep::~StepInfoSleep() = default;

void
StepInfoSleep::setData(models::steps::ParamsSpecific* new_data)
{
  m_step = dynamic_cast<models::steps::StepSleep*>(new_data);
  Q_ASSERT(m_step);
  qCDebug(W_SS) << "setData // loading from" << new_data << "loaded model"
                << m_step << m_step->toJson();
  loadDataToUi();
  setupConnections();
}

void
StepInfoSleep::loadDataToUi()
{
  //  auto* step{ dynamic_cast<steps::StepSleep*>(data) };

  QTime time{ 0, 0, 0 };
  time = time.addSecs(m_step->getSleepDuration());
  m_ui->sleepDurationTimeEdit->setTime(time);
}

void
StepInfoSleep::setupConnections()
{
  //  auto* step{ dynamic_cast<steps::StepSleep*>(data) };

  connect(m_ui->sleepDurationTimeEdit, &QTimeEdit::timeChanged, this, [this] {
    const QTime start{ 0, 0, 0 };
    const QTime value{ m_ui->sleepDurationTimeEdit->time() };
    emit onTimeChanged(start.secsTo(value));
  });
  connect(this,
          &StepInfoSleep::onTimeChanged,
          m_step,
          &steps::StepSleep::setSleepDuration);
}

} // namespace widgets
