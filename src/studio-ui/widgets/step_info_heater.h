#ifndef WIDGETS_STEP_INFO_HEATER_H
#define WIDGETS_STEP_INFO_HEATER_H

#include "abstract_step_widget.h"

namespace Ui {
class StepInfoHeater;
}

namespace models::steps {
class StepHeater;
}

namespace widgets {

class StepInfoHeater : public AbstractStepInfo
{
  Q_OBJECT

public:
  explicit StepInfoHeater(QWidget* parent = nullptr);
  ~StepInfoHeater() override;

private:
  std::unique_ptr<Ui::StepInfoHeater> m_ui{ nullptr };
  models::steps::StepHeater* m_step{ nullptr };

  // AbstractStepInfo interface
protected:
  void setData(models::steps::ParamsSpecific* new_data) override;
  void loadDataToUi() override;
  void setupConnections() override;
};

} // namespace widgets
#endif // WIDGETS_STEP_INFO_HEATER_H
