#include "plate_info.h"
#include "ui_plate_info.h"

#include "models/avaliable_seats_model.h"
#include "models/device/seat.h"
#include "models/plate_type_model.h"
#include "models/plates/plate.h"
#include "models/plates/reagent.h"
#include "utils/plate_manager.h"
#include "json/double_decorator.h"
#include "json/entity_collection.h"
#include "json/enumerator_decorator.h"
#include "json/int_decorator.h"
#include "json/string_decorator.h"
#include <QLoggingCategory>
#include <QMenu>
#include <memory>

Q_LOGGING_CATEGORY(W_PL, "PLATE WIDGET")

using namespace models;

namespace widgets {

PlateInfo::PlateInfo(QWidget* parent)
  : AbstractModelWidget{ parent }
  , m_ui{ std::make_unique<Ui::PlateInfo>() }
  , m_platesModel{ std::make_unique<PlateTypeModel>() }
  , m_seatsModel{ std::make_unique<AvaliableSeatsModel>() }
  , m_modelData{ std::make_unique<models::plates::Plate>() }
{
  m_ui->setupUi(this);
  m_ui->plateTypeComboBox->setModel(m_platesModel.get());
}

PlateInfo::~PlateInfo() = default;

void
PlateInfo::setJson(const QJsonObject& json)
{
  qCDebug(W_PL) << "setJson" << json;
  m_modelData = std::make_unique<plates::Plate>(json);
}

QJsonObject
PlateInfo::getJson() const
{
  qCDebug(W_PL) << "getJson";

  const auto reagents{ m_ui->pageReagents->getJson() };
  const auto reagents_arr =
    reagents.value(m_modelData->reagents->getKey()).toArray();

  qCDebug(W_PL) << "\tare reagents empty?" << reagents_arr
                << reagents_arr.isEmpty();

  if (!reagents_arr.isEmpty()) {
    m_modelData->reagents->update(reagents_arr);
  }

  qCDebug(W_PL) << "\tplate" << m_modelData->name->value()
                << "content:" << m_modelData->toJson();
  return m_modelData->toJson();
}

void
PlateInfo::loadAdditionalData(const QJsonObject& json)
{
  qCDebug(W_PL) << "loadAdditionalData" << json;
  if (json.isEmpty()) {
    return;
  }

  m_seatsModel->loadData(json);
  m_ui->platePositionComboBox->setModel(m_seatsModel.get());

  loadDataToUi();
  createConnections();
}

void
PlateInfo::createConnections()
{
  qCDebug(W_PL) << "createConnections";
  auto* plate{ dynamic_cast<plates::Plate*>(m_modelData.get()) };

  connect(m_ui->plateNameLineEdit, &QLineEdit::editingFinished, this, [this] {
    emit onModelTitleChanged(m_ui->plateNameLineEdit->text());
  });
  connect(
    this, &PlateInfo::onModelTitleChanged, plate, &plates::Plate::setPlateName);

  connect(m_ui->platePositionComboBox,
          QOverload<int>::of(&QComboBox::currentIndexChanged),
          plate,
          &plates::Plate::setPlateIdx);

  connect(m_ui->plateContentComboBox,
          QOverload<int>::of(&QComboBox::currentIndexChanged),
          plate,
          &plates::Plate::setPlateContent);

  connect(
    m_ui->plateTypeComboBox,
    QOverload<int>::of(&QComboBox::activated),
    this,
    [this]() {
      const QUuid current{
        m_ui->plateTypeComboBox->currentData(PlateTypeModel::UuidRole).toUuid()
      };
      emit onPlateTypeChanged(current.toString());
    });
  connect(
    this, &PlateInfo::onPlateTypeChanged, plate, &plates::Plate::setPlateSize);

  connect(m_ui->pageSleeves,
          &SleevesInfo::onSleevesTypeChanged,
          plate,
          &plates::Plate::setSleeveType);
}

void
PlateInfo::loadDataToUi()
{
  qCDebug(W_PL) << "loadDataToUi";
  const auto* plate{ dynamic_cast<plates::Plate*>(m_modelData.get()) };

  setObjectName("Widget " + plate->name->value());

  m_ui->platePositionComboBox->setCurrentIndex(plate->seat->value());
  m_ui->plateNameLineEdit->setText(plate->name->value());
  m_ui->plateContentComboBox->setCurrentIndex(plate->content->value());

  const auto type{ m_ui->plateTypeComboBox->findData(
    QVariant::fromValue(plate->size->value()), PlateTypeModel::UuidRole) };
  if (type != -1) {
    m_ui->plateTypeComboBox->setCurrentIndex(type);
  } else {
    m_ui->plateTypeComboBox->setCurrentIndex(0);
  }

  auto* reagent_info{ dynamic_cast<widgets::ReagentInfo*>(
    m_ui->stackedWidget->widget(Liquids)) };
  reagent_info->loadData(plate->toJson());
}

} // namespace widgets
