#ifndef WIDGETS_STEP_INFO_H
#define WIDGETS_STEP_INFO_H

#include "abstract_model_widget.h"

namespace Ui {
class StepInfo;
}

namespace models {
class AvaliablePlatesModel;
}

namespace models::steps {
class Step;
}

namespace widgets {
class AbstractStepInfo;

class StepInfo : public AbstractModelWidget
{
  Q_OBJECT

public:
  explicit StepInfo(QWidget* parent = nullptr);
  ~StepInfo() override;

  [[nodiscard]] QJsonObject getJson() const override;
  void setJson(const QJsonObject& json) override;
  void loadAdditionalData(const QJsonObject& json) override;

private:
  void disableAdvancedParams(bool status = false);
  void disableCustomIcon(bool status = false);

  std::unique_ptr<Ui::StepInfo> m_ui{ nullptr };
  std::unique_ptr<AbstractStepInfo> m_paramsSpecific{ nullptr };
  std::unique_ptr<models::steps::Step> m_modelData{ nullptr };
  std::unique_ptr<models::AvaliablePlatesModel> m_avaliablePlatesModel{
    nullptr
  };
  int m_stepType{ 0 };

protected:
  void createConnections() override;
  void loadDataToUi() override;

private slots:
  void onSleevesPositionAtEndLineEditActivated(int index);
};

} // namespace widgets
#endif // WIDGETS_STEP_INFO_H
