#include "category_protocol.h"
#include "ui_category_protocol.h"

#include "models/project/protocol.h"

namespace widgets {

CategoryProtocol::CategoryProtocol(QWidget* parent)
  : AbstractModelWidget{ parent }
  , m_ui{ std::make_unique<Ui::CategoryProtocol>() }
  , m_modelData{ std::make_unique<models::project::Protocol>() }
{
  m_ui->setupUi(this);
}

CategoryProtocol::~CategoryProtocol() = default;

void
CategoryProtocol::setJson(const QJsonObject& json)
{
  m_modelData = std::make_unique<models::project::Protocol>(json);
  createConnections();
  loadDataToUi();
}

QJsonObject
CategoryProtocol::getJson() const
{
  Q_ASSERT(m_modelData);
  return m_modelData->toJson();
}

} // namespace widgets
