#include "reagent_info.h"
#include "ui_reagent_info.h"

#include "delegates/delegate_reagent.h"
#include "models/plates/plate.h"
#include "models/plates/reagent.h"
#include "models/reagent_table_model.h"
#include <QJsonArray>
#include <QJsonObject>
#include <QMessageBox>

#include "logger.h"
#include <QLoggingCategory>
Q_LOGGING_CATEGORY(W_RI, "REAGENT WIDGET")

namespace widgets {

ReagentInfo::ReagentInfo(QWidget* parent)
  : QGroupBox{ parent }
  , m_ui{ std::make_unique<Ui::ReagentInfo>() }
  , m_model{ std::make_unique<models::ReagentTableModel>() }
  , m_reagentDelegate{ std::make_unique<widgets::ReagentDelegate>() }
{
  qCDebug(W_RI) << "ReagentInfo";
  m_ui->setupUi(this);

  m_ui->b_moveDownRow->setVisible(false);
  m_ui->b_moveUpRow->setVisible(false);

  m_ui->tableView->setModel(m_model.get());
  m_ui->tableView->setItemDelegate(m_reagentDelegate.get());
  m_ui->tableView->horizontalHeader()->setSectionResizeMode(
    QHeaderView::Stretch);
  m_ui->tableView->setCurrentIndex(m_model->index(0, 0, {}));

#ifdef calculateReady
  connect(ui->b_calculateVolume,
          &QToolButton::clicked,
          this,
          &ReagentInfo::calculateTotalVolume);
#else
  m_ui->controlsVolume->setEnabled(false);
  m_ui->b_calculateVolume->setVisible(false);
  m_ui->l_maxVolume->setVisible(false);
  m_ui->sb_maxVolume->setVisible(false);
  m_ui->l_totalVolume->setVisible(false);
  m_ui->sb_totalVolume->setVisible(false);
#endif // calculateReady

  connect(m_ui->tableView->selectionModel(),
          &QItemSelectionModel::currentRowChanged,
          this,
          &ReagentInfo::updateSelected);

  connect(m_ui->b_addRow, &QToolButton::clicked, this, &ReagentInfo::addItem);
  connect(
    m_ui->b_deleteRow, &QToolButton::clicked, this, &ReagentInfo::deleteItem);
  connect(m_ui->b_cutRow, &QToolButton::clicked, this, &ReagentInfo::cutItem);
  connect(
    m_ui->b_pasteRow, &QToolButton::clicked, this, &ReagentInfo::pasteItem);
}

ReagentInfo::~ReagentInfo() = default;

QJsonObject
ReagentInfo::getJson() const
{
  qCDebug(W_RI) << "getJson";
  return m_model->modelData();
}

void
ReagentInfo::setCurrentIndex(const QModelIndex& index)
{
  if (!index.isValid()) {
    return;
  }

  m_ui->tableView->scrollTo(index);
  m_ui->tableView->setCurrentIndex(index);
}

void
ReagentInfo::updateSelected(const QModelIndex& current,
                            const QModelIndex& previous)
{
  allowEditItem(true);
}

void
ReagentInfo::deleteItem()
{
  const auto index{ m_ui->tableView->currentIndex() };

  if (!index.isValid()) {
    return;
  }
  const auto title{ tr("Delete") };
  const auto name{ m_model->data(index, Qt::DisplayRole).toString() };
  const auto message{ tr("Delete '%1'?").arg(name) };

  if (QMessageBox::question(this, title, message) == 0) {
    return;
  }

  m_model->removeRow(index.row(), index.parent());
}

void
ReagentInfo::moveItemUp()
{}

void
ReagentInfo::moveItemDown()
{}

void
ReagentInfo::cutItem()
{
  const auto index{ m_ui->tableView->selectionModel()->currentIndex() };
  setCurrentIndex(m_model->cut(index));
  m_ui->b_cutRow->setEnabled(m_model->hasCutItem());
}

void
ReagentInfo::pasteItem()
{
  const auto previous{ m_ui->tableView->selectionModel()->currentIndex() };
  setCurrentIndex(m_model->paste(previous));
}

void
ReagentInfo::allowEditItem(bool allow)
{
  const auto current{ m_ui->tableView->selectionModel()->currentIndex() };
  if (!current.isValid()) {
    return;
  }

  const auto current_row{ current.row() };
  const auto max_row{ m_model->rowCount(current.parent()) };
  const auto is_last{ current_row == max_row - 1 };
  const auto is_first{ current_row == 0 };

  m_ui->b_copyRow->setEnabled(allow);
  m_ui->b_cutRow->setEnabled(allow);
  m_ui->b_deleteRow->setEnabled(allow);
  m_ui->b_moveDownRow->setEnabled(allow && !is_last);
  m_ui->b_moveUpRow->setEnabled(allow && !is_first);
  m_ui->b_pasteRow->setEnabled(allow && m_model->hasCutItem());
}

void
ReagentInfo::calculateTotalVolume() const
{
#ifdef calculateReady
  QList<models::plates::Reagent*> m_reagents = model->items;

  if (m_reagents.isEmpty())
    return;

  int total_volume = 0;
  for (const models::plates::Reagent* reagent : qAsConst(m_reagents))
    total_volume += reagent->reagent_volume->value();

  ui->sb_totalVolume->setValue(total_volume);
#endif // calculateReady
}

void
ReagentInfo::addItem()
{
  const auto current_idx{ m_ui->tableView->selectionModel()->currentIndex() };
  if (!m_model->insertRow(current_idx.row() + 1, current_idx.parent())) {
    return;
  }

  const auto child_idx{ m_model->index(
    current_idx.row(), current_idx.column(), current_idx.parent()) };
  if (!child_idx.isValid()) {
    return;
  }

  setCurrentIndex(child_idx);
}

void
ReagentInfo::loadData(const QJsonObject& json)
{
  qCDebug(W_RI) << "loadData" << json;
  m_model->setupModelData(json);
}

} // namespace widgets
