#ifndef WIDGETS_STEP_INFO_DRY_H
#define WIDGETS_STEP_INFO_DRY_H

#include "abstract_step_widget.h"

namespace Ui {
class StepInfoDry;
}

namespace models::steps {
class StepDry;
}

namespace widgets {

class StepInfoDry : public AbstractStepInfo
{
  Q_OBJECT

public:
  explicit StepInfoDry(QWidget* parent = nullptr);
  ~StepInfoDry() override;

private:
  std::unique_ptr<Ui::StepInfoDry> m_ui{ nullptr };
  models::steps::StepDry* m_step{ nullptr };

  // AbstractStepInfo interface
protected:
  void setData(models::steps::ParamsSpecific* new_data) override;
  void loadDataToUi() override;
  void setupConnections() override;
};

} // namespace widgets
#endif // WIDGETS_STEP_INFO_DRY_H
