#ifndef WIDGETS_STEP_INFO_BEADS_RELEASE_H
#define WIDGETS_STEP_INFO_BEADS_RELEASE_H

#include "abstract_step_widget.h"

namespace Ui {
class StepInfoBeadsRelease;
}

namespace models::steps {
class StepBeadsRelease;
}

namespace widgets {

class StepInfoBeadsRelease : public AbstractStepInfo
{
  Q_OBJECT

public:
  explicit StepInfoBeadsRelease(QWidget* parent = nullptr);
  ~StepInfoBeadsRelease() override;

private:
  std::unique_ptr<Ui::StepInfoBeadsRelease> m_ui{ nullptr };
  models::steps::StepBeadsRelease* m_step{ nullptr };

  // AbstractStepInfo interface
protected:
  void setData(models::steps::ParamsSpecific* new_data) override;
  void loadDataToUi() override;
  void setupConnections() override;
};

} // namespace widgets
#endif // WIDGETS_STEP_INFO_BEADS_RELEASE_H
