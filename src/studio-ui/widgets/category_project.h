#ifndef WIDGETS_CATEGORY_PROJECT_H
#define WIDGETS_CATEGORY_PROJECT_H

#include "abstract_model_widget.h"

namespace Ui {
class CategoryProject;
}

namespace models::project {
class Project;
}

namespace widgets {

class CategoryProject : public AbstractModelWidget
{
  Q_OBJECT

public:
  explicit CategoryProject(QWidget* parent = nullptr);
  ~CategoryProject() override;

private:
  std::unique_ptr<Ui::CategoryProject> m_ui{ nullptr };
  std::unique_ptr<models::project::Project> m_modelData{ nullptr };

  // AbstractModelWidget interface
public:
  void setJson(const QJsonObject& json) override;
  [[nodiscard]] auto getJson() const -> QJsonObject override;

protected:
  void createConnections() override {}
  void loadDataToUi() override {}
};

} // namespace widgets
#endif // WIDGETS_CATEGORY_PROJECT_H
