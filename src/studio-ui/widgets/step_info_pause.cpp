#include "step_info_pause.h"
#include "ui_step_info_pause.h"

#include "models/steps/step_pause.h"

#include "logger.h"
#include <QLoggingCategory>
Q_LOGGING_CATEGORY(W_SP, "W STEP PAUSE")

using namespace models;

namespace widgets {

StepInfoPause::StepInfoPause(QWidget* parent)
  : AbstractStepInfo{ parent }
  , m_ui{ std::make_unique<Ui::StepInfoPause>() }
{
  m_ui->setupUi(this);
}

StepInfoPause::~StepInfoPause() = default;

void
StepInfoPause::setData(models::steps::ParamsSpecific* new_data)
{
  m_step = dynamic_cast<models::steps::StepPause*>(new_data);
  Q_ASSERT(m_step);
  qCDebug(W_SP) << "setData // loading from" << new_data << "loaded model"
                << m_step << m_step->toJson();
  loadDataToUi();
  setupConnections();
}

void
StepInfoPause::loadDataToUi()
{
  //  auto* step{ static_cast<steps::StepPause*>(data) };
  qCDebug(W_SP) << "loadDatatoUi";

  m_ui->cb_printMessage->setChecked(m_step->getBlock());
  m_ui->tb_messageText->setPlainText(m_step->getMessage());
}

void
StepInfoPause::setupConnections()
{
  //  auto* step{ static_cast<steps::StepPause*>(data) };
  qCDebug(W_SP) << "setup connections";

  connect(m_ui->tb_messageText, &QPlainTextEdit::textChanged, this, [this] {
    emit onMessageChanged(m_ui->tb_messageText->toPlainText());
  });

  connect(this,
          &widgets::StepInfoPause::onMessageChanged,
          m_step,
          &steps::StepPause::setMessage);
  connect(m_ui->cb_printMessage,
          &QCheckBox::toggled,
          m_step,
          &steps::StepPause::setBlock);
}

} // namespace widgets
