#include "step_info_sleeves_collect.h"
#include "ui_step_info_sleeves_collect.h"

#include "models/steps/step_sleeves_pickup.h"

#include "logger.h"
#include <QLoggingCategory>
Q_LOGGING_CATEGORY(W_SSC, "W STEP SLEEVES COLLECT")

namespace widgets {

StepInfoSleevesCollect::StepInfoSleevesCollect(QWidget* parent)
  : AbstractStepInfo{ parent }
  , m_ui{ std::make_unique<Ui::StepInfoSleevesCollect>() }
{
  m_ui->setupUi(this);
}

StepInfoSleevesCollect::~StepInfoSleevesCollect() = default;

void
StepInfoSleevesCollect::setData(models::steps::ParamsSpecific* new_data)
{
  m_step = dynamic_cast<models::steps::StepSleevesPickup*>(new_data);
  Q_ASSERT(m_step);
  qCDebug(W_SSC) << "setData // loading from" << new_data << "loaded model"
                 << m_step << m_step->toJson();

  loadDataToUi();
  setupConnections();
}

void
StepInfoSleevesCollect::loadDataToUi()
{
  qCDebug(W_SSC) << "load data to ui";
}

void
StepInfoSleevesCollect::setupConnections()
{
  qCDebug(W_SSC) << "setup connections";
}

} // namespace widgets
