#ifndef WIDGETS_MIX_STAGE_INFO_H
#define WIDGETS_MIX_STAGE_INFO_H

#include "json/entity_collection.h"
#include <QGroupBox>

namespace Ui {
class MixStageInfo;
}

namespace models {
class MixPatternTableModel;
} // namespace models

namespace models::steps {
class MixingStage;
class StepMix;
} // namespace models::steps

using Stages = json::EntityCollection<models::steps::MixingStage>;

namespace widgets {
class MixStageDelegate;

class MixStageInfo : public QGroupBox
{
  Q_OBJECT

public:
  explicit MixStageInfo(QWidget* parent = nullptr);
  ~MixStageInfo() override;

private:
  std::unique_ptr<Ui::MixStageInfo> m_ui{ nullptr };

  std::unique_ptr<models::MixPatternTableModel> m_model{ nullptr };
  std::unique_ptr<widgets::MixStageDelegate> m_tableDelegate{ nullptr };

  json::EntityCollection<models::steps::MixingStage>* m_stages{ nullptr };

public:
  void addItem();

  void loadData(Stages* stages);
  [[nodiscard]] Stages* getJson() const;

private:
  void setCurrentIndex(const QModelIndex& index);
  void updateSelected(const QModelIndex& current, const QModelIndex& previous);
  void deleteItem();
  void moveItemUp();
  void moveItemDown();
#ifdef bufferEnabled
  void cutItem();
  void pasteItem();
#endif // bufferEnabled
  void allowEditItem(bool allow);
};

} // namespace widgets
#endif // WIDGETS_MIX_STAGE_INFO_H
