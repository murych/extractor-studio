#include "reagent_volume_spinbox.h"

namespace {
constexpr auto VOLUME_MIN{ 0 };
constexpr auto VOLUME_MAX{ 500 };
const auto VOLUME_SUFFIX{ QObject::tr(" uL") };
}

widgets::ReagentVolumeSpinBox::ReagentVolumeSpinBox(QWidget* parent)
  : QSpinBox{ parent }
{
  setRange(VOLUME_MIN, VOLUME_MAX);
  setAlignment(Qt::AlignVCenter | Qt::AlignRight);
  setSuffix(VOLUME_SUFFIX);
}
