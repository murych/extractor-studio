#ifndef WIDGETS_STEP_INFO_SLEEVES_RELEASE_H
#define WIDGETS_STEP_INFO_SLEEVES_RELEASE_H

#include "abstract_step_widget.h"

namespace Ui {
class StepInfoSleevesRelease;
}

namespace models::steps {
class StepSleevesLeave;
}

namespace widgets {

class StepInfoSleevesRelease : public AbstractStepInfo
{
  Q_OBJECT

public:
  explicit StepInfoSleevesRelease(QWidget* parent = nullptr);
  ~StepInfoSleevesRelease() override;

private:
  std::unique_ptr<Ui::StepInfoSleevesRelease> m_ui{ nullptr };
  models::steps::StepSleevesLeave* m_step{ nullptr };

  // AbstractStepInfo interface
protected:
  void setData(models::steps::ParamsSpecific* new_data) override;
  void setupConnections() override;
  void loadDataToUi() override;
};

} // namespace widgets
#endif // WIDGETS_STEP_INFO_SLEEVES_RELEASE_H
