#ifndef REAGENT_VOLUME_SPINBOX_H
#define REAGENT_VOLUME_SPINBOX_H

#include <QSpinBox>

namespace widgets {
class ReagentVolumeSpinBox : public QSpinBox
{
  Q_OBJECT
public:
  explicit ReagentVolumeSpinBox(QWidget* parent);
  ~ReagentVolumeSpinBox() override = default;
};

} // namespace widgets

#endif // REAGENT_VOLUME_SPINBOX_H
