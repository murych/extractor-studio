#include "device_info.h"
#include "ui_device_info.h"

#include "models/device/device_extractor.h"

using namespace models;

namespace widgets {

DeviceInfo::DeviceInfo(QWidget* parent)
  : AbstractModelWidget{ parent }
  , m_ui{ std::make_unique<Ui::DeviceInfo>() }
  , m_modelData{ std::make_unique<device::DeviceExtractor>() }
{
  m_ui->setupUi(this);
}

DeviceInfo::~DeviceInfo() = default;

void
DeviceInfo::setJson(const QJsonObject& json)
{
  m_modelData = std::make_unique<device::DeviceExtractor>(json);
  createConnections();
  loadDataToUi();
}

QJsonObject
DeviceInfo::getJson() const
{
  Q_ASSERT(m_modelData);
  return m_modelData->toJson();
}

} // namespace widgets
