#ifndef WIDGETS_PROJECT_INFO_H
#define WIDGETS_PROJECT_INFO_H

#include "abstract_model_widget.h"

namespace Ui {
class ProjectInfo;
}

namespace models::project {
class Project;
}

namespace widgets {

class ProjectInfo : public AbstractModelWidget
{
  Q_OBJECT

public:
  explicit ProjectInfo(QWidget* parent = nullptr);
  ~ProjectInfo() override;

  void setJson(const QJsonObject& json) override;
  [[nodiscard]] auto getJson() const -> QJsonObject override;

protected:
  void createConnections() override;
  void loadDataToUi() override;

private:
  std::unique_ptr<Ui::ProjectInfo> m_ui{ nullptr };
  std::unique_ptr<models::project::Project> m_modelData{ nullptr };
};

} // namespace widgets
#endif // WIDGETS_PROJECT_INFO_H
