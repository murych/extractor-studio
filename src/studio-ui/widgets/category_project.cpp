#include "category_project.h"
#include "ui_category_project.h"

#include "models/project/project_info.h"

namespace widgets {

CategoryProject::CategoryProject(QWidget* parent)
  : AbstractModelWidget{ parent }
  , m_ui{ std::make_unique<Ui::CategoryProject>() }
  , m_modelData{ std::make_unique<models::project::Project>() }
{
  m_ui->setupUi(this);
}

CategoryProject::~CategoryProject() = default;

void
CategoryProject::setJson(const QJsonObject& json)
{
  m_modelData = std::make_unique<models::project::Project>(json);
  createConnections();
  loadDataToUi();
}

QJsonObject
CategoryProject::getJson() const
{
  Q_ASSERT(m_modelData);
  return m_modelData->toJson();
}

} // namespace widgets
