#include "step_info.h"
#include "ui_step_info.h"

#include "models/avaliable_plates_model.h"
#include "models/moving_pos_model.h"
#include "models/moving_pos_proxy.h"
#include "models/project/layout.h"
#include "models/step_speed_model.h"
#include "models/steps/params_common.h"
#include "models/steps/params_specific.h"
#include "models/steps/step.h"
#include "widgets/abstract_step_widget.h"
#include "widgets/step_info_beads_collect.h"
#include "widgets/step_info_beads_release.h"
#include "widgets/step_info_dry.h"
#include "widgets/step_info_heater.h"
#include "widgets/step_info_manual_move.h"
#include "widgets/step_info_mix.h"
#include "widgets/step_info_pause.h"
#include "widgets/step_info_sleep.h"
#include "widgets/step_info_sleeves_collect.h"
#include "widgets/step_info_sleeves_release.h"
#include "json/entity_collection.h"
#include "json/enumerator_decorator.h"
#include "json/int_decorator.h"
#include "json/string_decorator.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(W_SI, "W STEP INFO")

using namespace models;

namespace {

[[nodiscard]] auto
createWidget(step_type type) -> widgets::AbstractStepInfo*
{
  switch (type) {
    case step_type::Heater:
      return new widgets::StepInfoHeater;

    case step_type::Pause:
      return new widgets::StepInfoPause;

    case step_type::Pickup:
      return new widgets::StepInfoSleevesCollect;

    case step_type::Leave:
      return new widgets::StepInfoSleevesRelease;

    case step_type::Mix:
      return new widgets::StepInfoMix;

    case step_type::Dry:
      return new widgets::StepInfoDry;

    case step_type::Sleep:
      return new widgets::StepInfoSleep;

    case step_type::Move:
      return new widgets::StepInfoManualMove;

    case step_type::Collect:
      return new widgets::StepInfoBeadsCollect;

    case step_type::Release:
      return new widgets::StepInfoBeadsRelease;

    default:
      return nullptr;
  }
}

}

namespace widgets {

StepInfo::StepInfo(QWidget* parent)
  : AbstractModelWidget{ parent }
  , m_ui{ std::make_unique<Ui::StepInfo>() }
  , m_avaliablePlatesModel{ std::make_unique<models::AvaliablePlatesModel>() }
{
  m_ui->setupUi(this);

  m_ui->sleevesPositionAtEndLineEdit->setModel(
    new models::MovingPosModel{ this });

  m_ui->horizontalExitSpeedComboBox->setModel(
    new models::StepSpeedModel{ this });
  m_ui->verticalExitSpeedComboBox->setModel(new models::StepSpeedModel{ this });
  m_ui->verticalTravelSpeedComboBox->setModel(
    new models::StepSpeedModel{ this });
}

StepInfo::~StepInfo() = default;

void
StepInfo::disableAdvancedParams(bool status)
{
  if (!status) {
    const auto adv_idx{ m_ui->tabWidget->indexOf(m_ui->paramsAdvanced) };
    m_ui->tabWidget->removeTab(adv_idx);
  }
}

void
StepInfo::disableCustomIcon(bool status)
{
  m_ui->stepIconComboBox->setVisible(status);
  m_ui->stepIconLabel->setVisible(status);
}

QJsonObject
StepInfo::getJson() const
{
  return m_modelData->toJson();
}

void
StepInfo::setJson(const QJsonObject& json)
{
  m_modelData = std::make_unique<steps::Step>(json);
  Q_ASSERT(m_modelData);

  auto type{ static_cast<step_type>(m_modelData->type->value()) };

  switch (type) {
    case step_type::Heater:
    case step_type::Pause:
    case step_type::Pickup:
    case step_type::Leave:
    case step_type::Sleep:
      disableAdvancedParams();
      disableCustomIcon();
      break;
    case step_type::Dry:
    case step_type::Move:
    case step_type::Collect:
    case step_type::Release:
      disableCustomIcon();
      break;
    default:
      break;
  }

  // временно, пока у этих этапов ВДРУГ не появятся осознанные параметры
  if (type != step_type::Pickup && type != step_type::Leave) {
    m_paramsSpecific.reset(createWidget(type));
    Q_ASSERT(m_paramsSpecific);

    m_paramsSpecific->setData(m_modelData->specific);

    m_ui->tabWidget->insertTab(0,
                               m_paramsSpecific.get(),
                               QIcon::fromTheme("documentinfo"),
                               tr("General"));
  }

  m_ui->tabWidget->setCurrentIndex(0);

  loadDataToUi();
  createConnections();
}

void
StepInfo::loadAdditionalData(const QJsonObject& json)
{
  // ожидаем информацию о добавленных плашках
  const project::Layout layout;
  if (json.isEmpty() ||
      json.value(layout.plates->getKey()).toArray().isEmpty()) {
    return;
  }

  m_avaliablePlatesModel->loadData(json);

  m_ui->relatedPlateComboBox->setEnabled(true);
  m_ui->relatedPlateComboBox->setModel(m_avaliablePlatesModel.get());

  m_ui->relatedPlateComboBox->setCurrentIndex(m_modelData->plate->value());

  connect(m_ui->relatedPlateComboBox,
          QOverload<int>::of(&QComboBox::currentIndexChanged),
          m_modelData.get(),
          &steps::Step::setRelatedPlate);
}

void
StepInfo::createConnections()
{
  qCDebug(W_SI) << "create connections";

  auto* common{ m_modelData->common };
  Q_ASSERT(common);

#ifndef complexEnum
  connect(m_ui->sleevesPositionAtEndLineEdit,
          QOverload<int>::of(&QComboBox::currentIndexChanged),
          common,
          &steps::ParamsCommon::setSleevesPosAtEnd);
#endif

  connect(m_ui->verticalTravelSpeedComboBox,
          QOverload<int>::of(&QComboBox::currentIndexChanged),
          common,
          &steps::ParamsCommon::setVerticalEnterSpeed);

  connect(m_ui->verticalExitSpeedComboBox,
          QOverload<int>::of(&QComboBox::currentIndexChanged),
          common,
          &steps::ParamsCommon::setVerticalLeaveSpeed);

  connect(m_ui->horizontalExitSpeedComboBox,
          QOverload<int>::of(&QComboBox::currentIndexChanged),
          common,
          &steps::ParamsCommon::setHorizontalSpeed);

  connect(m_ui->stepNameLineEdit, &QLineEdit::editingFinished, this, [this] {
    const auto new_name{ m_ui->stepNameLineEdit->text().simplified() };
    emit onModelTitleChanged(new_name);
  });
  connect(this,
          &StepInfo::onModelTitleChanged,
          m_modelData.get(),
          &steps::Step::setStepName);
}

void
StepInfo::loadDataToUi()
{
  qCDebug(W_SI) << "load data to ui";

  m_ui->stepNameLineEdit->setText(m_modelData->name->value());

  const auto end_pos{ steps::TRAVEL_POSITION_MAPPER.at(
    m_modelData->common->sleevesPosAtEnd->value()) };
  m_ui->sleevesPositionAtEndLineEdit->setCurrentIndex(
    m_ui->sleevesPositionAtEndLineEdit->findText(end_pos));

  m_ui->horizontalExitSpeedComboBox->setCurrentIndex(
    m_modelData->common->horizontalLeaveSpeed->value());
  m_ui->verticalExitSpeedComboBox->setCurrentIndex(
    m_modelData->common->verticalLeaveSpeed->value());
  m_ui->verticalTravelSpeedComboBox->setCurrentIndex(
    m_modelData->common->verticalEnterSpeed->value());
}

void
StepInfo::onSleevesPositionAtEndLineEditActivated(int index)
{
  const auto cur_idx{ m_ui->sleevesPositionAtEndLineEdit->model()->index(index,
                                                                         0) };
  const auto cur_data{ m_ui->sleevesPositionAtEndLineEdit->model()->data(
    cur_idx, Qt::EditRole) };
  qCDebug(W_SI) << cur_data;

  m_modelData->common->sleevesPosAtEnd->setValue(cur_data.toInt());
}

} // namespace widgets
