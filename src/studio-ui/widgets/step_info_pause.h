#ifndef WIDGETS_STEP_INFO_PAUSE_H
#define WIDGETS_STEP_INFO_PAUSE_H

#include "abstract_step_widget.h"

namespace Ui {
class StepInfoPause;
}

namespace models::steps {
class StepPause;
}

namespace widgets {

class StepInfoPause : public AbstractStepInfo
{
  Q_OBJECT

public:
  explicit StepInfoPause(QWidget* parent = nullptr);
  ~StepInfoPause() override;

private:
  std::unique_ptr<Ui::StepInfoPause> m_ui{ nullptr };
  models::steps::StepPause* m_step{ nullptr };

  // AbstractStepInfo interface
protected:
  void setData(models::steps::ParamsSpecific* new_data) override;
  void loadDataToUi() override;
  void setupConnections() override;

signals:
  void onMessageChanged(const QString& message);
};

} // namespace widgets
#endif // WIDGETS_STEP_INFO_PAUSE_H
