#include "step_info_beads_collect.h"
#include "ui_step_info_beads_collect.h"

#include "models/step_speed_model.h"
#include "models/steps/step_beads_collect.h"
#include "json/enumerator_decorator.h"
#include "json/int_decorator.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(W_SBC, "W BEADS COLLECT")

namespace widgets {

StepInfoBeadsCollect::StepInfoBeadsCollect(QWidget* parent)
  : AbstractStepInfo{ parent }
  , m_ui{ std::make_unique<Ui::StepInfoBeadsCollect>() }
{
  m_ui->setupUi(this);
  m_ui->collectSpeedComboBox->setModel(new models::StepSpeedModel{ this });
}

StepInfoBeadsCollect::~StepInfoBeadsCollect() = default;

void
StepInfoBeadsCollect::setData(models::steps::ParamsSpecific* new_data)
{
  m_step = dynamic_cast<models::steps::StepBeadsCollect*>(new_data);
  qCDebug(W_SBC) << "setData // loading from" << new_data << "loaded model"
                 << m_step << m_step->toJson();
  loadDataToUi();
  setupConnections();
}

void
StepInfoBeadsCollect::loadDataToUi()
{
  m_ui->collectSpeedComboBox->setCurrentIndex(m_step->speed->value());
  m_ui->collectCountSpinBox->setValue(m_step->count->value());
  m_ui->collectDurationSpinBox->setValue(m_step->duration->value());
}

void
StepInfoBeadsCollect::setupConnections()
{
  connect(m_ui->collectSpeedComboBox,
          QOverload<int>::of(&QComboBox::currentIndexChanged),
          m_step,
          &models::steps::StepBeadsCollect::setCollectSpeed);
  connect(m_ui->collectCountSpinBox,
          QOverload<int>::of(&QSpinBox::valueChanged),
          m_step,
          &models::steps::StepBeadsCollect::setCollectCount);
  connect(m_ui->collectDurationSpinBox,
          QOverload<int>::of(&QSpinBox::valueChanged),
          m_step,
          &models::steps::StepBeadsCollect::setCollectDuration);
}

} // namespace widgets
