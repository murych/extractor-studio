#include "project_info.h"
#include "ui_project_info.h"

#include "models/project/project_info.h"
#include "json/string_decorator.h"

namespace widgets {

ProjectInfo::ProjectInfo(QWidget* parent)
  : AbstractModelWidget{ parent }
  , m_ui{ std::make_unique<Ui::ProjectInfo>() }
  , m_modelData{ std::make_unique<models::project::Project>() }
{
  m_ui->setupUi(this);
}

ProjectInfo::~ProjectInfo() = default;

void
ProjectInfo::setJson(const QJsonObject& json)
{
  m_modelData = std::make_unique<models::project::Project>(json);
  createConnections();
  loadDataToUi();
}

QJsonObject
ProjectInfo::getJson() const
{
  Q_ASSERT(m_modelData);
  return m_modelData->toJson();
}

void
ProjectInfo::createConnections()
{
  connect(m_ui->projectTitleLineEdit, &QLineEdit::editingFinished, this, [&] {
    m_modelData->title->setValue(m_ui->projectTitleLineEdit->text());
  });
}

void
ProjectInfo::loadDataToUi()
{
  m_ui->projectTitleLineEdit->setText(m_modelData->title->value());
}

} // namespace widgets
