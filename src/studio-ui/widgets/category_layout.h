#ifndef WIDGETS_CATEGORY_LAYOUT_H
#define WIDGETS_CATEGORY_LAYOUT_H

#include "abstract_model_widget.h"

namespace Ui {
class CategoryLayout;
}

namespace models::project {
class Layout;
}

namespace widgets {

class CategoryLayout : public AbstractModelWidget
{
  Q_OBJECT

public:
  explicit CategoryLayout(QWidget* parent = nullptr);
  ~CategoryLayout() override;

private:
  std::unique_ptr<Ui::CategoryLayout> m_ui{ nullptr };
  std::unique_ptr<models::project::Layout> m_modelData{ nullptr };

  // AbstractModelWidget interface
public:
  void setJson(const QJsonObject& json) override;
  [[nodiscard]] auto getJson() const -> QJsonObject override;

protected:
  void createConnections() override {}
  void loadDataToUi() override {}
};

} // namespace widgets
#endif // WIDGETS_CATEGORY_LAYOUT_H
