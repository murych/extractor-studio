#ifndef WIDGETS_DEVICE_INFO_H
#define WIDGETS_DEVICE_INFO_H

#include "abstract_model_widget.h"

namespace Ui {
class DeviceInfo;
}

namespace models::device {
class DeviceExtractor;
}

namespace widgets {

class DeviceInfo : public AbstractModelWidget
{
  Q_OBJECT

public:
  explicit DeviceInfo(QWidget* parent = nullptr);
  ~DeviceInfo() override;

private:
  std::unique_ptr<Ui::DeviceInfo> m_ui{ nullptr };
  std::unique_ptr<models::device::DeviceExtractor> m_modelData{ nullptr };

  // AbstractModelWidget interface
public:
  void setJson(const QJsonObject& json) override;
  [[nodiscard]] auto getJson() const -> QJsonObject override;

protected:
  void createConnections() override {}
  void loadDataToUi() override {}
};

} // namespace widgets
#endif // WIDGETS_DEVICE_INFO_H
