#ifndef WIDGETS_STEP_INFO_SLEEP_H
#define WIDGETS_STEP_INFO_SLEEP_H

#include "abstract_step_widget.h"

namespace Ui {
class StepInfoSleep;
}

namespace models::steps {
class StepSleep;
}

namespace widgets {

class StepInfoSleep : public AbstractStepInfo
{
  Q_OBJECT

public:
  explicit StepInfoSleep(QWidget* parent = nullptr);
  ~StepInfoSleep() override;

private:
  std::unique_ptr<Ui::StepInfoSleep> m_ui{ nullptr };
  models::steps::StepSleep* m_step{ nullptr };

  // AbstractStepInfo interface
protected:
  void setData(models::steps::ParamsSpecific* new_data) override;
  void loadDataToUi() override;
  void setupConnections() override;

signals:
  void onTimeChanged(int time);
};

} // namespace widgets
#endif // WIDGETS_STEP_INFO_SLEEP_H
