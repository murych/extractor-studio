#include "step_info_heater.h"
#include "ui_step_info_heater.h"

#include "models/steps/step_heater.h"
#include "json/bool_decorator.h"
#include "json/double_decorator.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(W_SH, "W STEP HEATER")

using namespace models;

namespace widgets {

StepInfoHeater::StepInfoHeater(QWidget* parent)
  : AbstractStepInfo{ parent }
  , m_ui{ std::make_unique<Ui::StepInfoHeater>() }
{
  m_ui->setupUi(this);
}

StepInfoHeater::~StepInfoHeater() = default;

void
StepInfoHeater::setData(models::steps::ParamsSpecific* new_data)
{
  m_step = dynamic_cast<models::steps::StepHeater*>(new_data);
  qCDebug(W_SH) << "setData // loading from" << new_data << "loaded model"
                << m_step << m_step->toJson();
  loadDataToUi();
  setupConnections();
}

void
StepInfoHeater::loadDataToUi()
{
  m_ui->pauseExecutionUntilHeatedCheckBox->setChecked(
    m_step->blockUntilComplete->value());
  m_ui->targetTemperatureDoubleSpinBox->setValue(
    m_step->targetTemperature->value());
}

void
StepInfoHeater::setupConnections()
{
  connect(m_ui->pauseExecutionUntilHeatedCheckBox,
          &QCheckBox::toggled,
          m_step,
          &steps::StepHeater::setBlockExecution);
  connect(m_ui->targetTemperatureDoubleSpinBox,
          QOverload<double>::of(&QDoubleSpinBox::valueChanged),
          m_step,
          &steps::StepHeater::setTargetTemp);
}

} // namespace widgets
