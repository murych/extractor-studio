#include "sleeves_info.h"
#include "ui_sleeves_info.h"

#include "models/sleeves_type_model.h"
#include <QJsonObject>
#include <QUuid>

namespace widgets {

SleevesInfo::SleevesInfo(QWidget* parent)
  : QGroupBox{ parent }
  , m_ui{ std::make_unique<Ui::SleevesInfo>() }
  , m_sleevesModel{ std::make_unique<models::SleevesTypeModel>() }
{
  m_ui->setupUi(this);
  m_ui->sleevesTypeComboBox->setModel(m_sleevesModel.get());
  connect(m_ui->sleevesTypeComboBox,
          QOverload<int>::of(&QComboBox::activated),
          this,
          &SleevesInfo::onSleevesTypeIndexActivated);

  m_ui->sleevesTypeComboBox->setCurrentIndex(0);
  onSleevesTypeIndexActivated(m_ui->sleevesTypeComboBox->currentIndex());
}

void
SleevesInfo::onSleevesTypeIndexActivated(int idx)
{
  const auto new_idx{ m_ui->sleevesTypeComboBox->currentIndex() };
  const auto current{ m_sleevesModel->index(idx) };

  const auto uuid{
    m_sleevesModel->data(current, models::SleevesTypeModel::UuidRole).toString()
  };
  const auto json{ m_sleevesModel
                     ->data(current, models::SleevesTypeModel::JsonRole)
                     .toJsonObject() };

  emit onSleevesTypeChanged(uuid);
}

SleevesInfo::~SleevesInfo() = default;

} // namespace widgets
