#ifndef ABSTRACT_MODEL_WIDGET_H
#define ABSTRACT_MODEL_WIDGET_H

#include <QWidget>

namespace data {
class Entity;
}

namespace widgets {

class AbstractModelWidget : public QWidget
{
  Q_OBJECT
public:
  using QWidget::QWidget;
  ~AbstractModelWidget() override = default;

  [[nodiscard]] virtual auto getJson() const -> QJsonObject = 0;
  virtual void setJson(const QJsonObject& json) = 0;

  virtual void loadAdditionalData(const QJsonObject& json) { Q_UNUSED(json); }

signals:
  void onModelTitleChanged(const QString& title);

protected:
  virtual void createConnections() = 0;
  virtual void loadDataToUi() = 0;
};

} // namespace widgets

#endif // ABSTRACT_MODEL_WIDGET_H
