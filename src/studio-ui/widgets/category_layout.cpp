#include "category_layout.h"
#include "ui_category_layout.h"

#include "models/project/layout.h"

namespace widgets {

CategoryLayout::CategoryLayout(QWidget* parent)
  : AbstractModelWidget{ parent }
  , m_ui{ std::make_unique<Ui::CategoryLayout>() }
  , m_modelData{ std::make_unique<models::project::Layout>() }
{
  m_ui->setupUi(this);
}

CategoryLayout::~CategoryLayout() = default;

void
CategoryLayout::setJson(const QJsonObject& json)
{
  m_modelData = std::make_unique<models::project::Layout>(json);
  createConnections();
  loadDataToUi();
}

auto
CategoryLayout::getJson() const -> QJsonObject
{
  Q_ASSERT(m_modelData);
  return m_modelData->toJson();
}

} // namespace widgets
