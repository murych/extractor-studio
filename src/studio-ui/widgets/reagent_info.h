#ifndef REAGENT_INFO_H
#define REAGENT_INFO_H

#include <QGroupBox>
#include <QPersistentModelIndex>

namespace Ui {
class ReagentInfo;
} // namespace Ui

namespace models {
class ReagentTableModel;
}

namespace models::plates {
class Reagent;
}

namespace widgets {
class ReagentDelegate;

class ReagentInfo : public QGroupBox
{
  Q_OBJECT

public:
  explicit ReagentInfo(QWidget* parent = nullptr);
  ~ReagentInfo() override;

  // private slots:
  void calculateTotalVolume() const;

  // public slots:
  void addItem();
  void loadData(const QJsonObject& json);
  [[nodiscard]] auto getJson() const -> QJsonObject;

private:
  std::unique_ptr<Ui::ReagentInfo> m_ui{ nullptr };

  std::unique_ptr<models::ReagentTableModel> m_model{ nullptr };
  std::unique_ptr<widgets::ReagentDelegate> m_reagentDelegate{ nullptr };

  void setCurrentIndex(const QModelIndex& index);
  void updateSelected(const QModelIndex& current, const QModelIndex& previous);
  void deleteItem();
  void moveItemUp();
  void moveItemDown();
  void cutItem();
  void pasteItem();
  void allowEditItem(bool allow);
};

} // namespace widgets

#endif // REAGENT_INFO_H
