#include "step_info_sleeves_release.h"
#include "ui_step_info_sleeves_release.h"

#include <models/steps/step_sleeves_leave.h>

#include <QLoggingCategory>
#include <logger.h>
Q_LOGGING_CATEGORY(W_SSR, "W STEP SLEEVES RELEASE")

namespace widgets {

StepInfoSleevesRelease::StepInfoSleevesRelease(QWidget* parent)
  : AbstractStepInfo{ parent }
  , m_ui{ std::make_unique<Ui::StepInfoSleevesRelease>() }
{
  m_ui->setupUi(this);
}

StepInfoSleevesRelease::~StepInfoSleevesRelease() = default;

void
StepInfoSleevesRelease::setData(models::steps::ParamsSpecific* new_data)
{
  m_step = dynamic_cast<models::steps::StepSleevesLeave*>(new_data);
  qCDebug(W_SSR) << "setData // loading from" << new_data << "loaded model"
                 << m_step << m_step->toJson();
  loadDataToUi();
  setupConnections();
}

void
StepInfoSleevesRelease::loadDataToUi()
{
  qCDebug(W_SSR) << "load data to ui";
}

void
StepInfoSleevesRelease::setupConnections()
{
  qCDebug(W_SSR) << "setup connections";
}

} // namespace widgets
