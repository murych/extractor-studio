#ifndef WIDGETS_STEP_INFO_MANUAL_MOVE_H
#define WIDGETS_STEP_INFO_MANUAL_MOVE_H

#include "abstract_step_widget.h"

namespace Ui {
class StepInfoManualMove;
}

namespace models::steps {
class StepManualMove;
}

namespace widgets {

class StepInfoManualMove : public AbstractStepInfo
{
  Q_OBJECT

public:
  explicit StepInfoManualMove(QWidget* parent = nullptr);
  ~StepInfoManualMove() override;

private:
  std::unique_ptr<Ui::StepInfoManualMove> m_ui{ nullptr };
  models::steps::StepManualMove* m_step{ nullptr };

  // AbstractStepInfo interface
protected:
  void setData(models::steps::ParamsSpecific* new_data) override;
  void loadDataToUi() override;
  void setupConnections() override;
};

} // namespace widgets
#endif // WIDGETS_STEP_INFO_MANUAL_MOVE_H
