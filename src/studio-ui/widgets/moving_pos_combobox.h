#ifndef WIDGETS_MOVINGPOSCOMBOBOX_H
#define WIDGETS_MOVINGPOSCOMBOBOX_H

#include "models/moving_pos_model.h"
#include "models/moving_pos_proxy.h"
#include <QComboBox>

namespace widgets {

class MovingPosComboBox : public QComboBox
{
  Q_OBJECT
public:
  explicit MovingPosComboBox(QWidget* parent)
    : QComboBox{ parent }
    , m_source{ std::make_unique<models::MovingPosModel>() }
    , m_proxy{ std::make_unique<models::MovingPosProxy>() }
  {
    m_proxy->setSourceModel(m_source.get());
    setModel(m_proxy.get());
  }
  ~MovingPosComboBox() override = default;

  void setFilter(const QVector<models::steps::ETravelPos>& whitelist)
  {
    m_proxy->setWhiteList(whitelist);
  }

private:
  std::unique_ptr<models::MovingPosModel> m_source{ nullptr };
  std::unique_ptr<models::MovingPosProxy> m_proxy{ nullptr };
};

} // namespace widgets

#endif // WIDGETS_MOVINGPOSCOMBOBOX_H
