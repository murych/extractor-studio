#include "step_info_mix.h"
#include "ui_step_info_mix.h"

#include "models/moving_pos_model.h"
#include "models/step_speed_model.h"
#include "models/steps/moving_position.h"
#include "models/steps/step_mix.h"
#include "json/entity_collection.h"
#include "json/enumerator_decorator.h"
#include "json/int_decorator.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(W_SM, "W STEP MIX")

#define useModels

using namespace models;

namespace widgets {

StepInfoMix::StepInfoMix(QWidget* parent)
  : AbstractStepInfo{ parent }
  , m_ui{ std::make_unique<Ui::StepInfoMix>() }
{
  qCDebug(W_SM) << "constructor"
                << "\tparent =" << parent;
  m_ui->setupUi(this);
  m_ui->enteringLiquidSpeedComboBox->setModel(
    new models::StepSpeedModel{ this });
  m_ui->mixingStartPositionComboBox->setModel(
    new models::MovingPosModel{ this });
}

StepInfoMix::~StepInfoMix() = default;

void
StepInfoMix::setData(models::steps::ParamsSpecific* new_data)
{
  qCDebug(W_SM) << "setData // loading from" << new_data;

  m_step = dynamic_cast<models::steps::StepMix*>(new_data);
  Q_ASSERT(m_step);

  qCDebug(W_SM) << "loaded model" << m_step << m_step->toJson();
  loadDataToUi();
  setupConnections();
}

void
StepInfoMix::loadDataToUi()
{
  m_ui->mixingAmplitudeSpinBox->setValue(m_step->mixAmplitude->value());
  m_ui->patternRepeatCountSpinBox->setValue(m_step->patternRepeat->value());
  m_ui->enteringLiquidSpeedComboBox->setCurrentIndex(
    m_step->enterSpeed->value());
  m_ui->mixingStartPositionComboBox->setCurrentIndex(
    m_ui->mixingStartPositionComboBox->findText(
      steps::TRAVEL_POSITION_MAPPER.at(m_step->endPosition->value())));

  auto* mixing_stages{ dynamic_cast<widgets::MixStageInfo*>(m_ui->groupBox) };
#ifdef useModels
  mixing_stages->loadData(m_step->mixStages);
#else
  //  mixing_stages->loadData(step->toJson());
  mixing_stages->loadData(step);
#endif
}

void
StepInfoMix::setupConnections()
{
  connect(m_ui->patternRepeatCountSpinBox,
          QOverload<int>::of(&QSpinBox::valueChanged),
          m_step,
          &steps::StepMix::setPatternRepeat);

  connect(m_ui->enteringLiquidSpeedComboBox,
          QOverload<int>::of(&QComboBox::currentIndexChanged),
          m_step,
          &steps::StepMix::setEnterSpeed);

  //  connect(ui->mixingStartPositionComboBox,
  //          QOverload<int>::of(&QComboBox::currentIndexChanged),
  //          step,
  //          &steps::StepMix::setStartPosition);

  connect(m_ui->mixingAmplitudeSpinBox,
          QOverload<int>::of(&QSpinBox::valueChanged),
          m_step,
          &steps::StepMix::setMixAmplitude);
}

void
StepInfoMix::onMixingStartPositionComboBoxActivated(int index)
{
  const auto curent_index{ m_ui->mixingStartPositionComboBox->model()->index(
    index, 0) };
  const auto current_data{ m_ui->mixingStartPositionComboBox->model()->data(
    curent_index, Qt::EditRole) };

  m_step->endPosition->setValue(current_data.toInt());
}

} // namespace widgets
