/*
   This file is part of the KDE libraries
   SPDX-FileCopyrightText: 1997 Martin Jones <mjones@kde.org>
   SPDX-FileCopyrightText: 1999 Cristian Tibirna <ctibirna@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "kcolorbutton.h"

#include <QApplication>
#include <QClipboard>
#include <QColorDialog>
#include <QDrag>
#include <QMimeData>
#include <QMouseEvent>
#include <QPainter>
#include <QPointer>
#include <QStyle>
#include <QStyleOptionButton>
#include <qdrawutil.h>

class KColorButtonPrivate
{
public:
  KColorButtonPrivate(KColorButton* qq);

  void chooseColor();
  void colorChosen() const;

  KColorButton* q;
  QColor mDefaultColor;
  bool m_bdefaultColor : 1;
  bool m_alphaChannel : 1;

  QColor col;
  QPoint mPos;

  QPointer<QColorDialog> dialogPtr;

  void initStyleOption(QStyleOptionButton* opt) const;
};

/////////////////////////////////////////////////////////////////////
// Functions duplicated from KColorMimeData
// Should be kept in sync
void
populate_mime_data(QMimeData* mime_data, const QColor& color)
{
  mime_data->setColorData(color);
  mime_data->setText(color.name());
}

bool
can_decode(const QMimeData* mime_data)
{
  if (mime_data->hasColor()) {
    return true;
  }
  if (mime_data->hasText()) {
    const QString color_name = mime_data->text();
    if ((color_name.length() >= 4) && (color_name[0] == QLatin1Char('#'))) {
      return true;
    }
  }
  return false;
}

QColor
from_mime_data(const QMimeData* mime_data)
{
  if (mime_data->hasColor()) {
    return mime_data->colorData().value<QColor>();
  }
  if (can_decode(mime_data)) {
    return QColor(mime_data->text());
  }
  return {};
}

QDrag*
create_drag(const QColor& color, QObject* dragsource)
{
  auto* drag = new QDrag(dragsource);
  auto* mime = new QMimeData;
  populate_mime_data(mime, color);
  drag->setMimeData(mime);
  QPixmap colorpix(25, 20);
  colorpix.fill(color);
  QPainter p(&colorpix);
  p.setPen(Qt::black);
  p.drawRect(0, 0, 24, 19);
  p.end();
  drag->setPixmap(colorpix);
  drag->setHotSpot(QPoint(-5, -7));
  return drag;
}
/////////////////////////////////////////////////////////////////////

KColorButtonPrivate::KColorButtonPrivate(KColorButton* qq)
  : q(qq)
  , m_bdefaultColor(false)
  , m_alphaChannel(false)
{

  q->setAcceptDrops(true);

  QObject::connect(q, &KColorButton::clicked, q, [this]() { chooseColor(); });
}

KColorButton::KColorButton(QWidget* parent)
  : QPushButton(parent)
  , d(new KColorButtonPrivate(this))
{}

KColorButton::KColorButton(const QColor& c, QWidget* parent)
  : QPushButton(parent)
  , d(new KColorButtonPrivate(this))
{
  d->col = c;
}

KColorButton::KColorButton(const QColor& c,
                           const QColor& default_color,
                           QWidget* parent)
  : QPushButton(parent)
  , d(new KColorButtonPrivate(this))
{
  d->col = c;
  setDefaultColor(default_color);
}

KColorButton::~KColorButton() = default;

QColor
KColorButton::color() const
{
  return d->col;
}

void
KColorButton::setColor(const QColor& c)
{
  if (d->col != c) {
    d->col = c;
    update();
    Q_EMIT changed(d->col);
  }

  emit editingFinished();
}

void
KColorButton::setAlphaChannelEnabled(bool alpha)
{
  d->m_alphaChannel = alpha;
}

bool
KColorButton::isAlphaChannelEnabled() const
{
  return d->m_alphaChannel;
}

QColor
KColorButton::defaultColor() const
{
  return d->mDefaultColor;
}

void
KColorButton::setDefaultColor(const QColor& c)
{
  d->m_bdefaultColor = c.isValid();
  d->mDefaultColor = c;
}

void
KColorButtonPrivate::initStyleOption(QStyleOptionButton* opt) const
{
  opt->initFrom(q);
  opt->state |= q->isDown() ? QStyle::State_Sunken : QStyle::State_Raised;
  opt->features = QStyleOptionButton::None;
  if (q->isDefault()) {
    opt->features |= QStyleOptionButton::DefaultButton;
  }
  opt->text.clear();
  opt->icon = QIcon();
}

void
KColorButton::paintEvent(QPaintEvent* /*unused*/)
{
  QPainter painter(this);
  QStyle* style = QWidget::style();

  // First, we need to draw the bevel.
  QStyleOptionButton but_opt;
  d->initStyleOption(&but_opt);
  style->drawControl(QStyle::CE_PushButtonBevel, &but_opt, &painter, this);

  // OK, now we can muck around with drawing out pretty little color box
  // First, sort out where it goes
  QRect label_rect =
    style->subElementRect(QStyle::SE_PushButtonContents, &but_opt, this);
  int shift = style->pixelMetric(QStyle::PM_ButtonMargin, &but_opt, this) / 2;
  label_rect.adjust(shift, shift, -shift, -shift);
  int x = 0;
  int y = 0;
  int w = 0;
  int h = 0;
  label_rect.getRect(&x, &y, &w, &h);

  if (isChecked() || isDown()) {
    x += style->pixelMetric(QStyle::PM_ButtonShiftHorizontal, &but_opt, this);
    y += style->pixelMetric(QStyle::PM_ButtonShiftVertical, &but_opt, this);
  }

  QColor fill_col = isEnabled() ? d->col : palette().color(backgroundRole());
  qDrawShadePanel(&painter, x, y, w, h, palette(), /*sunken=*/true, 1, nullptr);
  if (fill_col.isValid()) {
    const QRect rect(x + 1, y + 1, w - 2, h - 2);
    if (fill_col.alpha() < 255) {
      QPixmap chessboard_pattern(16, 16);
      QPainter pattern_painter(&chessboard_pattern);
      pattern_painter.fillRect(0, 0, 8, 8, Qt::black);
      pattern_painter.fillRect(8, 8, 8, 8, Qt::black);
      pattern_painter.fillRect(0, 8, 8, 8, Qt::white);
      pattern_painter.fillRect(8, 0, 8, 8, Qt::white);
      pattern_painter.end();
      painter.fillRect(rect, QBrush(chessboard_pattern));
    }
    painter.fillRect(rect, fill_col);
  }

  if (hasFocus()) {
    QRect focus_rect =
      style->subElementRect(QStyle::SE_PushButtonFocusRect, &but_opt, this);
    QStyleOptionFocusRect focus_opt;
    focus_opt.initFrom(this);
    focus_opt.rect            = focus_rect;
    focus_opt.backgroundColor = palette().window().color();
    style->drawPrimitive(QStyle::PE_FrameFocusRect, &focus_opt, &painter, this);
  }
}

QSize
KColorButton::sizeHint() const
{
  QStyleOptionButton opt;
  d->initStyleOption(&opt);
  return style()->sizeFromContents(
    QStyle::CT_PushButton, &opt, QSize(40, 15), this);
}

QSize
KColorButton::minimumSizeHint() const
{
  QStyleOptionButton opt;
  d->initStyleOption(&opt);
  return style()->sizeFromContents(
    QStyle::CT_PushButton, &opt, QSize(3, 3), this);
}

void
KColorButton::dragEnterEvent(QDragEnterEvent* event)
{
  event->setAccepted(can_decode(event->mimeData()) && isEnabled());
}

void
KColorButton::dropEvent(QDropEvent* event)
{
  QColor c = from_mime_data(event->mimeData());
  if (c.isValid()) {
    setColor(c);
  }
}

void
KColorButton::keyPressEvent(QKeyEvent* e)
{
  int key = e->key() | e->modifiers();

  if (QKeySequence::keyBindings(QKeySequence::Copy).contains(key)) {
    auto* mime = new QMimeData;
    populate_mime_data(mime, color());
    QApplication::clipboard()->setMimeData(mime, QClipboard::Clipboard);
  } else if (QKeySequence::keyBindings(QKeySequence::Paste).contains(key)) {
    QColor color =
      from_mime_data(QApplication::clipboard()->mimeData(QClipboard::Clipboard));
    setColor(color);
  } else {
    QPushButton::keyPressEvent(e);
  }
}

void
KColorButton::mousePressEvent(QMouseEvent* e)
{
  d->mPos = e->pos();
  QPushButton::mousePressEvent(e);
}

void
KColorButton::mouseMoveEvent(QMouseEvent* e)
{
  if (((e->buttons() & Qt::LeftButton) != 0u) &&
      (e->pos() - d->mPos).manhattanLength() >
        QApplication::startDragDistance()) {
    create_drag(color(), this)->exec();
    setDown(false);
  }
}

void
KColorButtonPrivate::chooseColor()
{
  QColorDialog* dialog = dialogPtr.data();
  if (dialog != nullptr) {
    dialog->show();
    dialog->raise();
    dialog->activateWindow();
    return;
  }

  dialog = new QColorDialog(q);
  dialog->setCurrentColor(q->color());
  dialog->setOption(QColorDialog::ShowAlphaChannel, m_alphaChannel);
  dialog->setAttribute(Qt::WA_DeleteOnClose);
  QObject::connect(dialog, &QDialog::accepted, q, [this]() { colorChosen(); });
  dialogPtr = dialog;
  dialog->show();
}

void
KColorButtonPrivate::colorChosen() const
{
  QColorDialog* dialog = dialogPtr.data();
  if (dialog == nullptr) {
    return;
  }

  if (dialog->selectedColor().isValid()) {
    q->setColor(dialog->selectedColor());
  } else if (m_bdefaultColor) {
    q->setColor(mDefaultColor);
  }
}

#include "moc_kcolorbutton.cpp"
