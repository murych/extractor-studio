#ifndef WIDGETS_STEP_INFO_SLEEVES_COLLECT_H
#define WIDGETS_STEP_INFO_SLEEVES_COLLECT_H

#include "abstract_step_widget.h"

namespace Ui {
class StepInfoSleevesCollect;
}

namespace models::steps {
class StepSleevesPickup;
}

namespace widgets {

class StepInfoSleevesCollect : public AbstractStepInfo
{
  Q_OBJECT

public:
  explicit StepInfoSleevesCollect(QWidget* parent = nullptr);
  ~StepInfoSleevesCollect() override;

private:
  std::unique_ptr<Ui::StepInfoSleevesCollect> m_ui{ nullptr };
  models::steps::StepSleevesPickup* m_step{ nullptr };

  // AbstractStepInfo interface
protected:
  void setData(models::steps::ParamsSpecific* new_data) override;
  void loadDataToUi() override;
  void setupConnections() override;
};

} // namespace widgets
#endif // WIDGETS_STEP_INFO_SLEEVES_COLLECT_H
