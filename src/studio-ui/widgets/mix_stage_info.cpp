#include "mix_stage_info.h"
#include "ui_mix_stage_info.h"

#include "delegates/delegate_mix_stage.h"
#include "models/mix_pattern_tablemodel.h"
#include "models/steps/mixing_stage.h"
#include "models/steps/step_mix.h"
#include <QMessageBox>

#include "logger.h"
#include <QLoggingCategory>
Q_LOGGING_CATEGORY(W_MSI, "W STEP MIX STAGE")

using namespace models;

namespace widgets {

MixStageInfo::MixStageInfo(QWidget* parent)
  : QGroupBox{ parent }
  , m_ui{ std::make_unique<Ui::MixStageInfo>() }
  , m_model{ std::make_unique<MixPatternTableModel>() }
  , m_tableDelegate{ std::make_unique<MixStageDelegate>() }
{
  m_ui->setupUi(this);

  //  ui->b_moveDownMix->setVisible(false);
  //  ui->b_moveUpMix->setVisible(false);

  m_ui->tableView->setModel(m_model.get());
  m_ui->tableView->horizontalHeader()->setSectionResizeMode(
    QHeaderView::Stretch);
  m_ui->tableView->setItemDelegate(m_tableDelegate.get());

  connect(m_ui->tableView->selectionModel(),
          &QItemSelectionModel::currentRowChanged,
          this,
          &MixStageInfo::updateSelected);
  connect(m_ui->b_addMix, &QToolButton::clicked, this, &MixStageInfo::addItem);
  connect(
    m_ui->b_deleteMix, &QToolButton::clicked, this, &MixStageInfo::deleteItem);
  connect(m_ui->b_moveDownMix,
          &QToolButton::clicked,
          this,
          &MixStageInfo::moveItemDown);
  connect(
    m_ui->b_moveUpMix, &QToolButton::clicked, this, &MixStageInfo::moveItemUp);
}

MixStageInfo::~MixStageInfo() = default;

void
MixStageInfo::addItem()
{
  const auto current_idx{ m_ui->tableView->selectionModel()->currentIndex() };
  if (!m_model->insertRow(current_idx.row() + 1, current_idx.parent())) {
    return;
  }

  const auto child_idx{ m_model->index(
    current_idx.row(), current_idx.column(), current_idx.parent()) };
  if (!child_idx.isValid()) {
    return;
  }

  setCurrentIndex(child_idx);
}

void
MixStageInfo::loadData(Stages* stages)
{
  Q_ASSERT(stages);
  m_stages = stages;
  Q_ASSERT(m_stages);
  m_model->setupModelData(m_stages);
}

Stages*
MixStageInfo::getJson() const
{
  return m_stages;
}

void
MixStageInfo::setCurrentIndex(const QModelIndex& index)
{
  if (!index.isValid()) {
    return;
  }

  m_ui->tableView->scrollTo(index);
  m_ui->tableView->setCurrentIndex(index);
}

void
MixStageInfo::updateSelected(const QModelIndex& current,
                             const QModelIndex& previous)
{
  allowEditItem(true);
}

void
MixStageInfo::deleteItem()
{
  const auto index{ m_ui->tableView->selectionModel()->currentIndex() };
  if (!index.isValid()) {
    return;
  }

  const auto name{ m_model->data(index, Qt::DisplayRole).toString() };
  const auto message{ tr("Delete '%1'?").arg(name) };

  QMessageBox question;
  question.setText(message);
  question.setInformativeText(name);
  question.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
  question.setDefaultButton(QMessageBox::Cancel);
  question.setIcon(QMessageBox::Question);

  const auto ret{ question.exec() };
  if (ret == QMessageBox::Cancel) {
    return;
  }

  m_model->removeRow(index.row(), index.parent());
}

void
MixStageInfo::moveItemUp()
{}

void
MixStageInfo::moveItemDown()
{}

#ifdef bufferEnabled
void
MixStageInfo::cutItem()
{
  const QModelIndex index{ ui->tableView->selectionModel()->currentIndex() };
  if (!index.isValid())
    return;
  setCurrentIndex(model->cut(index));
}

void
MixStageInfo::pasteItem()
{
  const QModelIndex index{ ui->tableView->selectionModel()->currentIndex() };
  if (!index.isValid())
    return;
  setCurrentIndex(model->paste(index));
}
#endif // bufferEnabled

void
MixStageInfo::allowEditItem(bool allow)
{
  const auto index{ m_ui->tableView->selectionModel()->currentIndex() };
  if (!index.isValid()) {
    return;
  }

  const auto current_row{ index.row() };
  const auto maximum_row{ m_model->rowCount(index.parent()) };
  const auto is_last{ current_row == maximum_row - 1 };
  const auto is_first{ current_row == 0 };

  m_ui->b_moveDownMix->setEnabled(allow && !is_last);
  m_ui->b_moveUpMix->setEnabled(allow && !is_first);
}

} // namespace widgets
