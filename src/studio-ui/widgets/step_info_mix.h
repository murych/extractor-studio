#ifndef WIDGETS_STEP_INFO_MIX_H
#define WIDGETS_STEP_INFO_MIX_H

#include "abstract_step_widget.h"
#include <QPersistentModelIndex>

namespace Ui {
class StepInfoMix;
}

namespace models::steps {
class StepMix;
}

namespace widgets {

class StepInfoMix : public AbstractStepInfo
{
  Q_OBJECT

public:
  explicit StepInfoMix(QWidget* parent = nullptr);
  ~StepInfoMix() override;

private:
  std::unique_ptr<Ui::StepInfoMix> m_ui{ nullptr };
  models::steps::StepMix* m_step{ nullptr };

  // AbstractStepInfo interface
protected:
  void setData(models::steps::ParamsSpecific* new_data) override;
  void loadDataToUi() override;
  void setupConnections() override;
private slots:
  void onMixingStartPositionComboBoxActivated(int index);
};

} // namespace widgets
#endif // WIDGETS_STEP_INFO_MIX_H
