#ifndef PLATE_INFO_H
#define PLATE_INFO_H

#include "abstract_model_widget.h"

namespace Ui {
class PlateInfo;
}

namespace models {
class AvaliableSeatsModel;
class PlateTypeModel;
}

namespace models::plates {
class Plate;
class Reagent;
}

namespace widgets {

class PlateInfo : public AbstractModelWidget
{
  Q_OBJECT

public:
  PlateInfo(QWidget* parent = nullptr);
  ~PlateInfo() override;

private:
  std::unique_ptr<Ui::PlateInfo> m_ui{ nullptr };
  std::unique_ptr<models::AvaliableSeatsModel> m_seatsModel{ nullptr };
  std::unique_ptr<models::PlateTypeModel> m_platesModel{ nullptr };
  std::unique_ptr<models::plates::Plate> m_modelData{ nullptr };

  enum PlateInfoPages
  {
    Liquids = 0,
    Sleeves
  };

  // AbstractModelWidget interface
public:
  void setJson(const QJsonObject& json) override;
  [[nodiscard]] auto getJson() const -> QJsonObject override;

protected:
  void createConnections() override;
  void loadDataToUi() override;

  // AbstractModelWidget interface
public:
  void loadAdditionalData(const QJsonObject& json) override;

signals:
  void onPlateTypeChanged(const QString& uuid);
};

} // namespace widgets

#endif // PLATE_INFO_H
