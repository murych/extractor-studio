#include "step_info_beads_release.h"
#include "ui_step_info_beads_release.h"

#include "models/step_speed_model.h"
#include "models/steps/step_beads_release.h"
#include "json/enumerator_decorator.h"
#include "json/int_decorator.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(W_SBR, "W BEADS RELEASE")

using namespace models;

namespace widgets {

StepInfoBeadsRelease::StepInfoBeadsRelease(QWidget* parent)
  : AbstractStepInfo{ parent }
  , m_ui{ std::make_unique<Ui::StepInfoBeadsRelease>() }
{
  m_ui->setupUi(this);
  m_ui->releaseSpeedComboBox->setModel(new models::StepSpeedModel{ this });
}

StepInfoBeadsRelease::~StepInfoBeadsRelease() = default;

void
StepInfoBeadsRelease::setData(models::steps::ParamsSpecific* new_data)
{
  m_step = dynamic_cast<models::steps::StepBeadsRelease*>(new_data);
  qCDebug(W_SBR) << "setData // loading from" << new_data << "loaded model"
                 << m_step << m_step->toJson();
  loadDataToUi();
  setupConnections();
}

void
StepInfoBeadsRelease::loadDataToUi()
{
  m_ui->releaseSpeedComboBox->setCurrentIndex(m_step->speed->value());
  m_ui->releaseDurationSpinBox->setValue(m_step->duration->value());
  m_ui->releaseCountSpinBox->setValue(m_step->count->value());
}

void
StepInfoBeadsRelease::setupConnections()
{
  connect(m_ui->releaseSpeedComboBox,
          QOverload<int>::of(&QComboBox::currentIndexChanged),
          m_step,
          &steps::StepBeadsRelease::setReleaseSpeed);
  connect(m_ui->releaseCountSpinBox,
          QOverload<int>::of(&QSpinBox::valueChanged),
          m_step,
          &steps::StepBeadsRelease::setReleaseCount);
  connect(m_ui->releaseDurationSpinBox,
          QOverload<int>::of(&QSpinBox::valueChanged),
          m_step,
          &steps::StepBeadsRelease::setReleaseDuration);
}

} // namespace widgets
