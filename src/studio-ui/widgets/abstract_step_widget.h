#ifndef WIDGETS_ABSTRACTSTEPINFO_H
#define WIDGETS_ABSTRACTSTEPINFO_H

#include <QJsonObject>
#include <QWidget>

namespace data {
class Entity;
}

namespace models::steps {
class ParamsSpecific;
}

namespace widgets {

class AbstractStepInfo : public QWidget
{
  Q_OBJECT
public:
  using QWidget::QWidget;
  ~AbstractStepInfo() override = default;

#ifdef PEREMEN
  void setData(data::Entity* newData)
  {
    qDebug() << "AbstractStepInfo::setData";

    data = newData;

    loadDataToUi();
    setupConnections();
  }
#else
  virtual void setData(models::steps::ParamsSpecific* new_data) = 0;
#endif

protected:
  virtual void setupConnections() = 0;
  virtual void loadDataToUi() = 0;
#ifdef PEREMEN
  data::Entity* data{ nullptr }; // дан свыше
#endif
};

} // namespace widgets

#endif // WIDGETS_ABSTRACTSTEPINFO_H
