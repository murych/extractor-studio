#include "step_info_manual_move.h"
#include "ui_step_info_manual_move.h"

#include "models/step_speed_model.h"
#include "models/steps/step_manual_move.h"
#include "utils/device_manager.h"
#include "json/double_decorator.h"
#include "json/int_decorator.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(W_SMM, "W STEP MANUAL MOVE")

using namespace models;

namespace widgets {

StepInfoManualMove::StepInfoManualMove(QWidget* parent)
  : AbstractStepInfo{ parent }
  , m_ui{ std::make_unique<Ui::StepInfoManualMove>() }
{
  m_ui->setupUi(this);
  m_ui->travelSpeedComboBox->setModel(new models::StepSpeedModel{ this });
}

StepInfoManualMove::~StepInfoManualMove() = default;

void
StepInfoManualMove::setData(models::steps::ParamsSpecific* new_data)
{
  m_step = dynamic_cast<models::steps::StepManualMove*>(new_data);
  Q_ASSERT(m_step);
  qCDebug(W_SMM) << "setData // loading from" << new_data << "loaded model"
                 << m_step << m_step->toJson();
  loadDataToUi();
  setupConnections();
}

void
StepInfoManualMove::loadDataToUi()
{
  m_ui->axisToMoveComboBox->setCurrentIndex(m_step->targetAxis->value());
  utils::DeviceManager manager;
  auto axis_list = manager.getAxisList();
  std::for_each(
    axis_list.begin(), axis_list.end(), [this](const QString& axis) {
      m_ui->axisToMoveComboBox->addItem(axis);
    });
  m_ui->travelDistanceDoubleSpinBox->setValue(m_step->targetPosition->value());
}

void
StepInfoManualMove::setupConnections()
{
  connect(m_ui->axisToMoveComboBox,
          QOverload<int>::of(&QComboBox::currentIndexChanged),
          m_step,
          &steps::StepManualMove::setTargetAxis);
  connect(m_ui->travelDistanceDoubleSpinBox,
          QOverload<double>::of(&QDoubleSpinBox::valueChanged),
          m_step,
          &steps::StepManualMove::setTargetPosition);
}

} // namespace widgets
