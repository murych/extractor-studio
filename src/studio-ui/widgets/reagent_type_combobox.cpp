#include "reagent_type_combobox.h"

#include "models/reagent_type_model.h"

widgets::ReagentTypeComboBox::ReagentTypeComboBox(QWidget* parent)
  : QComboBox{ parent }
  , m_model{ std::make_unique<models::ReagentTypesModel>() }
{
  setModel(m_model.get());
  setCurrentIndex(0);
}

widgets::ReagentTypeComboBox::~ReagentTypeComboBox() = default;
