#ifndef WIDGETS_CATEGORY_PROTOCOL_H
#define WIDGETS_CATEGORY_PROTOCOL_H

#include "abstract_model_widget.h"

namespace Ui {
class CategoryProtocol;
}

namespace models::project {
class Protocol;
}

namespace widgets {

class CategoryProtocol : public AbstractModelWidget
{
  Q_OBJECT

public:
  explicit CategoryProtocol(QWidget* parent = nullptr);
  ~CategoryProtocol() override;

private:
  std::unique_ptr<Ui::CategoryProtocol> m_ui{ nullptr };
  std::unique_ptr<models::project::Protocol> m_modelData{ nullptr };

  // AbstractModelWidget interface
public:
  void setJson(const QJsonObject& json) override;
  [[nodiscard]] auto getJson() const -> QJsonObject override;

protected:
  void createConnections() override {}
  void loadDataToUi() override {}
};

} // namespace widgets
#endif // WIDGETS_CATEGORY_PROTOCOL_H
