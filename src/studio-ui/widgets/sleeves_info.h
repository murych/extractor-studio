#ifndef SLEEVES_INFO_H
#define SLEEVES_INFO_H

#include <QGroupBox>

namespace Ui {
class SleevesInfo;
}

namespace models {
class SleevesTypeModel;
}

namespace widgets {

class SleevesInfo : public QGroupBox
{
  Q_OBJECT

public:
  explicit SleevesInfo(QWidget* parent = nullptr);
  ~SleevesInfo() override;

private:
  std::unique_ptr<Ui::SleevesInfo> m_ui{ nullptr };
  std::unique_ptr<models::SleevesTypeModel> m_sleevesModel{ nullptr };

  void onSleevesTypeIndexActivated(int idx);

signals:
  void onSleevesTypeChanged(const QString& uuid);
};

} // namespace widgets

#endif // SLEEVES_INFO_H
