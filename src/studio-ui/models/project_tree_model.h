#ifndef MODELS_PROJECTTREEMODEL_H
#define MODELS_PROJECTTREEMODEL_H

#include <QAbstractItemModel>
#include <memory>

namespace models {
class ProjectTreeItem;

class ProjectTreeModel : public QAbstractItemModel
{
  Q_OBJECT

public:
  enum Sections
  {
    Project,
    Layout,
    Protocol
  };

  enum SubSections
  {
    Info,
    Device
  };

  explicit ProjectTreeModel(QObject* parent = nullptr);
  ~ProjectTreeModel() override;

  // QAbstractItemModel interface

  [[nodiscard]] auto index(int row, int column, const QModelIndex& parent) const
    -> QModelIndex override;
  [[nodiscard]] auto parent(const QModelIndex& child) const
    -> QModelIndex override;
  [[nodiscard]] auto rowCount(const QModelIndex& parent) const -> int override;
  [[nodiscard]] auto columnCount(const QModelIndex& parent) const
    -> int override;
  [[nodiscard]] auto data(const QModelIndex& index,
                          int role = Qt::DisplayRole) const
    -> QVariant override;
  auto setData(const QModelIndex& index, const QVariant& value, int role)
    -> bool override;
  [[nodiscard]] auto headerData(int section,
                                Qt::Orientation orientation,
                                int role) const -> QVariant override;
  auto insertRows(int row, int count, const QModelIndex& parent)
    -> bool override;
  auto removeRows(int row, int count, const QModelIndex& parent)
    -> bool override;
  [[nodiscard]] auto flags(const QModelIndex& index) const
    -> Qt::ItemFlags override;

  [[nodiscard]] auto supportedDropActions() const -> Qt::DropActions override;
  [[nodiscard]] auto supportedDragActions() const -> Qt::DropActions override;

  QPersistentModelIndex sectionLayout;
  QPersistentModelIndex sectionProtocol;

  void setupModelData(const QJsonObject& json);
  [[nodiscard]] auto itemFromIndex(const QModelIndex& index) const
    -> ProjectTreeItem*;
  [[nodiscard]] auto isItemItem(const QModelIndex& index) const -> bool;
  [[nodiscard]] auto hasCutItem() const { return m_cutItem != nullptr; }

  auto moveUp(const QModelIndex& index) -> QModelIndex;
  auto moveDown(const QModelIndex& index) -> QModelIndex;
  auto cut(const QModelIndex& index) -> QModelIndex;
  auto paste(const QModelIndex& index) -> QModelIndex;

  //! @todo переписать в одну функцию с выбором индекса раздела
  [[nodiscard]] auto getProject() const -> QJsonObject;
  [[nodiscard]] auto getLayout() const -> QJsonObject;
  [[nodiscard]] auto getProtocol() const -> QJsonObject;

  [[nodiscard]] auto getJson() const -> QJsonObject;

  void clear();

  auto addStep(const QModelIndex& index, int type) -> bool;
  auto addPlate(const QModelIndex& index, int type) -> bool;

private:
  QPersistentModelIndex m_root;
  QPersistentModelIndex m_sectionProject;

  auto moveItem(ProjectTreeItem* parent, int old_row, int new_row)
    -> QModelIndex;

  std::unique_ptr<ProjectTreeItem> m_rootItem{ nullptr };
  ProjectTreeItem* m_cutItem{ nullptr };
};

} // namespace models

#endif // MODELS_PROJECTTREEMODEL_H
