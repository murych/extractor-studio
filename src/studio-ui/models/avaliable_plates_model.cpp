#include "avaliable_plates_model.h"

#include "models/plates/plate.h"
#include "models/project/layout.h"
#include "json/entity_collection.h"
#include "json/string_decorator.h"
#include <QIcon>

namespace models {

void
AvaliablePlatesModel::loadData(const QJsonObject& json)
{
  m_data = json;
}

int
AvaliablePlatesModel::rowCount(const QModelIndex& parent) const
{
  const models::project::Layout a_layout{ m_data };
  return a_layout.plates->derivedEntities().count();
}

QVariant
AvaliablePlatesModel::data(const QModelIndex& index, int role) const
{
  if (!index.isValid()) {
    return {};
  }

  const models::project::Layout a_layout{ m_data };
  const auto* plate{ a_layout.plates->derivedEntities().at(index.row()) };

  if (role == Qt::DisplayRole) {
    return plate->name->value();
  }

  if (role == Qt::DecorationRole) {
    return QIcon{ ":/48px/color/grid_view_48px.png" };
  }

  return {};
}

} // namespace models
