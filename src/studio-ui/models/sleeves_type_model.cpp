#include "sleeves_type_model.h"

#include "models/device/sleeves.h"
#include "utils/sleeves_manager.h"
#include "json/string_decorator.h"

namespace models {

SleevesTypeModel::SleevesTypeModel(QObject* parent)
  : QAbstractListModel{ parent }
{
  m_data.clear();
  const utils::SleevesManager manager;
  const auto jsons{ manager.sleeves() };
  std::transform(jsons.cbegin(),
                 jsons.cend(),
                 std::back_inserter(m_data),
                 [](const auto& json) { return new device::Sleeves{ json }; });
}

SleevesTypeModel::~SleevesTypeModel()
{
  qDeleteAll(m_data);
}

int
SleevesTypeModel::rowCount(const QModelIndex& parent) const
{
  Q_UNUSED(parent);
  return m_data.size();
}

QVariant
SleevesTypeModel::data(const QModelIndex& index, int role) const
{
  if (!index.isValid()) {
    return {};
  }

  const auto* sleeve{ m_data.at(index.row()) };
  Q_ASSERT(sleeve);

  switch (role) {
    case Qt::DisplayRole:
      return sleeve->name->value();
    case UuidRole:
      return sleeve->id();
    case JsonRole:
      return sleeve->toJson();
  }

  return {};
}

} // namespace models
