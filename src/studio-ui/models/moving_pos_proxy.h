#ifndef MODELS_MOVINGPOSPROXY_H
#define MODELS_MOVINGPOSPROXY_H

#include "models/steps/moving_position.h"
#include <QSortFilterProxyModel>

namespace models {

class MovingPosProxy : public QSortFilterProxyModel
{
  Q_OBJECT
public:
  using QSortFilterProxyModel::QSortFilterProxyModel;
  ~MovingPosProxy() override = default;

  void setWhiteList(std::vector<steps::ETravelPos>&& new_white_list);

  [[nodiscard]] auto headerData(int section,
                                Qt::Orientation orientation,
                                int role) const -> QVariant override;

protected:
  [[nodiscard]] auto filterAcceptsRow(int source_row,
                                      const QModelIndex& source_parent) const
    -> bool override;

private:
  std::vector<steps::ETravelPos> m_whiteList;
};

} // namespace models

#endif // MODELS_MOVINGPOSPROXY_H
