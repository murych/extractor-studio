#ifndef MODELS_AVALIABLESEATSMODEL_H
#define MODELS_AVALIABLESEATSMODEL_H

#include <QAbstractListModel>
#include <QJsonObject>

namespace models {

namespace device {
class Seat;
}

class AvaliableSeatsModel : public QAbstractListModel
{
  Q_OBJECT
public:
  using QAbstractListModel::QAbstractListModel;
  ~AvaliableSeatsModel() override = default;

  void loadData(const QJsonObject& json);

  // QAbstractItemModel interface

  [[nodiscard]] int rowCount(const QModelIndex& parent) const override;
  [[nodiscard]] QVariant data(const QModelIndex& index,
                              int role) const override;

private:
#define json_models
#ifdef json_models
  QJsonObject m_data{};
#else
  QVector<device::Seat*> m_data{};
#endif
};

} // namespace models

#endif // MODELS_AVALIABLESEATSMODEL_H
