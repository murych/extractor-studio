#include "moving_pos_model.h"

#include "models/steps/moving_position.h"

namespace models {

int
MovingPosModel::rowCount(const QModelIndex& parent) const
{
  Q_UNUSED(parent);
  return static_cast<int>(steps::TRAVEL_POSITION_MAPPER.size());
}

QVariant
MovingPosModel::data(const QModelIndex& index, int role) const
{
  if (!index.isValid()) {
    return {};
  }

  switch (role) {
    case Qt::DisplayRole:
      return steps::TRAVEL_POSITION_MAPPER.at(index.row());
    case Qt::EditRole:
      return index.row();
    default:
      break;
  }

  return {};
}

} // namespace models
