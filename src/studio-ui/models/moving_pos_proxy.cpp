#include "moving_pos_proxy.h"

namespace models {

void
MovingPosProxy::setWhiteList(std::vector<steps::ETravelPos>&& new_white_list)
{
  m_whiteList = new_white_list;
  invalidateFilter();
}

QVariant
MovingPosProxy::headerData(int section,
                           Qt::Orientation orientation,
                           int role) const
{
  return sourceModel()->headerData(section, orientation, role);
}

bool
MovingPosProxy::filterAcceptsRow(int source_row,
                                 const QModelIndex& source_parent) const
{
  const auto req_idx{ sourceModel()->index(source_row, 0, source_parent) };
  const auto req_type{ static_cast<steps::ETravelPos>(
    sourceModel()->data(req_idx, Qt::EditRole).toUInt()) };

  return std::find(std::cbegin(m_whiteList),
                   std::cend(m_whiteList),
                   req_type) != std::end(m_whiteList);
}

} // namespace models
