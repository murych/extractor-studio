#include "mix_pattern_tablemodel.h"

#include "models/steps/mixing_stage.h"
#include "models/steps/step_mix.h"
#include "json/entity_collection.h"
#include "json/enumerator_decorator.h"
#include "json/int_decorator.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(MTM, "MIX MODEL")

namespace models {

MixPatternTableModel::MixPatternTableModel(QObject* parent)
  : QAbstractTableModel{ parent }
  , m_stages{ new Stages }
{}

int
MixPatternTableModel::rowCount(const QModelIndex& parent) const
{
  if (parent.isValid()) {
    return 0;
  }
  Q_ASSERT(m_stages);
  return m_stages->derivedEntities().count();
}

int
MixPatternTableModel::columnCount(const QModelIndex& parent) const
{
  if (parent.isValid()) {
    return 0;
  }

  return Cols::PausePos + 1;
}

QVariant
MixPatternTableModel::data(const QModelIndex& index, int role) const
{
  if (!index.isValid()) {
    return QVariant{};
  }

  Q_ASSERT(m_stages);
  steps::MixingStage* stage{ m_stages->derivedEntities().at(index.row()) };
  Q_ASSERT(stage);

  switch (static_cast<Qt::ItemDataRole>(role)) {
    case Qt::DisplayRole: {
      switch (static_cast<Cols>(index.column())) {
        case Time: {
          QTime time{ 0, 0, 0 };
          time = time.addSecs(stage->duration->value());
          return time;
        }
        case Speed:
          return stage->speed->valueDescription();
        case PauseDur: {
          QTime time{ 0, 0, 0 };
          time = time.addSecs(stage->pauseDuration->value());
          return time;
        }
        case PausePos: {
          return stage->pausePosition->valueDescription();
        }
        default:
          return QVariant{};
      }
      break;
    }

    case Qt::EditRole: {
      switch (static_cast<Cols>(index.column())) {
        case Time: {
          QTime time{ 0, 0, 0 };
          time = time.addSecs(stage->duration->value());
          return time;
        }
        case Speed: {
          return stage->speed->value();
        }
        case PauseDur: {
          QTime time{ 0, 0, 0 };
          time = time.addSecs(stage->pauseDuration->value());
          return time;
        }
        case PausePos: {
          //          QMap<int, QString>::const_iterator iter{
          //            steps::travelPositionMapper.find(stage->pausePosition->value())
          //          };
          //          int i = iter;
          return stage->pausePosition->value();
        }
        default:
          return QVariant{};
      }

      break;
    }

    default:
      break;
  }

  return QVariant{};
}

bool
MixPatternTableModel::setData(const QModelIndex& index,
                              const QVariant& value,
                              int role)
{
  if (!index.isValid() || role != Qt::EditRole) {
    return false;
  }

  qCDebug(MTM) << "setting data on index" << index;

  Q_ASSERT(m_stages);
  auto* stage{ m_stages->derivedEntities().at(index.row()) };

  Q_ASSERT(stage);

  switch (static_cast<Cols>(index.column())) {
    case Time: {
      const QTime start{ 0, 0, 0 };
      const QTime time{ QTime::fromString(value.toString(),
                                          QStringLiteral("HH:mm:ss")) };
      stage->duration->setValue(start.secsTo(time));
      break;
    }
    case Speed: {
      stage->speed->setValue(value.toInt());
      break;
    }
    case PauseDur: {
      const QTime start{ 0, 0, 0 };
      const QTime time{ QTime::fromString(value.toString(),
                                          QStringLiteral("HH:mm:ss")) };
      stage->pauseDuration->setValue(start.secsTo(time));
      break;
    }
    case PausePos: {
      stage->pausePosition->setValue(value.toInt());
      break;
    }
  }

  emit dataChanged(index, index);
  return true;
}

QVariant
MixPatternTableModel::headerData(int section,
                                 Qt::Orientation orientation,
                                 int role) const
{
  if (role != Qt::DisplayRole) {
    return QVariant{};
  }

  const steps::MixingStage stage;

  switch (orientation) {
    case Qt::Horizontal:
      switch (static_cast<Cols>(section)) {
        case Time:
          return stage.duration->label();
        case Speed:
          return stage.speed->label();
        case PauseDur:
          return stage.pauseDuration->label();
        case PausePos:
          return stage.pausePosition->label();
        default:
          return QVariant{};
      }
      break;
    case Qt::Vertical:
      return section + 1;
  }

  return QVariant{};
}

bool
MixPatternTableModel::insertRows(int row, int count, const QModelIndex& parent)
{
  if (parent.isValid()) {
    return false;
  }

  beginInsertRows(QModelIndex{}, row, row + count - 1);

  for (int idx = 0; idx < count; ++idx) {
    Q_ASSERT(m_stages);
    auto* new_item = new steps::MixingStage;
    m_stages->derivedEntities().insert(row, std::move(new_item));
  }
  endInsertRows();

  return true;
}

bool
MixPatternTableModel::removeRows(int row, int count, const QModelIndex& parent)
{
  if (parent.isValid()) {
    return false;
  }

  Q_ASSERT(m_stages);
  if (m_stages->derivedEntities().isEmpty()) {
    return false;
  }

  if (row < 0 || row > m_stages->derivedEntities().count() ||
      (row + count) > m_stages->derivedEntities().count()) {
    return false;
  }

  beginRemoveRows(QModelIndex{}, row, row + count - 1);

  for (int idx = 0; idx < count; ++idx) {
    m_stages->derivedEntities().removeAt(row);
  }

  endRemoveRows();

  return true;
}

Qt::ItemFlags
MixPatternTableModel::flags(const QModelIndex& index) const
{
  return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
}

void
MixPatternTableModel::clear()
{
  Q_ASSERT(m_stages);
  beginResetModel();
  m_stages->clear();
  endResetModel();
}

void
MixPatternTableModel::setupModelData(
  json::EntityCollection<steps::MixingStage>* stages)
{
  Q_ASSERT(stages);
  m_stages = stages;
  Q_ASSERT(m_stages);
}

json::EntityCollection<steps::MixingStage>*
MixPatternTableModel::modelData() const
{
  Q_ASSERT(m_stages);
  return m_stages;
}

bool
MixPatternTableModel::hasCutItem() const
{
  return m_cutItem != nullptr;
}

} // namespace models
