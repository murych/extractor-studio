#ifndef MIXPATTERNTABLEMODEL_H
#define MIXPATTERNTABLEMODEL_H

#include "json/entity_collection.h"
#include <QAbstractTableModel>
#include <QJsonObject>

#define useModels

namespace models::steps {
class MixingStage;
} // namespace models::steps

namespace models {

using Stages = json::EntityCollection<models::steps::MixingStage>;

class MixPatternTableModel : public QAbstractTableModel
{
  Q_OBJECT
public:
  explicit MixPatternTableModel(QObject* parent = nullptr);
  ~MixPatternTableModel() override = default;

  // QAbstractItemModel interface
  [[nodiscard]] int rowCount(const QModelIndex& parent) const override;
  [[nodiscard]] int columnCount(const QModelIndex& parent) const override;
  [[nodiscard]] QVariant data(const QModelIndex& index,
                              int role) const override;
  bool setData(const QModelIndex& index,
               const QVariant& value,
               int role) override;
  [[nodiscard]] QVariant headerData(int section,
                                    Qt::Orientation orientation,
                                    int role) const override;
  bool insertRows(int row, int count, const QModelIndex& parent) override;
  bool removeRows(int row, int count, const QModelIndex& parent) override;
  [[nodiscard]] Qt::ItemFlags flags(const QModelIndex& index) const override;

  void clear();

#ifdef useModels
  void setupModelData(Stages* stages);
  [[nodiscard]] Stages* modelData() const;
  [[nodiscard]] bool hasCutItem() const;
#else
  void setupModelData(const QJsonObject& json);
  QJsonObject modelData() const;
  bool hasCutItem() const { return cutItem.isEmpty(); }
#endif

  [[nodiscard]] QModelIndex cut(const QModelIndex& index);
  [[nodiscard]] QModelIndex paste(const QModelIndex& index);

  enum Cols
  {
    Time = 0,
    Speed,
    PauseDur,
    PausePos
  };

private:
#ifdef useModels
  Stages* m_stages{ nullptr };
  models::steps::MixingStage* m_cutItem{ nullptr };
#else
  QList<QJsonObject> items{};
  QJsonObject cutItem{};
#endif
};

} // namespace models

#endif // MIXPATTERNTABLEMODEL_H
