#ifndef MODELS_SLEEVESTYPEMODEL_H
#define MODELS_SLEEVESTYPEMODEL_H

#include <QAbstractListModel>

namespace models::device {
class Sleeves;
} // namespace device

namespace models {

class SleevesTypeModel : public QAbstractListModel
{
  Q_OBJECT
public:
  explicit SleevesTypeModel(QObject* parent = nullptr);
  ~SleevesTypeModel() override;

  // QAbstractItemModel interface
  [[nodiscard]] auto rowCount(const QModelIndex& parent) const -> int override;
  [[nodiscard]] auto data(const QModelIndex& index, int role) const
    -> QVariant override;

  enum DataRoles
  {
    UuidRole = Qt::UserRole + 100,
    JsonRole
  };
  Q_ENUM(DataRoles)

private:
  std::vector<device::Sleeves*> m_data;
};

} // namespace models

#endif // MODELS_SLEEVESTYPEMODEL_H
