#include "avaliable_seats_model.h"

#include "models/device/device_extractor.h"
#include "models/device/seat.h"
#include "models/project/project_info.h"
#include "utils/device_manager.h"
#include "json/bool_decorator.h"
#include "json/entity_collection.h"
#include "json/enumerator_decorator.h"
#include "json/int_decorator.h"

namespace models {

void
AvaliableSeatsModel::loadData(const QJsonObject& json)
{
  if (json.isEmpty()) {
    return;
  }

  utils::DeviceManager manager;

  const models::project::Project info{ json };
  const auto requested_type{ static_cast<device_type>(
    info.deviceType->value()) };
  const auto requested_family{ static_cast<extractor_model>(
    info.deviceFamily->value()) };

  const QVector<QJsonObject> avaliable_extractors =
    manager.availableTypesInFamily(requested_type);

  models::device::DeviceExtractor extractor;
  QVectorIterator<QJsonObject> iter{ avaliable_extractors };
  while (iter.hasNext()) {
    const auto cur_json{ iter.next() };
    const auto family{ static_cast<extractor_model>(
      cur_json.value(extractor.model->key()).toInt()) };
    if (family == requested_family) {
      extractor.update(cur_json);
    }
  }

  m_data = extractor.toJson();
}

int
AvaliableSeatsModel::rowCount(const QModelIndex& parent) const
{
  Q_UNUSED(parent);
  const models::device::DeviceExtractor extractor{ m_data };
  return extractor.seats->derivedEntities().count();
}

QVariant
AvaliableSeatsModel::data(const QModelIndex& index, int role) const
{
  if (!index.isValid()) {
    return QVariant{};
  }

  const models::device::DeviceExtractor extractor{ m_data };
  auto* seat{ extractor.seats->derivedEntities().at(index.row()) };

  if (role == Qt::DisplayRole) {
    return tr("Seat #%1").arg(seat->idx->value() + 1);
  }

  if (role == Qt::DecorationRole) {
    return seat->hasHeater->value()
             ? QStringLiteral(":/48px/heating_48px.png")
             : QStringLiteral(":/48px/grid_view_48px.png");
  }

  return QVariant{};
}

} // namespace models
