#ifndef REAGENT_TYPE_MODEL_H
#define REAGENT_TYPE_MODEL_H

#include <QAbstractListModel>

namespace models {

class ReagentTypesModel : public QAbstractListModel
{
  Q_OBJECT

public:
  using QAbstractListModel::QAbstractListModel;
  ~ReagentTypesModel() override = default;

  [[nodiscard]] auto rowCount(const QModelIndex& parent) const -> int override;
  [[nodiscard]] auto data(const QModelIndex& index, int role) const
    -> QVariant override;
};

} // namespace models

#endif // REAGENT_TYPE_MODEL_H
