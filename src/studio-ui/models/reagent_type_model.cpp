#include "reagent_type_model.h"

#include "models/plates/reagent.h"
#include "utils/icon_mapper.h"
#include <QIcon>

namespace models {

auto
ReagentTypesModel::rowCount(const QModelIndex& parent) const -> int
{
  Q_UNUSED(parent);
  return 3;
}

auto
ReagentTypesModel::data(const QModelIndex& index, int role) const -> QVariant
{
  if (!index.isValid()) {
    return {};
  }

  switch (role) {
    case Qt::DisplayRole:
      return plates::Reagent::reagent_type_mapper.at(index.row());

    case Qt::EditRole:
      return index.row();

    case Qt::DecorationRole:
      return utils::IconMapper::reagentIcon(index.row());
  }

  return {};
}

} // namespace models
