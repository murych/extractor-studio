#ifndef MODELS_PROJECTTREEITEM_H
#define MODELS_PROJECTTREEITEM_H

#include <QIcon>
#include <QJsonObject>
#include <QVector>
#include <memory>

namespace models {

struct ProjectTreeItem
{
  enum ERoles
  {
    SectionRole = Qt::UserRole + 10,
    DepthRole,
    ContentRole
  };

  enum class ESection
  {
    Unknown,
    Project,
    InfoProject,
    InfoDevice,
    Layout,
    Protocol
  };

  enum class EDepth
  {
    Unknown,
    Root,
    Category,
    Subcategory,
    Item
  };

  explicit ProjectTreeItem(ProjectTreeItem* parent = nullptr);
  ~ProjectTreeItem();

  [[nodiscard]] auto parent() -> ProjectTreeItem*;
  [[nodiscard]] auto child(int number) -> ProjectTreeItem*;
  [[nodiscard]] auto childCount() const -> int;
  [[nodiscard]] auto childIdx() const -> int;
  [[nodiscard]] static auto columnCount() -> int;
  auto insertChildren(int position, int count) -> bool;
  auto removeChildren(int position, int count) -> bool;

  auto setData(const QString& title,
               const QIcon& icon,
               ESection section,
               EDepth depth,
               const QJsonObject& json = {}) -> bool;

  [[nodiscard]] auto takeChild(int row) -> ProjectTreeItem*;
  void insertChild(int row, ProjectTreeItem* item);
  void swapChildren(int old_row, int new_row);
  [[nodiscard]] auto rowOfChild(ProjectTreeItem* child) const -> int;
  [[nodiscard]] auto children() const { return m_childItems; }

  [[nodiscard]] auto title() const { return m_title; }
  [[nodiscard]] auto section() const { return m_section; }
  [[nodiscard]] auto depth() const { return m_depth; }
  [[nodiscard]] auto icon() const { return m_icon; }
  [[nodiscard]] auto content() const { return m_content; }

  void setTitle(const QString& title)
  {
    if (m_title == title) {
      return;
    }
    m_title = title;
  }

  void setContent(const QJsonObject& json)
  {
    if (m_content == json) {
      return;
    }
    m_content = json;
  }

private:
  QString m_title;
  ESection m_section{ ESection::Project };
  EDepth m_depth{ EDepth::Root };
  QIcon m_icon{ ":/icons/icons/bomb.png" };
  QJsonObject m_content;
  ProjectTreeItem* m_parentItem{ nullptr };
  std::vector<ProjectTreeItem*> m_childItems;
};

using tree_role    = ProjectTreeItem::ERoles;
using tree_section = ProjectTreeItem::ESection;
using tree_depth   = ProjectTreeItem::EDepth;

} // namespace models

#endif // MODELS_PROJECTTREEITEM_H
