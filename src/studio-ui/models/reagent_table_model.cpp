#include "reagent_table_model.h"

#include "models/plates/plate.h"
#include "models/plates/reagent.h"
#include "reagent_type_model.h"
#include "utils/icon_mapper.h"
#include "json/entity_collection.h"
#include "json/enumerator_decorator.h"
#include "json/int_decorator.h"
#include "json/string_decorator.h"
#include <QJsonArray>
#include <QJsonDocument>

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(RTM, "REAGENT MODEL")

namespace models {

int
ReagentTableModel::rowCount(const QModelIndex& parent) const
{
  if (parent.isValid()) {
    return 0;
  }

  return items.count();
}

int
ReagentTableModel::columnCount(const QModelIndex& parent) const
{
  if (parent.isValid()) {
    return 0;
  }

  return Cols::Color + 1;
}

QVariant
ReagentTableModel::data(const QModelIndex& index, int role) const
{
  if (!index.isValid()) {
    return {};
  }

  const auto json{ items.at(index.row()) };
  plates::Reagent reagent{ json };

  switch (static_cast<Qt::ItemDataRole>(role)) {
    case Qt::DisplayRole: {
      switch (static_cast<Cols>(index.column())) {
        case Name:
          return reagent.name();
        case Volume:
          return reagent.reagentVolume->value();
        case Color:
          return reagent.reagentColor->value();
        case Type:
          return reagent.reagentType->valueDescription();

        default:
          break;
      }
      break;
    }
    case Qt::DecorationRole: {
      switch (static_cast<Cols>(index.column())) {
        case Type:
          return QIcon{ utils::IconMapper::reagentIcon(index.row()) };
        case Color:
          return QColor{ reagent.reagentColor->value() };
        default:
          break;
      }
      break;
    }
    default:
      break;
  }

  return {};
}

bool
ReagentTableModel::setData(const QModelIndex& index,
                           const QVariant& value,
                           int role)
{
  //  qCDebug(RTM) << "setData" << index << value << role;

  if (!index.isValid() || static_cast<Qt::ItemDataRole>(role) != Qt::EditRole) {
    return false;
  }

#ifdef useModels
  plates::Reagent* reagent = items.at(index.row());
  Q_ASSERT(reagent);
#else
  plates::Reagent reagent{ items.at(index.row()) };
#endif

  switch (static_cast<Cols>(index.column())) {
    case Name:
      reagent.reagentName->setValue(value.toString());
      break;
    case Volume:
      reagent.reagentVolume->setValue(value.toInt());
      break;
    case Color:
      reagent.reagentColor->setValue(value.toString());
      break;
    case Type:
      reagent.reagentType->setValue(value.toInt());
      break;
  }

  items.replace(index.row(), reagent.toJson());

  emit dataChanged(index, index, { Qt::DisplayRole, Qt::EditRole });
  return true;
}

QVariant
ReagentTableModel::headerData(int section,
                              Qt::Orientation orientation,
                              int role) const
{
  if (role != Qt::DisplayRole) {
    return {};
  }

  const plates::Reagent reagent;

  switch (orientation) {
    case Qt::Horizontal: {
      switch (static_cast<Cols>(section)) {
        case Name:
          return reagent.reagentName->label();
        case Volume:
          return reagent.reagentVolume->label();
        case Color:
          return reagent.reagentColor->label();
        case Type:
          return reagent.reagentType->label();
      }
    }
    case Qt::Vertical:
      return QString::number(section + 1);
  }

  return {};
}

bool
ReagentTableModel::insertRows(int row, int count, const QModelIndex& parent)
{
  if (parent.isValid()) {
    return false;
  }

  beginInsertRows({}, row, row + count - 1);

  for (int idx = 0; idx < count; ++idx) {
#ifdef useModels
    auto* newItem = new plates::Reagent;

    if (items.isEmpty())
      items.append(newItem);
    else
      items.insert(row, newItem);
#else
    const plates::Reagent reagent;
    if (items.isEmpty()) {
      items.append(reagent.toJson());
    } else {
      items.insert(row, reagent.toJson());
    }
#endif
  }

  endInsertRows();

  return true;
}

bool
ReagentTableModel::removeRows(int row, int count, const QModelIndex& parent)
{
  if (parent.isValid()) {
    return false;
  }

  if (items.isEmpty()) {
    return false;
  }

  if (row < 0 || row > items.count() || (row + count) > items.count()) {
    return false;
  }

  beginRemoveRows({}, row, row + count - 1);
  for (int idx = 0; idx < count; ++idx) {
    items.removeAt(row);
  }
  endRemoveRows();

  return true;
}

Qt::ItemFlags
ReagentTableModel::flags(const QModelIndex& index) const
{
  return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
}

void
ReagentTableModel::clear()
{
#ifdef useModels
  qDeleteAll(items);
  items.clear();
  delete cutItem;
  cutItem = nullptr;
#endif
  items.clear();
  beginResetModel();
  endResetModel();
}

void
ReagentTableModel::setupModelData(const QJsonObject& json)
{
  qCDebug(RTM) << "setupModelData" << json;
  clear();
  const plates::Plate plate{ json };
  QListIterator<plates::Reagent*> iter{ plate.reagents->derivedEntities() };

  beginInsertRows(QModelIndex{}, items.count(), items.count());
  while (iter.hasNext()) {
#ifdef useModels
    items.append(iter.next());
#else
    items.append(iter.next()->toJson());
#endif
  }
  endInsertRows();
}

QJsonObject
ReagentTableModel::modelData() const
{
  qCDebug(RTM) << "modelData";
#ifdef useModels
  const plates::Plate aPlate;
  QJsonObject result{};
  QJsonArray array{};
  QListIterator<plates::Reagent*> iter{ items };
  while (iter.hasNext())
    array.append(iter.next()->toJson());

  result[aPlate.reagents->getKey()] = array;
  qCDebug(RTM) << "\t " << result;
#else
  const plates::Plate a_plate;
  QJsonArray array;
  QListIterator<QJsonObject> iter{ items };
  while (iter.hasNext()) {
    array.append(iter.next());
  }
  //  result[aPlate.reagents->getKey()] = array;

  //  QJsonObject result{};
  //  result.insert(aPlate.reagents->getKey(), array);

  const QJsonObject result{ { a_plate.reagents->getKey(), array } };
#endif
  return result;
}

// QModelIndex
// ReagentTableModel::moveUp(const QModelIndex& index)
//{
//  if (!index.isValid() || index.row() <= 0)
//    return index;

//  plates::Reagent* item{ itemFromIndex(index) };
//  Q_ASSERT(item);
//}

// QModelIndex
// ReagentTableModel::moveDown(const QModelIndex& index)
//{}

QModelIndex
ReagentTableModel::cut(const QModelIndex& index)
{
  if (!index.isValid()) {
    return index;
  }
  int row{ index.row() };
#ifdef useModels
  delete cutItem;
#else
  cutItem = QJsonObject{};
#endif
  beginRemoveRows(index.parent(), row, row);
  cutItem = items.takeAt(row);
  endRemoveRows();
  //  Q_ASSERT(cutItem);

  if (row > 0) {
    --row;
    return createIndex(row, 0 /*, items.at(row)*/);
  }

  return QModelIndex{};
}

QModelIndex
ReagentTableModel::paste(const QModelIndex& index)
{
  if (!index.isValid() || cutItem.isEmpty()) {
    return index;
  }

  const int row{ index.row() + 1 };

  beginInsertRows(index.parent(), row, row);
  items.insert(row, cutItem);
#ifdef useModels
  plates::Reagent* child = cutItem;
  cutItem                = nullptr;
#else
  cutItem = QJsonObject{};
#endif
  endInsertRows();

  return createIndex(row, 0 /*, child*/);
}

#ifdef moveReady
bool
ReagentTableModel::moveRows(const QModelIndex& sourceParent,
                            int sourceRow,
                            int count,
                            const QModelIndex& destinationParent,
                            int destinationChild)
{
}
#endif

} // namespace models
