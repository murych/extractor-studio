#include "project_tree_model.h"

#include "models/plates/plate.h"
#include "models/project/extractor_project.h"
#include "models/project/layout.h"
#include "models/project/project_info.h"
#include "models/project/protocol.h"
#include "models/steps/step.h"
#include "project_tree_item.h"
#include "utils/icon_mapper.h"
#include "json/entity_collection.h"
#include "json/string_decorator.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(PTM, "PROJECT TREE MODEL")

namespace {
constexpr auto COLUMN{ 0 };
}

namespace models {

ProjectTreeModel::ProjectTreeModel(QObject* parent)
  : QAbstractItemModel{ parent }
  , m_rootItem{ std::make_unique<ProjectTreeItem>() }
{}

ProjectTreeModel::~ProjectTreeModel()
{
  delete m_cutItem;
}

QModelIndex
ProjectTreeModel::index(int row, int column, const QModelIndex& parent) const
{
  if (parent.isValid() && parent.column() != 0) {
    return {};
  }

  auto* parent_item{ itemFromIndex(parent) };
  if (parent_item == nullptr) {
    return {};
  }

  auto* child_item{ parent_item->child(row) };
  if (child_item != nullptr) {
    return createIndex(row, column, child_item);
  }

  return {};
}

QModelIndex
ProjectTreeModel::parent(const QModelIndex& child) const
{
  if (!child.isValid()) {
    return {};
  }

  auto* child_item{ itemFromIndex(child) };
  auto* parent_item{ child_item != nullptr ? child_item->parent() : nullptr };

  if (parent_item == m_rootItem.get() || parent_item == nullptr) {
    return {};
  }

  return createIndex(parent_item->childIdx(), 0, parent_item);
}

int
ProjectTreeModel::rowCount(const QModelIndex& parent) const
{
  const auto* parent_item{ itemFromIndex(parent) };
  return parent_item != nullptr ? parent_item->childCount() : 0;
}

int
ProjectTreeModel::columnCount(const QModelIndex& parent) const
{
  Q_UNUSED(parent)
  return m_rootItem->columnCount();
}

QVariant
ProjectTreeModel::data(const QModelIndex& index, int role) const
{
  if (!index.isValid()) {
    return {};
  }

  auto* const item{ itemFromIndex(index) };
  switch (role) {
    case Qt::DisplayRole:
    case Qt::EditRole:
      return item->title();
    case Qt::DecorationRole:
      return item->icon();
    case tree_role::SectionRole:
      return static_cast<uint>(item->section());
    case tree_role::DepthRole:
      return static_cast<uint>(item->depth());
    case tree_role::ContentRole:
      return item->content();
    default:
      break;
  }

  return {};
}

bool
ProjectTreeModel::setData(const QModelIndex& index,
                          const QVariant& value,
                          int role)
{
  auto* item{ itemFromIndex(index) };

  switch (role) {
    case Qt::EditRole: {
      item->setTitle(value.toString());
      emit dataChanged(index, index, { Qt::DisplayRole, Qt::EditRole });
      return true;
    }

    case tree_role::ContentRole: {
      item->setContent(value.toJsonObject());
      emit dataChanged(
        index, index, { Qt::DisplayRole, tree_role::ContentRole });
      return true;
    }

    default:
      break;
  }

  return false;
}

QVariant
ProjectTreeModel::headerData(int section,
                             Qt::Orientation orientation,
                             int role) const
{
  if (orientation == Qt::Horizontal && role == Qt::DisplayRole) {
    return QStringLiteral("Header");
  }

  return {};
}

bool
ProjectTreeModel::insertRows(int row, int count, const QModelIndex& parent)
{
  auto* parent_item{ itemFromIndex(parent) };
  if (parent_item == nullptr) {
    return false;
  }

  beginInsertRows(parent, row, row + count - 1);
  const auto success{ parent_item->insertChildren(row, count) };
  endInsertRows();

  return success;
}

bool
ProjectTreeModel::removeRows(int row, int count, const QModelIndex& parent)
{
  auto* parent_item{ itemFromIndex(parent) };
  if (parent_item == nullptr) {
    return false;
  }

  beginRemoveRows(parent, row, row + count - 1);
  const auto success{ parent_item->removeChildren(row, count) };
  endRemoveRows();

  return success;
}

Qt::ItemFlags
ProjectTreeModel::flags(const QModelIndex& index) const
{
  if (!index.isValid()) {
    return Qt::NoItemFlags;
  }

  auto the_flags{ QAbstractItemModel::flags(index) };
  the_flags |= Qt::ItemIsSelectable | Qt::ItemIsEnabled;

  if (isItemItem(index)) {
    the_flags |= Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled;
  }

  return the_flags;
}

void
ProjectTreeModel::setupModelData(const QJsonObject& json)
{
  Q_ASSERT(m_rootItem);
  m_rootItem->insertChildren(0, Protocol + 1);

  m_sectionProject = index(Project, COLUMN, m_root);
  sectionLayout    = index(Layout, COLUMN, m_root);
  sectionProtocol  = index(Protocol, COLUMN, m_root);

  auto* item_project{ m_rootItem->child(Sections::Project) };
  Q_ASSERT(item_project);
  item_project->setData(tr("Project"),
                        QIcon::fromTheme("labplot-workbook"),
                        tree_section::Project,
                        tree_depth::Category);
  item_project->insertChildren(0, 2);

  auto* item_device{ item_project->child(Device) };
  item_device->setData(tr("Device Info"),
                       QIcon::fromTheme("uav"),
                       tree_section::InfoDevice,
                       tree_depth::Subcategory);

  auto* item_info{ item_project->child(Info) };
  const project::Project abstract_project;
  item_info->setData(tr("Project Info"),
                     QIcon::fromTheme("documentinfo"),
                     tree_section::InfoProject,
                     tree_depth::Subcategory,
                     json.contains(abstract_project.key())
                       ? json.value(abstract_project.key()).toObject()
                       : abstract_project.toJson());

  auto* item_layout{ m_rootItem->child(Layout) };
  Q_ASSERT(item_layout);
  item_layout->setData(tr("Layout"),
                       QIcon::fromTheme("show-grid"),
                       tree_section::Layout,
                       tree_depth::Category);

  const project::Layout abstract_layout;
  if (json.contains(abstract_layout.key()) &&
      json.value(abstract_layout.key()).isObject()) {
    const auto layout_json{ json.value(abstract_layout.key()).toObject() };

    if (layout_json.contains(abstract_layout.plates->getKey()) &&
        layout_json.value(abstract_layout.plates->getKey()).isArray()) {

      const auto plates_json{
        layout_json.value(abstract_layout.plates->getKey()).toArray()
      };

      const auto plates_count{ plates_json.count() };
      item_layout->insertChildren(0, plates_count);

      for (int plate_idx = 0; plate_idx < plates_count; ++plate_idx) {
        const auto plate_json{ plates_json.at(plate_idx).toObject() };
        auto* plate_item{ item_layout->child(plate_idx) };
        plate_item->setData(plate_json.value("title").toString(),
                            QIcon{ ":/48px/color/grid_view_48px.png" },
                            tree_section::Layout,
                            tree_depth::Item,
                            plate_json);
      }
    }
  }

  auto* item_protocol{ m_rootItem->child(Protocol) };
  Q_ASSERT(item_protocol);
  item_protocol->setData(tr("Protocol"),
                         QIcon::fromTheme("view-list-details"),
                         tree_section::Protocol,
                         tree_depth::Category);

  const project::Protocol abstract_protocol;
  if (json.contains(abstract_protocol.key()) &&
      json.value(abstract_protocol.key()).isObject()) {
    const auto protocol_json{ json.value(abstract_protocol.key()).toObject() };
    if (protocol_json.contains(abstract_protocol.steps->getKey()) &&
        protocol_json.value(abstract_protocol.steps->getKey()).isArray()) {
      const auto steps_array{
        protocol_json.value(abstract_protocol.steps->getKey()).toArray()
      };
      const auto steps_count{ steps_array.count() };

      item_protocol->insertChildren(0, steps_count);

      for (int idx = 0; idx < steps_count; ++idx) {
        const auto step_json{ steps_array.at(idx).toObject() };
        const auto step_name{ step_json.value("title").toString() };
        const auto step_type{ step_json.value("type").toInt() };
        auto* step_item{ item_protocol->child(idx) };
        step_item->setData(step_name,
                           utils::IconMapper::stepIcon(step_type),
                           tree_section::Protocol,
                           tree_depth::Item,
                           step_json);
      }
    }
  }
}

ProjectTreeItem*
ProjectTreeModel::itemFromIndex(const QModelIndex& index) const
{
  if (!index.isValid()) {
    return m_rootItem.get();
  }

  auto* item{ static_cast<ProjectTreeItem*>(index.internalPointer()) };
  return item;
}

Qt::DropActions
ProjectTreeModel::supportedDropActions() const
{
  return Qt::MoveAction;
}

Qt::DropActions
ProjectTreeModel::supportedDragActions() const
{
  return Qt::MoveAction;
}

QModelIndex
ProjectTreeModel::moveUp(const QModelIndex& index)
{
  if (!index.isValid() || index.row() <= 0) {
    return index;
  }

  if (!isItemItem(index)) {
    return index;
  }

  auto* item{ itemFromIndex(index) };
  Q_ASSERT(item);

  auto* parent{ item->parent() };
  Q_ASSERT(parent);

  return moveItem(parent, index.row(), index.row() - 1);
}

QModelIndex
ProjectTreeModel::moveDown(const QModelIndex& index)
{
  if (!index.isValid()) {
    return index;
  }

  if (!isItemItem(index)) {
    return index;
  }

  auto* item{ itemFromIndex(index) };
  Q_ASSERT(item);

  auto* parent{ item->parent() };
  Q_ASSERT(parent);

  const auto new_row{ index.row() + 1 };

  if (parent == nullptr || parent->childCount() <= new_row) {
    return index;
  }

  return moveItem(parent, index.row(), new_row);
}

QModelIndex
ProjectTreeModel::cut(const QModelIndex& index)
{
  if (!index.isValid()) {
    return index;
  }

  delete m_cutItem;
  m_cutItem = itemFromIndex(index);
  Q_ASSERT(m_cutItem);

  auto* parent{ m_cutItem->parent() };
  Q_ASSERT(parent);

  auto row{ parent->rowOfChild(m_cutItem) };
  Q_ASSERT(row == index.row());

  beginRemoveRows(index.parent(), row, row);
  auto* child{ parent->takeChild(row) };
  endRemoveRows();

  Q_ASSERT(child == m_cutItem);
  child = nullptr;

  if (row > 0) {
    --row;
    return createIndex(row, 0, parent->child(row));
  }

  if (parent != m_rootItem.get()) {
    auto* grand_parent{ parent->parent() };
    Q_ASSERT(grand_parent);
    return createIndex(grand_parent->rowOfChild(parent), 0, parent);
  }

  return {};
}

QModelIndex
ProjectTreeModel::paste(const QModelIndex& index)
{
  if (!index.isValid() || m_cutItem == nullptr) {
    return index;
  }

  if (!isItemItem(index)) {
    return index;
  }

  auto* sibling{ itemFromIndex(index) };
  Q_ASSERT(sibling);

  if (m_cutItem->section() != sibling->section()) {
    return index;
  }

  auto* parent{ sibling->parent() };
  Q_ASSERT(parent);

  auto row{ parent->rowOfChild(sibling) + 1 };

  beginInsertRows(index.parent(), row, row);
  parent->insertChild(row, m_cutItem);
  auto* child{ m_cutItem };
  m_cutItem = nullptr;
  endInsertRows();

  return createIndex(row, 0, child);
}

QJsonObject
ProjectTreeModel::getProject() const
{
  const auto pinfo{ index(Info, COLUMN, m_sectionProject) };
  Q_ASSERT(pinfo.isValid());
  auto* info_item{ itemFromIndex(pinfo) };
  Q_ASSERT(info_item);

  return info_item->content();
}

QJsonObject
ProjectTreeModel::getLayout() const
{
  auto* layout_item{ itemFromIndex(sectionLayout) };
  Q_ASSERT(layout_item);
  const auto children{ layout_item->children() };

  project::Layout layout;

  std::for_each(children.cbegin(), children.cend(), [&layout](const auto& it) {
    auto* plate{ new plates::Plate{ it->content() } };
    layout.plates->addEntity(plate);
  });

  return layout.toJson();
}

QJsonObject
ProjectTreeModel::getProtocol() const
{
  auto* protocol_item{ itemFromIndex(sectionProtocol) };
  Q_ASSERT(protocol_item);
  const auto children{ protocol_item->children() };

  project::Protocol protocol;
  int counter{ 0 };

  std::for_each(children.cbegin(), children.cend(), [&](const auto& it) {
    auto* step{ new steps::Step{ it->content() } };
    step->setStepIdx(++counter);
    protocol.steps->addEntity(step);
  });

  return protocol.toJson();
}

QJsonObject
ProjectTreeModel::getJson() const
{
  project::ExtractorProject project;
  project.info->update(getProject());
  project.layout->update(getLayout());
  project.protocol->update(getProtocol());

  return project.toJson();
}

void
ProjectTreeModel::clear()
{
  m_rootItem = std::make_unique<ProjectTreeItem>();
  delete m_cutItem;
  m_cutItem = nullptr;

  beginResetModel();
  endResetModel();
}

QModelIndex
ProjectTreeModel::moveItem(ProjectTreeItem* parent, int old_row, int new_row)
{
  Q_ASSERT(0 <= old_row && old_row < parent->childCount() && 0 <= new_row &&
           new_row < parent->childCount());

  parent->swapChildren(old_row, new_row);
  auto old_index{ createIndex(old_row, 0, parent->child(old_row)) };
  auto new_index{ createIndex(new_row, 0, parent->child(new_row)) };

  emit dataChanged(old_index, new_index);

  return new_index;
}

bool
ProjectTreeModel::isItemItem(const QModelIndex& index) const
{
  return static_cast<tree_depth>(data(index, tree_role::DepthRole).toUInt()) ==
         tree_depth::Item;
}

auto
ProjectTreeModel::addStep(const QModelIndex& index, int type) -> bool
{
  auto* item_protocol{ m_rootItem->child(Protocol) };
  if (item_protocol == nullptr) {
    return false;
  }
  auto* item_step{ item_protocol->child(index.row()) };
  if (item_step == nullptr) {
    return false;
  }

  const steps::Step step{ static_cast<steps::Step::EStepType>(type) };
  item_step->setData(step.getStepName(),
                     utils::IconMapper::stepIcon(step.getStepType()),
                     tree_section::Protocol,
                     tree_depth::Item,
                     step.toJson());
  return true;
}

bool
ProjectTreeModel::addPlate(const QModelIndex& index, int type)
{
  auto* item_layout{ m_rootItem->child(Layout) };
  if (item_layout == nullptr) {
    return false;
  }
  auto* item_plate{ item_layout->child(index.row()) };
  if (item_plate == nullptr) {
    return false;
  }

  const plates::Plate plate;
  item_plate->setData(plate.name->value(),
                      QIcon{ ":/48px/color/grid_view_48px.png" },
                      tree_section::Layout,
                      tree_depth::Item,
                      plate.toJson());
  return true;
}

} // namespace models
