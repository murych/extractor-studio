#include "plate_type_model.h"

#include "models/device/plate.h"
#include "utils/plate_manager.h"
#include "json/string_decorator.h"

namespace models {

PlateTypeModel::PlateTypeModel(QObject* parent)
  : QAbstractListModel{ parent }
{
  m_data.clear();
  const utils::PlateManager manager;
  const auto jsons{ manager.plates() };
  std::transform(jsons.cbegin(),
                 jsons.cend(),
                 std::back_inserter(m_data),
                 [](const auto& json) { return new device::Plate{ json }; });
}

PlateTypeModel::~PlateTypeModel()
{
  qDeleteAll(m_data);
}

int
PlateTypeModel::rowCount(const QModelIndex& parent) const
{
  Q_UNUSED(parent);
  return m_data.size();
}

QVariant
PlateTypeModel::data(const QModelIndex& index, int role) const
{
  if (!index.isValid()) {
    return {};
  }

  const auto* plate{ m_data.at(index.row()) };
  Q_ASSERT(plate);

  switch (role) {
    case Qt::DisplayRole:
      return plate->name->value();
    case UuidRole:
      return plate->id();
    case JsonRole:
      return plate->toJson();
  }

  return {};
}

} // namespace models
