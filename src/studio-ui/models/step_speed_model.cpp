#include "step_speed_model.h"

#include "models/steps/moving_speed.h"
#include <QIcon>

namespace {
const std::unordered_map<models::steps::ETravelSpeed, QString>
  SPEED_ICON_MAPPER{ { models::steps::ETravelSpeed::Unknown, QLatin1String{} },
                     { models::steps::ETravelSpeed::Slow,
                       QStringLiteral(":/48px/color/inactive_state_48px.png") },
                     { models::steps::ETravelSpeed::Medium,
                       QStringLiteral(":/48px/color/active_state_48px.png") },
                     { models::steps::ETravelSpeed::Fast,
                       QStringLiteral(":/48px/color/final_state_48px.png") } };
}

namespace models {

auto
StepSpeedModel::rowCount(const QModelIndex& parent) const -> int
{
  Q_UNUSED(parent);
  return steps::TRAVEL_SPEED_MAPPER.size();
}

auto
StepSpeedModel::data(const QModelIndex& index, int role) const -> QVariant
{
  if (!index.isValid()) {
    return {};
  }

  switch (role) {
    case Qt::DisplayRole:
      return steps::TRAVEL_SPEED_MAPPER.at(index.row());

    case Qt::EditRole:
      return index.row();

    case Qt::DecorationRole:
      return QIcon{ SPEED_ICON_MAPPER.at(
        static_cast<steps::ETravelSpeed>(index.row())) };
  }

  return {};
}

} // namespace models
