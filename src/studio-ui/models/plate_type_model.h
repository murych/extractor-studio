#ifndef MODELS_PLATETYPEMODEL_H
#define MODELS_PLATETYPEMODEL_H

#include <QAbstractListModel>

namespace models {

namespace device {
class Plate;
}

class PlateTypeModel : public QAbstractListModel
{
  Q_OBJECT
public:
  explicit PlateTypeModel(QObject* parent = nullptr);
  ~PlateTypeModel() override;

  // QAbstractItemModel interface
  [[nodiscard]] int rowCount(const QModelIndex& parent) const override;
  [[nodiscard]] QVariant data(const QModelIndex& index,
                              int role) const override;

  enum DataRoles
  {
    UuidRole = Qt::UserRole + 100,
    JsonRole
  };
  Q_ENUM(DataRoles)

private:
  std::vector<device::Plate*> m_data;
};

} // namespace models

#endif // MODELS_PLATETYPEMODEL_H
