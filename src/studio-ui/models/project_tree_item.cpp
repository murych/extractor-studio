#include "project_tree_item.h"

namespace models {

ProjectTreeItem::ProjectTreeItem(ProjectTreeItem* parent)
  : m_parentItem{ parent }
{}

ProjectTreeItem::~ProjectTreeItem()
{
  qDeleteAll(m_childItems);
}

ProjectTreeItem*
ProjectTreeItem::parent()
{
  return m_parentItem;
}

ProjectTreeItem*
ProjectTreeItem::child(int number)
{
  if (number < 0 || number >= m_childItems.size()) {
    return nullptr;
  }

  return m_childItems.at(number);
}

int
ProjectTreeItem::childCount() const
{
  return m_childItems.size();
}

int
ProjectTreeItem::columnCount()
{
  return 1;
}

bool
ProjectTreeItem::insertChildren(int position, int count)
{
  if (position < 0 || position > m_childItems.size()) {
    return false;
  }

  for (int row = 0; row < count; ++row) {
    auto* item{ new ProjectTreeItem{ this } };
    m_childItems.emplace(m_childItems.begin() + position, item);
  }

  return true;
}

bool
ProjectTreeItem::removeChildren(int position, int count)
{
  if (position < 0 || position + count > m_childItems.size()) {
    return false;
  }

  for (int row = 0; row < count; ++row) {
    auto* item{ m_childItems.at(position) };
    delete item;
    m_childItems.erase(m_childItems.begin() + position);
  }

  return true;
}

int
ProjectTreeItem::childIdx() const
{
  if (m_parentItem == nullptr) {
    return 0;
  }

  const auto iter{ std::find_if(
    m_parentItem->m_childItems.cbegin(),
    m_parentItem->m_childItems.cend(),
    [&](auto* child) { return child == const_cast<ProjectTreeItem*>(this); }) };
  return iter == m_parentItem->m_childItems.end()
           ? 0
           : std::distance(m_parentItem->m_childItems.cbegin(), iter);
}

bool
ProjectTreeItem::setData(const QString& title,
                         const QIcon& icon,
                         ESection section,
                         EDepth depth,
                         const QJsonObject& json)
{
  if (title.isNull() || section == ESection::Unknown ||
      depth == EDepth::Unknown) {
    return false;
  }

  m_title   = title;
  m_icon    = icon;
  m_section = section;
  m_depth   = depth;
  m_content = json;

  return true;
}

ProjectTreeItem*
ProjectTreeItem::takeChild(int row)
{
  auto* item{ m_childItems.at(row) };
  m_childItems.erase(m_childItems.begin() + row);
  return item;
}

void
ProjectTreeItem::insertChild(int row, ProjectTreeItem* item)
{
  item->m_parentItem = this;
  m_childItems.emplace(m_childItems.begin() + row, item);
}

void
ProjectTreeItem::swapChildren(int old_row, int new_row)
{
  std::swap(m_childItems.at(old_row), m_childItems.at(new_row));
}

int
ProjectTreeItem::rowOfChild(ProjectTreeItem* child) const
{
  const auto iter{ std::find(m_childItems.begin(), m_childItems.end(), child) };
  return iter == m_childItems.end()
           ? 0
           : std::distance(m_childItems.cbegin(), iter);
}

} // namespace models
