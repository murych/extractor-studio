#ifndef MODELS_AVALIABLEPLATESMODEL_H
#define MODELS_AVALIABLEPLATESMODEL_H

#include <QAbstractListModel>
#include <QJsonObject>

namespace models {

class AvaliablePlatesModel : public QAbstractListModel
{
  Q_OBJECT
public:
  using QAbstractListModel::QAbstractListModel;
  ~AvaliablePlatesModel() override = default;

  void loadData(const QJsonObject& json);

  // QAbstractItemModel interface
  [[nodiscard]] int rowCount(const QModelIndex& parent) const override;
  [[nodiscard]] QVariant data(const QModelIndex& index,
                              int role) const override;

private:
  QJsonObject m_data;
};

} // namespace models

#endif // MODELS_AVALIABLEPLATESMODEL_H
