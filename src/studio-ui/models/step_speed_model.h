#ifndef MODELS_STEPSPEEDMODEL_H
#define MODELS_STEPSPEEDMODEL_H

#include <QAbstractListModel>

namespace models {

class StepSpeedModel : public QAbstractListModel
{
  Q_OBJECT
public:
  using QAbstractListModel::QAbstractListModel;
  ~StepSpeedModel() override = default;

  [[nodiscard]] auto rowCount(const QModelIndex& parent) const -> int override;
  [[nodiscard]] auto data(const QModelIndex& index, int role) const
    -> QVariant override;
};

} // namespace models

#endif // MODELS_STEPSPEEDMODEL_H
