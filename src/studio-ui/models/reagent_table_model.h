#ifndef REAGENTTABLEMODEL_H
#define REAGENTTABLEMODEL_H

#include <QAbstractTableModel>
#include <QJsonObject>

namespace models {

namespace plates {
class Reagent;
}

class ReagentTableModel : public QAbstractTableModel
{
  Q_OBJECT
public:
  using QAbstractTableModel::QAbstractTableModel;
  ~ReagentTableModel() override = default;

  [[nodiscard]] int rowCount(const QModelIndex& parent) const override;
  [[nodiscard]] int columnCount(const QModelIndex& parent) const override;
  [[nodiscard]] QVariant data(const QModelIndex& index,
                              int role) const override;
  bool setData(const QModelIndex& index,
               const QVariant& value,
               int role) override;
  [[nodiscard]] QVariant headerData(int section,
                                    Qt::Orientation orientation,
                                    int role) const override;
  bool insertRows(int row, int count, const QModelIndex& parent) override;
  bool removeRows(int row, int count, const QModelIndex& parent) override;
  [[nodiscard]] Qt::ItemFlags flags(const QModelIndex& index) const override;

  void clear();

  void setupModelData(const QJsonObject& json);
  [[nodiscard]] QJsonObject modelData() const;

  [[nodiscard]] bool hasCutItem() const { return cutItem.isEmpty(); }
  QModelIndex cut(const QModelIndex& index);
  QModelIndex paste(const QModelIndex& index);

  QList<QJsonObject> items;
  QJsonObject cutItem;

  enum Cols
  {
    Name = 0,
    Volume,
    Type,
    Color,
  };
};

} // namespace models

#endif // REAGENTTABLEMODEL_H
