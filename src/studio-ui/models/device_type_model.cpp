#include "device_type_model.h"

#include "models/device/abstract_device.h"
#include "utils/device_manager.h"
#include "json/string_decorator.h"
#include <algorithm>

namespace models {

DeviceTypeModel::DeviceTypeModel(QObject* parent)
  : QAbstractListModel{ parent }
{
  m_data.clear();
  std::transform(
    device::GenericDevice::device_type_mapper.begin(),
    device::GenericDevice::device_type_mapper.end(),
    std::back_inserter(m_data),
    [](const auto& device_type_pair) { return device_type_pair.second; });
}

int
DeviceTypeModel::rowCount(const QModelIndex& parent) const
{
  return m_data.size();
}

QVariant
DeviceTypeModel::data(const QModelIndex& index, int role) const
{
  if (!index.isValid()) {
    return {};
  }

  if (static_cast<Qt::ItemDataRole>(role) == Qt::DisplayRole) {
    return m_data.at(index.row());
  }

  return QVariant{};
}

} // namespace models

namespace models {

DeviceFamilyModel::DeviceFamilyModel(const int family, QObject* parent)
  : QAbstractListModel{ parent }
{
  utils::DeviceManager manager;
  m_data.clear();
  m_data = manager.availableTypesInFamily(family);
}

int
DeviceFamilyModel::rowCount(const QModelIndex& parent) const
{
  return m_data.size();
}

QVariant
DeviceFamilyModel::data(const QModelIndex& index, int role) const
{
  if (!index.isValid()) {
    return {};
  }

  const models::device::GenericDevice a_device;

  switch (role) {
    case Qt::DisplayRole:
      return m_data.at(index.row()).value(a_device.title->key());
    case JsonRole:
      return m_data.at(index.row());
  }

  return {};
}

} // namespace models
