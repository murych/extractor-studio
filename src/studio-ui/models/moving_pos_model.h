#ifndef MOVING_POS_MODEL_H
#define MOVING_POS_MODEL_H

#include <QAbstractListModel>

namespace models {

class MovingPosModel : public QAbstractListModel
{
  Q_OBJECT
public:
  using QAbstractListModel::QAbstractListModel;
  ~MovingPosModel() override = default;

  [[nodiscard]] auto rowCount(const QModelIndex& parent) const -> int override;
  [[nodiscard]] auto data(const QModelIndex& index, int role) const
    -> QVariant override;
};

} // namespace models

#endif // MOVING_POS_MODEL_H
