#ifndef MODELS_DEVICETYPEMODEL_H
#define MODELS_DEVICETYPEMODEL_H

#include <QAbstractListModel>

namespace models {

class DeviceTypeModel : public QAbstractListModel
{
  Q_OBJECT
public:
  explicit DeviceTypeModel(QObject* parent = nullptr);
  ~DeviceTypeModel() override = default;

  // QAbstractItemModel interface
  [[nodiscard]] int rowCount(
    const QModelIndex& parent = QModelIndex{}) const override;
  [[nodiscard]] QVariant data(const QModelIndex& index,
                              int role) const override;

private:
  std::vector<QString> m_data;
};

class DeviceFamilyModel : public QAbstractListModel
{
  Q_OBJECT
public:
  explicit DeviceFamilyModel(int family, QObject* parent = nullptr);
  ~DeviceFamilyModel() override = default;

  enum DataRoles
  {
    JsonRole = Qt::UserRole + 100
  };

  // QAbstractItemModel interface
  [[nodiscard]] int rowCount(const QModelIndex& parent) const override;
  [[nodiscard]] QVariant data(const QModelIndex& index,
                              int role) const override;

private:
  QVector<QJsonObject> m_data;
};

} // namespace models

#endif // MODELS_DEVICETYPEMODEL_H
