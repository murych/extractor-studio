#ifndef MODELS_STEPLEAVE_H
#define MODELS_STEPLEAVE_H

#include "params_specific.h"

namespace models::steps {

class StepSleevesLeave : public steps::ParamsSpecific
{
  Q_OBJECT
public:
  explicit StepSleevesLeave(QObject* parent = nullptr);
  explicit StepSleevesLeave(const QJsonObject& json, QObject* parent);
  ~StepSleevesLeave() override;
};

} // namespace models

#endif // MODELS_STEPLEAVE_H
