#include "params_specific.h"

namespace models::steps {

ParamsSpecific::ParamsSpecific(QObject* parent)
  : json::Entity{ "specific", parent }
{}

ParamsSpecific::ParamsSpecific(const QJsonObject& json, QObject* parent)
  : ParamsSpecific{ parent }
{
  update(json);
}

ParamsSpecific::~ParamsSpecific() = default;

} // namespace models
