#ifndef MODELS_STEPBEADSCOLLECT_H
#define MODELS_STEPBEADSCOLLECT_H

#include "params_specific.h"

namespace json {
class IntDecorator;
class EnumeratorDecorator;
} // namespace json

namespace models::steps {

class StepBeadsCollect : public steps::ParamsSpecific
{
  Q_OBJECT
  Q_PROPERTY(int collectCount READ collectCount WRITE setCollectCount NOTIFY
               collectCountChanged)
  Q_PROPERTY(int collectDuration READ collectDuration WRITE setCollectDuration
               NOTIFY collectDuraitonChanged)
  Q_PROPERTY(int collectSpeed READ collectSpeed WRITE setCollectSpeed NOTIFY
               collectSpeedChanged)

public:
  explicit StepBeadsCollect(QObject* parent = nullptr);
  explicit StepBeadsCollect(const QJsonObject& json, QObject* parent);
  ~StepBeadsCollect() override;

  json::IntDecorator* count{ nullptr };
  json::IntDecorator* duration{ nullptr };
  json::EnumeratorDecorator* speed{ nullptr };

  [[nodiscard]] int collectCount() const;
  void setCollectCount(int count);

  [[nodiscard]] int collectDuration() const;
  void setCollectDuration(int dur);

  [[nodiscard]] int collectSpeed() const;
  void setCollectSpeed(int speed);

signals:
  void collectCountChanged();
  void collectDuraitonChanged();
  void collectSpeedChanged();
};

} // namespace models

#endif // MODELS_STEPBEADSCOLLECT_H
