#ifndef ABSTRACTSTEP_H
#define ABSTRACTSTEP_H

#include "json/entity.h"

#define USE_GET_SET

namespace json {
class EnumeratorDecorator;
}

namespace models::steps {

class ParamsCommon : public json::Entity
{
  Q_OBJECT

public:
  explicit ParamsCommon(QObject* parent = nullptr);
  explicit ParamsCommon(const QJsonObject& json, QObject* parent = nullptr);
  ~ParamsCommon() override;

  json::EnumeratorDecorator* sleevesPosAtEnd{ nullptr };
  json::EnumeratorDecorator* verticalEnterSpeed{ nullptr };
  json::EnumeratorDecorator* verticalLeaveSpeed{ nullptr };
  json::EnumeratorDecorator* horizontalLeaveSpeed{ nullptr };

  [[nodiscard]] int getSleevesPosAtEnd() const;
  void setSleevesPosAtEnd(int new_value);

  [[nodiscard]] int getVerticalEnterSpeed() const;
  void setVerticalEnterSpeed(int new_value);

  [[nodiscard]] int getVerticalLeaveSpeed() const;
  void setVerticalLeaveSpeed(int new_value);

  [[nodiscard]] int getHorizontalSpeed() const;
  void setHorizontalSpeed(int new_value);

signals:
  void sleevesPosAtEndChanged();
  void verticalEnterSpeedChanged();
  void verticalLeaveSpeedChanged();
  void horizontalLeaveSpeedChanged();
};

} // namespace models

#endif // ABSTRACTSTEP_H
