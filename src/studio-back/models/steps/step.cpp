#include "step.h"

#include "params_common.h"
#include "params_specific.h"
#include "utils/step_factory.h"
#include "json/enumerator_decorator.h"
#include "json/int_decorator.h"
#include "json/string_decorator.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(M_STEP, "MODEL STEP")

namespace {
const auto KEY_DUATION{ QStringLiteral("duration") };
const auto KEY_RELATED_PLATE{ QStringLiteral("related_plate") };
const auto KEY_COMMON{ QStringLiteral("common") };
const auto KEY_SPECIFIC{ QStringLiteral("specific") };
}

namespace models::steps {

const std::unordered_map<int, QString> Step::step_type_mapper{
  { EStepType::Unknown, "" },
  { EStepType::Pickup, "Pick-up" },
  { EStepType::Leave, "Leave" },
  { EStepType::Mix, "Mix" },
  { EStepType::Dry, "Dry" },
  { EStepType::Sleep, "Sleep" },
  { EStepType::Move, "Manual Move" },
  { EStepType::Collect, "Collect Beads" },
  { EStepType::Release, "Release Beads" },
  { EStepType::Heater, "Manual Heat" },
  { EStepType::Pause, "Pause" }
};

Step::Step(QObject* parent)
  : json::Entity{ "step", parent }
  , idx{ dynamic_cast<json::IntDecorator*>(
      addDataItem(new json::IntDecorator{ json::defaults::KEY_IDX,
                                          "Step Index",
                                          0,
                                          this })) }
  , name{ dynamic_cast<json::StringDecorator*>(
      addDataItem(new json::StringDecorator{ json::defaults::KEY_TITLE,
                                             "Step Name",
                                             QLatin1String{},
                                             this })) }
  , duration{ dynamic_cast<json::IntDecorator*>(addDataItem(
      new json::IntDecorator{ KEY_DUATION, "Step Duration", 0, this })) }
  , type{ dynamic_cast<json::EnumeratorDecorator*>(
      addDataItem(new json::EnumeratorDecorator{ json::defaults::KEY_TYPE,
                                                 "Step Type",
                                                 EStepType::Unknown,
                                                 step_type_mapper,
                                                 this })) }
  , plate{ dynamic_cast<json::IntDecorator*>(addDataItem(
      new json::IntDecorator{ KEY_RELATED_PLATE, "Related Plate", 0, this })) }
  , common{ dynamic_cast<steps::ParamsCommon*>(
      addChild(KEY_COMMON, new steps::ParamsCommon{ this })) }
{
  Q_ASSERT(common);
}

Step::Step(const QJsonObject& json, QObject* parent)
  : Step{ parent }
{
  qCDebug(M_STEP) << "construct from json" << parent << json;
  update(json);

  if (json.contains(common->key()) && json.value(common->key()).isObject()) {
    common->update(json.value(common->key()).toObject());
  }

  const utils::StepFactory factory;
  const QJsonObject step_type{ { type->key(), type->value() } };
  specific = dynamic_cast<steps::ParamsSpecific*>(
    addChild(KEY_SPECIFIC, utils::StepFactory::create(step_type)));

  if (json.contains(specific->key()) &&
      json.value(specific->key()).isObject()) {
    specific->update(json.value(specific->key()).toObject());
  }
}

Step::Step(EStepType t_type, QObject* parent)
  : Step{ parent }
{
  qCDebug(M_STEP) << "construct from type" << parent << type;

  type->setValue(static_cast<int>(t_type));
  name->setValue(type->valueDescription());

  const QJsonObject json{ { type->key(), type->value() } };
  const utils::StepFactory factory;
  specific = dynamic_cast<steps::ParamsSpecific*>(
    addChild(KEY_SPECIFIC, utils::StepFactory::create(json)));

  Q_ASSERT(specific);
}

Step::~Step()
{
  delete idx;
  delete name;
  delete type;
  delete duration;
  delete plate;
}

int
Step::getStepIdx() const
{
  return idx->value();
}

QString
Step::getStepIdxS() const
{
  return QString::number(idx->value());
}

void
Step::setStepIdx(const int new_idx)
{
  idx->setValue(new_idx);
  emit stepIdxChanged();
}

QString
Step::getStepName() const
{
  return name->value();
}

void
Step::setStepName(const QString& new_name)
{
  name->setValue(new_name);
  emit stepNameChanged();
}

int
Step::getRelatedPlate() const
{
  return plate->value();
}

void
Step::setRelatedPlate(const int new_plate)
{
  plate->setValue(new_plate);
  emit relatedPlateChanged();
}

int
Step::getStepType() const
{
  return type->value();
}

void
Step::setStepType(const int new_type)
{
  type->setValue(new_type);

  emit stepTypeChanged();
}

QString
Step::getStepTypeDescription() const
{
  return type->valueDescription();
}

} // namespace models
