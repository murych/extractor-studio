#ifndef MIXINGSTAGE_H
#define MIXINGSTAGE_H

#include "json/entity.h"

namespace json {
class IntDecorator;
class EnumeratorDecorator;
} // namespace json

namespace models::steps {

class MixingStage : public json::Entity
{
  Q_OBJECT
  Q_PROPERTY(int idx READ getIdx WRITE setIdx NOTIFY idxChanged)
  Q_PROPERTY(
    int duration READ getDuration WRITE setDuration NOTIFY durationChanged)
  Q_PROPERTY(int speed READ getSpeed WRITE setSpeed NOTIFY speedChanged)

public:
  explicit MixingStage(QObject* parent = nullptr);
  MixingStage(const QJsonObject& json, QObject* parent);
  ~MixingStage() override;

  std::unique_ptr<json::IntDecorator> idx{ nullptr };
  //! время перемешивания в секундах
  std::unique_ptr<json::IntDecorator> duration{ nullptr };
  //! скорость перемешивания
  std::unique_ptr<json::EnumeratorDecorator> speed{ nullptr };

  std::unique_ptr<json::IntDecorator> pauseDuration{ nullptr };
  std::unique_ptr<json::EnumeratorDecorator> pausePosition{ nullptr };

  [[nodiscard]] int getIdx() const;
  void setIdx(int idx);

  [[nodiscard]] int getDuration() const;
  void setDuration(int duration);

  [[nodiscard]] int getSpeed() const;
  void setSpeed(int speed);

signals:
  void idxChanged();
  void durationChanged();
  void speedChanged();

  //! иконка для перемешивания
  //! @todo переделать в маппер такой же как для типа скорости?
  //  QString speed_icon{ QStringLiteral(":/48/color/error_48px.png") };
};

} // namespace models

#endif // MIXINGSTAGE_H
