#ifndef MOVING_SPEED_H
#define MOVING_SPEED_H

#include <QObject>
#include <QString>
#include <unordered_map>

namespace models::steps {

enum class ETravelSpeed : int
{
  Unknown = 0,
  Slow,
  Medium,
  Fast,
  User
};

static const std::unordered_map<int, QString> TRAVEL_SPEED_MAPPER{
  { static_cast<int>(ETravelSpeed::Unknown), QLatin1String{} },
  { static_cast<int>(ETravelSpeed::Slow), QObject::tr("Slow") },
  { static_cast<int>(ETravelSpeed::Medium), QObject::tr("Medium") },
  { static_cast<int>(ETravelSpeed::Fast), QObject::tr("Fast") }
};

} // namespace models::steps

#endif // MOVING_SPEED_H
