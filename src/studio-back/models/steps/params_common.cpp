#include "params_common.h"

#include "json/enumerator_decorator.h"
#include "moving_position.h"
#include "moving_speed.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(MAS, "MODEL STEP PARAMS")

namespace {
const auto KEY_SLEEVES_POS_END{ QStringLiteral("sleeves_pos_at_end") };
const auto KEY_HORIZONTAL_SPEED{ QStringLiteral("horizontal_leave_speed") };
const auto KEY_VERTICAL_ENTER_SPEED{ QStringLiteral("vertical_enter_speed") };
const auto KEY_VERTICAL_LEAVE_SPEED{ QStringLiteral("vertical_leave_speed") };
}

namespace models::steps {

ParamsCommon::ParamsCommon(QObject* parent)
  : json::Entity{ "common", parent }
  , sleevesPosAtEnd{ dynamic_cast<json::EnumeratorDecorator*>(addDataItem(
      new json::EnumeratorDecorator{ KEY_SLEEVES_POS_END,
                                     "Sleeves Position At End",
                                     static_cast<int>(ETravelPos::Unknown),
                                     TRAVEL_POSITION_MAPPER,
                                     this })) }
  , verticalEnterSpeed{ dynamic_cast<json::EnumeratorDecorator*>(addDataItem(
      new json::EnumeratorDecorator{ KEY_VERTICAL_ENTER_SPEED,
                                     "Vertical Enter Speed",
                                     static_cast<int>(ETravelSpeed::Medium),
                                     TRAVEL_SPEED_MAPPER,
                                     this })) }
  , verticalLeaveSpeed{ dynamic_cast<json::EnumeratorDecorator*>(addDataItem(
      new json::EnumeratorDecorator{ KEY_VERTICAL_LEAVE_SPEED,
                                     "Vertical Leave Speed",
                                     static_cast<int>(ETravelSpeed::Slow),
                                     TRAVEL_SPEED_MAPPER,
                                     this })) }
  , horizontalLeaveSpeed{ dynamic_cast<json::EnumeratorDecorator*>(addDataItem(
      new json::EnumeratorDecorator{ KEY_HORIZONTAL_SPEED,
                                     "Horizontal Exit Speed",
                                     static_cast<int>(ETravelSpeed::Fast),
                                     TRAVEL_SPEED_MAPPER,
                                     this })) }
{}

ParamsCommon::ParamsCommon(const QJsonObject& json, QObject* parent)
  : ParamsCommon{ parent }
{
  update(json);
}

ParamsCommon::~ParamsCommon()
{
  delete sleevesPosAtEnd;
  delete verticalEnterSpeed;
  delete verticalLeaveSpeed;
  delete horizontalLeaveSpeed;
}

int
ParamsCommon::getSleevesPosAtEnd() const
{
  return sleevesPosAtEnd->value();
}

void
ParamsCommon::setSleevesPosAtEnd(const int new_value)
{
  sleevesPosAtEnd->setValue(new_value);
  emit sleevesPosAtEndChanged();
}

int
ParamsCommon::getVerticalEnterSpeed() const
{
  return verticalEnterSpeed->value();
}

void
ParamsCommon::setVerticalEnterSpeed(const int new_value)
{
  verticalEnterSpeed->setValue(new_value);
  emit verticalEnterSpeedChanged();
}

int
ParamsCommon::getVerticalLeaveSpeed() const
{
  return verticalLeaveSpeed->value();
}

void
ParamsCommon::setVerticalLeaveSpeed(int new_value)
{
  verticalLeaveSpeed->setValue(new_value);
  emit verticalLeaveSpeedChanged();
}

int
ParamsCommon::getHorizontalSpeed() const
{
  return horizontalLeaveSpeed->value();
}

void
ParamsCommon::setHorizontalSpeed(const int new_value)
{
  horizontalLeaveSpeed->setValue(new_value);
  emit horizontalLeaveSpeedChanged();
}

} // namespace models
