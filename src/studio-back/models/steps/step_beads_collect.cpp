#include "step_beads_collect.h"

#include "json/enumerator_decorator.h"
#include "json/int_decorator.h"
#include "moving_speed.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(MSBC, "MODEL BEADS COLLECT")

namespace {
const auto DEFAULT_TITLE{ QObject::tr("Collect Beads") };
}

namespace models::steps {

StepBeadsCollect::StepBeadsCollect(QObject* parent)
  : steps::ParamsSpecific{ parent }
  , count{ dynamic_cast<json::IntDecorator*>(addDataItem(
      new json::IntDecorator{ "count", "Collect count", 0, this })) }
  , duration{ dynamic_cast<json::IntDecorator*>(addDataItem(
      new json::IntDecorator{ "duration", "Collect Duration", 0, this })) }
  , speed{ dynamic_cast<json::EnumeratorDecorator*>(addDataItem(
      new json::EnumeratorDecorator{ "speed",
                                     "Collect Speed",
                                     static_cast<int>(ETravelSpeed::Slow),
                                     TRAVEL_SPEED_MAPPER,
                                     this })) }
{
}

StepBeadsCollect::StepBeadsCollect(const QJsonObject& json, QObject* parent)
  : StepBeadsCollect{ parent }
{
  update(json);
}

StepBeadsCollect::~StepBeadsCollect()
{
  delete count;
  delete duration;
  delete speed;
}

int
StepBeadsCollect::collectCount() const
{
  return count->value();
}

void
StepBeadsCollect::setCollectCount(const int new_count)
{
  qCDebug(MSBC) << "StepBeadsCollect::setCollectCount" << new_count;
  count->setValue(new_count);

  emit collectCountChanged();
}

int
StepBeadsCollect::collectDuration() const
{
  return duration->value();
}

void
StepBeadsCollect::setCollectDuration(const int dur)
{
  qCDebug(MSBC) << "StepBeadsCollect::setCollectDuration" << dur;
  duration->setValue(dur);

  emit collectDuraitonChanged();
}

int
StepBeadsCollect::collectSpeed() const
{
  return speed->value();
}

void
StepBeadsCollect::setCollectSpeed(const int new_speed)
{
  qCDebug(MSBC) << "StepBeadsCollect::setCollectSpeed" << new_speed;
  speed->setValue(new_speed);

  emit collectSpeedChanged();
}

} // namespace models
