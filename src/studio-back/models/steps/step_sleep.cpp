#include "step_sleep.h"

#include "json/int_decorator.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(MSS, "STEP SLEEP")

namespace json::defaults {
static const QString KEY_DURATION{ QStringLiteral("duration") };
static const QString LABEL_DURATION{ QObject::tr("Sleep Duration") };
}

namespace models::steps {

StepSleep::StepSleep(QObject* parent)
  : steps::ParamsSpecific{ parent }
  , m_sleepDuration{ dynamic_cast<json::IntDecorator*>(
      addDataItem(new json::IntDecorator{ json::defaults::KEY_DURATION,
                                          json::defaults::LABEL_DURATION,
                                          0,
                                          this })) }
{
}

StepSleep::StepSleep(const QJsonObject& json,QObject* parent)
  : StepSleep{ parent }
{
  update(json);
}

StepSleep::~StepSleep()
{
  delete m_sleepDuration;
}

int
StepSleep::getSleepDuration() const
{
  return m_sleepDuration->value();
}

void
StepSleep::setSleepDuration(const int value)
{
  qCDebug(MSS) << "StepSleep::setSleepDuration"
               << "old value" << m_sleepDuration->value() << "new value"
               << value;

  m_sleepDuration->setValue(value);
  emit sleepDurationChanged();
}

} // namespace models
