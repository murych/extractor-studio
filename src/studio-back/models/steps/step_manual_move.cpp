#include "step_manual_move.h"

#include "json/double_decorator.h"
#include "json/enumerator_decorator.h"
#include "json/int_decorator.h"
#include "moving_speed.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(MSMM, "MODEL MANUAL MOVE")

namespace {
const auto KEY_AXIS{ QStringLiteral("target_axis") };
const auto KEY_POSITION{ QStringLiteral("target_position") };

const auto LABEL_AXIS{ QObject::tr("Target Axis") };
const auto LABEL_TARGET{ QObject::tr("Target Position") };
}

namespace models::steps {

StepManualMove::StepManualMove(QObject* parent)
  : steps::ParamsSpecific{ parent }
  , targetAxis{ dynamic_cast<json::IntDecorator*>(
      addDataItem(new json::IntDecorator{ KEY_AXIS, LABEL_AXIS, 0, this })) }
  , targetPosition{ dynamic_cast<json::DoubleDecorator*>(addDataItem(
      new json::DoubleDecorator{ KEY_POSITION, LABEL_TARGET, 0.0, this })) }
  , speed{ dynamic_cast<json::EnumeratorDecorator*>(addDataItem(
      new json::EnumeratorDecorator{ "speed",
                                     "Speed",
                                     static_cast<int>(ETravelSpeed::Slow),
                                     TRAVEL_SPEED_MAPPER,
                                     this })) }
{}

StepManualMove::StepManualMove(const QJsonObject& json,QObject* parent)
  : StepManualMove{ parent }
{
  update(json);
}

StepManualMove::~StepManualMove()
{
  delete targetAxis;
  delete targetPosition;
}

void
StepManualMove::setTargetAxis(int data)
{
  if (data != targetAxis->value()) {
    targetAxis->setValue(data);
    emit targetAxisChanged();
  }
}

void
StepManualMove::setTargetPosition(double data)
{
  if (!qFuzzyCompare(data, targetPosition->value())) {
    targetPosition->setValue(data);
    emit targetPositionChanged();
  }
}

} // namespace models
