#include "step_pause.h"

#include "json/bool_decorator.h"
#include "json/string_decorator.h"

namespace {
const QString DEFAULT_TITLE{ QObject::tr("Pause") };
}

namespace models::steps {

StepPause::StepPause(QObject* parent)
  : steps::ParamsSpecific{ parent }
  , m_message{ dynamic_cast<json::StringDecorator*>(
      addDataItem(new json::StringDecorator{ "message",
                                             "Message",
                                             QLatin1String{},
                                             this })) }
  , m_blockUntilNotice{ dynamic_cast<json::BoolDecorator*>(addDataItem(
      new json::BoolDecorator{ "block", "Block until notice", false, this })) }
{
#ifdef PEREMEN
  common->name->setValue(defaultTitle);
  common->type->setValue(StepType::block);

  common->icon = QString{ ":/48px/sleep_mode_48px.png" };
#endif
}

StepPause::StepPause(const QJsonObject& json,QObject* parent)
  : StepPause{ parent }
{
  update(json);
#ifdef PEREMEN
  if (common->name->value().isEmpty())
    common->name->setValue(defaultTitle);
  common->type->setValue(StepType::block);
#endif
}

StepPause::~StepPause()
{
  delete m_message;
  delete m_blockUntilNotice;
}

QString
StepPause::getMessage() const
{
  return m_message->value();
}

bool
StepPause::getBlock() const
{
  return m_blockUntilNotice->value();
}
void
StepPause::setMessage(const QString& value)
{
  m_message->setValue(value);
  emit messageChanged();
}

void
StepPause::setBlock(const bool value)
{
  m_blockUntilNotice->setValue(value);
  emit blockChanged();
}

} // namespace models
