#ifndef MODELS_STEPBEADSRELEASE_H
#define MODELS_STEPBEADSRELEASE_H

#include "params_specific.h"

namespace json {
class IntDecorator;
class EnumeratorDecorator;
} // namespace json

namespace models::steps {

class StepBeadsRelease : public steps::ParamsSpecific
{
  Q_OBJECT
  Q_PROPERTY(int uiReleaseCount READ releaseCount WRITE setReleaseCount NOTIFY
               releaseCountChanged)
  Q_PROPERTY(int uiReleaseDuration READ releaseDuration WRITE setReleaseDuration
               NOTIFY releaseDurationChanged)
  Q_PROPERTY(int uiReleaseSpeed READ releaseSpeed WRITE setReleaseSpeed NOTIFY
               releaseSpeedChanged)

public:
  explicit StepBeadsRelease(QObject* parent = nullptr);
  explicit StepBeadsRelease(const QJsonObject& json, QObject* parent);
  ~StepBeadsRelease() override;

  json::IntDecorator* count{ nullptr };
  json::IntDecorator* duration{ nullptr };
  json::EnumeratorDecorator* speed{ nullptr };

  [[nodiscard]] int releaseCount() const;
  [[nodiscard]] int releaseDuration() const;
  [[nodiscard]] int releaseSpeed() const;

  void setReleaseCount(int value);
  void setReleaseDuration(int value);
  void setReleaseSpeed(int value);

signals:
  void releaseCountChanged();
  void releaseDurationChanged();
  void releaseSpeedChanged();
};

} // namespace models

#endif // MODELS_STEPBEADSRELEASE_H
