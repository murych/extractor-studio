#ifndef MODELS_STEPPAUSE_H
#define MODELS_STEPPAUSE_H

#include "params_specific.h"

namespace json {
class BoolDecorator;
}

#define useGetSet

namespace models::steps {

class StepPause : public steps::ParamsSpecific
{
  Q_OBJECT
public:
  explicit StepPause(QObject* parent = nullptr);
  StepPause(const QJsonObject& json,QObject* parent);
  ~StepPause() override;

private:
  json::StringDecorator* m_message{ nullptr };
  json::BoolDecorator* m_blockUntilNotice{ nullptr };

#ifdef useGetSet
public:
  [[nodiscard]] QString getMessage() const;
  [[nodiscard]] bool getBlock() const;

  void setMessage(const QString& value);
  void setBlock(bool value);

signals:
  void messageChanged();
  void blockChanged();
#endif
};

} // namespace models

#endif // MODELS_STEPPAUSE_H
