#ifndef MODELS_STEPSLEEP_H
#define MODELS_STEPSLEEP_H

#include "params_specific.h"

namespace json {
class IntDecorator;
}

#define USE_GET_SET

namespace models::steps {

class StepSleep : public steps::ParamsSpecific
{
  Q_OBJECT
#ifdef useGetSet
  Q_PROPERTY(int uiSleepDuration READ getSleepDuration WRITE setSleepDuration
               NOTIFY sleepDurationChanged)
#endif
public:
  explicit StepSleep(QObject* parent = nullptr);
  explicit StepSleep(const QJsonObject& json, QObject* parent);
  ~StepSleep() override;

  [[nodiscard]] int getSleepDuration() const;
  void setSleepDuration(int value);

private:
  json::IntDecorator* const m_sleepDuration{ nullptr };

signals:
  void sleepDurationChanged();
};

} // namespace models

#endif // MODELS_STEPSLEEP_H
