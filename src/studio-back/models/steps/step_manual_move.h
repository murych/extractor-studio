#ifndef MODELS_STEPMANUALMOVE_H
#define MODELS_STEPMANUALMOVE_H

#include "params_specific.h"

namespace json {
class IntDecorator;
class DoubleDecorator;
class EnumeratorDecorator;
} // namespace json

namespace models::steps {

class StepManualMove : public steps::ParamsSpecific
{
  Q_OBJECT
public:
  explicit StepManualMove(QObject* parent = nullptr);
  explicit StepManualMove(const QJsonObject& json, QObject* parent);
  ~StepManualMove() override;

  json::IntDecorator* targetAxis{ nullptr };
  json::DoubleDecorator* targetPosition{ nullptr };
  json::EnumeratorDecorator* speed{ nullptr };

  void setTargetAxis(int data);
  void setTargetPosition(double data);

signals:
  void targetAxisChanged();
  void targetPositionChanged();
};

} // namespace models

#endif // MODELS_STEPMANUALMOVE_H
