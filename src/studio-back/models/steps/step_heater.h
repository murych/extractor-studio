#ifndef MODELS_STEPHEATER_H
#define MODELS_STEPHEATER_H

#include "params_specific.h"

namespace json {
class DoubleDecorator;
class BoolDecorator;
} // namespace json

namespace models::steps {

class StepHeater : public steps::ParamsSpecific
{
  Q_OBJECT
public:
  explicit StepHeater(QObject* parent = nullptr);
  explicit StepHeater(const QJsonObject& json, QObject* parent);
  ~StepHeater() override;

  json::BoolDecorator* blockUntilComplete{ nullptr };
  json::DoubleDecorator* targetTemperature{ nullptr };

  [[nodiscard]] bool getBlockExecution() const;
  [[nodiscard]] double getTargetTemp() const;

  void setBlockExecution(bool value);
  void setTargetTemp(double value);

signals:
  void blockExecutionChanged();
  void targetTempChanged();
};

} // namespace models

#endif // MODELS_STEPHEATER_H
