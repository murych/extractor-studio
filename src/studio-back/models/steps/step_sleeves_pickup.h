#ifndef STEPPICKUP_H
#define STEPPICKUP_H

#include "params_specific.h"

namespace models::steps {

class StepSleevesPickup : public steps::ParamsSpecific
{
  Q_OBJECT
public:
  explicit StepSleevesPickup(QObject* parent = nullptr);
  explicit StepSleevesPickup(const QJsonObject& json, QObject* parent);
  ~StepSleevesPickup() override;
};

}

#endif // STEPPICKUP_H
