#ifndef MODELS_STEPDRY_H
#define MODELS_STEPDRY_H

#include "params_specific.h"

namespace json {
class IntDecorator;
class EnumeratorDecorator;
} // namespace json

namespace models::steps {

class StepDry : public steps::ParamsSpecific
{
  Q_OBJECT
  Q_PROPERTY(
    int uiDuration READ getDuration WRITE setDuration NOTIFY durationChanged)
  Q_PROPERTY(
    int uiPosition READ getPosition WRITE setPosition NOTIFY positionChanged)

public:
  explicit StepDry(QObject* parent = nullptr);
  explicit StepDry(const QJsonObject& json, QObject* parent);
  ~StepDry() override;

  json::IntDecorator* duration{ nullptr };
  json::EnumeratorDecorator* position{ nullptr };

  [[nodiscard]] int getDuration() const;
  [[nodiscard]] int getPosition() const;

  void setDuration(int value);
  void setPosition(int value);

signals:
  void durationChanged();
  void positionChanged();
};

} // namespace models

#endif // MODELS_STEPDRY_H
