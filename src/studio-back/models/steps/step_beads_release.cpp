#include "step_beads_release.h"

#include "json/enumerator_decorator.h"
#include "json/int_decorator.h"
#include "moving_speed.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(MSBR, "MODEL BEADS RELEASE")

namespace {
const QString DEFAULT_TITLE{ QObject::tr("Release Beads") };
}

namespace models::steps {

StepBeadsRelease::StepBeadsRelease(QObject* parent)
  : steps::ParamsSpecific{ parent }
  , count{ dynamic_cast<json::IntDecorator*>(
      addDataItem(new json::IntDecorator{ "count", "Count", 0, this })) }
  , duration{ dynamic_cast<json::IntDecorator*>(
      addDataItem(new json::IntDecorator{ "duration", "Duration", 0, this })) }
  , speed{ dynamic_cast<json::EnumeratorDecorator*>(addDataItem(
      new json::EnumeratorDecorator{ "speed",
                                     "Speed",
                                     static_cast<int>(ETravelSpeed::Slow),
                                     TRAVEL_SPEED_MAPPER,
                                     this })) }
{}

StepBeadsRelease::StepBeadsRelease(const QJsonObject& json, QObject* parent)
  : StepBeadsRelease{ parent }
{
  update(json);
}

StepBeadsRelease::~StepBeadsRelease()
{
  delete count;
  delete duration;
  delete speed;
}

int
StepBeadsRelease::releaseCount() const
{
  return count->value();
}

int
StepBeadsRelease::releaseDuration() const
{
  return duration->value();
}

int
StepBeadsRelease::releaseSpeed() const
{
  return speed->value();
}

void
StepBeadsRelease::setReleaseCount(const int value)
{
  qCDebug(MSBR) << "StepBeadsRelease::setReleaseCount" << value;
  count->setValue(value);

  emit releaseCountChanged();
}

void
StepBeadsRelease::setReleaseDuration(const int value)
{
  qCDebug(MSBR) << "StepBeadsRelease::setReleaseDuration" << value;
  duration->setValue(value);

  emit releaseDurationChanged();
}

void
StepBeadsRelease::setReleaseSpeed(const int value)
{
  qCDebug(MSBR) << "StepBeadsRelease::setReleaseSpeed" << value;
  speed->setValue(value);

  emit releaseSpeedChanged();
}

} // namespace models
