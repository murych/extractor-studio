#include "step_dry.h"

#include "json/enumerator_decorator.h"
#include "json/int_decorator.h"
#include "moving_position.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(MSD, "MODEL DRY")

namespace {
const QString DEFAULT_TITLE{ QObject::tr("Dry Beads") };
} // namespace

namespace models::steps {

StepDry::StepDry(QObject* parent)
  : steps::ParamsSpecific{ parent }
  , duration{ dynamic_cast<json::IntDecorator*>(addDataItem(
      new json::IntDecorator{ "duration", "Dry Duration", 0, this })) }
  , position{ dynamic_cast<json::EnumeratorDecorator*>(addDataItem(
      new json::EnumeratorDecorator{ "position",
                                     "Dry Position",
                                     static_cast<int>(ETravelPos::AtWorkPos),
                                     TRAVEL_POSITION_MAPPER,
                                     this })) }
{}

StepDry::StepDry(const QJsonObject& json, QObject* parent)
  : StepDry{ parent }
{
  update(json);
}

StepDry::~StepDry()
{
  delete duration;
  delete position;
}

int
StepDry::getDuration() const
{
  return duration->value();
}

int
StepDry::getPosition() const
{
  return position->value();
}

void
StepDry::setDuration(const int value)
{
  duration->setValue(value);
  emit durationChanged();
}

void
StepDry::setPosition(const int value)
{
  position->setValue(value);
  emit positionChanged();
}

} // namespace models
