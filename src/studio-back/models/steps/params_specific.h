#ifndef MODELS_STEPS_PARAMSSPECIFIC_H
#define MODELS_STEPS_PARAMSSPECIFIC_H

#include "json/entity.h"

namespace models::steps {

class ParamsSpecific : public json::Entity
{
  Q_OBJECT
public:
  explicit ParamsSpecific(QObject* parent = nullptr);
  explicit ParamsSpecific(const QJsonObject& json, QObject* parent = nullptr);
  ~ParamsSpecific() override;
};

} // namespace models

#endif // MODELS_STEPS_PARAMSSPECIFIC_H
