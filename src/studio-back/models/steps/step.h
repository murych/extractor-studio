#ifndef MODELS_STEPS_STEP_H
#define MODELS_STEPS_STEP_H

#include "json/entity.h"

namespace json {
class IntDecorator;
class EnumeratorDecorator;
} // namespace

namespace models::steps {

class ParamsCommon;
class ParamsSpecific;

class Step : public json::Entity
{
  Q_OBJECT

public:
  enum EStepType
  {
    Unknown = 0,
    Pickup  = 1, //!< зохват колпачков
    Leave   = 2, //!< сброс колпачков
    Mix     = 3, //!< перемешивание
    Dry     = 4, //!< сушка частиц
    Sleep   = 5, //!< обычная пауза
    Move    = 6, //!< ручное перемещение
    Collect = 7, //!< сбор магнитных частиц
    Release = 8, //!< сброс магнитных частиц
    Heater  = 9, //!< управление нагревателем
    Pause = 10, //!< блокирование выполнение, ожидание оператора
    User = 11
  };
  Q_ENUM(EStepType)

  explicit Step(QObject* parent = nullptr);
  explicit Step(const QJsonObject& json, QObject* parent = nullptr);
  explicit Step(EStepType type, QObject* parent = nullptr);
  ~Step() override;

  json::IntDecorator* idx{ nullptr };
  json::StringDecorator* name{ nullptr };
  json::IntDecorator* duration{ nullptr };
  json::EnumeratorDecorator* type{ nullptr };
  json::IntDecorator* plate{ nullptr };

  steps::ParamsCommon* common{ nullptr };
  steps::ParamsSpecific* specific{ nullptr };

  [[nodiscard]] int getStepIdx() const;
  [[nodiscard]] QString getStepIdxS() const;
  void setStepIdx(int new_idx);

  [[nodiscard]] QString getStepName() const;
  void setStepName(const QString& new_name);

  [[nodiscard]] int getRelatedPlate() const;
  void setRelatedPlate(int new_plate);

  [[nodiscard]] int getStepType() const;
  void setStepType(int new_type);

  [[nodiscard]] QString getStepTypeDescription() const;

  const static std::unordered_map<int, QString> step_type_mapper;

  [[nodiscard]] auto paramsSpecific() const { return specific; }
  [[nodiscard]] auto paramsCommon() const { return common; }

signals:
  void stepIdxChanged();
  void stepNameChanged();
  void stepTypeChanged();
  void relatedPlateChanged();
};

} // namespace models::steps

using step_type = models::steps::Step::EStepType;

#endif // MODELS_STEPS_STEP_H
