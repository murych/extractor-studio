#ifndef MOVING_POSITION_H
#define MOVING_POSITION_H

#include <QObject>
#include <QString>
#include <unordered_map>

namespace models::steps {

enum class ETravelPos : int
{
  Unknown = 0,
  AtZero,
  AtWorkPos,

  AtWellSurface,

  AtLiquidSurface,
  AtLiquidMiddle,
  AtLiquidBottom
};

static const std::unordered_map<int, QString> TRAVEL_POSITION_MAPPER{
  { static_cast<int>(ETravelPos::Unknown), QLatin1String{} },
  { static_cast<int>(ETravelPos::AtZero), QObject::tr("Axis Zero") },
  { static_cast<int>(ETravelPos::AtWorkPos), QObject::tr("Above Well") },
  { static_cast<int>(ETravelPos::AtWellSurface),
    QObject::tr("At the Well Surface") },
  { static_cast<int>(ETravelPos::AtLiquidSurface),
    QObject::tr("At the Top of Liquid") },
  { static_cast<int>(ETravelPos::AtLiquidMiddle),
    QObject::tr("At the Middle of Liquid") },
  { static_cast<int>(ETravelPos::AtLiquidBottom),
    QObject::tr("At the Bottom of Liquid") }
};

} // namespace models::steps

#endif // MOVING_POSITION_H
