#include "step_heater.h"

#include "json/bool_decorator.h"
#include "json/double_decorator.h"

namespace {
const QString DEFAULT_TITLE{ QObject::tr("Manual Heater Control") };
}

namespace models::steps {

StepHeater::StepHeater(QObject* parent)
  : steps::ParamsSpecific{ parent }
  , blockUntilComplete{ dynamic_cast<json::BoolDecorator*>(
      addDataItem(new json::BoolDecorator{ "block_until_complete",
                                           "Block Until Complete",
                                           false,
                                           this })) }
  , targetTemperature{ dynamic_cast<json::DoubleDecorator*>(
      addDataItem(new json::DoubleDecorator{ "target_temperature",
                                             "Target Temperature",
                                             0.0,
                                             this })) }
{}

StepHeater::StepHeater(const QJsonObject& json,QObject* parent)
  : StepHeater{ parent }
{
  update(json);
}

StepHeater::~StepHeater()
{
  delete blockUntilComplete;
  delete targetTemperature;
}

bool
StepHeater::getBlockExecution() const
{
  return blockUntilComplete->value();
}

double
StepHeater::getTargetTemp() const
{
  return targetTemperature->value();
}

void
StepHeater::setBlockExecution(const bool value)
{
  blockUntilComplete->setValue(value);
  emit blockExecutionChanged();
}

void
StepHeater::setTargetTemp(const double value)
{
  targetTemperature->setValue(value);
  emit targetTempChanged();
}

} // namespace models
