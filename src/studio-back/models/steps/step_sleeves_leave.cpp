#include "step_sleeves_leave.h"

namespace models::steps {

namespace {
const QString DEFAULT_TITLE{ QObject::tr("Release Sleeves") };
}

StepSleevesLeave::StepSleevesLeave(QObject* parent)
  : steps::ParamsSpecific{ parent }
{}

StepSleevesLeave::StepSleevesLeave(const QJsonObject& json,QObject* parent)
  : StepSleevesLeave{ parent }
{
  update(json);
}

StepSleevesLeave::~StepSleevesLeave() = default;

} // namespace models
