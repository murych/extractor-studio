#include "step_sleeves_pickup.h"

namespace {
const QString DEFAULT_TITLE{ QObject::tr("Release Sleeves") };
}

namespace models::steps {

StepSleevesPickup::StepSleevesPickup(QObject* parent)
  : steps::ParamsSpecific{ parent }
{}

StepSleevesPickup::StepSleevesPickup(const QJsonObject& json,QObject* parent)
  : StepSleevesPickup{ parent }
{
  update(json);
}

StepSleevesPickup::~StepSleevesPickup() = default;

} // namespace models
