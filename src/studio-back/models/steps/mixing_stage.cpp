#include "mixing_stage.h"

#include "moving_position.h"
#include "moving_speed.h"
#include "json/enumerator_decorator.h"
#include "json/int_decorator.h"

namespace {
const auto KEY_DURATION{ QStringLiteral("duration") };
const auto KEY_SPEED{ QStringLiteral("speed") };
const auto KEY_PAUSE_DURATION{ QStringLiteral("pause_duration") };
const auto KEY_PAUSE_POSITION{ QStringLiteral("pause_position") };

const auto TITLE_IDX{ QObject::tr("Mixing Idx") };
const auto TITLE_DURATION{ QObject::tr("Mixing duration") };
const auto TITLE_SPEED{ QObject::tr("Mixing speed") };
const auto TITLE_PAUSE_DURATION{ QObject::tr("Pause duration") };
const auto TITLE_PAUSE_POSITION{ QObject::tr("Pause position") };

const auto VALUE_DURATION{ 300 };
} // namespace defaults

namespace models::steps {

MixingStage::MixingStage(QObject* parent)
  : json::Entity{ QStringLiteral("mix_stage"), parent }
  , idx{ dynamic_cast<json::IntDecorator*>(addDataItem(
      new json::IntDecorator{ json::defaults::KEY_IDX, TITLE_IDX, 0, this })) }
  , duration{ dynamic_cast<json::IntDecorator*>(
      addDataItem(new json::IntDecorator{ KEY_DURATION,
                                          TITLE_DURATION,
                                          VALUE_DURATION,
                                          this })) }
  , speed{ dynamic_cast<json::EnumeratorDecorator*>(addDataItem(
      new json::EnumeratorDecorator{ KEY_SPEED,
                                     TITLE_SPEED,
                                     static_cast<int>(ETravelSpeed::Medium),
                                     TRAVEL_SPEED_MAPPER,
                                     this })) }
  , pauseDuration{ dynamic_cast<json::IntDecorator*>(
      addDataItem(new json::IntDecorator{ KEY_PAUSE_DURATION,
                                          TITLE_PAUSE_DURATION,
                                          0,
                                          this })) }
  , pausePosition{ dynamic_cast<json::EnumeratorDecorator*>(addDataItem(
      new json::EnumeratorDecorator{ KEY_PAUSE_POSITION,
                                     TITLE_PAUSE_POSITION,
                                     static_cast<int>(ETravelPos::AtWorkPos),
                                     TRAVEL_POSITION_MAPPER,
                                     this })) }
{}

MixingStage::MixingStage(const QJsonObject& json, QObject* parent)
  : MixingStage{ parent }
{
  update(json);
}

int
MixingStage::getIdx() const
{
  return idx->value();
}

MixingStage::~MixingStage() = default;

void
MixingStage::setIdx(const int t_idx)
{
  idx->setValue(t_idx);
  emit idxChanged();
}

int
MixingStage::getDuration() const
{
  return duration->value();
}

void
MixingStage::setDuration(const int t_duration)
{
  duration->setValue(t_duration);
  emit durationChanged();
}

int
MixingStage::getSpeed() const
{
  return speed->value();
}

void
MixingStage::setSpeed(const int t_speed)
{
  speed->setValue(t_speed);
  emit speedChanged();
}

} // namespace models
