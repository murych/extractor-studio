#ifndef STEPMIX_H
#define STEPMIX_H

#include "params_specific.h"

#define USE_GET_SET

namespace json {
template<typename T>
class EntityCollection;

class IntDecorator;
class EnumeratorDecorator;
}

namespace models::steps {

class MixingStage;

class StepMix : public steps::ParamsSpecific
{
  Q_OBJECT
#ifdef useGetSet
  Q_PROPERTY(int uiPatternRepeat READ getPatternRepeat WRITE setPatternRepeat
               NOTIFY patternRepeatChanged)
  Q_PROPERTY(int uiMixAmplitude READ getMixAmplitude WRITE setMixAmplitude
               NOTIFY mixAmplitudeChanged)
  Q_PROPERTY(int uiEnterSpeed READ getEnterSpeed WRITE setEnterSpeed NOTIFY
               enterSpeedChanged)
  Q_PROPERTY(int uiEndPosition READ getStartPosition WRITE setStartPosition
               NOTIFY endPositionChanged)
#endif
public:
  explicit StepMix(QObject* parent = nullptr);
  explicit StepMix(const QJsonObject& json, QObject* parent);
  ~StepMix() override;

  json::IntDecorator* patternRepeat{ nullptr };
  json::IntDecorator* mixAmplitude{ nullptr };
  json::EnumeratorDecorator* enterSpeed{ nullptr };
  json::EnumeratorDecorator* endPosition{ nullptr };

  json::EntityCollection<MixingStage>* mixStages{ nullptr };

  [[nodiscard]] int getPatternRepeat() const;
  [[nodiscard]] int getMixAmplitude() const;
  [[nodiscard]] int getEnterSpeed() const;
  [[nodiscard]] int getStartPosition() const;

  void setPatternRepeat(int value);
  void setMixAmplitude(int value);
  void setEnterSpeed(int value);
  void setStartPosition(int value);

signals:
  void patternRepeatChanged();
  void mixAmplitudeChanged();
  void enterSpeedChanged();
  void endPositionChanged();
  void mixStagesChanged();
};

} // namespace models

#endif // STEPMIX_H
