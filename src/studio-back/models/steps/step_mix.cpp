#include "step_mix.h"

#include "json/entity_collection.h"
#include "json/enumerator_decorator.h"
#include "json/int_decorator.h"
#include "mixing_stage.h"
#include "moving_position.h"
#include "moving_speed.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(MSM, "MODEL MIX")

namespace {
const auto DEFAULT_TITLE{ QObject::tr("Mix") };

const auto KEY_PATTERN_REPEATS{ QStringLiteral("pattern_repeat") };
const auto KEY_MIX_AMPLITUDE{ QStringLiteral("mix_amplitude") };
const auto KEY_END_POSITION{ QStringLiteral("end_position") };
const auto KEY_ENTER_SPEED{ QStringLiteral("enter_speed") };
const auto KEY_MIXING_STAGES{ QStringLiteral("stages") };
}


namespace models::steps {

StepMix::StepMix(QObject* parent)
  : steps::ParamsSpecific{ parent }
  , patternRepeat{ dynamic_cast<json::IntDecorator*>(
      addDataItem(new json::IntDecorator{ KEY_PATTERN_REPEATS,
                                          "Pattern Repeat",
                                          10,
                                          this })) }
  , mixAmplitude{ dynamic_cast<json::IntDecorator*>(
      addDataItem(new json::IntDecorator{ KEY_MIX_AMPLITUDE,
                                          "Mix Amplitude",
                                          100,
                                          this })) }
  , enterSpeed{ dynamic_cast<json::EnumeratorDecorator*>(addDataItem(
      new json::EnumeratorDecorator{ KEY_ENTER_SPEED,
                                     "Enter speed",
                                     static_cast<int>(ETravelSpeed::Slow),
                                     TRAVEL_SPEED_MAPPER,
                                     this })) }
  , endPosition{ dynamic_cast<json::EnumeratorDecorator*>(
      addDataItem(new json::EnumeratorDecorator{
        KEY_END_POSITION,
        "Mix end position",
        static_cast<int>(ETravelPos::AtLiquidMiddle),
        TRAVEL_POSITION_MAPPER,
        this })) }
  , mixStages{ dynamic_cast<json::EntityCollection<MixingStage>*>(
      addChildCollection(
        new json::EntityCollection<MixingStage>{ KEY_MIXING_STAGES, this })) }
{
}

StepMix::StepMix(const QJsonObject& json,QObject* parent)
  : StepMix{ parent }
{
  update(json);
}

StepMix::~StepMix()
{
  delete patternRepeat;
  delete mixAmplitude;
  delete enterSpeed;
  delete mixStages;
  delete endPosition;
}

int
StepMix::getPatternRepeat() const
{
  return patternRepeat->value();
}

int
StepMix::getMixAmplitude() const
{
  return mixAmplitude->value();
}

int
StepMix::getEnterSpeed() const
{
  return enterSpeed->value();
}

int
StepMix::getStartPosition() const
{
  return endPosition->value();
}

void
StepMix::setPatternRepeat(const int value)
{
  qCDebug(MSM) << "StepMix::setPatternRepeat" << value;
  patternRepeat->setValue(value);
  emit patternRepeatChanged();
}

void
StepMix::setMixAmplitude(const int value)
{
  qCDebug(MSM) << "StepMix::setMixAmplitude" << value;
  //  if (value < 0 || value > 100)
  //    return;

  mixAmplitude->setValue(value);
  emit mixAmplitudeChanged();
}

void
StepMix::setEnterSpeed(const int value)
{
  qCDebug(MSM) << "StepMix::setEnterSpeed" << value;

  enterSpeed->setValue(value);
  emit enterSpeedChanged();
}

void
StepMix::setStartPosition(const int value)
{

  endPosition->setValue(value);
  emit endPositionChanged();
}

} // namespace models
