#include "abstract_device.h"

#include "json/enumerator_decorator.h"
#include "json/string_decorator.h"

namespace models::device {

const std::unordered_map<int, QString> GenericDevice::device_type_mapper{
  { GenericDevice::EDeviceType::Unknown, "" },
  { GenericDevice::EDeviceType::Extractor, "Extractor" }
};

GenericDevice::GenericDevice(QObject* parent)
  : json::Entity{ "device", parent }
  , title{ dynamic_cast<json::StringDecorator*>(
      addDataItem(new json::StringDecorator{ json::defaults::KEY_TITLE,
                                             "Device Title",
                                             QLatin1String{},
                                             this })) }
  , description{ dynamic_cast<json::StringDecorator*>(
      addDataItem(new json::StringDecorator{ json::defaults::KEY_DESCRIPTION,
                                             "Device Description",
                                             QLatin1String{},
                                             this })) }
  , type{ dynamic_cast<json::EnumeratorDecorator*>(
      addDataItem(new json::EnumeratorDecorator{ json::defaults::KEY_TYPE,
                                                 "Device Type",
                                                 EDeviceType::Unknown,
                                                 device_type_mapper,
                                                 this })) }
{
}

GenericDevice::GenericDevice(const QJsonObject& json, QObject* parent)
  : GenericDevice{ parent }
{
  update(json);
}

GenericDevice::~GenericDevice()
{
  delete title;
  delete description;
  delete type;
}

} // namespace models
