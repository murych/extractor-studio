#ifndef MODELS_DEVICE_HEATER_H
#define MODELS_DEVICE_HEATER_H

#include "json/entity.h"

namespace json {
class IntDecorator;
class DoubleDecorator;
} // namespace json

namespace models::device {

class Heater : public json::Entity
{
  Q_OBJECT
public:
  explicit Heater(QObject* parent = nullptr);
  explicit Heater(const QJsonObject& json, QObject* parent = nullptr);
  ~Heater() override;

  json::DoubleDecorator* tempMin{ nullptr };
  json::DoubleDecorator* tempMax{ nullptr };
  json::IntDecorator* address{ nullptr };
};

} // namespace models

#endif // MODELS_DEVICE_HEATER_H
