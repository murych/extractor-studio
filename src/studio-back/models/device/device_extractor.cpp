#include "device_extractor.h"

#include "extractor_coords.h"
#include "motor.h"
#include "seat.h"
#include "json/entity_collection.h"
#include "json/enumerator_decorator.h"

namespace json::defaults {
static const QString KEY_MODEL{ QStringLiteral("model") };
static const QString KEY_SEATS{ QStringLiteral("seats") };
static const QString KEY_AXIS{ QStringLiteral("axis") };
static const QString KEY_COORDS{ QStringLiteral("coords") };
}

namespace models::device {

const std::unordered_map<int, QString> DeviceExtractor::extractor_type_mapper{
  { DeviceExtractor::EExtractorType::Unknown, "" },
  { DeviceExtractor::EExtractorType::Standard96, "Extractor 96" }
};

DeviceExtractor::DeviceExtractor(QObject* parent)
  : GenericDevice{ parent }
  , model{ dynamic_cast<json::EnumeratorDecorator*>(
      addDataItem(new json::EnumeratorDecorator(json::defaults::KEY_MODEL,
                                                "Device Model",
                                                0,
                                                extractor_type_mapper,
                                                this))) }
  , seats{ dynamic_cast<json::EntityCollection<Seat>*>(addChildCollection(
      new json::EntityCollection<Seat>{ json::defaults::KEY_SEATS, this })) }
  , motors{ dynamic_cast<json::EntityCollection<Motor>*>(addChildCollection(
      new json::EntityCollection<Motor>{ json::defaults::KEY_AXIS, this })) }
  , coords{ dynamic_cast<ExtractorCoords*>(
      addChild(json::defaults::KEY_COORDS, new ExtractorCoords{ this })) }
{
}

DeviceExtractor::DeviceExtractor(const QJsonObject& json, QObject* parent)
  : DeviceExtractor{ parent }
{
  update(json);
}

DeviceExtractor::~DeviceExtractor()
{
  delete model;
  delete seats;
  delete motors;
  delete coords;
}

} // namespace models
