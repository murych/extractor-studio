#ifndef MODELS_DEVICE_EXTRACTORCOORDS_H
#define MODELS_DEVICE_EXTRACTORCOORDS_H

#include "json/entity.h"

namespace json {
class DoubleDecorator;
} // namespace data

namespace models::device {

class ExtractorCoords : public json::Entity
{
  Q_OBJECT
public:
  explicit ExtractorCoords(QObject* parent = nullptr);
  explicit ExtractorCoords(const QJsonObject& json, QObject* parent);
  ~ExtractorCoords() override;

  json::DoubleDecorator* heightTravel{ nullptr };
  json::DoubleDecorator* heightDepo{ nullptr };
  json::DoubleDecorator* sleevesPickup{ nullptr };
  json::DoubleDecorator* sleevesRelease{ nullptr };
};

} // namespace models

#endif // MODELS_DEVICE_EXTRACTORCOORDS_H
