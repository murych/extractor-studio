#ifndef MODELS_DEVICE_MOTOR_H
#define MODELS_DEVICE_MOTOR_H

#include "json/entity.h"

namespace json {
class IntDecorator;
class EnumeratorDecorator;
} // namespace json

namespace models::device {

class Motor : public json::Entity
{
  Q_OBJECT
public:
  //! @todo тоже переделать в сложный enum как для позиций
  enum class EMicrostep
  {
    Unknown   = 0,
    Full      = 1,
    Half      = 2,
    Quater    = 3,
    Sixteenth = 4
  };
  Q_ENUM(EMicrostep)

  explicit Motor(QObject* parent = nullptr);
  explicit Motor(const QJsonObject& json, QObject* parent);
  ~Motor() override;

  json::StringDecorator* axis{ nullptr };
  json::StringDecorator* name{ nullptr };
  json::IntDecorator* speedLow{ nullptr };
  json::IntDecorator* speedMed{ nullptr };
  json::IntDecorator* speedHigh{ nullptr };
  json::IntDecorator* stepsPerMm{ nullptr };
  json::EnumeratorDecorator* microstep{ nullptr };
  json::IntDecorator* coordMax{ nullptr };
  json::IntDecorator* coordMin{ nullptr };

  [[nodiscard]] qint32 mmToSteps(double mm) const;
  [[nodiscard]] double stepsToMm(qint32 steps) const;

  [[nodiscard]] int getReqSpeed(int type) const;

private:
  const static std::unordered_map<int, QString> microstep_mapper;

  [[nodiscard]] static uint8_t msToInt(int ms);
  double m_offset{ 0.0 };
};

} // namespace models

#endif // MODELS_DEVICE_MOTOR_H
