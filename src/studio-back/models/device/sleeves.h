#ifndef MODELS_DEVICE_SLEEVES_H
#define MODELS_DEVICE_SLEEVES_H

#include "json/entity.h"

namespace json {
class IntDecorator;
class EnumeratorDecorator;
class DoubleDecorator;
}

namespace models::device {

class Sleeves : public json::Entity
{
  Q_OBJECT
  Q_DISABLE_COPY_MOVE(Sleeves)
public:
  enum class ESleeveType
  {
    Unknown = 0,
    Generic96
  };
  Q_ENUM(ESleeveType)

  explicit Sleeves(QObject* parent = nullptr);
  explicit Sleeves(const QJsonObject& json, QObject* parent = nullptr);
  ~Sleeves() override;

  json::StringDecorator* name{ nullptr };
  json::IntDecorator* numRow{ nullptr };
  json::IntDecorator* numCol{ nullptr };
  json::DoubleDecorator* rodDiameter{ nullptr };
  json::DoubleDecorator* rodHeight{ nullptr };
  json::EnumeratorDecorator* type{ nullptr };

  // private:
  const static std::unordered_map<int, QString> sleeve_type_mapper;
};

} // namespace models

using sleeve_type = models::device::Sleeves::ESleeveType;

#endif // MODELS_DEVICE_SLEEVES_H
