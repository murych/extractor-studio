#include "heater.h"

#include "json/double_decorator.h"
#include "json/int_decorator.h"

namespace models::device {

Heater::Heater(QObject* parent)
  : json::Entity{ QStringLiteral("heater"), parent }
  , tempMin{ dynamic_cast<json::DoubleDecorator*>(
      addDataItem(new json::DoubleDecorator{ "temp_min",
                                             "Minimal Termperature",
                                             0.0,
                                             this })) }
  , tempMax{ dynamic_cast<json::DoubleDecorator*>(
      addDataItem(new json::DoubleDecorator{ "temp_max",
                                             "Maximum Temperature",
                                             200.0,
                                             this })) }
  , address{ dynamic_cast<json::IntDecorator*>(addDataItem(
      new json::IntDecorator{ "address", QLatin1String{}, 0, this })) }
{}

Heater::Heater(const QJsonObject& json, QObject* parent)
  : Heater{ parent }
{
  update(json);
}

Heater::~Heater()
{
  delete tempMax;
  delete tempMin;
  delete address;
}

} // namespace models
