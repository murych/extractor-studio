#ifndef MODELS_DEVICE_PLATE_H
#define MODELS_DEVICE_PLATE_H

#include "json/entity.h"

namespace json {
class IntDecorator;
class BoolDecorator;
class DoubleDecorator;
}

namespace models::device {

class Heater;

class Seat : public json::Entity
{
  Q_OBJECT
public:
  explicit Seat(QObject* parent = nullptr);
  explicit Seat(const QJsonObject& json, QObject* parent);
  ~Seat() override;

  json::IntDecorator* idx{ nullptr };
  json::BoolDecorator* hasHeater{ nullptr };
  json::DoubleDecorator* coordinate{ nullptr };

  models::device::Heater* heater{ nullptr };
};

} // namespace models

#endif // MODELS_DEVICE_PLATE_H
