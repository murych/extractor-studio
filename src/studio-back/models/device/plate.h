#ifndef DEVICE_PLATE_H
#define DEVICE_PLATE_H

#include "json/entity.h"

namespace json {
class DoubleDecorator;
class IntDecorator;
class EnumeratorDecorator;
}

namespace models::device {

class Plate : public json::Entity
{
  Q_OBJECT
public:
  enum EPlateSize
  {
    Unknown = 0,
    Standard96Plate,
    User
  };
  Q_ENUM(EPlateSize)

  explicit Plate(QObject* parent = nullptr);
  explicit Plate(const QJsonObject& json, QObject* parent = nullptr);
  ~Plate() override;

  json::EnumeratorDecorator* type{ nullptr };
  json::StringDecorator* name{ nullptr };
  json::DoubleDecorator* height{ nullptr };
  json::DoubleDecorator* width{ nullptr };
  json::DoubleDecorator* depth{ nullptr };
  json::IntDecorator* numRows{ nullptr };
  json::IntDecorator* numCols{ nullptr };
  json::DoubleDecorator* vialVolume{ nullptr };
  json::DoubleDecorator* vialDiameter{ nullptr };
  json::DoubleDecorator* vialHeight{ nullptr };

  [[nodiscard]] double calculateLiquidHeight(double liq_volume) const;

  // private:
  const static std::unordered_map<int, QString> plate_size_mapper;
  const static QMap<int, QUuid> plate_size_uuid_mapper;
};

} // namespace models

using plate_size = models::device::Plate::EPlateSize;

#endif // DEVICE_PLATE_H
