#include "extractor_coords.h"

#include "json/double_decorator.h"

namespace json::defaults {
static const QString KEY_HEIGHT_TRAVEL{ QStringLiteral("height_travel") };
static const QString KEY_HEIGHT_SLEEVES_DEPO{ QStringLiteral("height_depo") };
static const QString KEY_SLEEVES_PICKUP{ QStringLiteral("sleeves_pickup") };
static const QString KEY_SLEEVES_RELEASE{ QStringLiteral("sleeves_release") };
}

namespace models::device {

ExtractorCoords::ExtractorCoords(QObject* parent)
  : json::Entity{ "coords", parent }
  , heightTravel{ dynamic_cast<json::DoubleDecorator*>(
      addDataItem(new json::DoubleDecorator{ json::defaults::KEY_HEIGHT_TRAVEL,
                                             QLatin1String{},
                                             0,
                                             this })) }
  , heightDepo{ dynamic_cast<json::DoubleDecorator*>(addDataItem(
      new json::DoubleDecorator{ json::defaults::KEY_HEIGHT_SLEEVES_DEPO,
                                 QLatin1String{},
                                 0,
                                 this })) }
  , sleevesPickup{ dynamic_cast<json::DoubleDecorator*>(
      addDataItem(new json::DoubleDecorator{ json::defaults::KEY_SLEEVES_PICKUP,
                                             QLatin1String{},
                                             0,
                                             this })) }
  , sleevesRelease{ dynamic_cast<json::DoubleDecorator*>(addDataItem(
      new json::DoubleDecorator{ json::defaults::KEY_SLEEVES_RELEASE,
                                 QLatin1String{},
                                 0,
                                 this })) }
{
}

ExtractorCoords::ExtractorCoords(const QJsonObject& json, QObject* parent)
  : ExtractorCoords{ parent }
{
  update(json);
}

ExtractorCoords::~ExtractorCoords()
{
  delete heightTravel;
  delete heightDepo;
  delete sleevesPickup;
  delete sleevesRelease;
}

} // namespace models
