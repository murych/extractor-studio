#include "seat.h"

#include "json/bool_decorator.h"
#include "json/double_decorator.h"
#include "json/int_decorator.h"
#include "models/device/heater.h"

namespace json::defaults {
static const QString KEY_HAS_HEATER{ QStringLiteral("has_heater") };
static const QString KEY_HEATER{ QStringLiteral("heater") };
static const QString KEY_COORD{ QStringLiteral("coord") };
}

namespace models::device {

Seat::Seat(QObject* parent)
  : json::Entity{ QStringLiteral("plate"), parent }
  , idx{ dynamic_cast<json::IntDecorator*>(
      addDataItem(new json::IntDecorator{ json::defaults::KEY_IDX,
                                          QStringLiteral("Plate Index"),
                                          0,
                                          this })) }
  , hasHeater{ dynamic_cast<json::BoolDecorator*>(
      addDataItem(new json::BoolDecorator{ json::defaults::KEY_HAS_HEATER,
                                           "Has heater",
                                           false,
                                           this })) }
  , coordinate{ dynamic_cast<json::DoubleDecorator*>(
      addDataItem(new json::DoubleDecorator{ json::defaults::KEY_COORD,
                                             "Coordinate",
                                             0.0,
                                             this })) }
  , heater{ dynamic_cast<Heater*>(
      addChild(json::defaults::KEY_HEATER, new Heater{ this })) }
{
}

Seat::Seat(const QJsonObject& json, QObject* parent)
  : Seat{ parent }
{
  update(json);
}

Seat::~Seat()
{
  delete idx;
  delete hasHeater;
  delete coordinate;
  delete heater;
}

} // namespace models
