#include "motor.h"

#include "models/steps/moving_speed.h"
#include "json/enumerator_decorator.h"
#include "json/int_decorator.h"
#include "json/string_decorator.h"

namespace json::defaults {
const auto KEY_AXIS{ QStringLiteral("axis") };
const auto KEY_SPEED_LOW{ QStringLiteral("speed_low") };
const auto KEY_SPEED_MEDIUM{ QStringLiteral("speed_medium") };
const auto KEY_SPEED_HIGH{ QStringLiteral("speed_high") };
const auto KEY_MICROSTEP{ QStringLiteral("microstep") };
const auto KEY_COORD_MAX{ QStringLiteral("coord_max") };
const auto KEY_COORD_MIN{ QStringLiteral("coord_min") };

constexpr auto VALUE_SPEED_LOW{ 10 };
constexpr auto VALUE_SPEED_MEDIUM{ 250 };
constexpr auto VALUE_SPEED_HIGH{ 500 };
} // namespace json::defaults

namespace models::device {

const std::unordered_map<int, QString> Motor::microstep_mapper{
  { static_cast<int>(Motor::EMicrostep::Full), "1/1 Step" },
  { static_cast<int>(Motor::EMicrostep::Half), "1/2 Step" },
  { static_cast<int>(Motor::EMicrostep::Quater), "1/4 Step" },
  { static_cast<int>(Motor::EMicrostep::Sixteenth), "1/16 Step" },
  { static_cast<int>(Motor::EMicrostep::Unknown), "" }
};

Motor::Motor(QObject* parent)
  : json::Entity{ QStringLiteral("motor"), parent }
  , axis{ dynamic_cast<json::StringDecorator*>(
      addDataItem(new json::StringDecorator{ json::defaults::KEY_AXIS,
                                             "Motor Axis",
                                             QLatin1String{},
                                             this })) }
  , name{ dynamic_cast<json::StringDecorator*>(
      addDataItem(new json::StringDecorator{ json::defaults::KEY_TITLE,
                                             "Motor Name",
                                             QLatin1String{},
                                             this })) }
  , speedLow{ dynamic_cast<json::IntDecorator*>(
      addDataItem(new json::IntDecorator{ json::defaults::KEY_SPEED_LOW,
                                          "Low Speed",
                                          json::defaults::VALUE_SPEED_LOW,
                                          this })) }
  , speedMed{ dynamic_cast<json::IntDecorator*>(
      addDataItem(new json::IntDecorator{ json::defaults::KEY_SPEED_MEDIUM,
                                          "Medium Speed",
                                          json::defaults::VALUE_SPEED_MEDIUM,
                                          this })) }
  , speedHigh{ dynamic_cast<json::IntDecorator*>(
      addDataItem(new json::IntDecorator{ json::defaults::KEY_SPEED_HIGH,
                                          "High Speed",
                                          json::defaults::VALUE_SPEED_HIGH,
                                          this })) }
  , microstep{ dynamic_cast<json::EnumeratorDecorator*>(addDataItem(
      new json::EnumeratorDecorator{ json::defaults::KEY_MICROSTEP,
                                     "Microstep Mode",
                                     static_cast<int>(EMicrostep::Unknown),
                                     microstep_mapper,
                                     this })) }
  , coordMax{ dynamic_cast<json::IntDecorator*>(
      addDataItem(new json::IntDecorator{ json::defaults::KEY_COORD_MAX,
                                          "Maximum travel",
                                          0,
                                          this })) }
  , coordMin{ dynamic_cast<json::IntDecorator*>(
      addDataItem(new json::IntDecorator{ json::defaults::KEY_COORD_MIN,
                                          "Minimum travel",
                                          0,
                                          this })) }
{
}

Motor::Motor(const QJsonObject& json, QObject* parent)
  : Motor{ parent }
{
  update(json);
}

Motor::~Motor()
{
  delete axis;
  delete name;
  delete speedLow;
  delete speedMed;
  delete speedHigh;
  delete stepsPerMm;
  delete microstep;
}

quint8
Motor::msToInt(const int ms)
{
  switch (static_cast<EMicrostep>(ms)) {
    case EMicrostep::Full:
      return 1;
    case EMicrostep::Half:
      return 2;
    case EMicrostep::Quater:
      return 4;
    case EMicrostep::Sixteenth:
      return 16;
    default:
      return 0;
  }

  return 0;
}

qint32
Motor::mmToSteps(const double mm) const
{
  const quint8 ms_int{ msToInt(microstep->value()) };
  const qint32 result{ static_cast<qint32>((mm + m_offset) *
                                           stepsPerMm->value() * ms_int) };

  return result;
}

double
Motor::stepsToMm(const qint32 steps) const
{
  const auto steps_d{ static_cast<double>(steps) };
  const auto ms_d{ static_cast<double>(msToInt(microstep->value())) };
  const auto sps_d{ static_cast<double>(stepsPerMm->value()) };
  return (steps_d / (sps_d * ms_d)) - m_offset;
}

int
Motor::getReqSpeed(int type) const
{
  switch (static_cast<steps::ETravelSpeed>(type)) {
    case steps::ETravelSpeed::Medium:
      return speedMed->value();
    case steps::ETravelSpeed::Fast:
      return speedHigh->value();
    default:
      return speedLow->value();
  }
}

} // namespace models
