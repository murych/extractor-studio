#include "sleeves.h"

#include "json/double_decorator.h"
#include "json/enumerator_decorator.h"
#include "json/int_decorator.h"
#include "json/string_decorator.h"

namespace json::defaults {
static const QString KEY_ROWS{ QStringLiteral("num_rows") };
static const QString KEY_COLS{ QStringLiteral("num_cols") };
static const QString KEY_ROD_HEIGHT{ QStringLiteral("rod_height") };
static const QString KEY_ROD_DIAMETER{ QStringLiteral("rod_diameter") };
} // namespace data

namespace models::device {

const std::unordered_map<int, QString> Sleeves::sleeve_type_mapper{
  { static_cast<int>(Sleeves::ESleeveType::Unknown), "" },
  { static_cast<int>(Sleeves::ESleeveType::Generic96), "Generic" }
};

Sleeves::Sleeves(QObject* parent)
  : json::Entity{ QStringLiteral("sleeves"), parent }
  , name{ dynamic_cast<json::StringDecorator*>(
      addDataItem(new json::StringDecorator{ json::defaults::KEY_TITLE,
                                             "Sleeves name",
                                             QLatin1String{},
                                             this })) }
  , numRow{ dynamic_cast<json::IntDecorator*>(addDataItem(
      new json::IntDecorator{ json::defaults::KEY_ROWS, "Rows", 0, this })) }
  , numCol{ dynamic_cast<json::IntDecorator*>(addDataItem(
      new json::IntDecorator{ json::defaults::KEY_COLS, "Columns", 0, this })) }
  , rodDiameter{ dynamic_cast<json::DoubleDecorator*>(
      addDataItem(new json::DoubleDecorator{ json::defaults::KEY_ROD_DIAMETER,
                                             "Rod Diameter",
                                             0.0,
                                             this })) }
  , rodHeight{ dynamic_cast<json::DoubleDecorator*>(
      addDataItem(new json::DoubleDecorator{ json::defaults::KEY_ROD_HEIGHT,
                                             "Rod Height",
                                             0.0,
                                             this })) }
  , type{ dynamic_cast<json::EnumeratorDecorator*>(addDataItem(
      new json::EnumeratorDecorator{ json::defaults::KEY_TYPE,
                                     "Type",
                                     static_cast<int>(ESleeveType::Unknown),
                                     sleeve_type_mapper,
                                     this })) }
{
}

Sleeves::Sleeves(const QJsonObject& json, QObject* parent)
  : Sleeves{ parent }
{
  update(json);
}

Sleeves::~Sleeves()
{
  delete name;
  delete type;
  delete numCol;
  delete numRow;
  delete rodDiameter;
  delete rodHeight;
}

} // namespace models
