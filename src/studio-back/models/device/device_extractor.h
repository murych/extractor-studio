#ifndef MODELS_DEVICE_DEVICEEXTRACTOR_H
#define MODELS_DEVICE_DEVICEEXTRACTOR_H

#include "abstract_device.h"

namespace json {
template<typename T>
class EntityCollection;

class EnumeratorDecorator;
} // namespace json

namespace models::device {
class Seat;
class Motor;
class ExtractorCoords;

class DeviceExtractor : public GenericDevice
{
  Q_OBJECT
public:
  enum EExtractorType
  {
    Unknown = 0,
    Standard96
  };
  Q_ENUM(EExtractorType)

  explicit DeviceExtractor(QObject* parent = nullptr);
  explicit DeviceExtractor(const QJsonObject& json, QObject* parent = nullptr);
  ~DeviceExtractor() override;

  json::EnumeratorDecorator* model{ nullptr };
  json::EntityCollection<Seat>* seats{ nullptr };
  json::EntityCollection<Motor>* motors{ nullptr };

  ExtractorCoords* coords{ nullptr };

  const static std::unordered_map<int, QString> extractor_type_mapper;
};

} // namespace models

using extractor_model = models::device::DeviceExtractor::EExtractorType;

#endif // MODELS_DEVICE_DEVICEEXTRACTOR_H
