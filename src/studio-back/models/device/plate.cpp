#include "plate.h"

#include "json/double_decorator.h"
#include "json/enumerator_decorator.h"
#include "json/int_decorator.h"
#include "json/string_decorator.h"
#include <cmath>

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(MDP, "MODEL DEVICE PLATE")

namespace data::defaults {
static const QString KEY_HEIGHT{ QStringLiteral("height") };
static const QString KEY_WIDTH{ QStringLiteral("width") };
static const QString KEY_DEPTH{ QStringLiteral("depth") };
static const QString KEY_ROWS{ QStringLiteral("num_rows") };
static const QString KEY_COLS{ QStringLiteral("num_cols") };
static const QString KEY_V_VOLUME{ QStringLiteral("vial_volume") };
static const QString KEY_V_DIAMETER{ QStringLiteral("vial_diameter") };
static const QString KEY_V_HEIGHT{ QStringLiteral("vial_height") };
} // namespace data::defaults

using namespace data;

namespace models::device {

const std::unordered_map<int, QString> Plate::plate_size_mapper{
  { Plate::EPlateSize::Unknown, "" },
  { Plate::EPlateSize::Standard96Plate, "Standard 96 plate" }
};

Plate::Plate(QObject* parent)
  : json::Entity{ QStringLiteral("params"), parent }
  , type{ dynamic_cast<json::EnumeratorDecorator*>(addDataItem(
      new json::EnumeratorDecorator{ json::defaults::KEY_TYPE,
                                     "Plate type",
                                     static_cast<int>(EPlateSize::Unknown),
                                     plate_size_mapper,
                                     this })) }
  , name{ dynamic_cast<json::StringDecorator*>(
      addDataItem(new json::StringDecorator{ json::defaults::KEY_TITLE,
                                             QStringLiteral("Plate Name"),
                                             QLatin1String{},
                                             this })) }
  , height{ dynamic_cast<json::DoubleDecorator*>(
      addDataItem(new json::DoubleDecorator{ defaults::KEY_HEIGHT,
                                             "Plate height",
                                             0.0,
                                             this })) }
  , width{ dynamic_cast<json::DoubleDecorator*>(
      addDataItem(new json::DoubleDecorator{ defaults::KEY_WIDTH,
                                             "Plate width",
                                             0.0,
                                             this })) }
  , depth{ dynamic_cast<json::DoubleDecorator*>(
      addDataItem(new json::DoubleDecorator{ defaults::KEY_DEPTH,
                                             "Plate depth",
                                             0.0,
                                             this })) }
  , numRows{ dynamic_cast<json::IntDecorator*>(addDataItem(
      new json::IntDecorator{ defaults::KEY_ROWS, "Rows", 0, this })) }
  , numCols{ dynamic_cast<json::IntDecorator*>(addDataItem(
      new json::IntDecorator{ defaults::KEY_COLS, "Cols", 0, this })) }
  , vialVolume{ dynamic_cast<json::DoubleDecorator*>(
      addDataItem(new json::DoubleDecorator{ defaults::KEY_V_VOLUME,
                                             "Vial volume",
                                             0.0,
                                             this })) }
  , vialDiameter{ dynamic_cast<json::DoubleDecorator*>(
      addDataItem(new json::DoubleDecorator{ defaults::KEY_V_DIAMETER,
                                             "Vial diameter",
                                             0.0,
                                             this })) }
  , vialHeight{ dynamic_cast<json::DoubleDecorator*>(
      addDataItem(new json::DoubleDecorator{ defaults::KEY_V_HEIGHT,
                                             "Vial height",
                                             0.0,
                                             this })) }
{
}

Plate::Plate(const QJsonObject& json, QObject* parent)
  : Plate{ parent }
{
  update(json);
}

Plate::~Plate()
{
  delete height;
  delete width;
  delete depth;
  delete numCols;
  delete numRows;
  delete vialDiameter;
  delete vialHeight;
  delete vialVolume;
}

/*!
 * \brief Plate::calculateLiquidHeight
 * \param liqVolume - объем налитых реагентов [мкл] / [мм3]
 * \return
 * - если \f$ V_l < V_2 \f$: \f$ h_l = \frac{V_l}{\pi r^2} \f$
 * - если \f$ V_2 \leq V_l < V_1 \f$: \f$ h_l = h_2 + \frac{V_l - V_2}{a^2} \f$
 * - иначе: -1
 * \warning модель требует уточнения под каждый тип плашек
 */
double
Plate::calculateLiquidHeight(double liq_volume) const
{
  qCDebug(MDP) << "calculating volume" << liq_volume << "in plate" << id();
  qCDebug(MDP) << "avaliable volume" << vialVolume->value();
  qCDebug(MDP) << "vial height" << vialHeight->value() << "vial diameter"
               << vialDiameter->value();

  const auto v_1{ std::pow(vialDiameter->value(), 2) *
                  (vialHeight->value() / 2.0) };
  const auto v_2{ M_PI * std::pow(vialDiameter->value(), 2) *
                  (vialHeight->value() / 0) };

  qCDebug(MDP) << "V1 =" << v_1 << "V2 =" << v_2;

  if (liq_volume < v_2) {
    return liq_volume * M_1_PI * (1 / std::pow(vialDiameter->value(), 2));
  }

  if (liq_volume >= v_2 && liq_volume < v_1) {
    return (vialHeight->value() / 2 +
            (liq_volume - v_2) / std::pow(vialDiameter->value(), 2));
  }

  return -1;
}

} // namespace models
