#ifndef MODELS_DEVICE_ABSTRACTDEVICE_H
#define MODELS_DEVICE_ABSTRACTDEVICE_H

#include "json/entity.h"

namespace json {
class EnumeratorDecorator;
}

namespace models::device {

class GenericDevice : public json::Entity
{
  Q_OBJECT
public:
  enum /*class*/ EDeviceType
  {
    Unknown   = 0,
    Extractor = 1
  };
  Q_ENUM(EDeviceType)

  explicit GenericDevice(QObject* parent = nullptr);
  explicit GenericDevice(const QJsonObject& json, QObject* parent = nullptr);
  ~GenericDevice() override;

  json::StringDecorator* title{ nullptr };
  json::StringDecorator* description{ nullptr };
  json::EnumeratorDecorator* type{ nullptr };

  static const std::unordered_map<int, QString> device_type_mapper;
};

} // namespace models

using device_type = models::device::GenericDevice::EDeviceType;

#endif // MODELS_DEVICE_ABSTRACTDEVICE_H
