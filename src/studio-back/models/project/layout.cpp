#include "layout.h"

#include "../plates/plate.h"
#include "json/entity_collection.h"

namespace models::project {

Layout::Layout(QObject* parent)
  : Entity{ "layout", parent }
  , plates{ dynamic_cast<json::EntityCollection<plates::Plate>*>(
      addChildCollection(
        new json::EntityCollection<plates::Plate>{ "plates", this })) }
{
  connect(plates,
          &json::EntityCollectionBase::collectionChanged,
          this,
          &Layout::modified);
}

Layout::Layout(const QJsonObject& json,QObject* parent)
  : Layout{ parent }
{
  update(json);
}

Layout::~Layout()
{
  delete plates;
}

} // namespace models
