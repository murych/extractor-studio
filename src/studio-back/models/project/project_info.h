#ifndef MODELS_PROJECT_PROJECT_H
#define MODELS_PROJECT_PROJECT_H

#include "json/entity.h"

namespace json {
class IntDecorator;
class DateTimeDecorator;
} // namespace json

namespace models::project {

class Project : public json::Entity
{
  Q_OBJECT
public:
  explicit Project(QObject* parent = nullptr);
  Project(const QJsonObject& json, QObject* parent = nullptr);
  ~Project() override;

  json::DateTimeDecorator* dateCreated{ nullptr };
  json::DateTimeDecorator* dateModified{ nullptr };
  json::StringDecorator* title{ nullptr };
  json::StringDecorator* author{ nullptr };
  json::StringDecorator* description{ nullptr };
  json::IntDecorator* revision{ nullptr };

  json::IntDecorator* deviceType{ nullptr };
  json::IntDecorator* deviceFamily{ nullptr };
};

} // namespace models::project

#endif // MODELS_PROJECT_PROJECT_H
