#ifndef LAYOUT_H
#define LAYOUT_H

#include "json/entity.h"

namespace json {
template<typename T>
class EntityCollection;
} // namespace json

namespace models::plates {
class Plate;
} // namespace models::plates

namespace models::project {

class Layout : public json::Entity
{
  Q_OBJECT
public:
  explicit Layout(QObject* parent = nullptr);
  explicit Layout(const QJsonObject& json, QObject* parent = nullptr);
  ~Layout() override;

  json::EntityCollection<plates::Plate>* plates{ nullptr };

signals:
  void modified();
};

} // namespace project::models

#endif // LAYOUT_H
