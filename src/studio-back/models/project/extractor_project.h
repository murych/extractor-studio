#ifndef EXTRACTORPROJECT_H
#define EXTRACTORPROJECT_H

#include "json/entity.h"

namespace json {
class IntDecorator;
} // namespace json

namespace models::project {

class Project;
class Laboratory;
class Layout;
class Protocol;

class ExtractorProject : public json::Entity
{
  Q_OBJECT

public:
  explicit ExtractorProject(QObject* parent = nullptr);
  ExtractorProject(const QJsonObject& json, QObject* parent = nullptr);
  ~ExtractorProject() override;

  json::IntDecorator* specMajorVersion{ nullptr };
  json::IntDecorator* specMinorVersion{ nullptr };

  Project* info{ nullptr };
  Laboratory* laboratory{ nullptr };
  Layout* layout{ nullptr };
  Protocol* protocol{ nullptr };

  void setSpecVersion(int major, int minor) const;
};

}

#endif // EXTRACTORPROJECT_H
