#ifndef LABORATORY_H
#define LABORATORY_H

#include "json/entity.h"

namespace json {
class StringDecorator;
} // namespace json

namespace models::project {

class Laboratory : public json::Entity
{
  Q_OBJECT
  Q_PROPERTY(QString name READ getName WRITE setName NOTIFY nameChanged)
  Q_PROPERTY(
    QString address READ getAddress WRITE setAddress NOTIFY addressChanged)
  Q_PROPERTY(QString phone READ getPhone WRITE setPhone NOTIFY phoneChanged)
  Q_PROPERTY(QString email READ getEmail WRITE setEmail NOTIFY emailChanged)

public:
  explicit Laboratory(QObject* parent = nullptr);
  explicit Laboratory(const QJsonObject& json, QObject* parent);
  ~Laboratory() override;

  [[nodiscard]] QString getName() const;
  void setName(const QString& t_name) const;

  [[nodiscard]] QString getAddress() const;
  void setAddress(const QString& t_address) const;

  [[nodiscard]] QString getPhone() const;
  void setPhone(const QString& t_phone) const;

  [[nodiscard]] QString getEmail() const;
  void setEmail(const QString& t_email) const;

  json::StringDecorator* name{ nullptr };
  json::StringDecorator* address{ nullptr };
  json::StringDecorator* phone{ nullptr };
  json::StringDecorator* email{ nullptr };

signals:
  void nameChanged();
  void addressChanged();
  void phoneChanged();
  void emailChanged();
};

} // namespace models

#endif // LABORATORY_H
