#include "project_info.h"

#include "json/datetime_decorator.h"
#include "json/int_decorator.h"
#include "json/string_decorator.h"

namespace json::defaults {
const QString KEY_DATE_CREATED{ QStringLiteral("date_created") };
const QString KEY_DATE_MODIFIED{ QStringLiteral("date_modified") };
const QString KEY_AUTHOR{ QStringLiteral("author") };
const QString KEY_REVISION{ QStringLiteral("revision") };
const QString KEY_DEVICE_TYPE{ QStringLiteral("device_type") };
const QString KEY_DEVICE_FAMILY{ QStringLiteral("device_family") };
} // namespace data

namespace models::project {

Project::Project(QObject* parent)
  : json::Entity{ "project_info", parent }
  , dateCreated{ dynamic_cast<json::DateTimeDecorator*>(
      addDataItem(new json::DateTimeDecorator{ json::defaults::KEY_DATE_CREATED,
                                               "Date created",
                                               {},
                                               this })) }
  , dateModified{ dynamic_cast<json::DateTimeDecorator*>(addDataItem(
      new json::DateTimeDecorator{ json::defaults::KEY_DATE_MODIFIED,
                                   "Date modified",
                                   {},
                                   this })) }
  , title{ dynamic_cast<json::StringDecorator*>(
      addDataItem(new json::StringDecorator{ json::defaults::KEY_TITLE,
                                             "Project title",
                                             QLatin1String{},
                                             this })) }
  , author{ dynamic_cast<json::StringDecorator*>(
      addDataItem(new json::StringDecorator{ json::defaults::KEY_AUTHOR,
                                             "Project author",
                                             QLatin1String{},
                                             this })) }
  , description{ dynamic_cast<json::StringDecorator*>(
      addDataItem(new json::StringDecorator{ json::defaults::KEY_DESCRIPTION,
                                             "Project description",
                                             QLatin1String{},
                                             this })) }
  , revision{ dynamic_cast<json::IntDecorator*>(
      addDataItem(new json::IntDecorator{ json::defaults::KEY_REVISION,
                                          "Project revision",
                                          0,
                                          this })) }
  , deviceType{ dynamic_cast<json::IntDecorator*>(
      addDataItem(new json::IntDecorator{ json::defaults::KEY_DEVICE_TYPE,
                                          "Device Type",
                                          0,
                                          this })) }
  , deviceFamily{ dynamic_cast<json::IntDecorator*>(
      addDataItem(new json::IntDecorator{ json::defaults::KEY_DEVICE_FAMILY,
                                          "Device Family",
                                          0,
                                          this })) }
{}

Project::Project(const QJsonObject& json, QObject* parent)
  : Project{ parent }
{
  update(json);
}

Project::~Project()
{
  delete dateCreated;
  delete dateModified;
  delete title;
  delete author;
  delete description;
}

} // namespace models
