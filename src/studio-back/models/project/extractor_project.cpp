#include "extractor_project.h"

#include "laboratory.h"
#include "layout.h"
#include "project_info.h"
#include "protocol.h"
#include "json/int_decorator.h"

namespace json::defaults {
const QString KEY_SPEC_MINOR{ QStringLiteral("spec_minor_version") };
const QString KEY_SPEC_MAJOR{ QStringLiteral("spec_major_version") };
}

namespace models::project {

ExtractorProject::ExtractorProject(QObject* parent)
  : json::Entity{ "project", parent }
  , specMajorVersion{ dynamic_cast<json::IntDecorator*>(
      addDataItem(new json::IntDecorator{ json::defaults::KEY_SPEC_MAJOR,
                                          "spec major version",
                                          0,
                                          this })) }
  , specMinorVersion{ dynamic_cast<json::IntDecorator*>(
      addDataItem(new json::IntDecorator{ json::defaults::KEY_SPEC_MINOR,
                                          "spec minor version",
                                          1,
                                          this })) }
  , info{ dynamic_cast<Project*>(
      addChild(QStringLiteral("project_info"), new Project{ this })) }
  , laboratory{ dynamic_cast<Laboratory*>(
      addChild(QStringLiteral("lab_info"), new Laboratory{ this })) }
  , layout{ dynamic_cast<Layout*>(
      addChild(QStringLiteral("layout"), new Layout{ this })) }
  , protocol{ dynamic_cast<Protocol*>(
      addChild(QStringLiteral("protocol"), new Protocol{ this })) }
{}

ExtractorProject::ExtractorProject(const QJsonObject& json, QObject* parent)
  : ExtractorProject{ parent }
{
  update(json);
}

ExtractorProject::~ExtractorProject()
{
  delete info;
  delete laboratory;
  delete layout;
  delete protocol;
}

void
ExtractorProject::setSpecVersion(const int major, const int minor) const
{
  specMajorVersion->setValue(major);
  specMinorVersion->setValue(minor);
}

} // namespave models
