#include "laboratory.h"

#include "json/string_decorator.h"

namespace models::project {

Laboratory::Laboratory(QObject* parent)
  : json::Entity{ QStringLiteral("laboratory"), parent }
  , name{ dynamic_cast<json::StringDecorator*>(
      addDataItem(new json::StringDecorator{ "lab_name",
                                             "Laboratory Name",
                                             "Lab Name",
                                             this })) }
  , address{ dynamic_cast<json::StringDecorator*>(
      addDataItem(new json::StringDecorator{ "lab_address",
                                             "Laboratory Address",
                                             "",
                                             this })) }
  , phone{ dynamic_cast<json::StringDecorator*>(
      addDataItem(new json::StringDecorator{ "lab_phone",
                                             "Laboratory Phone",
                                             "88005553535",
                                             this })) }
  , email{ dynamic_cast<json::StringDecorator*>(
      addDataItem(new json::StringDecorator{ "lab_email",
                                             "Laboratory Email",
                                             "research@lab.ogr",
                                             this })) }
{}

Laboratory::Laboratory(const QJsonObject& json, QObject* parent)
  : Laboratory{ parent }
{
  update(json);
}

Laboratory::~Laboratory()
{
  delete name;
  delete address;
  delete phone;
  delete email;
}

QString
Laboratory::getAddress() const
{
  return address->value();
}

void
Laboratory::setAddress(const QString& t_address) const
{
  address->setValue(t_address);
}

QString
Laboratory::getPhone() const
{
  return phone->value();
}

void
Laboratory::setPhone(const QString& t_phone) const
{
  phone->setValue(t_phone);
}

QString
Laboratory::getEmail() const
{
  return email->value();
}

void
Laboratory::setEmail(const QString& t_email) const
{
  email->setValue(t_email);
}

QString
models::project::Laboratory::getName() const
{
  return name->value();
}

void
Laboratory::setName(const QString& t_name) const
{
  name->setValue(t_name);
}

} // namespace models
