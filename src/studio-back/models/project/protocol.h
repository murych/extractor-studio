#ifndef PROTOCOL_H
#define PROTOCOL_H

#include "json/entity.h"

namespace json {
template<typename T>
class EntityCollection;
} // namespace json

namespace models::steps {
class Step;
} // namespace models::steps

namespace models::project {

class Protocol : public json::Entity
{
  Q_OBJECT
public:
  explicit Protocol(QObject* parent = nullptr);
  Protocol(const QJsonObject& json, QObject* parent = nullptr);
  ~Protocol() override;

  json::EntityCollection<steps::Step>* steps{ nullptr };

signals:
  void modified();
};

} // namespace project::models

#endif // PROTOCOL_H
