#include "protocol.h"

#include "../steps/step.h"
#include "json/entity_collection.h"

namespace models::project {

Protocol::Protocol(QObject* parent)
  : json::Entity{ "protocol", parent }
  , steps{ dynamic_cast<json::EntityCollection<steps::Step>*>(
      addChildCollection(
        new json::EntityCollection<steps::Step>{ "steps", this })) }
{
  connect(steps,
          &json::EntityCollectionBase::collectionChanged,
          this,
          &Protocol::modified);
}

Protocol::Protocol(const QJsonObject& json,QObject* parent)
  : Protocol{ parent }
{
  update(json);
}

Protocol::~Protocol()
{
  delete steps;
}

} // namespace models
