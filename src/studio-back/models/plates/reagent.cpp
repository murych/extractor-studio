#include "reagent.h"

#include "json/enumerator_decorator.h"
#include "json/int_decorator.h"
#include "json/string_decorator.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(M_REAGENT, "MODEL REAGENT")

namespace json::defaults {
const auto KEY_COLOR{ QStringLiteral("color") };
const auto KEY_VOLUME{ QStringLiteral("volume") };

constexpr auto DEFAULT_VOLUME{ 5 };
} // namespace data

namespace models::plates {

const std::unordered_map<int, QString> Reagent::reagent_type_mapper{
  { static_cast<int>(Reagent::EReagentType::Unknown), "" },
  { static_cast<int>(Reagent::EReagentType::Reagent), QObject::tr("Reagent") },
  { static_cast<int>(Reagent::EReagentType::Sample), QObject::tr("Sample") }
};

Reagent::Reagent(QObject* parent)
  : json::Entity{ QStringLiteral("reagent"), parent }
  , reagentName{ dynamic_cast<json::StringDecorator*>(
      addDataItem(new json::StringDecorator{ json::defaults::KEY_TITLE,
                                             tr("Reagent Name"),
                                             QLatin1String{},
                                             this })) }
  , reagentVolume{ dynamic_cast<json::IntDecorator*>(
      addDataItem(new json::IntDecorator{ json::defaults::KEY_VOLUME,
                                          tr("Reagent Volume, μL"),
                                          json::defaults::DEFAULT_VOLUME,
                                          this })) }
  , reagentColor{ dynamic_cast<json::StringDecorator*>(
      addDataItem(new json::StringDecorator{ json::defaults::KEY_COLOR,
                                             tr("Reagent Color"),
                                             QStringLiteral("#8b0012"),
                                             this })) }
  , reagentType{ dynamic_cast<json::EnumeratorDecorator*>(addDataItem(
      new json::EnumeratorDecorator{ json::defaults::KEY_TYPE,
                                     tr("Reagent Type"),
                                     static_cast<int>(EReagentType::Reagent),
                                     reagent_type_mapper,
                                     this })) }
{
}

Reagent::Reagent(const QJsonObject& json, QObject* parent)
  : Reagent{ parent }
{
  update(json);
}

Reagent::~Reagent()
{
  delete reagentName;
  delete reagentType;
  delete reagentVolume;
  delete reagentColor;
}

QString
Reagent::name() const
{
  return reagentName->value();
}

int
Reagent::volume() const
{
  return reagentVolume->value();
}

int
Reagent::type() const
{
  return reagentType->value();
}

QString
Reagent::color() const
{
  return reagentColor->value();
}

void
Reagent::setName(const QString& value)
{
  reagentName->setValue(value);
  emit nameChanged();
}

void
Reagent::setVolume(const int value)
{
  reagentVolume->setValue(value);
  emit volumeChanged();
}

void
Reagent::setType(const int value)
{
  reagentType->setValue(value);
  emit typeChanged();
}

void
Reagent::setColor(const QString& value)
{
  reagentColor->setValue(value);
  emit colorChanged();
}

} // namespace models
