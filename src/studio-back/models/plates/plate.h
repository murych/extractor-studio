#ifndef PLATE_H
#define PLATE_H

#include "json/entity.h"

namespace json {
template<typename T>
class EntityCollection;

class IntDecorator;
class EnumeratorDecorator;
}

namespace models::plates {

class Reagent;

class Plate : public json::Entity
{
  Q_OBJECT
  Q_PROPERTY(json::IntDecorator* ui_plateIdx MEMBER seat NOTIFY plateIdxChanged)
  Q_PROPERTY(
    json::StringDecorator* ui_plateName MEMBER name NOTIFY plateNameChanged)
  Q_PROPERTY(json::EnumeratorDecorator* ui_plateType MEMBER content NOTIFY
               plateTypeChanged)
  Q_PROPERTY(json::StringDecorator* ui_plateBarcode MEMBER barcode NOTIFY
               plateBarcodeChanged)
public:
  enum class EPlateContent
  {
    Undefined = 0,
    Liquids,
    Sleeves
  };
  Q_ENUM(EPlateContent)

  explicit Plate(QObject* parent = nullptr);
  explicit Plate(const QJsonObject& json, QObject* parent = nullptr);
  ~Plate() override;

  json::StringDecorator* name{ nullptr };
  json::IntDecorator* seat{ nullptr };
  json::EnumeratorDecorator* content{ nullptr };
  json::StringDecorator* barcode{ nullptr };
  json::EntityCollection<Reagent>* reagents{ nullptr };

  json::StringDecorator* size{ nullptr };
  json::StringDecorator* sleeves{ nullptr };

  [[nodiscard]] int getReagentVolume() const;

  void setPlateName(const QString& new_name);
  void setPlateIdx(int new_idx);
  void setPlateSize(const QString& new_size);
  void setPlateContent(int new_content);
  void setSleeveType(const QString& new_type);

signals:
  void plateNameChanged();
  void plateIdxChanged();
  void plateSizeChanged();
  void plateTypeChanged();
  void plateBarcodeChanged();
  void plateSleevesChanged();

private:
  const static std::unordered_map<int, QString> plate_content_mapper;
};

} // namespace models

using plate_type = models::plates::Plate::EPlateContent;

#endif // PLATE_H
