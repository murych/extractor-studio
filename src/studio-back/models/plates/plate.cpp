#include "plate.h"

#include "json/entity_collection.h"
#include "json/enumerator_decorator.h"
#include "json/int_decorator.h"
#include "json/string_decorator.h"
#include "models/plates/reagent.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(MP, "MODEL PLATE")

namespace data::defaults {
const QString KEY_SEAT{ QStringLiteral("seat") };
const QString KEY_SIZE{ QStringLiteral("size") };
const QString KEY_SLEEVES{ QStringLiteral("sleeves") };
const QString KEY_REAGENTS{ QStringLiteral("reagents") };
} // namespace data::defaults

namespace models::plates {

const std::unordered_map<int, QString> Plate::plate_content_mapper{
  { static_cast<int>(plate_type::Liquids), "Liquids" },
  { static_cast<int>(plate_type::Sleeves), "Sleeves" }
};

Plate::Plate(QObject* parent)
  : Entity{ QStringLiteral("plate"), parent }
  , name{ dynamic_cast<json::StringDecorator*>(
      addDataItem(new json::StringDecorator{ json::defaults::KEY_TITLE,
                                             "Plate Name",
                                             "New Plate",
                                             this })) }
  , seat{ dynamic_cast<json::IntDecorator*>(
      addDataItem(new json::IntDecorator{ data::defaults::KEY_SEAT,
                                          "Plate Seat",
                                          0,
                                          this })) }
  , content{ dynamic_cast<json::EnumeratorDecorator*>(addDataItem(
      new json::EnumeratorDecorator{ json::defaults::KEY_TYPE,
                                     "Plate Content",
                                     static_cast<int>(EPlateContent::Liquids),
                                     plate_content_mapper,
                                     this })) }
  , reagents{ dynamic_cast<json::EntityCollection<Reagent>*>(addChildCollection(
      new json::EntityCollection<Reagent>{ data::defaults::KEY_REAGENTS })) }
  , size{ dynamic_cast<json::StringDecorator*>(
      addDataItem(new json::StringDecorator{ data::defaults::KEY_SIZE,
                                             "Plate Size",
                                             QLatin1String{},
                                             this })) }
  , sleeves{ dynamic_cast<json::StringDecorator*>(
      addDataItem(new json::StringDecorator{ data::defaults::KEY_SLEEVES,
                                             "Sleeves Type",
                                             QLatin1String{},
                                             this })) }
{
  qRegisterMetaType<models::plates::Reagent*>("Reagent");
}

Plate::Plate(const QJsonObject& json, QObject* parent)
  : Plate{ parent }
{
  update(json);

  const auto contains_name{ json.contains(name->key()) };
  const auto name_is_empty{ json.value(name->key()).toString().isEmpty() };

  if (!contains_name || name_is_empty) {
    name->setValue("New Plate");
  }
}

Plate::~Plate()
{
  delete barcode;
  delete name;
  delete seat;
  delete size;
  delete reagents;
}

int
Plate::getReagentVolume() const
{
  const auto reagents_temp{ reagents->derivedEntities() };
  return std::accumulate(reagents_temp.cbegin(),
                         reagents_temp.cend(),
                         0,
                         [](int accumulator, const Reagent* reagent) {
                           return accumulator + reagent->reagentVolume->value();
                         });
}

void
Plate::setPlateIdx(const int new_idx)
{
  seat->setValue(new_idx);
  emit plateIdxChanged();
}

void
Plate::setPlateSize(const QString& new_size)
{
  size->setValue(new_size);
  emit plateSizeChanged();
}

void
Plate::setPlateContent(const int new_content)
{
  content->setValue(new_content);
  emit plateTypeChanged();
}

void
Plate::setPlateName(const QString& new_name)
{
  name->setValue(new_name);
  emit plateNameChanged();
}

void
Plate::setSleeveType(const QString& new_type)
{
  sleeves->setValue(new_type);
  emit plateSleevesChanged();
}

} // namespace models
