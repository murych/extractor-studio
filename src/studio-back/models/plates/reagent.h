#ifndef REAGENT_H
#define REAGENT_H

#include "json/entity.h"

namespace json {
class IntDecorator;
class EnumeratorDecorator;
}

namespace models::plates {

class Reagent : public json::Entity
{
  Q_OBJECT
  Q_PROPERTY(json::StringDecorator* ui_name MEMBER reagentName CONSTANT)
  Q_PROPERTY(json::IntDecorator* ui_volume MEMBER reagentVolume CONSTANT)
  Q_PROPERTY(json::EnumeratorDecorator* ui_type MEMBER reagentType CONSTANT)

public:
  enum class EReagentType
  {
    Unknown = 0,
    Reagent,
    Sample,
    User
  };
  Q_ENUM(EReagentType)

  explicit Reagent(QObject* parent = nullptr);
  explicit Reagent(const QJsonObject& json, QObject* parent = nullptr);
  ~Reagent() override;

  json::StringDecorator* reagentName{ nullptr };
  json::IntDecorator* reagentVolume{ nullptr };
  json::StringDecorator* reagentColor{ nullptr };
  json::EnumeratorDecorator* reagentType{ nullptr };

  const static std::unordered_map<int, QString> reagent_type_mapper;

  [[nodiscard]] QString icon() const { return m_icon; }
  [[nodiscard]] QString name() const;
  [[nodiscard]] int volume() const;
  [[nodiscard]] int type() const;
  [[nodiscard]] QString color() const;

private:
  const QString m_icon{ QStringLiteral(":/48/color/error_48px.png") };

public slots:
  void setType(int value);
  void setVolume(int value);
  void setName(const QString& value);
  void setColor(const QString& value);

signals:
  void nameChanged();
  void volumeChanged();
  void typeChanged();
  void colorChanged();
};

} // namespace models

#endif // REAGENT_H
