#ifndef CONFIG_LABORATORYCONFIG_H
#define CONFIG_LABORATORYCONFIG_H

#include "abstract_config.h"
#include <QSettings>
#include <QString>

namespace config {

class LaboratoryConfig : public AbstractConfig
{
public:
  LaboratoryConfig();
  ~LaboratoryConfig() override = default;

  LaboratoryConfig(const LaboratoryConfig&)                        = delete;
  LaboratoryConfig(LaboratoryConfig&&) noexcept                    = delete;
  auto operator=(const LaboratoryConfig&) -> LaboratoryConfig&     = delete;
  auto operator=(LaboratoryConfig&&) noexcept -> LaboratoryConfig& = delete;

  // LaboratoryConfig interface
  void readSettings() override;
  void writeSettings() override;

  [[nodiscard]] auto labName() const { return m_labName; }
  [[nodiscard]] auto labPhone() const { return m_labPhone; }
  [[nodiscard]] auto labEmail() const { return m_labEmail; }
  [[nodiscard]] auto labAddress() const { return m_labAddress; }

  bool setLabName(const QString& name);
  bool setLabPhone(const QString& phone);
  bool setLabEmail(const QString& email);
  bool setLabAddress(const QString& address);

private:
  QSettings m_settings;

  QString m_labName{ QStringLiteral("Lab Name") };
  QString m_labPhone{ QStringLiteral("88005553535") };
  QString m_labEmail{ QStringLiteral("research@lab.org") };
  QString m_labAddress{ QLatin1String() };
};

} // namespace config

#endif // CONFIG_LABORATORYCONFIG_H
