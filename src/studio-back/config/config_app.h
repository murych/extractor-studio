#ifndef CONFIG_APP_H
#define CONFIG_APP_H

#include "abstract_config.h"
#include <QDir>
#include <QPoint>
#include <QSettings>
#include <QSize>

namespace config {

class ApplicationConfig : public AbstractConfig
{
public:
  ApplicationConfig();
  ~ApplicationConfig() override = default;

  ApplicationConfig(const ApplicationConfig&)                        = delete;
  ApplicationConfig(ApplicationConfig&&) noexcept                    = delete;
  auto operator=(const ApplicationConfig&) -> ApplicationConfig&     = delete;
  auto operator=(ApplicationConfig&&) noexcept -> ApplicationConfig& = delete;

  // AbstractConfig interface
  void readSettings() override;
  void writeSettings() override;

  [[nodiscard]] auto firstRun() const { return m_firstRun; }
  [[nodiscard]] auto workDir() const { return m_workDir; }
  [[nodiscard]] auto windowSize() const { return m_windowSize; }
  [[nodiscard]] auto windowPos() const { return m_windowPos; }

  void setWorkDir(const QString& work_dir) { m_workDir = work_dir; }
  void setWindowSize(QSize size) { m_windowSize = size; }
  void setWindowPos(QPoint point) { m_windowPos = point; }

private:
  QSettings m_settings;

  bool m_firstRun{ false };
  QString m_workDir{ QDir::homePath() };
  QSize m_windowSize{ 1000, 700 };
  QPoint m_windowPos{ 400, 0 };

  struct Implementation;
};

} // namespace config

#endif // CONFIG_APP_H
