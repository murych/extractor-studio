#ifndef CONFIG_ABSTRACTCONFIG_H
#define CONFIG_ABSTRACTCONFIG_H

namespace config {

class AbstractConfig
{
public:
  AbstractConfig() = default;
  virtual ~AbstractConfig() = default;

  AbstractConfig(const AbstractConfig&)                        = delete;
  AbstractConfig(AbstractConfig&&) noexcept                    = delete;
  auto operator=(const AbstractConfig&) -> AbstractConfig&     = delete;
  auto operator=(AbstractConfig&&) noexcept -> AbstractConfig& = delete;

  virtual void readSettings()  = 0;
  virtual void writeSettings() = 0;
};

} // namespace config

#endif // CONFIG_ABSTRACTCONFIG_H
