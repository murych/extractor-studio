#include "config_app.h"

#include <QCoreApplication>

namespace config {
struct ApplicationConfig::Implementation
{
  const QString k_header   = QStringLiteral("application");
  const QString k_win_pos  = QStringLiteral("window_pos");
  const QString k_win_size = QStringLiteral("window_size");
  const QString k_work_dir = QStringLiteral("cur_work_dir");
};

ApplicationConfig::ApplicationConfig()
  : m_settings{ QSettings::IniFormat,
                QSettings::UserScope,
                QCoreApplication::organizationName(),
                QCoreApplication::applicationName() }
{
}

void
ApplicationConfig::readSettings()
{
  Implementation headers;

  m_settings.beginGroup(headers.k_header);
  m_windowPos = m_settings.value(headers.k_win_pos, QPoint{ 400, 0 }).toPoint();
  m_windowSize =
    m_settings.value(headers.k_win_size, QSize{ 1000, 700 }).toSize();
  m_workDir = m_settings.value(headers.k_work_dir, QDir::homePath()).toString();
  m_settings.endGroup();
}

void
ApplicationConfig::writeSettings()
{
  Implementation headers;

  m_settings.beginGroup(headers.k_header);
  m_settings.setValue(headers.k_win_pos, m_windowPos);
  m_settings.setValue(headers.k_win_size, m_windowSize);
  m_settings.setValue(headers.k_work_dir, m_workDir);
  m_settings.endGroup();
}

} // namespace config
