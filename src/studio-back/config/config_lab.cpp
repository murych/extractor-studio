#include "config_lab.h"

#include "json/string_decorator.h"
#include "models/project/laboratory.h"
#include <QCoreApplication>

namespace config {

LaboratoryConfig::LaboratoryConfig()
  : m_settings{ QSettings::IniFormat,
                QSettings::UserScope,
                QCoreApplication::organizationName(),
                QCoreApplication::applicationName() }
{
}

void
LaboratoryConfig::readSettings()
{
  models::project::Laboratory lab;

  m_settings.beginGroup(lab.key());
  m_labName = m_settings.value(lab.name->key(), lab.name->value()).toString();
  m_labPhone =
    m_settings.value(lab.phone->key(), lab.phone->value()).toString();
  m_labEmail =
    m_settings.value(lab.email->key(), lab.email->value()).toString();
  m_labAddress =
    m_settings.value(lab.address->key(), lab.address->value()).toString();
  m_settings.endGroup(); // laboratory
}

void
LaboratoryConfig::writeSettings()
{
  models::project::Laboratory lab;

  m_settings.beginGroup(lab.key());
  m_settings.setValue(lab.name->key(), m_labName);
  m_settings.setValue(lab.phone->key(), m_labPhone);
  m_settings.setValue(lab.email->key(), m_labEmail);
  m_settings.setValue(lab.address->key(), m_labAddress);
  m_settings.endGroup(); // laboratory
}

bool
LaboratoryConfig::setLabName(const QString& name)
{
  m_labName = name;
  return true;
}

bool
LaboratoryConfig::setLabPhone(const QString& phone)
{
  m_labPhone = phone;
  return true;
}

bool
LaboratoryConfig::setLabEmail(const QString& email)
{
  m_labEmail = email;
  return true;
}

bool
LaboratoryConfig::setLabAddress(const QString& address)
{
  m_labAddress = address;
  return true;
}

} // namespace config
