#include "commands.h"

namespace gcode {

QString
Command::get(GCODES code)
{
  switch (code) {
    case GCODES::RapidMove:
      return QStringLiteral("G0 %1%2\n");
    case GCODES::LinearMove:
      return QStringLiteral("G1 %1%2 F%3\n");
    case GCODES::DWELL:
      return QStringLiteral("G4 P%1\n");
    case GCODES::AutoHome:
      return QStringLiteral("G28 %1\n");
    case GCODES::ShakeDriver:
      return QStringLiteral("G56 %1%2 P%3 F%4\n");
    default:
      break;
  }
  return {};
}

QString
Command::get(MCODES code)
{
  switch (code) {
    case MCODES::UnconditionalStop:
      return QStringLiteral("M01 %1\n");
    case MCODES::EnableHeater:
      return QStringLiteral("M80 T%1\n");
    case MCODES::DisableHeater:
      return QStringLiteral("M81 T%1\n");
    case MCODES::SetPlateTemperature:
      return QStringLiteral("M104 T%1 S%2\n");
    case MCODES::ReportTemperature:
      return QStringLiteral("M105 T%1\n");
    case MCODES::SetFanSpeed:
      return QStringLiteral("M106 T%1\n");
    case MCODES::FanOff:
      return QStringLiteral("M108 T%1\n");
    case MCODES::WaitForPlateTemperature:
      return QStringLiteral("M109 T%1 S%2\n");
    case MCODES::EnableStepper:
    case MCODES::DisableStepper:
    case MCODES::SetAxisStepsPerUnit:
    case MCODES::EmergencyStop:
    case MCODES::GetCurrentPosition:
    case MCODES::GetFirmwareInfo:
    case MCODES::SerialPrint:
    case MCODES::SetMoveMaximumAcceleration:
    case MCODES::SetMaximumFeedrate:
    case MCODES::SetStartingAcceleration:
    case MCODES::SetAdvancedSettings:
    case MCODES::SetHomeOffset:
    case MCODES::DisableRgb:
    case MCODES::EnableRgb:
    case MCODES::SetRgb:
    case MCODES::SetMicrostep:
    case MCODES::CheckSleeves:
    case MCODES::DisableBuzzer:
    case MCODES::EnableBuzzer:
    case MCODES::SetBuzzer:
      break;
  }

  return {};
}

QString
Command::get(MULTI code)
{
  switch (code) {
    case MULTI::RapidMove:
      return QStringLiteral("G0 %1\n");
    case MULTI::LinearMove:
      return QStringLiteral("G1 %1 F%2\n");
    case MULTI::AutoHome:
      return QStringLiteral("G28 %1\n");
    case MULTI::AllHome:
      return QLatin1String{ "G28" };
  }

  return {};
}

} // namespace gcode
