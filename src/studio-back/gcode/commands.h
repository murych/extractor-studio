#ifndef COMMANDS_H
#define COMMANDS_H

#include <QString>

namespace gcode {

struct Command
{
  enum class GCODES
  {
    //! `G0 [<axis><pos>]`
    RapidMove = 0,
    //! `G1 [<axis><pos>] [F<feedrate>]`
    LinearMove = 1,
    //! `G4 [P<time (ms)>]`
    DWELL = 4,
    //! Приводы едут в "рабочую позицию", если текущая позиция
    //! определена
    ParkToolhead = 27,
    //! Приводы едут в нули осей до пересечения с оптопарами, затем едут в
    //! "рабочую позицию"
    //! `G28 [<axis>]`
    AutoHome     = 28,
    MachineCoord = 53,
    //! `G56 [<axis><amplitude>] [P<cycles>] [F<feedrate>]`
    ShakeDriver       = 56,
    ShakeLocal        = 69,
    AbsolutePosition  = 90,
    RelativePostition = 91,
    SetPosition       = 92
  };

  enum class MCODES
  {
    UnconditionalStop          = 0,
    EnableStepper              = 17,
    DisableStepper             = 18,
    EnableHeater               = 80,
    DisableHeater              = 81,
    SetAxisStepsPerUnit        = 92,
    SetPlateTemperature        = 104,
    ReportTemperature          = 105,
    SetFanSpeed                = 106,
    FanOff                     = 108,
    WaitForPlateTemperature    = 109,
    EmergencyStop              = 112,
    GetCurrentPosition         = 114,
    GetFirmwareInfo            = 115,
    SerialPrint                = 118,
    SetMoveMaximumAcceleration = 201,
    SetMaximumFeedrate         = 203,
    SetStartingAcceleration    = 204,
    SetAdvancedSettings        = 205,
    SetHomeOffset              = 206,
    DisableRgb                 = 300,
    EnableRgb                  = 301,
    SetRgb                     = 302,
    SetMicrostep               = 350,
    CheckSleeves               = 400,
    DisableBuzzer              = 500,
    EnableBuzzer               = 501,
    SetBuzzer                  = 502
  };

  enum class MULTI
  {
    //! `G0 [<axis 1><pos 1>] ... [<axis n><pos n>]
    RapidMove = 0,
    //! `G1 [<axis 1><pos 1>] ... [<axis n><pos n>] [F<feedrate>]
    LinearMove = 1,
    //! `G28 [<axis 1>] ... [<axis n>]`
    AutoHome = 28,
    AllHome  = 29,
  };

  static QString get(GCODES code);
  static QString get(MCODES code);
  static QString get(MULTI code);
};

using gcodes = Command::GCODES;
using mcodes = Command::MCODES;
using multi = Command::MULTI;

} // namespace gcode

#endif // COMMANDS_H
