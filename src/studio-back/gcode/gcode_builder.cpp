#include "gcode_builder.h"

#include "json/entity_collection.h"
#include "json/enumerator_decorator.h"
#include "json/int_decorator.h"
#include "generators/generator_beads_collect.h"
#include "generators/generator_beads_release.h"
#include "generators/generator_conclusion.h"
#include "generators/generator_dry.h"
#include "generators/generator_heater.h"
#include "generators/generator_homing.h"
#include "generators/generator_linear_move.h"
#include "generators/generator_mix.h"
#include "generators/generator_pause.h"
#include "generators/generator_preamble.h"
#include "generators/generator_sleep.h"
#include "generators/generator_sleeves_pickup.h"
#include "generators/generator_sleeves_release.h"
#include "generators/generator_tool_change.h"
#include "models/device/device_extractor.h"
#include "models/device/heater.h"
#include "models/device/motor.h"
#include "models/device/plate.h"
#include "models/device/seat.h"
#include "models/plates/plate.h"
#include "models/project/extractor_project.h"
#include "models/project/layout.h"
#include "models/project/project_info.h"
#include "models/project/protocol.h"
#include "models/steps/params_common.h"
#include "models/steps/step.h"
#include "models/steps/step_manual_move.h"
#include "utils/device_manager.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(GC_B, "GCODE BUILDER")

using namespace models;

namespace gcode {

// ----------------------------------------------------------------------------

struct GCodeBuilder::Implementation
{
  explicit Implementation(const models::project::ExtractorProject& project)
    : m_project{ project }
  {
    const utils::DeviceManager manager;
    m_device = std::make_unique<device::DeviceExtractor>(
      manager.availableTypesInFamily(project.info->deviceFamily->value())
        .at(0));
  }

  template<typename... Args>
  [[nodiscard]] std::optional<std::unique_ptr<IGCodeGenerator>>
  generatorFactory(steps::Step::EStepType type,
                   steps::Step* step,
                   Args&&... args) const;

  [[nodiscard]] std::optional<QString> generateToolChange(
    const QListIterator<steps::Step*>& iterator) const;

  [[nodiscard]] std::optional<QString> generatePreamble() const;
  [[nodiscard]] std::optional<QString> generateInit() const;
  [[nodiscard]] std::optional<QString> generateDeInit() const;

  [[nodiscard]] inline auto* device() const { return m_device.get(); }
  [[nodiscard]] inline auto* project() const { return &m_project; }

private:
  std::unique_ptr<device::DeviceExtractor> m_device;
  const models::project::ExtractorProject& m_project;
};

// ----------------------------------------------------------------------------

GCodeBuilder::GCodeBuilder(const models::project::ExtractorProject& project)
  : implementation{ std::make_unique<Implementation>(project) }
{
}

GCodeBuilder::~GCodeBuilder() = default;

std::optional<QString>
GCodeBuilder::build() const
{
  QString result;

  // generate preamble
  const auto preamble{ implementation->generatePreamble() };
  if (!preamble.has_value()) {
    return std::nullopt;
  }
  result.append(preamble.value());

  // generate protocol initialisation code
  const auto init{ implementation->generateInit() };
  if (!init.has_value()) {
    return std::nullopt;
  }
  result.append(init.value());

  // generate things for protocol steps
  const auto steps{
    implementation->project()->protocol->steps->derivedEntities()
  };
  QListIterator iter{ steps };
  while (iter.hasNext()) {
    if (iter.hasPrevious()) {
      const auto toolchange{ implementation->generateToolChange(iter) };
      if (toolchange.has_value()) {
        result.append(toolchange.value());
      }
    }

    const auto step{ iter.next() };
    const auto step_type{ static_cast<steps::Step::EStepType>(
      step->type->value()) };
    const auto generator{ implementation->generatorFactory(step_type, step) };
    if (!generator.has_value()) {
      qCCritical(GC_B) << this << "no generator for step #"
                       << step->getStepIdx() << step->getStepName()
                       << step->getStepTypeDescription();
      return std::nullopt;
    }
    const auto gcodes{ generator.value()->generate() };
    if (!gcodes.has_value()) {
      qCCritical(GC_B) << this << "generation failed for step #"
                       << step->getStepIdx() << step->getStepName()
                       << step->getStepTypeDescription();
      return std::nullopt;
    }
    result.append(gcodes.value());
  }

  // generate conclusion
  const auto deinit{ implementation->generateDeInit() };
  if (!deinit.has_value()) {
    return std::nullopt;
  }
  result.append(deinit.value());

  return result;
}

// ----------------------------------------------------------------------------

template<typename... Args>
std::optional<std::unique_ptr<IGCodeGenerator>>
GCodeBuilder::Implementation::generatorFactory(steps::Step::EStepType type,
                                               steps::Step* step,
                                               Args&&... args) const
{
  switch (type) {
    case steps::Step::User:
    case steps::Step::Unknown:
      return std::nullopt;

    case steps::Step::Pickup:
      return std::make_unique<gcode::GeneratorSleevesPickup>(step,
                                                             m_device.get());
    case steps::Step::Leave:
      return std::make_unique<gcode::GeneratorSleevesRelease>(step,
                                                              m_device.get());
    case steps::Step::Mix:
      return std::make_unique<gcode::GeneratorMix>(step);
    case steps::Step::Dry: {
      return std::make_unique<gcode::GeneratorDry>(
        step, nullptr, nullptr, nullptr, m_device->coords);
    }
    case steps::Step::Sleep:
      return std::make_unique<gcode::GeneratorSleep>(step);
    case steps::Step::Move: {
      const auto params{ dynamic_cast<models::steps::StepManualMove*>(
        step->paramsSpecific()) };
      const auto engine{ m_device->motors->derivedEntities().at(
        params->targetAxis->value()) };
      return std::make_unique<gcode::GeneratorLinearMove>(step, engine);
    }
    case steps::Step::Collect:
      return std::make_unique<gcode::GeneratorBeadsCollect>(step);
    case steps::Step::Release: {
      return std::make_unique<gcode::GeneratorBeadsRelease>(
        step, nullptr, nullptr, nullptr, m_device->coords);
    }
    case steps::Step::Heater: {
      return std::make_unique<gcode::GeneratorHeater>(step, nullptr);
    }
    case steps::Step::Pause:
      return std::make_unique<GeneratorPause>(step);
  }
  return std::nullopt;
}

std::optional<QString>
GCodeBuilder::Implementation::generateToolChange(
  const QListIterator<steps::Step*>& iterator) const
{
  if (!iterator.hasPrevious()) {
    return std::nullopt;
  }

  const auto next{ iterator.peekNext() };
  const auto previous{ iterator.peekPrevious() };

  const auto next_step_type{ next->getStepType() };
  const auto next_plate_idx{ next->getRelatedPlate() };
  const auto prev_plate_idx{ previous->getRelatedPlate() };

  const auto need_to_change{ [next_step_type]() -> bool {
    switch (next_step_type) {
      case steps::Step::EStepType::Move:
      case steps::Step::EStepType::Sleep:
      case steps::Step::EStepType::Pause:
        return false;
      default:
        return true;
    }
  }() };

  if ((prev_plate_idx != next_plate_idx) && need_to_change) {
    const auto* plate{ m_project.layout->plates->derivedEntities().at(
      next->getRelatedPlate()) };
    const GeneratorToolChange generator{ plate, m_device.get() };
    return generator.generate();
  }

  return std::nullopt;
}

std::optional<QString>
GCodeBuilder::Implementation::generatePreamble() const
{
  const GeneratorPreamble generator{ m_project.info };
  return generator.generate();
}

std::optional<QString>
GCodeBuilder::Implementation::generateInit() const
{
  const GeneratorInitialization homing{ m_device.get() };
  auto result{ homing.generate() };
  if (!result.has_value()) {
    return std::nullopt;
  }

  // toolchange to 0 seat;
  const auto* start_plate{ m_project.layout->plates->derivedEntities().at(0) };
  const GeneratorToolChange tool_change{ start_plate, m_device.get() };
  const auto tool_change_result{ tool_change.generate() };
  if (!tool_change_result.has_value()) {
    return result.value();
  }

  return result.value() + tool_change_result.value();
}

std::optional<QString>
GCodeBuilder::Implementation::generateDeInit() const
{
  const GeneratorConclusion generator{ m_device.get() };
  return generator.generate();
}

} // namespace gcode
