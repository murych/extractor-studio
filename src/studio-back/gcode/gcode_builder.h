#ifndef GCODE_GCODEBUILDER_H
#define GCODE_GCODEBUILDER_H

#include <QJsonObject>
#include <optional>

namespace models::project {
class ExtractorProject;
}

namespace gcode {

class GCodeBuilder
{
public:
  explicit GCodeBuilder(const models::project::ExtractorProject& project);
  ~GCodeBuilder();

  std::optional<QString> build() const;

private:
  struct Implementation;
  std::unique_ptr<Implementation> implementation{ nullptr };
};

} // namespace gcode

#endif // GCODE_GCODEBUILDER_H
