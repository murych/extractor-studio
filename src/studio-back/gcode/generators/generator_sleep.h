#ifndef GCODE_GENERATORSLEEP_H
#define GCODE_GENERATORSLEEP_H

#include "abstract_gcode_generator.h"

namespace models::steps {
class Step;
}

namespace gcode {

struct GeneratorSleep : public gcode::IGCodeGenerator
{
  explicit GeneratorSleep(const models::steps::Step* step)
    : m_step{ step }
  {
  }

  [[nodiscard]] GenerationResult generate() const override;

private:
  const models::steps::Step* m_step{ nullptr };
};

} // namespace gcode

#endif // GCODE_GENERATORSLEEP_H
