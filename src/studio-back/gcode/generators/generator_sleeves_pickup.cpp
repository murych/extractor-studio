#include "generator_sleeves_pickup.h"

#include "json/double_decorator.h"
#include "json/entity_collection.h"
#include "json/string_decorator.h"
#include "gcode/commands.h"
#include "models/device/device_extractor.h"
#include "models/device/extractor_coords.h"
#include "models/device/motor.h"
#include "models/device/seat.h"
#include "models/plates/plate.h"
#include "models/steps/params_common.h"
#include "models/steps/step.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(G_SP, "GCODE GEN SLEEVES PICKUP")

namespace gcode {

/*!
 * \brief GeneratorSleevesPickup::generate
 * \todo надо ли тут вообще иметь информацию об индексе посадочного места? может
 * можно передавать указатель на буферуню плашку из генератора-родителя?
 * \todo сделать движения Linear, добавить возможность регулировать скорость
 * \return
 */
IGCodeGenerator::GenerationResult
GeneratorSleevesPickup::generate() const
{
  if (m_step == nullptr || m_extractor == nullptr) {
    qCCritical(G_SP) << "GENERATE"
                     << "objects required are null, exiting";
    return std::nullopt;
  }

  const auto seatCurrentIdx{ m_step->getRelatedPlate() };
  const auto seatBufferIdx{ seatCurrentIdx ==
                                m_extractor->seats->baseEntities().size()
                              ? seatCurrentIdx - 1
                              : seatCurrentIdx + 1 };
  const auto* seatCurrent{ m_extractor->seats->derivedEntities().at(
    seatCurrentIdx) };
  const auto seatBuffer{ m_extractor->seats->derivedEntities().at(
    seatBufferIdx) };
  if (seatCurrent == nullptr || seatBuffer == nullptr) {
    return std::nullopt;
  }

  const auto engines{ m_extractor->motors->derivedEntities() };
  const auto* axisSleeves{ *std::find_if(
    engines.cbegin(), engines.cend(), [](const auto* engine) {
      return engine->axis->value() == "Z";
    }) };
  const auto* axisShield{ *std::find_if(
    engines.cbegin(), engines.cend(), [](const auto* engine) {
      return engine->axis->value() == "A";
    }) };
  const auto* axisGrabber{ *std::find_if(
    engines.cbegin(), engines.cend(), [](const auto* engine) {
      return engine->axis->value() == "C";
    }) };
  if (axisSleeves == nullptr || axisShield == nullptr ||
      axisGrabber == nullptr) {
    return std::nullopt;
  }

  // шаг 0 - шапка
  auto result{ IGCodeGenerator::stepHeader(m_step->getStepIdx(),
                                           m_step->getStepTypeDescription(),
                                           m_step->getStepName()) };

  // шаг 1 -- отъезжаем шторкой в буферную зону
  result.append(Command::get(gcodes::RapidMove)
                  .arg(axisSleeves->axis->value(),
                       QString::number(seatBuffer->coordinate->value())));

  // шаг 2 -- отъезжаем шторкой
  result.append(
    Command::get(gcodes::RapidMove)
      .arg(axisGrabber->axis->value(),
           QString::number(m_extractor->coords->sleevesRelease->value())));
  result.append(
    Command::get(gcodes::RapidMove)
      .arg(axisSleeves->axis->value(),
           QString::number(m_extractor->coords->heightDepo->value())));
  result.append(
    Command::get(gcodes::RapidMove)
      .arg(axisGrabber->axis->value(),
           QString::number(m_extractor->coords->sleevesPickup->value())));
  result.append(
    Command::get(gcodes::RapidMove)
      .arg(axisSleeves->axis->value(),
           QString::number(m_extractor->coords->heightTravel->value())));

  result.append(Command::get(gcodes::RapidMove)
                  .arg(axisShield->axis->value(),
                       QString::number(seatCurrent->coordinate->value())));

  return result;
}

} // namespace gcode
