#include "generator_conclusion.h"

#include "json/bool_decorator.h"
#include "json/entity_collection.h"
#include "json/int_decorator.h"
#include "json/string_decorator.h"
#include "gcode/commands.h"
#include "models/device/device_extractor.h"
#include "models/device/heater.h"
#include "models/device/motor.h"
#include "models/device/seat.h"

namespace gcode {

IGCodeGenerator::GenerationResult
GeneratorConclusion::generate() const
{
  if (m_extractor == nullptr) {
    return std::nullopt;
  }

  auto result{ QStringLiteral("; === SETTING DOWN ===\n") +
               Command::get(gcodes::DWELL).arg(500) +
               Command::get(multi::AllHome) };

  const auto seats{ m_extractor->seats->derivedEntities() };
  std::for_each(seats.cbegin(), seats.cend(), [&](const auto* seat) {
    if (seat->hasHeater->value()) {
      const auto* heater{ seat->heater };
      result.append(
        Command::get(mcodes::DisableHeater).arg(heater->address->value()));
      result.append(Command::get(mcodes::FanOff).arg(heater->address->value()));
    }
  });

  return result;
}

} // namespace gcode
