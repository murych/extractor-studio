#include "generator_dry.h"

#include "json/double_decorator.h"
#include "json/int_decorator.h"
#include "json/string_decorator.h"
#include "gcode/commands.h"
#include "models/device/extractor_coords.h"
#include "models/device/motor.h"
#include "models/device/plate.h"
#include "models/plates/plate.h"
#include "models/steps/moving_position.h"
#include "models/steps/params_common.h"
#include "models/steps/step.h"
#include "models/steps/step_dry.h"
#include "utils/plate_manager.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(G_GD, "GCODE GEN DRY")

namespace {
constexpr auto WELL_BOTTOM_OFFSET{ 1.0 };
}

namespace gcode {

GeneratorDry::GeneratorDry(const models::steps::Step* step,
                           const models::plates::Plate* plate,
                           const models::device::Motor* engineSleeves,
                           const models::device::Motor* engineMagnets,
                           const models::device::ExtractorCoords* coords)
  : m_step{ step }
  , m_plate{ plate }
  , m_engineSleeves{ engineSleeves }
  , m_engineMagnets{ engineMagnets }
  , m_coords{ coords }
{
  const utils::PlateManager plate_manager;
  if (plate_manager.contains(m_plate->size->value())) {
    m_plateDescription = std::make_unique<models::device::Plate>(
      plate_manager.plate(m_plate->size->value()));
  }
}

/*!
 * \brief GeneratorDry::generate
 * \details последовательность операций:
 *
 * 1. поднимаемся осями Z и B в указанную позицию
 * 2. dwell-имся указанное время
 * 3. спускаемся осями в позциию end
 *
 */
IGCodeGenerator::GenerationResult
GeneratorDry::generate() const
{
  if (m_plate == nullptr || m_engineMagnets == nullptr ||
      m_engineSleeves == nullptr || m_coords == nullptr) {
    qCritical(G_GD) << "generate"
                    << "required parameters are null, exiting";
    return std::nullopt;
  }
  if (m_plateDescription == nullptr) {
    qCritical(G_GD) << "generate"
                    << "no plate description found for required plate, exiting";
    return std::nullopt;
  }
  if (m_engineMagnets->axis->value() != "B" ||
      m_engineSleeves->axis->value() != "Z") {
    qCritical(G_GD) << "generate"
                    << "got wrong engines, exiting";
    return std::nullopt;
  }

  auto result{ IGCodeGenerator::stepHeader(m_step->getStepIdx(),
                                         m_step->getStepTypeDescription(),
                                         m_step->getStepName()) };

  const auto* params{ dynamic_cast<models::steps::StepDry*>(
    m_step->paramsSpecific()) };
  const auto* common{ m_step->paramsCommon() };

  const auto enter_speed{ QString::number(
    m_engineSleeves->getReqSpeed(common->getVerticalEnterSpeed())) };

  const auto where_to_dry{ static_cast<models::steps::ETravelPos>(
    params->getPosition()) };
  const auto dry_pos{ [where_to_dry, this]() -> QString {
    switch (where_to_dry) {
      case models::steps::ETravelPos::AtWellSurface:
        return QString::number(m_coords->heightDepo->value());
      default:
        return QString::number(m_coords->heightTravel->value());
    }
  }() };
  QStringList moving{
    QStringLiteral("%1%2").arg(m_engineMagnets->axis->value(), dry_pos),
    QStringLiteral("%1%2").arg(m_engineSleeves->axis->value(), dry_pos)
  };
  result.append(
    Command::get(multi::LinearMove).arg(moving.join(" "), enter_speed));

  // шаг 2
  result.append(Command::get(gcodes::DWELL).arg(params->duration->value()));

  // шаг 3
  const auto exit_speed{ QString::number(
    m_engineSleeves->getReqSpeed(common->getVerticalLeaveSpeed())) };
  const auto liquid_height{ m_plateDescription->calculateLiquidHeight(
    m_plate->getReagentVolume()) };
  const auto where_to_end{ static_cast<models::steps::ETravelPos>(
    common->getSleevesPosAtEnd()) };
  const auto exit_target{ [where_to_end, liquid_height, this]() -> QString {
    switch (where_to_end) {
      case models::steps::ETravelPos::Unknown:
      case models::steps::ETravelPos::AtZero:
      case models::steps::ETravelPos::AtWorkPos:
        return QString::number(m_coords->heightTravel->value());
      case models::steps::ETravelPos::AtWellSurface:
        return QString::number(m_coords->heightDepo->value());
      case models::steps::ETravelPos::AtLiquidSurface:
        return QString::number(m_engineSleeves->coordMax->value() -
                               liquid_height);
      case models::steps::ETravelPos::AtLiquidMiddle:
        return QString::number(m_engineSleeves->coordMax->value() -
                               liquid_height / 2);
      case models::steps::ETravelPos::AtLiquidBottom:
        return QString::number(m_engineSleeves->coordMax->value() -
                               WELL_BOTTOM_OFFSET);
        break;
    }
  }() };
  moving.clear();
  moving << QStringLiteral("%1%2").arg(m_engineSleeves->axis->value(),
                                       exit_target)
         << QStringLiteral("%1%2").arg(m_engineMagnets->axis->value(),
                                       exit_target);
  result.append(
    Command::get(multi::LinearMove).arg(moving.join(" "), exit_speed));

  return result;
}

} // namespace gcode
