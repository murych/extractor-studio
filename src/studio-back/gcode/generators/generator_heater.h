#ifndef GCODE_GENERATORHEATER_H
#define GCODE_GENERATORHEATER_H

#include "abstract_gcode_generator.h"

namespace models {
namespace device {
class DeviceExtractor;
class Heater;
}
namespace steps {
class Step;
}
}

namespace gcode {

struct GeneratorHeater : public gcode::IGCodeGenerator
{
  explicit GeneratorHeater(const models::steps::Step* step,
                           const models::device::Heater* heater)
    : m_step{ step }
    , m_heater{ heater }
  {
  }

  [[nodiscard]] IGCodeGenerator::GenerationResult generate() const override;

private:
  const models::steps::Step* m_step{ nullptr };
  const models::device::Heater* m_heater{ nullptr };
};

} // namespace gcode

#endif // GCODE_GENERATORHEATER_H
