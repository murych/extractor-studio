#ifndef GCODE_GENERATORLINEARMOVE_H
#define GCODE_GENERATORLINEARMOVE_H

#include "abstract_gcode_generator.h"

namespace models {
namespace steps {
class Step;
}
namespace device {
class Motor;
}
}

namespace gcode {

struct GeneratorLinearMove : public gcode::IGCodeGenerator
{
  explicit GeneratorLinearMove(const models::steps::Step* step,
                               const models::device::Motor* engine)
    : m_step{ step }
    , m_motor{ engine }
  {
  }

  [[nodiscard]] GenerationResult generate() const override;

private:
  const models::steps::Step* m_step{ nullptr };
  const models::device::Motor* m_motor{ nullptr };
};

} // namespace gcode

#endif // GCODE_GENERATORLINEAgenerateRMOVE_H
