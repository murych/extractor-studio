#include "generator_tool_change.h"

#include "json/double_decorator.h"
#include "json/entity_collection.h"
#include "json/int_decorator.h"
#include "gcode/commands.h"
#include "models/device/device_extractor.h"
#include "models/device/seat.h"
#include "models/plates/plate.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(G_TC, "GCODE GEN TOOL CHANGE")

namespace {
const auto HEADER{ QStringLiteral("\n; === Moving to seat #%1 ===\n") };
}

namespace gcode {

GeneratorToolChange::GeneratorToolChange(
  const models::plates::Plate* plate,
  const models::device::DeviceExtractor* device)
{
  const auto seats{ device->seats->derivedEntities() };
  const auto req_idx{ plate->seat->value() };
  const auto iter{ std::find_if(
    seats.constBegin(), seats.constEnd(), [req_idx](const auto& seat) {
      return seat->idx->value() == req_idx;
    }) };
  if (iter == seats.constEnd()) {
    return;
  }
  next_seat = *iter;
}

GeneratorToolChange::GeneratorToolChange(const models::device::Seat* seat)
  : next_seat{ seat }
{
}

IGCodeGenerator::GenerationResult
GeneratorToolChange::generate() const
{
  if (next_seat == nullptr) {
    return std::nullopt;
  }

  const QStringList moving{
    QStringLiteral("X%1").arg(next_seat->coordinate->value()),
    QStringLiteral("A%1").arg(next_seat->coordinate->value())
  };
  return HEADER.arg(next_seat->idx->value()) +
         Command::get(multi::LinearMove).arg(moving.join(" ") + '\n');
}

} // namespace gcode
