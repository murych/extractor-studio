#include "generator_pause.h"

#include "gcode/commands.h"
#include "models/steps/step.h"
#include "models/steps/step_pause.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(G_GP, "GCODE GEN PAUSE")

namespace {
const auto COMMENT{ QStringLiteral(";# %1\n") };
}

namespace gcode {

IGCodeGenerator::GenerationResult
GeneratorPause::generate() const
{
  if (m_step == nullptr) {
    qCCritical(G_GP) << "GENERATE"
                     << "required objects are null, exiting";
    return std::nullopt;
  }

  auto result{ IGCodeGenerator::stepHeader(m_step->getStepIdx(),
                                           m_step->getStepTypeDescription(),
                                           m_step->getStepName()) };

  const auto* params{ dynamic_cast<models::steps::StepPause*>(
    m_step->paramsSpecific()) };
  if (!params->getMessage().isEmpty()) {
    const auto messages{ params->getMessage().split('\n') };
    std::for_each(
      messages.cbegin(), messages.cend(), [&result](const auto& message) {
        result.append(COMMENT.arg(message.simplified()));
      });
  }

  result.append(Command::get(mcodes::UnconditionalStop));

  return result;
}

} // namespace gcode
