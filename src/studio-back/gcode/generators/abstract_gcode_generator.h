#ifndef GCODE_GCODEGENERATOR_H
#define GCODE_GCODEGENERATOR_H

#include <QString>
#include <optional>

namespace gcode {

struct IGCodeGenerator
{
  using GenerationResult = std::optional<QString>;

  IGCodeGenerator()                                              = default;
  virtual ~IGCodeGenerator()                                     = default;
  IGCodeGenerator(const IGCodeGenerator&)                        = default;
  IGCodeGenerator(IGCodeGenerator&&) noexcept                    = default;
  auto operator=(const IGCodeGenerator&) -> IGCodeGenerator&     = default;
  auto operator=(IGCodeGenerator&&) noexcept -> IGCodeGenerator& = default;

  [[nodiscard]] virtual GenerationResult generate() const = 0;

protected:
  static QString stepHeader(int idx, const QString& type, const QString& title)
  {
    return QStringLiteral("; === Step #%1 :: %2 :: %3 ===\n")
      .arg(QString::number(idx), type, title);
  }
};

} // namespace gcode

#endif // GCODE_GCODEGENERATOR_H
