#ifndef GCODE_GENERATORBEADSRELEASE_H
#define GCODE_GENERATORBEADSRELEASE_H

#include "abstract_gcode_generator.h"
#include <memory>

namespace models {
namespace steps {
class Step;
}
namespace device {
class Plate;
class DeviceExtractor;
class Motor;
class ExtractorCoords;
}
namespace plates {
class Plate;
}
}

namespace json {
template<typename T>
class EntityCollection;
}

namespace gcode {

struct GeneratorBeadsRelease : public gcode::IGCodeGenerator
{
  explicit GeneratorBeadsRelease(const models::steps::Step* step,
                                 const models::plates::Plate* plate,
                                 const models::device::Motor* engineMagnets,
                                 const models::device::Motor* engineSleeves,
                                 const models::device::ExtractorCoords* coords);

  [[nodiscard]] GenerationResult generate() const override;

private:
  const models::steps::Step* m_step{ nullptr };
  const models::plates::Plate* m_plate{ nullptr };

  const models::device::Motor* m_engineMagnets{ nullptr };
  const models::device::Motor* m_engineSleeves{ nullptr };

  const models::device::ExtractorCoords* m_coords{ nullptr };

  std::unique_ptr<models::device::Plate> m_plateDescription{ nullptr };
};

} // namespace gcode

#endif // GCODE_GENERATORBEADSRELEASE_H
