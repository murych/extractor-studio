#include "generator_sleeves_release.h"

#include "json/double_decorator.h"
#include "json/entity_collection.h"
#include "gcode/commands.h"
#include "models/device/device_extractor.h"
#include "models/device/extractor_coords.h"
#include "models/device/seat.h"
#include "models/plates/plate.h"
#include "models/steps/step.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(G_SR, "GCODE GEN SLEEVES RELEASE")

namespace gcode {

IGCodeGenerator::GenerationResult
GeneratorSleevesRelease::generate() const
{
  if (m_extractor == nullptr || m_step == nullptr) {
    return std::nullopt;
  }

  auto result{ IGCodeGenerator::stepHeader(m_step->getStepIdx(),
                                           m_step->getStepTypeDescription(),
                                           m_step->getStepName()) };

  const auto seatCurrentIdx{ m_step->getRelatedPlate() };
  const auto seatBufferIdx{ seatCurrentIdx ==
                                m_extractor->seats->baseEntities().size()
                              ? seatCurrentIdx - 1
                              : seatCurrentIdx + 1 };
  const auto* seatCurrent{ m_extractor->seats->derivedEntities().at(
    seatCurrentIdx) };
  const auto seatBuffer{ m_extractor->seats->derivedEntities().at(
    seatBufferIdx) };
  if (seatCurrent == nullptr || seatBuffer == nullptr) {
    return std::nullopt;
  }

  result.append(Command::get(gcodes::RapidMove)
                  .arg("A", QString::number(seatBuffer->coordinate->value())));

  result.append(
    Command::get(gcodes::RapidMove)
      .arg("Z", QString::number(m_extractor->coords->heightDepo->value())));
  result.append(
    Command::get(gcodes::RapidMove)
      .arg("C", QString::number(m_extractor->coords->sleevesRelease->value())));
  result.append(
    Command::get(gcodes::RapidMove)
      .arg("Z", QString::number(m_extractor->coords->heightTravel->value())));
  result.append(
    Command::get(gcodes::RapidMove)
      .arg("C", QString::number(m_extractor->coords->sleevesPickup->value())));

  result.append(Command::get(gcodes::RapidMove)
                  .arg("A", QString::number(seatCurrent->coordinate->value())));

  return result;
}

} // namespace gcode
