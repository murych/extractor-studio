#include "generator_linear_move.h"

#include "json/double_decorator.h"
#include "json/enumerator_decorator.h"
#include "json/int_decorator.h"
#include "json/string_decorator.h"
#include "gcode/commands.h"
#include "models/device/motor.h"
#include "models/steps/moving_speed.h"
#include "models/steps/params_common.h"
#include "models/steps/step.h"
#include "models/steps/step_manual_move.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(G_LM, "GCODE GEN MANUAL MOVE")

namespace gcode {

IGCodeGenerator::GenerationResult
GeneratorLinearMove::generate() const
{
  const auto params{ dynamic_cast<models::steps::StepManualMove*>(
    m_step->paramsSpecific()) };
  const auto common{ m_step->paramsCommon() };

  const auto target_pos{ QString::number(params->targetPosition->value()) };
  const auto speed_value{ static_cast<models::steps::ETravelSpeed>(
    params->speed->value()) };
  const auto req_speed{ [speed_value, this]() -> QString {
    switch (speed_value) {
      case models::steps::ETravelSpeed::Unknown:
      case models::steps::ETravelSpeed::User:
      case models::steps::ETravelSpeed::Slow:
        return QString::number(m_motor->speedLow->value());
      case models::steps::ETravelSpeed::Medium:
        return QString::number(m_motor->speedMed->value());
      case models::steps::ETravelSpeed::Fast:
        return QString::number(m_motor->speedHigh->value());
    }
  }() };

  return IGCodeGenerator::stepHeader(m_step->getStepIdx(),
                                     m_step->getStepTypeDescription(),
                                     m_step->getStepName()) +
         Command::get(gcodes::LinearMove)
           .arg(m_motor->axis->value(), target_pos, req_speed);
}

} // namespace gcode
