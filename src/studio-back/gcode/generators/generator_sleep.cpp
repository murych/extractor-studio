#include "generator_sleep.h"

#include "gcode/commands.h"
#include "models/steps/step.h"
#include "models/steps/step_sleep.h"

namespace gcode {

IGCodeGenerator::GenerationResult
GeneratorSleep::generate() const
{
  if (m_step == nullptr) {
    return std::nullopt;
  }

  const auto* params{ dynamic_cast<models::steps::StepSleep*>(
    m_step->paramsSpecific()) };
  if (params == nullptr) {
    return std::nullopt;
  }

  const auto result{
    IGCodeGenerator::stepHeader(m_step->getStepIdx(),
                                m_step->getStepTypeDescription(),
                                m_step->getStepName()) +
    Command::get(gcodes::DWELL).arg(params->getSleepDuration())
  };

  return result;
}

} // namespace gcode
