#ifndef GCODE_GENERATORSLEEVESPICKUP_H
#define GCODE_GENERATORSLEEVESPICKUP_H

#include "abstract_gcode_generator.h"

namespace models {
namespace device {
class Motor;
class DeviceExtractor;
}
namespace steps {
class Step;
}
}

namespace gcode {

struct GeneratorSleevesPickup : public gcode::IGCodeGenerator
{
  explicit GeneratorSleevesPickup(const models::steps::Step* step,
                                  const models::device::DeviceExtractor* device)
    : m_step{ step }
    , m_extractor{ device }
  {
  }

  [[nodiscard]] GenerationResult generate() const override;

private:
  const models::steps::Step* m_step{ nullptr };
  const models::device::DeviceExtractor* m_extractor{ nullptr };
};

} // namespace gcode

#endif // GCODE_GENERATORSLEEVESPICKUP_H
