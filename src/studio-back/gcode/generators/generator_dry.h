#ifndef GCODE_GENERATORDRY_H
#define GCODE_GENERATORDRY_H

#include "abstract_gcode_generator.h"
#include <memory>

namespace models {
namespace steps {
class Step;
}
namespace plates {
class Plate;
}
namespace device {
class Plate;
class Motor;
class DeviceExtractor;
class ExtractorCoords;
}
}

namespace gcode {

struct GeneratorDry : public gcode::IGCodeGenerator
{
  explicit GeneratorDry(const models::steps::Step* step,
                        const models::plates::Plate* plate,
                        const models::device::Motor* engineSleeves,
                        const models::device::Motor* engineMagnets,
                        const models::device::ExtractorCoords* coords);

  [[nodiscard]] GenerationResult generate() const override;

private:
  const models::steps::Step* m_step{ nullptr };
  const models::plates::Plate* m_plate{ nullptr };
  const models::device::Motor* m_engineSleeves{ nullptr };
  const models::device::Motor* m_engineMagnets{ nullptr };
  const models::device::ExtractorCoords* m_coords{ nullptr };

  std::unique_ptr<models::device::Plate> m_plateDescription{ nullptr };
};

} // namespace gcode

#endif // GCODE_GENERATORDRY_H
