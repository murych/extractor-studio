#include "generator_beads_collect.h"

#include "models/steps/mixing_stage.h"
#include "models/steps/moving_speed.h"
#include "models/steps/step.h"
#include "models/steps/step_beads_collect.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(G_BC, "GCODE GEN BEADS COLLECT")

using namespace models;

namespace gcode {

IGCodeGenerator::GenerationResult
GeneratorBeadsCollect::generate() const
{
  if (!m_step) {
    return std::nullopt;
  }

  const auto index{ m_step->getStepIdx() };
  const auto type{ m_step->getStepTypeDescription() };
  const auto name{ m_step->getStepName() };

  const auto* collect_specifics{ dynamic_cast<models::steps::StepBeadsCollect*>(
    m_step->paramsSpecific()) };
  if (!collect_specifics) {
    return std::nullopt;
  }

  return IGCodeGenerator::stepHeader(index, type, name);
}

} // namespace gcode
