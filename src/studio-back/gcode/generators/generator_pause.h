#ifndef GCODE_GENERATORPAUSE_H
#define GCODE_GENERATORPAUSE_H

#include "abstract_gcode_generator.h"

namespace models::steps {
class Step;
}

namespace gcode {

struct GeneratorPause : public gcode::IGCodeGenerator
{
  explicit GeneratorPause(const models::steps::Step* step)
    : m_step{ step }
  {
  }

  [[nodiscard]] GenerationResult generate() const override;

private:
  const models::steps::Step* m_step{ nullptr };
};

} // namespace gcode

#endif // GCODE_GENERATORPAUSE_H
