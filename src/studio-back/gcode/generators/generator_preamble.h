#ifndef GCODE_GENERATORPREAMBLE_H
#define GCODE_GENERATORPREAMBLE_H

#include "abstract_gcode_generator.h"

namespace models::project {
class Project;
}

namespace gcode {

struct GeneratorPreamble : public gcode::IGCodeGenerator
{
  explicit GeneratorPreamble(const models::project::Project* project);
  [[nodiscard]] GenerationResult generate() const override;

private:
  const models::project::Project* m_project{ nullptr };
};

} // namespace gcode

#endif // GCODE_GENERATORPREAMBLE_H
