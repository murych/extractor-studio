#ifndef GCODE_GENERATORCONCLUSION_H
#define GCODE_GENERATORCONCLUSION_H

#include "abstract_gcode_generator.h"

namespace models::device {
class DeviceExtractor;
}

namespace gcode {

struct GeneratorConclusion : public gcode::IGCodeGenerator
{
  explicit GeneratorConclusion(const models::device::DeviceExtractor* extractor)
    : m_extractor{ extractor }
  {
  }
  ~GeneratorConclusion() override = default;

  [[nodiscard]] GenerationResult generate() const override;

private:
  const models::device::DeviceExtractor* m_extractor{ nullptr };
};

} // namespace gcode

#endif // GCODE_GENERATORCONCLUSION_H
