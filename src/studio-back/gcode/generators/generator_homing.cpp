#include "generator_homing.h"

#include "json/bool_decorator.h"
#include "json/double_decorator.h"
#include "json/entity_collection.h"
#include "json/int_decorator.h"
#include "json/string_decorator.h"
#include "gcode/commands.h"
#include "models/device/device_extractor.h"
#include "models/device/heater.h"
#include "models/device/motor.h"
#include "models/device/seat.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(G_HA, "GCODE GEN HOME ALL")

namespace {
const auto HEADER{ QStringLiteral("; === SETTING UP ===\n") };
}

namespace gcode {

IGCodeGenerator::GenerationResult
GeneratorInitialization::generate() const
{
  auto result{ HEADER };

  QListIterator seats_iter{ m_device->seats->derivedEntities() };
  while (seats_iter.hasNext()) {
    const auto* seat{ seats_iter.next() };
    if (seat->hasHeater->value()) {
      const auto* heater{ seat->heater };
      result.append(
        Command::get(mcodes::EnableHeater).arg(heater->address->value()));
      result.append(Command::get(mcodes::FanOff).arg(heater->address->value()));
      result.append(Command::get(mcodes::SetPlateTemperature)
                      .arg(QString::number(heater->address->value()),
                           QString::number(heater->tempMin->value())));
    }
  }

  QListIterator motors_iter{ m_device->motors->derivedEntities() };
  while (motors_iter.hasNext()) {
    const auto motor_axis{ motors_iter.next()->axis->value() };
    result.append(Command::get(gcodes::AutoHome).arg(motor_axis));
  }

  result.append(Command::get(gcodes::DWELL).arg(500));

  return result;
}

} // namespace gcode
