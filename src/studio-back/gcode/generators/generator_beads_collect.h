#ifndef GCODE_GENERATORBEADSCOLLECT_H
#define GCODE_GENERATORBEADSCOLLECT_H

#include "abstract_gcode_generator.h"

namespace models::steps {
class Step;
}

namespace gcode {

struct GeneratorBeadsCollect : public gcode::IGCodeGenerator
{
  explicit GeneratorBeadsCollect(const models::steps::Step* step)
    : m_step{ step }
  {
  }
  ~GeneratorBeadsCollect() override = default;

  [[nodiscard]] GenerationResult generate() const override;

private:
  const models::steps::Step* const m_step{ nullptr };
};

} // namespace gcode

#endif // GCODE_GENERATORBEADSCOLLECT_H
