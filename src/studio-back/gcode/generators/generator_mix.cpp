#include "generator_mix.h"

#include "models/steps/step.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(G_GM, "GCODE GEN MIX")

namespace gcode {

IGCodeGenerator::GenerationResult
GeneratorMix::generate() const
{
  if (m_step == nullptr) {
    qCCritical(G_GM) << "GENERATE"
                     << "required items are null, exiting";
    return std::nullopt;
  }

  const auto result{ IGCodeGenerator::stepHeader(
    m_step->getStepIdx(),
    m_step->getStepTypeDescription(),
    m_step->getStepName()) };

  return result;
}

} // namespace gcode
