#include "generator_preamble.h"

#include "json/datetime_decorator.h"
#include "json/int_decorator.h"
#include "json/string_decorator.h"
#include "models/project/project_info.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(G_PR, "GCODE GEN PREAMBLE")

namespace gcode {

GeneratorPreamble::GeneratorPreamble(const models::project::Project* project)
  : m_project{ project }
{
}

IGCodeGenerator::GenerationResult
GeneratorPreamble::generate() const
{
  if (m_project == nullptr) {
    return std::nullopt;
  }
  if (m_project->title->value().isNull() ||
      m_project->title->value().isEmpty()) {
    return std::nullopt;
  }

  const json::DateTimeDecorator dtd{ "", "", QDateTime::currentDateTime() };
  const QStringList result{
    QStringLiteral("; =========================================="),
    QStringLiteral("; Project title: %1").arg(m_project->title->value()),
    QStringLiteral("; Project author: %1").arg(m_project->author->value()),
    QStringLiteral("; Project revision: %1").arg(m_project->revision->value()),
    QStringLiteral("; Project creation date: %1")
      .arg(m_project->dateCreated->toPrettyDateString()),
    QStringLiteral("; Protocol generated: %1").arg(dtd.toPrettyDateString()),
    QStringLiteral("; =========================================="),
    QStringLiteral("\n\n")
  };

  return result.join('\n');
}

} // namespace gcode
