#ifndef GCODE_GENERATORMIX_H
#define GCODE_GENERATORMIX_H

#include "abstract_gcode_generator.h"

namespace models::steps {
class Step;
}

namespace gcode {

struct GeneratorMix : public gcode::IGCodeGenerator
{
  explicit GeneratorMix(const models::steps::Step* step)
    : m_step{ step }
  {
  }

  [[nodiscard]] GenerationResult generate() const override;

private:
  const models::steps::Step* m_step{ nullptr };
};

} // namespace gcode

#endif // GCODE_GENERATORMIX_H
