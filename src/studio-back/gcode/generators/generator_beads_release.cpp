#include "generator_beads_release.h"

#include "json/double_decorator.h"
#include "json/enumerator_decorator.h"
#include "json/int_decorator.h"
#include "json/string_decorator.h"
#include "gcode/commands.h"
#include "models/device/device_extractor.h"
#include "models/device/extractor_coords.h"
#include "models/device/motor.h"
#include "models/device/plate.h"
#include "models/device/sleeves.h" // понадобится позже для расчета расплескивани
#include "models/plates/plate.h"
#include "models/steps/moving_position.h"
#include "models/steps/moving_speed.h"
#include "models/steps/params_common.h"
#include "models/steps/step.h"
#include "models/steps/step_beads_release.h"
#include "utils/plate_manager.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(G_BR, "GCODE GEN BEADS RELEASE")

namespace {
constexpr auto WAIT{ 500 };
constexpr auto WELL_BOTTOM_OFFSET{ 1.0 };
}

namespace gcode {

GeneratorBeadsRelease::GeneratorBeadsRelease(
  const models::steps::Step* step,
  const models::plates::Plate* plate,
  const models::device::Motor* engineMagnets,
  const models::device::Motor* engineSleeves,
  const models::device::ExtractorCoords* coords)
  : m_step{ step }
  , m_plate{ plate }
  , m_engineMagnets{ engineMagnets }
  , m_engineSleeves{ engineSleeves }
  , m_coords{ coords }
{
  const utils::PlateManager plate_manager;
  if (plate_manager.contains(m_plate->size->value())) {
    m_plateDescription = std::make_unique<models::device::Plate>(
      plate_manager.plate(m_plate->size->value()));
  } else {
    return;
  }
}

/*!
 * \brief GeneratorBeadsRelease::generate
 * \details последовательность выплюнутых операций:
 *
 * 1. рассчитать середину жидкости
 * 2. спуститься на середину жидкости
 * 3. поднять магниты в рабоее положение
 * 4. рассчитать количество циклов перемешивания с какой-нибудь константной
 * амплитудой, умещающиеся в `duration` секунд
 * 5. скомандовать перемешивание
 * 6. скомандовать паузу на 0.5 сек
 * 7. повторить шаги 5-6 `count` раз
 * 8. поднять колпачки на уровень жидкости
 * 9. подождать 0.5 сек
 * 10. поднять колпачки в рабочее положение
 */
IGCodeGenerator::GenerationResult
GeneratorBeadsRelease::generate() const
{
  if (!m_step || !m_plate || !m_engineSleeves || !m_engineMagnets ||
      !m_coords) {
    return std::nullopt;
  }
  if (!m_plateDescription) {
    return std::nullopt;
  }
  if (m_engineMagnets->axis->value() != "B" ||
      m_engineSleeves->axis->value() != "Z") {
    return std::nullopt;
  }

  auto result{ IGCodeGenerator::stepHeader(m_step->getStepIdx(),
                                         m_step->getStepTypeDescription(),
                                         m_step->getStepName()) };

  const auto* params{ dynamic_cast<models::steps::StepBeadsRelease*>(
    m_step->paramsSpecific()) };
  const auto* commons{ m_step->paramsCommon() };

  // шаг 1
  const auto liquid_height{ m_plateDescription->calculateLiquidHeight(
    m_plate->getReagentVolume()) };

  // шаг 2
  const auto enter_speed{ QString::number(
    m_engineSleeves->getReqSpeed(commons->getVerticalEnterSpeed())) };
  const auto height{ QString::number(m_engineSleeves->coordMax->value() -
                                     liquid_height / 2) };
  const QStringList moving{
    QStringLiteral("%1%2").arg(m_engineSleeves->axis->value(), height),
    QStringLiteral("%1%2").arg(m_engineMagnets->axis->value(), height)
  };
  result.append(
    Command::get(multi::LinearMove).arg(moving.join(" "), enter_speed));

  // шаг 3
  const auto height_travel{ QString::number(m_coords->heightTravel->value()) };
  result.append(Command::get(gcodes::RapidMove)
                  .arg(m_engineMagnets->axis->value(), height_travel));

  // шаг 7 (контринтуитивно, а)
  const auto req_amplitude{ QString::number(liquid_height /
                                            4) }; // 25% от высоты жидкости
  const auto req_cycles{ QString::number(100500) };
  const auto shake_speed{ QString::number(
    m_engineSleeves->getReqSpeed(params->speed->value())) };
  for (int repeat = 0; repeat < params->count->value(); ++repeat) {
    // шаг 5
    result.append(Command::get(gcodes::ShakeDriver)
                    .arg(m_engineSleeves->axis->value(),
                         req_amplitude,
                         req_cycles,
                         shake_speed));
    // шаг 6
    result.append(Command::get(gcodes::DWELL).arg(WAIT));
  }

  // шаг 8
  const auto depo_height{ QString::number(m_coords->heightDepo->value()) };
  result.append(Command::get(gcodes::RapidMove)
                  .arg(m_engineSleeves->axis->value(), depo_height));

  // шаг 9
  result.append(Command::get(gcodes::DWELL).arg(WAIT));

  // шаг 10
  const auto where_sleeves_at_end{ static_cast<models::steps::ETravelPos>(
    commons->getSleevesPosAtEnd()) };
  const auto exit_target{
    [where_sleeves_at_end, liquid_height, this]() -> QString {
      switch (where_sleeves_at_end) {
        case models::steps::ETravelPos::Unknown:
        case models::steps::ETravelPos::AtZero:
        case models::steps::ETravelPos::AtWorkPos:
          return QString::number(m_coords->heightTravel->value());
          break;
        case models::steps::ETravelPos::AtWellSurface:
          return QString::number(m_coords->heightDepo->value());
        case models::steps::ETravelPos::AtLiquidSurface:
          return QString::number(m_engineSleeves->coordMax->value() -
                                 liquid_height);
        case models::steps::ETravelPos::AtLiquidMiddle:
          return QString::number(m_engineSleeves->coordMax->value() -
                                 liquid_height / 2);
        case models::steps::ETravelPos::AtLiquidBottom:
          return QString::number(m_engineSleeves->coordMax->value() -
                                 WELL_BOTTOM_OFFSET);
      }
    }()
  };
  result.append(Command::get(gcodes::RapidMove)
                  .arg(m_engineSleeves->axis->value(), exit_target));

  return result;
}

} // namespace gcode
