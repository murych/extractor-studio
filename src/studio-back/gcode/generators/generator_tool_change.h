#ifndef GCODE_GENERATORTOOLCHANGE_H
#define GCODE_GENERATORTOOLCHANGE_H

#include "abstract_gcode_generator.h"

namespace models::device {
class DeviceExtractor;
class Seat;
}

namespace models::plates {
class Plate;
}

namespace gcode {

struct GeneratorToolChange : public gcode::IGCodeGenerator
{
  explicit GeneratorToolChange(const models::plates::Plate* plate,
                               const models::device::DeviceExtractor* device);
  explicit GeneratorToolChange(const models::device::Seat* seat);

  [[nodiscard]] GenerationResult generate() const override;

private:
  const models::device::Seat* next_seat{ nullptr };
};

} // namespace gcode

#endif // GCODE_GENERATORTOOLCHANGE_H
