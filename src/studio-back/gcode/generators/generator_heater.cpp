#include "generator_heater.h"

#include "json/double_decorator.h"
#include "json/int_decorator.h"
#include "gcode/commands.h"
#include "models/device/heater.h"
#include "models/steps/step.h"
#include "models/steps/step_heater.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(G_HT, "GCODE GEN HEATER")

namespace gcode {

IGCodeGenerator::GenerationResult
GeneratorHeater::generate() const
{
  if (m_step == nullptr || m_heater == nullptr) {
    qCCritical(G_HT) << "generate"
                     << "required objects are null, exiting";
    return std::nullopt;
  }

  const auto* params{ dynamic_cast<models::steps::StepHeater*>(
    m_step->paramsSpecific()) };

  auto result{ IGCodeGenerator::stepHeader(m_step->getStepIdx(),
                                           m_step->getStepTypeDescription(),
                                           m_step->getStepName()) };

  const auto cmd{
    params->getBlockExecution()
      ? Command::get(mcodes::WaitForPlateTemperature)
          .arg(QString::number(m_heater->address->value()),
               QString::number(params->targetTemperature->value()))
      : Command::get(mcodes::SetPlateTemperature)
          .arg(QString::number(m_heater->address->value()),
               QString::number(params->targetTemperature->value()))
  };
  result.append(cmd);

  return result;
}

} // namespace gcode
