#ifndef GCODE_GENERATORHOMING_H
#define GCODE_GENERATORHOMING_H

#include "abstract_gcode_generator.h"

namespace models::device {
class DeviceExtractor;
}

namespace gcode {

struct GeneratorInitialization : public gcode::IGCodeGenerator
{
  explicit GeneratorInitialization(
    const models::device::DeviceExtractor* device)
    : m_device{ device }
  {
  }

  [[nodiscard]] GenerationResult generate() const override;

private:
  const models::device::DeviceExtractor* m_device{ nullptr };
};

} // namespace gcode

#endif // GCODE_GENERATORHOMING_H
