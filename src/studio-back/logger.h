#ifndef LOGGER_H
#define LOGGER_H

#include <QDateTime>
#include <QDir>
#include <QFile>
#include <QStringList>
#include <QTextStream>
#include <QTimer>
#include <QUrl>
#include <memory>

class Logger : public QObject
{
  Q_OBJECT
  Q_PROPERTY(QUrl logsPath READ logsPath CONSTANT)
  Q_PROPERTY(QUrl logsFile READ logsFile CONSTANT)
  Q_PROPERTY(
    int errorCount READ errorCount WRITE setErrorCount NOTIFY errorCountChanged)
  Q_PROPERTY(QString logText READ logText NOTIFY logTextChanged)

  Logger(QObject* parent = nullptr);

public:
  enum class LogLevel
  {
    ErrorsOnly,
    Terse,
    Default
  };

  static Logger* instance();
  static void messageOutput(QtMsgType type,
                            const QMessageLogContext& ctx,
                            const QString& msg);

  [[nodiscard]] QUrl logsPath() const;
  [[nodiscard]] QUrl logsFile() const;

  [[nodiscard]] int errorCount() const;
  void setErrorCount(int count);
  [[nodiscard]] QString logText() const;

  void setLogLevel(LogLevel level);

signals:
  void logTextChanged();
  void errorCountChanged();

private:
  void append(const QString& line);
  void fallbackMessageOutput(const QString& msg);
  bool removeOldFiles();

  QDir m_logDir;
  std::unique_ptr<QFile> m_logFile;
  std::unique_ptr<QTimer> m_updateTimer;

  QTextStream m_stderr;
  QTextStream m_fileOut;

  QDateTime m_startTime;
  LogLevel m_logLevel{ LogLevel::Default };
  QStringList m_logText;
  int m_maxLineCount{ 200 };
  int m_errorCount{ 0 };
};

#endif // LOGGER_H
