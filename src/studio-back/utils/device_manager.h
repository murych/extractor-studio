#ifndef DEVICEMANAGER_H
#define DEVICEMANAGER_H

#include <QJsonObject>
#include <QMultiHash>
#include <unordered_set>

namespace utils {

class DeviceManager
{
public:
  DeviceManager();

  [[nodiscard]] QVector<QJsonObject> availableTypesInFamily(int family) const;
  [[nodiscard]] std::unordered_set<QString> getAxisList() const;

private:
  //! \param int - тип устройства as stated in
  //! models::device::GenericDevice::eDeviceType
  //! \param QJsonObject - json такого устройства
  QMultiHash<int, QJsonObject> m_devices;

  void loadDevices();
};

} // namespace utils

#endif // DEVICEMANAGER_H
