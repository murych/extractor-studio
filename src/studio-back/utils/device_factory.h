#ifndef UTILS_DEVICEFACTORY_H
#define UTILS_DEVICEFACTORY_H

#include <QJsonObject>

namespace json {
class Entity;
} // namespace json

namespace utils {

struct DeviceFactory
{
  [[nodiscard]] static auto factoryMethod(const QJsonObject& json)
    -> json::Entity*;
};

} // namespace utils

#endif // UTILS_DEVICEFACTORY_H
