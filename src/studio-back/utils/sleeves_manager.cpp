#include "sleeves_manager.h"

#include "models/device/sleeves.h"
#include "utils/resources.h"
#include <QDir>
#include <QFile>
#include <QJsonDocument>

namespace utils {

SleevesManager::SleevesManager()
{
  init_resource();
  loadData();
}

void
SleevesManager::loadData()
{
  const QDir dir{ ":/sleeves/" };
  if (!dir.exists()) {
    return;
  }

  const auto entries{ dir.entryInfoList() };
  std::for_each(entries.cbegin(), entries.cend(), [&](const QFileInfo& info) {
    QFile file{ info.absoluteFilePath() };
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
      return;
    }
    const auto data{ QJsonDocument::fromJson(file.readAll()).object() };
    const models::device::Sleeves sleeve{ data };
    m_sleeves.insert(sleeve.id(), sleeve.toJson());
  });
}

} // namespace utils
