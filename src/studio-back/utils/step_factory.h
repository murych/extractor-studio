#ifndef STEP_FACTORIES_H
#define STEP_FACTORIES_H

#include <QJsonObject>

namespace json {
class Entity;
}

namespace utils {

struct StepFactory
{
  [[nodiscard]] static auto create(const QJsonObject& json)
    -> json::Entity*;
};

} // namespace utils

#endif // STEP_FACTORIES_H
