#include "step_factory.h"

#include "models/steps/step.h"
#include "models/steps/step_beads_collect.h"
#include "models/steps/step_beads_release.h"
#include "models/steps/step_dry.h"
#include "models/steps/step_heater.h"
#include "models/steps/step_manual_move.h"
#include "models/steps/step_mix.h"
#include "models/steps/step_pause.h"
#include "models/steps/step_sleep.h"
#include "models/steps/step_sleeves_leave.h"
#include "models/steps/step_sleeves_pickup.h"

#include <QLoggingCategory>
Q_LOGGING_CATEGORY(U_FS, "FACTORY PARAMS SPECIFIC")

namespace utils {

json::Entity*
StepFactory::create(const QJsonObject& json)
{
  using StepType = models::steps::Step::EStepType;
  const auto step_type{ static_cast<StepType>(json.value("type").toInt()) };

  switch (step_type) {
    case StepType::Heater:
      return new models::steps::StepHeater;

    case StepType::Pause:
      return new models::steps::StepPause;

    case StepType::Pickup:
      return new models::steps::StepSleevesPickup;

    case StepType::Leave:
      return new models::steps::StepSleevesLeave;

    case StepType::Mix:
      return new models::steps::StepMix;

    case StepType::Dry:
      return new models::steps::StepDry;

    case StepType::Sleep:
      return new models::steps::StepSleep;

    case StepType::Move:
      return new models::steps::StepManualMove;

    case StepType::Collect:
      return new models::steps::StepBeadsCollect;

    case StepType::Release:
      return new models::steps::StepBeadsRelease;

    default:
      break;
  }

  return nullptr;
}

} // namespace utils
