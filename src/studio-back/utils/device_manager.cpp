#include "device_manager.h"

#include "json/entity_collection.h"
#include "json/enumerator_decorator.h"
#include "json/string_decorator.h"
#include "models/device/abstract_device.h"
#include "models/device/device_extractor.h"
#include "models/device/motor.h"
#include "utils/device_factory.h"
#include "utils/resources.h"
#include <QDir>
#include <QFileInfo>
#include <QJsonDocument>
#include <algorithm>

namespace utils {

DeviceManager::DeviceManager()
{
  init_resource();
  loadDevices();
}

void
DeviceManager::loadDevices()
{
  using namespace models::device;

  const QDir devices_descriptions{ ":/devices/" };
  if (!devices_descriptions.exists()) {
    return;
  }

  QListIterator<QFileInfo> iter{ devices_descriptions.entryInfoList() };
  while (iter.hasNext()) {
    const QFileInfo file_info{ iter.next() };
    QFile file{ file_info.absoluteFilePath() };
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
      break;
    }

    const auto data{ QJsonDocument::fromJson(file.readAll()).object() };
    const std::unique_ptr<GenericDevice> device{ dynamic_cast<GenericDevice*>(
      utils::DeviceFactory::factoryMethod(data)) };
    m_devices.insert(device->type->value(), device->toJson());
  }
}

QVector<QJsonObject>
DeviceManager::availableTypesInFamily(int family) const
{
  QVector<QJsonObject> result;
  QMultiHash<int, QJsonObject>::const_iterator iter{ m_devices.find(family) };
  while (iter != m_devices.constEnd() && iter.key() == family) {
    result.append(iter.value());
    ++iter;
  }
  return result;
}

std::unordered_set<QString>
DeviceManager::getAxisList() const
{
  std::unordered_set<QString> result;
  std::for_each(
    m_devices.begin(), m_devices.end(), [&](const QJsonObject& json_device) {
      models::device::DeviceExtractor extractor(json_device);
      for (int index = 0; index < extractor.motors->derivedEntities().size();
           index++) {
        const auto* motor = extractor.motors->getEntity(index);
        result.insert(QStringLiteral("%1 (%2)").arg(motor->name->value(),
                                                    motor->axis->value()));
      }
    });
  return result;
}

} // namespace utils
