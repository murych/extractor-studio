#include "plate_manager.h"

#include "models/device/plate.h"
#include "utils/resources.h"
#include <QDir>
#include <QFile>
#include <QJsonDocument>

namespace utils {

PlateManager::PlateManager()
{
  init_resource();
  loadData();
}

void
PlateManager::loadData()
{
  const QDir devices_descriptions{ ":/plates/" };
  if (!devices_descriptions.exists()) {
    return;
  }

  const auto entries{ devices_descriptions.entryInfoList() };
  std::for_each(entries.cbegin(), entries.cend(), [&](const QFileInfo& info) {
    QFile file{ info.absoluteFilePath() };
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
      return;
    }

    const auto data{ QJsonDocument::fromJson(file.readAll()).object() };
    const models::device::Plate plate{ data };

    m_plates.insert(plate.id(), plate.toJson());
  });
}

} // namespace utils
