#ifndef UTILS_PLATEMANAGER_H
#define UTILS_PLATEMANAGER_H

#include <QJsonObject>
#include <QUuid>

namespace utils {

struct PlateManager
{
  PlateManager();

  [[nodiscard]] auto plates() const { return m_plates.values(); }
  [[nodiscard]] auto plate(const QUuid& key) const
  {
    return m_plates.value(key);
  }
  [[nodiscard]] auto contains(const QUuid& key) const
  {
    return m_plates.contains(key);
  }

private:
  QMap<QUuid, QJsonObject> m_plates;
  void loadData();
};

} // namespace utils

#endif // UTILS_PLATEMANAGER_H
