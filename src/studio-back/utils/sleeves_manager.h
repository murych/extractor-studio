#ifndef UTILS_SLEEVESMANAGER_H
#define UTILS_SLEEVESMANAGER_H

#include <QJsonObject>
#include <QUuid>

namespace utils {

struct SleevesManager
{
  SleevesManager();

  [[nodiscard]] auto sleeves() const { return m_sleeves.values(); }
  [[nodiscard]] auto sleeve(const QUuid& key) const
  {
    return m_sleeves.value(key);
  }
  [[nodiscard]] auto contains(const QUuid& key) const
  {
    return m_sleeves.contains(key);
  }

private:
  QMap<QUuid, QJsonObject> m_sleeves;
  void loadData();
};

} // namespace utils

#endif // UTILS_SLEEVESMANAGER_H
