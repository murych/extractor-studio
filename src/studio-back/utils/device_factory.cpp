#include "device_factory.h"

#include "models/device/device_extractor.h"
#include "json/enumerator_decorator.h"
#include <memory>

using namespace models::device;

namespace utils {

json::Entity*
DeviceFactory::factoryMethod(const QJsonObject& json)
{
  const GenericDevice a_device;
  const auto device_type{ static_cast<GenericDevice::EDeviceType>(
    json.value(a_device.type->key())
      .toInt(GenericDevice::EDeviceType::Unknown)) };

  if (device_type == GenericDevice::EDeviceType::Extractor) {
    return new DeviceExtractor{ json };
  }

  return nullptr;
}

} // namespace utils
