#include "logger.h"

#include <QDateTime>
#include <QDebug>
#include <QFileInfo>
#include <QLoggingCategory>
#include <QStandardPaths>
#include <QTextStream>

#define LOG_TO_FILE
#define APP_ORG "Syntol"
#define APP_NAME "extractor-studio"
static const QString FMT_DATE_TIME("yyyy-MM-ddThh:mm:ss.zzz");

Q_LOGGING_CATEGORY(CATEGORY_LOGGER, "LOG")

Logger::Logger(QObject* parent)
  : QObject{ parent }
  , m_logDir{ QStandardPaths::writableLocation(
      QStandardPaths::GenericDataLocation) }
  , m_logFile{ std::make_unique<QFile>() }
  , m_updateTimer{ std::make_unique<QTimer>() }
  , m_stderr{ stderr, QIODevice::WriteOnly }
  , m_fileOut{ m_logFile.get() }
  , m_startTime{ QDateTime::currentDateTime() }
{
  m_updateTimer->setSingleShot(true);
  m_updateTimer->setInterval(100);

  connect(m_updateTimer.get(), &QTimer::timeout, this, &Logger::logTextChanged);

#ifdef LOG_TO_FILE
  m_logDir.mkdir(APP_ORG);

  if (!m_logDir.exists(APP_ORG)) {
    fallbackMessageOutput(QStringLiteral("Failed to create logs directory"));
    return;
  }
  if (!m_logDir.cd(APP_ORG)) {
    fallbackMessageOutput(QStringLiteral("Failed to access logs directory"));
    return;
  } else if (!removeOldFiles()) {
    fallbackMessageOutput(QStringLiteral("Failed to remove old files"));
    return;
  }

  const auto file_name{
    QStringLiteral("%1-%2.log")
      .arg(APP_NAME, m_startTime.toString(QStringLiteral("yyyyMMdd")))
  };
  const auto file_path{ m_logDir.absoluteFilePath(file_name) };
  m_logFile->setFileName(file_path);

  if (!m_logFile->open(QIODevice::WriteOnly | QIODevice::Append)) {
    fallbackMessageOutput(QStringLiteral("Failed to open log file: %1")
                            .arg(m_logFile->errorString()));
  }
#endif
}

Logger*
Logger::instance()
{
  static auto* logger = new Logger();
  return logger;
}

void
Logger::messageOutput(QtMsgType type,
                      const QMessageLogContext& ctx,
                      const QString& msg)
{
  const QDateTime cdt{ QDateTime::currentDateTime() };
  const QString text{ QStringLiteral("[%1] [%2] %3")
                        .arg(cdt.toString(FMT_DATE_TIME), ctx.category, msg) };
  const QString critical_text{ QStringLiteral(
    "<font color=\"#ff1f00\">%1</font>") };

#ifdef LOG_TO_FILE
  // writing everything in the file regardless of the log level
  if (Logger::instance()->m_logFile->isOpen()) {
    Logger::instance()->m_fileOut << text << Qt::endl;
  }
#endif

  const bool filter_non_error{ Logger::instance()->m_logLevel ==
                                 LogLevel::ErrorsOnly &&
                               type != QtCriticalMsg };
  const bool filter_debug{ Logger::instance()->m_logLevel == LogLevel::Terse &&
                           type == QtDebugMsg };

  if (filter_non_error || filter_debug) {
    return;
  }

  Logger::instance()->m_stderr << text << Qt::endl;

  const bool filter_without_category{ !strcmp(ctx.category, "default") };
  const bool filter_pretty{ type == QtDebugMsg };

  if (filter_without_category || filter_pretty) {
    return;
  }

#ifndef QT_DEBUG
  Logger::instance()->append(type == QtCriticalMsg ? critical_text.arg(text)
                                                   : text);
#endif

  Logger::instance()->setErrorCount(Logger::instance()->errorCount() +
                                    (type == QtCriticalMsg ? 1 : 0));
}

QUrl
Logger::logsPath() const
{
  return QUrl::fromLocalFile(m_logDir.absolutePath());
}

QUrl
Logger::logsFile() const
{
  return QUrl::fromLocalFile(m_logFile->fileName());
}

int
Logger::errorCount() const
{
  return m_errorCount;
}

void
Logger::setErrorCount(const int count)
{
  if (m_errorCount == count) {
    return;
  }

  m_errorCount = count;
  emit errorCountChanged();
}

QString
Logger::logText() const
{
  return m_logText.join("<br/>");
}

void
Logger::setLogLevel(LogLevel level)
{
  if (m_logLevel == level) {
    return;
  }

  m_logLevel = level;
}

void
Logger::append(const QString& line)
{
  m_logText.append(line);

  if (m_logText.size() > m_maxLineCount) {
    m_logText.removeFirst();
  }

  if (!m_updateTimer->isActive()) {
    m_updateTimer->start();
  }
}

void
Logger::fallbackMessageOutput(const QString& msg)
{
  m_stderr << "[" << CATEGORY_LOGGER().categoryName() << "] " << msg
           << Qt::endl;
}

bool
Logger::removeOldFiles()
{
  constexpr int max_file_count{ 99 };
  const QFileInfoList files{ m_logDir.entryInfoList(
    QDir::Files, QDir::Time | QDir::Reversed) };
  const int excess_file_count{ files.size() - max_file_count };

  for (qsizetype i = 0; i < excess_file_count; ++i) {
    const QFileInfo& file_info{ files.at(i) };
    if (m_logDir.remove(file_info.fileName())) {
      fallbackMessageOutput(
        QStringLiteral("Failed to remove file: %1").arg(file_info.fileName()));
      return false;
    }
  }

  return true;
}
