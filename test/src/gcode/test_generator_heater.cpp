#include <gtest/gtest.h>

#include "gcode/generators/generator_heater.h"
#include "models/device/heater.h"
#include "models/steps/step.h"
#include "models/steps/step_heater.h"
#include "json/int_decorator.h"

TEST(GCodeTests, TestGeneratorHeaterWithoutAnything)
{
  const gcode::GeneratorHeater generator{ nullptr, nullptr };
  const auto result{ generator.generate() };

  EXPECT_FALSE(result.has_value());
}

TEST(GCodeTests, TestGeneratorHeaterWithoutHeater)
{
  const models::steps::Step step{ models::steps::Step::EStepType::Heater };
  const gcode::GeneratorHeater generator{ &step, nullptr };
  const auto result{ generator.generate() };

  EXPECT_FALSE(result.has_value());
}

TEST(GCodeTests, TestGeneratorHeaterWithoutStep)
{
  const models::device::Heater heater;
  const gcode::GeneratorHeater generator{ nullptr, &heater };
  const auto result{ generator.generate() };

  EXPECT_FALSE(result.has_value());
}

TEST(GCodeTests, TestGeneratorHeaterSuccess)
{
  const auto step_name{ QStringLiteral("SET HEATER 1") };
  constexpr auto step_idx{ 1 };
  constexpr auto heater_address{ 0x05 };
  constexpr auto heater_target{ 50.2 };

  models::device::Heater heater;
  heater.address->setValue(heater_address);

  models::steps::Step step{ models::steps::Step::EStepType::Heater };
  step.setStepIdx(step_idx);
  step.setStepName(step_name);

  auto* specifics{ dynamic_cast<models::steps::StepHeater*>(
    step.paramsSpecific()) };
  specifics->setTargetTemp(heater_target);
  specifics->setBlockExecution(false);

  const gcode::GeneratorHeater generator{ &step, &heater };
  const auto result{ generator.generate() };
  EXPECT_TRUE(result.has_value());

  const auto lines{ result.value().split('\n') };
  EXPECT_EQ(lines.size(), 3);
  EXPECT_EQ(lines.at(0),
            QStringLiteral("; === Step #1 :: Manual Heat :: SET HEATER 1 ==="));
  EXPECT_EQ(lines.at(1), QStringLiteral("M104 T5 S50.2"));

  specifics->setTargetTemp(42.69);
  specifics->setBlockExecution(true);
  const auto new_result{ generator.generate() };
  EXPECT_TRUE(new_result.has_value());
  const auto new_lines{ new_result.value().split('\n') };
  EXPECT_EQ(new_lines.size(), 3);
  EXPECT_EQ(new_lines.at(1), QStringLiteral("M109 T5 S42.69"))
    << new_lines.at(1).toStdString();
}
