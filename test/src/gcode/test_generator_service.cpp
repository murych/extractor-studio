#include <gtest/gtest.h>

#include "gcode/generators/generator_conclusion.h"
#include "gcode/generators/generator_preamble.h"
#include "models/device/device_extractor.h"
#include "models/device/heater.h"
#include "models/device/motor.h"
#include "models/device/seat.h"
#include "models/project/project_info.h"
#include "json/bool_decorator.h"
#include "json/datetime_decorator.h"
#include "json/entity_collection.h"
#include "json/int_decorator.h"
#include "json/string_decorator.h"
#include <QDateTime>

TEST(GCodeTests, PreambleGeneratorSuccess)
{
  const auto author{ QStringLiteral("Foo") };
  const auto title{ QStringLiteral("Bar") };
  const auto revision{ 1 };
  const QDateTime cdt{ QDateTime::currentDateTime() };
  const json::DateTimeDecorator date_decorator{ "", "", cdt };

  models::project::Project project;
  project.author->setValue(author);
  project.title->setValue(title);
  project.revision->setValue(revision);
  project.dateCreated->setValue(cdt);

  const gcode::GeneratorPreamble generator{ &project };
  const auto result{ generator.generate() };

  ASSERT_TRUE(result.has_value());

  const auto lines{ result.value().split('\n') };
  EXPECT_TRUE(lines.at(1).contains(title));
  EXPECT_TRUE(lines.at(2).contains(author));
  EXPECT_TRUE(lines.at(3).contains(QString::number(revision)))
    << lines.at(3).toStdString();
  EXPECT_TRUE(lines.at(4).contains(date_decorator.toPrettyDateString()));
}

TEST(GCodeTests, PreambleGeneratorNoProjectFail)
{
  const gcode::GeneratorPreamble generator{ nullptr };
  const auto result{ generator.generate() };
  EXPECT_FALSE(result.has_value());
}

TEST(GCodeTests, PreambleGeneratorNoTitleFail)
{
  models::project::Project project;
  project.author->setValue("foo");
  const gcode::GeneratorPreamble generator{ &project };
  const auto result{ generator.generate() };
  EXPECT_FALSE(result.has_value());
}

TEST(GCodeTests, ConclusionGeneratorSuccess)
{
  constexpr auto engines_max{ 5 };
  constexpr auto seats_max{ 5 };
  constexpr std::array<int, engines_max> engines_addresses{ 1, 2, 3, 4, 5 };
  constexpr std::array<QChar, engines_max> engines_axis{
    'X', 'A', 'Z', 'B', 'C'
  };
  constexpr auto heaters_max{ 2 };
  constexpr std::array<int, heaters_max> heaters_addrs{ 0x0f, 0x10 };

  models::device::DeviceExtractor device;
  device.motors->clear();
  device.seats->clear();
  for (auto idx = 0; idx < engines_max; ++idx) {
    auto* engine{ new models::device::Motor{ &device } };
    engine->axis->setValue(engines_axis.at(idx));
    device.motors->addEntity(engine);
  }
  for (auto idx = 0; idx < seats_max; ++idx) {
    auto* seat{ new models::device::Seat{ &device } };
    if (idx == 1 || idx == 4) {
      seat->hasHeater->setValue(true);
      seat->heater->address->setValue(idx == 1 ? heaters_addrs.at(0)
                                               : heaters_addrs.at(1));
    }
    device.seats->addEntity(seat);
  }

  const gcode::GeneratorConclusion generator{ &device };
  const auto result{ generator.generate() };
  ASSERT_TRUE(result.has_value());

  const auto lines{ result.value().split('\n') };
  EXPECT_EQ(lines.size(), 1 + 1 + 1 + 2 + 2) << result.value().toStdString();
}

TEST(GCodeTests, ConclusionGeneratorWithoutMainObjectFail)
{
  const gcode::GeneratorConclusion generator{ nullptr };
  const auto result{ generator.generate() };
  ASSERT_FALSE(result.has_value());
}
